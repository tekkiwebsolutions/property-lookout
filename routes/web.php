<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', ['as' => 'home', 'uses'=>'HomeController@index']);
// Route::get('LanguageSwitch/{lang}', ['as' => 'switch_language', 'uses'=>'HomeController@switchLang']);

Route::get('/reccuring_payment_cron','UserController@reccuring_payment_cron');
// Route::get('/testGoogleStorage', ['as' => 'google_storage', 'uses'=>'TestStorageController@test']);
// Route::post('/testGoogleStorage', ['as' => 'google_storage', 'uses'=>'TestStorageController@testPost']);


// 25-09-2018 packages start
// without login routes
Route::get('/packages/{email?}/{type?}', ['as' => 'packages', 'uses'=>'UserController@packages']);
Route::post('/insert-package', ['as' => 'insert_package', 'uses'=>'UserController@insertPackage']);
Route::post('/update-package', ['as' => 'update_package', 'uses'=>'UserController@updatePackage']);
Route::post('/agent/insert-access-key', ['as' => 'insert_access_key', 'uses'=>'UserController@insertAccessKey']);
Route::get('/user/register-payment', ['as' => 'register_payment', 'uses'=>'UserController@registerPayment']);
Route::get('/user/payment_checkout/{transaction_id}', ['as' => 'register_payment_checkout', 'uses'=>'UserController@checkout']);
Route::post('/user/charge-payment/{transaction_id}', ['as' => 'register_charge_payment', 'uses'=>'UserController@chargePayment']);
Route::match(['get','post'],'/user/payment_success_url/{transaction_id}/payment-success',['as'=>'payment_success_url','uses' => 'CreditController@paymentSuccess']);
Route::match(['get','post'],'/user/paypal_notify_url/{transaction_id}/paypal-notify',['as'=>'paypal_notify_url','uses' => 'CreditController@paypalNotify']);
// without login routes

// Route::match(['get','post'],'staff-invited-agents',['as'=>'staff_invited_agents','uses' => 'UserController@staff_invited_agents']);
// Route::match(['get','post'],'staff_Property_manage',['as'=>'staff_Property_manage','uses' => 'UserController@staff_Property_manage']);
// Route::match(['get','post'],'settings_new',['as'=>'settings_new','uses' => 'UserController@settings_new']);
// 25-09-2018 packages start


// Route::match(['get','post'],'panorama-check',['as'=>'panorama_check','uses' => 'UserController@panorama']);

// Agent Start

Route::group(['middleware' => 'only_agent_access'], function() {
    Route::group(['prefix'=>'agents'], function(){
        Route::get('settings', ['as' => 'agent_settings', 'uses'=>'AgentController@agentSetting']);
        Route::get('setting-view', ['as' => 'settings_view', 'uses'=>'AgentController@agentSettingView']);

        //8-10-2018
        Route::get('properties-list', ['as' => 'agent_properties_list', 'uses'=>'PropertyController@propertiesList']);

        //House Property
        Route::get('create-house', ['as' => 'create_house', 'uses'=>'HouseController@createHouse']);
        Route::post('create-house', ['as' => 'create_house', 'uses'=>'HouseController@addHouse']);
        Route::get('edit-house/{id}', ['as' => 'edit_house', 'uses'=>'HouseController@editHouse']);
        Route::post('edit-house/{id}', ['as' => 'edit_house', 'uses'=>'HouseController@updateHouse']);
        Route::post('delete-house', ['as' => 'delete_house', 'uses'=>'HouseController@delete_house']);
        Route::post('sold-house', ['as' => 'sold_house', 'uses'=>'HouseController@sold_house']);
        Route::get('user-chat-messages', ['as' => 'user_chat_messages', 'uses'=>'ChatController@user_chat_list']);

        // add credit routes
        Route::get('credits-list', ['as' => 'agent_credit_list', 'uses'=>'CreditController@creditList']);
        Route::post('add-credit', ['as' => 'agent_add_credit', 'uses'=>'CreditController@addCredit']);
        Route::get('payment_checkout/{transaction_id}', ['as' => 'credit_payment_checkout', 'uses'=>'CreditController@checkout']);
        Route::post('charge-payment/{transaction_id}', ['as' => 'credit_charge_payment', 'uses'=>'CreditController@chargePayment']);
        Route::match(['get','post'],'payment_success_url/{transaction_id}/payment-success',['as'=>'payment_success_url','uses' => 'CreditController@paymentSuccess']);
        Route::match(['get','post'],'paypal_notify_url/{transaction_id}/paypal-notify',['as'=>'paypal_notify_url','uses' => 'CreditController@paypalNotify']);

        // use credit routes
        Route::get('add-credit-list', ['as' => 'add_credit_list', 'uses'=>'CreditController@addCreditList']);
        Route::get('add-property-credit/{ad_id}', ['as' => 'add_property_credit', 'uses'=>'CreditController@addPropertyCredit']);
        Route::post('add-credit-property', ['as' => 'add_credit_property', 'uses'=>'CreditController@addCreditProperty']);

        // remove credits route

        // developer interaction routes are kept here

        Route::match(['get','post'],'developer-requests',['as'=>'developer_requests','uses' => 'AgentController@developerRequests']);
        Route::match(['get','post'],'accept-cancel-request',['as'=>'accept_cancel_request','uses' => 'AgentController@acceptCancelRequest']);

        // developer interaction routes are kept here

        Route::get('reports', ['as' => 'agent_reports', 'uses'=>'AgentController@agentReports']);
    });
});

// these routes are not kept in only agent access because developer also needs these routes
Route::group(['prefix'=>'agents'], function(){

    Route::post('setting-update', ['as' => 'agent_setting_update', 'uses'=>'AgentController@agentSettingsUpdate']);
    Route::post('update-profile', ['as' => 'update_agent_profile', 'uses'=>'AgentController@updateAgentProfile']);

    Route::match(['get','post'],'upload-property-image',['as'=>'upload_property_image','uses' => 'HouseController@uploadPropertyImage']);
    Route::match(['get','post'],'delete-property-image',['as'=>'delete_property_media','uses' => 'HouseController@deletePropertyMedia']);
    Route::match(['get','post'],'apend-property-image/{property_type}/{image_type}',['as'=>'append_property_media','uses' => 'HouseController@appendPropertyMedia']);

    Route::get('refresh-captcha', ['as' => 'refresh_captcha', 'uses'=>'PropertyController@captchaString']);

    Route::post('create-apartment', ['as' => 'create_apartment', 'uses'=>'ApartmentController@addApartment']);
    Route::post('create-apartment-tower', ['as' => 'create_apartment_tower', 'uses'=>'ApartmentController@addApartmentTower']);
    Route::post('delete-tower', ['as' => 'delete_tower', 'uses'=>'ApartmentController@delete_tower']);
    Route::post('add-floor', ['as' => 'add_floor', 'uses'=>'ApartmentController@addFloor']);
    Route::get('apend-floor-units/{id}', ['as' => 'apend_floor_units', 'uses'=>'ApartmentController@appendFloorUnits']);

    //Apartment Property
    // Route::get('create-apartment', ['as' => 'create_apartment', 'uses'=>'ApartmentController@createApartment']);
    // Route::post('create-units', ['as' => 'create_units', 'uses'=>'ApartmentController@createUnits']);

    // Route::get('create-floor/{id}', ['as' => 'create_floor', 'uses'=>'ApartmentController@createFloor']);
    // Route::post('create-floor/{id}', ['as' => 'create_floor', 'uses'=>'ApartmentController@addFloor']);

    // Route::get('create-units/{id}/{floor_id}/{unit_id}', ['as' => 'create_floor', 'uses'=>'ApartmentController@createUnits']);
    Route::post('add-units', ['as' => 'add_units', 'uses'=>'ApartmentController@addUnits']);

    // previous and edit links routes
    Route::get('edit-apartment/{id}/{type?}', ['as' => 'edit_apartment', 'uses'=>'ApartmentController@editApartment']);
    Route::get('apartment-tower/{id}', ['as' => 'apartment_tower', 'uses'=>'ApartmentController@apartmentTower']);
    Route::get('edit-floors/{id}/{type?}', ['as' => 'edit_floors', 'uses'=>'ApartmentController@editFloors']);
    Route::get('edit-previous-units/{id}/{floor_id}/{unit_id}/{type?}', ['as' => 'edit_previous_units', 'uses'=>'ApartmentController@editPreviousUnits']);

    Route::get('apend-apartment-units/{id}/{type}', ['as' => 'apend_apartment_units', 'uses'=>'ApartmentController@appendApartmnetUnits']);
    Route::post('apartment-edit', ['as' => 'apartment_edit', 'uses'=>'ApartmentController@apartmentEdit']);
    Route::post('floor-edit', ['as' => 'floor_edit', 'uses'=>'ApartmentController@floorEdit']);
    Route::get('edit-units/{id}/{floor_id}/{unit_id}', ['as' => 'edit_units', 'uses'=>'ApartmentController@editUnits']);
    Route::post('units-edit', ['as' => 'units_edit', 'uses'=>'ApartmentController@unitsEdit']);
    // apartment list units sold/available status change
    Route::post('sold-unit-status-change', ['as' => 'sold_unit_status_change', 'uses'=>'ApartmentController@soldUnitStatusChange']);
    Route::post('avail-unit-status-change', ['as' => 'avail_unit_status_change', 'uses'=>'ApartmentController@availUnitStatusChange']);
    Route::get('remove-credits', ['as' => 'remove_credits', 'uses'=>'CreditController@removeCredits']);

    // Route::get('apartment-edit/{id}', ['as' => 'apartment_edit', 'uses'=>'ApartmentController@apartmentEdit']);
});
Route::get('agent/{slug}', ['as' => 'agent_profile', 'uses'=>'AgentController@agentProfile']);
Route::get('agent/not-found', ['as' => 'agent_not_found', 'uses'=>'AgentController@agentNotFound']);
Route::get('propertyShow/{type}/{slug}', ['as' => 'property_show', 'uses'=>'PropertyController@property_show']);
Route::get('frontend/agentsList', ['as' => 'frontend_agent_list', 'uses'=>'HomeController@agent_list']);
Route::post('leave-review',['as'=>'leave_review','uses'=>'PropertyController@leaveReview']);


// Agent Ends

//multiple Images

Route::get('/multiple-images', ['as' => 'multiple_images', 'uses'=>'HomeController@multipleImages']);


//before login
//Route::get('agent-profile', ['as' => 'agent_profile', 'uses'=>'AgentController@agentProfile']);


// 10-sep-2018
//Route::get('about-us', ['as' => 'about_us_page', 'uses'=>'HomeController@aboutUs']);
Route::get('page/{slug}', ['as' => 'single_page', 'uses'=>'PostController@showPage']);

// 26-09-2018 design
//Route::get('agent-bio', ['as' => 'agent_bio', 'uses'=>'HomeController@agentBio']);
// Route::get('account-setting', ['as' => 'account_setting', 'uses'=>'HomeController@accountSetting']);

// Route::get('blog/author/{id}', ['as' => 'author_blog_posts', 'uses'=>'PostController@authorPosts']);

// Design links Start
Route::get('properties', ['as' => 'properties_page', 'uses'=>'HomeController@properties']);


Route::get('agent-cms', ['as' => 'agent_cms', 'uses'=>'HomeController@agentCms']);
Route::get('agent-create-apartment', ['as' => 'agent_create_apartment', 'uses'=>'HomeController@CreateApartment']);

Route::get('agent-create-units', ['as' => 'agent_create_units', 'uses'=>'HomeController@CreateUnits']);
Route::get('agent-create-house', ['as' => 'agent_create_house_one', 'uses'=>'HomeController@CreateHouse']);

Route::get('agent-edit-cms', ['as' => 'agent_edit_cms', 'uses'=>'HomeController@agentEditcms']);

// agent start 18-sept-2018
Route::get('property-available', ['as' => 'property_available', 'uses'=>'HomeController@property_available']);
Route::get('message', ['as' => 'message', 'uses'=>'HomeController@Message']);
Route::get('chat', ['as' => 'chat', 'uses'=>'HomeController@Chat']);

// 20-09-2018

//Listing page

// 14-sep-2018
Route::get('property-detail', ['as' => 'property_detail', 'uses'=>'HomeController@property_detail']);
Route::get('property-unit-detail', ['as' => 'property_unit_detail', 'uses'=>'HomeController@property_unit_detail']);
Route::get('property-sold', ['as' => 'property_sold', 'uses'=>'HomeController@property_sold']);
// Design links End


Route::get('contact-us', ['as' => 'contact_us_page', 'uses'=>'HomeController@contactUs']);

Route::post('contact-us', ['uses'=>'HomeController@contactUsPost']);


// Route::get('chat', ['as' => 'chat', 'uses'=>'ChatController@chat_show']);
Route::get('chat-room/{agent_id}/{user_id}/{window_type}', ['as' => 'chant_room', 'uses'=>'ChatController@index']);
Route::get('/agent/chat-list', ['as' => 'agent_chat_list', 'uses'=>'ChatController@agent_chat_list']);
Route::post('/insert-chat', ['as' => 'insert_chat', 'uses'=>'ChatController@insert_chat']);
Route::post('/insert-guest-user', ['as' => 'insert_guest_user', 'uses'=>'ChatController@insert_guest_user']);
Route::post('/ajax-chat-after-interval', ['as' => 'ajax_chat_after_interval', 'uses'=>'ChatController@ajax_chat_after_interval']);


Route::get('blog', ['as' => 'blog', 'uses'=>'PostController@blogIndex']);
Route::get('blog/{slug}', ['as' => 'blog_single', 'uses'=>'PostController@blogSingle']);


Route::get('listing', ['as' => 'listing', 'uses'=>'AdsController@listing']);
Route::get('ad/{slug}', ['as' => 'single_ad', 'uses'=>'AdsController@singleAd']);
Route::get('embedded/{slug}', ['as' => 'embedded_ad', 'uses'=>'AdsController@embeddedAd']);


Route::post('save-ad-as-favorite', ['as' => 'save_ad_as_favorite', 'uses'=>'UserController@saveAdAsFavorite']);
Route::post('report-post', ['as' => 'report_ads_pos', 'uses'=>'AdsController@reportAds']);
Route::post('reply-by-email', ['as' => 'reply_by_email_post', 'uses'=>'UserController@replyByEmailPost']);

// Password reset routes...
Route::post('send-password-reset-link', ['as' => 'send_reset_link', 'uses'=>'UserController@postEmailResetPassword']);
Route::get('password/reset/{token}', ['as' => 'password_reset_url', 'uses' => 'UserController@passwordResetForm']);
Route::post('password/reset/{token}', [ 'uses' => 'UserController@passwordResetPost']);


//Auth::routes();

// Password reset routes...
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
//Route::post('password/reset', 'Auth\ResetPasswordController@postReset')->name('password.reset');


Route::post('get-sub-category-by-category', ['as'=>'get_sub_category_by_category', 'uses' => 'AdsController@getSubCategoryByCategory']);
Route::post('get-brand-by-category', ['as'=>'get_brand_by_category', 'uses' => 'AdsController@getBrandByCategory']);
Route::post('get-category-info', ['as'=>'get_category_info', 'uses' => 'AdsController@getParentCategoryInfo']);
Route::post('get-state-by-country', ['as'=>'get_state_by_country', 'uses' => 'AdsController@getStateByCountry']);
Route::post('get-city-by-state', ['as'=>'get_city_by_state', 'uses' => 'AdsController@getCityByState']);
Route::post('switch/product-view', ['as'=>'switch_grid_list_view', 'uses' => 'AdsController@switchGridListView']);



Route::group(['prefix'=>'login'], function(){
    //Native login route
    Route::get('/', ['as' => 'login', 'uses'=>'UserController@login']);
    Route::post('/', ['uses'=>'UserController@loginPost']);

    //Social login route

    Route::get('facebook', ['as' => 'facebook_redirect', 'uses'=>'SocialLogin@redirectFacebook']);
    Route::get('facebook-callback', ['as' => 'facebook_callback', 'uses'=>'SocialLogin@callbackFacebook']);

    Route::get('google', ['as' => 'google_redirect', 'uses'=>'SocialLogin@redirectGoogle']);
    Route::get('google-callback', ['as' => 'google_callback', 'uses'=>'SocialLogin@callbackGoogle']);

});

Route::resource('user', 'UserController');



//pages start 1-oct-2018
    Route::get('index-posts/create/{type}', ['uses' => 'PostController@create_post']);
    Route::post('index-posts/create/{type}', ['uses' => 'PostController@create_post']);


//pages end 1-oct-2018

//Dashboard Route
Route::group(['prefix'=>'dashboard', 'middleware' => 'dashboard'], function(){

    Route::group(['prefix'=>'users'], function(){
        Route::get('social-settings', ['as'=>'social_url_settings', 'uses' => 'SettingsController@userSocialUrlSettings']);
        Route::post('save_settings', ['as'=>'save_settings', 'uses' => 'SettingsController@userUpdate']);
    });
    Route::get('/', ['as'=>'dashboard', 'uses' => 'DashboardController@dashboard']);

    Route::group(['middleware'=>'only_admin_access'], function(){

        Route::group(['prefix'=>'settings'], function(){
            Route::get('theme-settings', ['as'=>'theme_settings', 'uses' => 'SettingsController@ThemeSettings']);
            Route::get('modern-theme-settings', ['as'=>'modern_theme_settings', 'uses' => 'SettingsController@modernThemeSettings']);
            Route::get('social-url-settings', ['as'=>'social_url_settings', 'uses' => 'SettingsController@SocialUrlSettings']);
            Route::get('general', ['as'=>'general_settings', 'uses' => 'SettingsController@GeneralSettings']);
            Route::get('payments', ['as'=>'payment_settings', 'uses' => 'SettingsController@PaymentSettings']);
            Route::get('ad', ['as'=>'ad_settings', 'uses' => 'SettingsController@AdSettings']);
            Route::get('languages', ['as'=>'language_settings', 'uses' => 'LanguageController@index']);
            Route::post('languages', ['uses' => 'LanguageController@store']);
            Route::post('languages-delete', ['as'=>'delete_language', 'uses' => 'LanguageController@destroy']);

            Route::get('storage', ['as'=>'file_storage_settings', 'uses' => 'SettingsController@StorageSettings']);
            Route::get('social', ['as'=>'social_settings', 'uses' => 'SettingsController@SocialSettings']);
            Route::get('blog', ['as'=>'blog_settings', 'uses' => 'SettingsController@BlogSettings']);
            Route::get('other', ['as'=>'other_settings', 'uses' => 'SettingsController@OtherSettings']);
            Route::post('other', ['as'=>'other_settings', 'uses' => 'SettingsController@OtherSettingsPost']);

            //Save settings / options
            Route::post('save-settings', ['as'=>'save_settings', 'uses' => 'SettingsController@update']);
            Route::get('monetization', ['as'=>'monetization', 'uses' => 'SettingsController@monetization']);

        });

        Route::group(['prefix'=>'location'], function(){
            Route::get('country', ['as'=>'country_list', 'uses' => 'LocationController@countries']);
            Route::get('country-data', ['as'=>'get_countries_data', 'uses' => 'LocationController@getCountriesData']);
            Route::get('states', ['as'=>'state_list', 'uses' => 'LocationController@stateList']);
            Route::post('states', [ 'uses' => 'LocationController@saveState']);
            Route::get('states/{id}/edit', ['as'=>'edit_state', 'uses' => 'LocationController@stateEdit']);
            Route::post('states/{id}/edit', ['uses' => 'LocationController@stateEditPost']);
            Route::post('states/delete', ['as'=>'delete_state', 'uses' => 'LocationController@stateDestroy']);
            Route::get('state-data', ['as'=>'get_state_data', 'uses' => 'LocationController@getStatesData']);
            Route::get('cities', ['as'=>'city_list', 'uses' => 'LocationController@cityList']);
            Route::post('cities', ['uses' => 'LocationController@saveCity']);
            Route::get('city-data', ['as'=>'get_city_data', 'uses' => 'LocationController@getCityData']);

            Route::get('cities/{id}/edit', ['as'=>'edit_city', 'uses' => 'LocationController@cityEdit']);
            Route::post('cities/{id}/edit', ['uses' => 'LocationController@cityEditPost']);
            Route::post('city/delete', ['as'=>'delete_city', 'uses' => 'LocationController@cityDestroy']);
        });

        Route::group(['prefix'=>'categories'], function(){
            Route::get('/', ['as'=>'parent_categories', 'uses' => 'CategoriesController@index']);
            Route::post('/', ['uses' => 'CategoriesController@store']);

            Route::get('edit/{id}', ['as'=>'edit_categories', 'uses' => 'CategoriesController@edit']);
            Route::post('edit/{id}', ['uses' => 'CategoriesController@update']);

            Route::post('delete-categories', ['as'=>'delete_categories', 'uses' => 'CategoriesController@destroy']);
        });

        Route::group(['prefix'=>'distances'], function(){
            Route::get('/', ['as'=>'admin_brands', 'uses' => 'BrandsController@index']);
            Route::post('/', ['uses' => 'BrandsController@store']);
            Route::get('edit/{id}', ['as'=>'edit_brands', 'uses' => 'BrandsController@edit']);
            Route::post('edit/{id}', ['uses' => 'BrandsController@update']);
            Route::post('delete-distances', ['as'=>'delete_brands', 'uses' => 'BrandsController@destroy']);
        });

        Route::group(['prefix'=>'posts'], function(){
            Route::get('/', ['as'=>'posts', 'uses' => 'PostController@posts']);
            Route::get('data', ['as'=>'posts_data', 'uses' => 'PostController@postsData']);

            Route::get('create', ['as'=>'create_new_post', 'uses' => 'PostController@createPost']);
            Route::post('create', ['uses' => 'PostController@storePost']);
            Route::post('delete', ['as'=>'delete_post','uses' => 'PostController@destroyPost']);

            Route::get('edit/{slug}', ['as'=>'edit_post', 'uses' => 'PostController@editPost']);
            Route::post('edit/{slug}', ['uses' => 'PostController@updatePost']);
        });

        Route::group(['prefix'=>'pages'], function(){
            Route::get('/', ['as'=>'pages', 'uses' => 'PostController@index']);
            Route::get('data', ['as'=>'pages_data', 'uses' => 'PostController@pagesData']);

            Route::get('create', ['as'=>'create_new_page', 'uses' => 'PostController@create']);
            Route::post('create', ['uses' => 'PostController@store']);
            Route::post('delete', ['as'=>'delete_page','uses' => 'PostController@destroy']);

            Route::get('edit/{slug}', ['as'=>'edit_page', 'uses' => 'PostController@edit']);
            Route::post('edit/{slug}', ['uses' => 'PostController@updatePage']);
        });

        Route::group(['prefix'=>'slider'], function(){
            Route::get('/', ['as'=>'slider', 'uses' => 'SliderController@index']);
            Route::get('create', ['as'=>'create_slider', 'uses' => 'SliderController@create']);
            Route::post('create', ['uses' => 'SliderController@store']);
            Route::post('crop', ['as'=>'create_crop', 'uses' => 'SliderController@postCrop']);
            Route::post('delete', ['as'=>'delete_slider', 'uses' => 'SliderController@destroy']);
            Route::post('update-caption', ['as'=>'update_slider_caption', 'uses' => 'SliderController@update']);
        });


        Route::get('approved', ['as'=>'approved_ads', 'uses' => 'AdsController@index']);
        Route::get('pending', ['as'=>'admin_pending_ads', 'uses' => 'AdsController@adminPendingAds']);
        Route::get('blocked', ['as'=>'admin_blocked_ads', 'uses' => 'AdsController@adminBlockedAds']);
        Route::post('status-change', ['as'=>'ads_status_change', 'uses' => 'AdsController@adStatusChange']);

        Route::get('ad-reports', ['as'=>'ad_reports', 'uses' => 'AdsController@reports']);
        Route::get('agent-reports', ['as'=>'agent_reports', 'uses' => 'AgentController@reports']);

        Route::get('postsIndex', ['uses' => 'PostController@create_post']);
        Route::post('postsIndex', ['uses' => 'PostController@create_post']);

        Route::get('property-reviews', ['as'=>'property_reviews', 'uses' => 'AdsController@reviews']);
        Route::get('users', ['as'=>'users', 'uses' => 'UserController@index']);
        Route::get('users-data', ['as'=>'get_users_data', 'uses' => 'UserController@usersData']);
        Route::get('users-info/{id}', ['as'=>'user_info', 'uses' => 'UserController@userInfo']);
        Route::post('change-user-status', ['as'=>'change_user_status', 'uses' => 'UserController@changeStatus']);
        Route::post('change-ad-status', ['as'=>'change_ad_status', 'uses' => 'AdsController@changeStatus']);
        Route::post('change-user-feature', ['as'=>'change_user_feature', 'uses' => 'UserController@changeFeature']);
        Route::post('delete-reports', ['as'=>'delete_report', 'uses' => 'AdsController@deleteReports']);

        Route::get('contact-messages', ['as'=>'contact_messages', 'uses' => 'HomeController@contactMessages']);
        Route::get('contact-messages-data', ['as'=>'contact_messages_data', 'uses' => 'HomeController@contactMessagesData']);
        Route::get('contact-messages-reply/{id}', ['as'=>'contact_messages_reply', 'uses' => 'HomeController@contactUsMessageReply']);
        Route::post('contact-messages-reply', ['as'=>'contact_messages_reply_send', 'uses' => 'HomeController@contactUsMessageReplySend']);

        Route::group(['prefix'=>'administrators'], function(){
            Route::get('/', ['as'=>'administrators', 'uses' => 'UserController@administrators']);
            Route::get('create', ['as'=>'add_administrator', 'uses' => 'UserController@addAdministrator']);
            Route::post('create', ['uses' => 'UserController@storeAdministrator']);
            Route::post('block-unblock', ['as'=>'administratorBlockUnblock','uses' => 'UserController@administratorBlockUnblock']);
        });

        Route::get('developer-contact-messages', ['as'=>'developer_contact_messages', 'uses' => 'HomeController@developerContactMessages']);
        Route::get('developer-contact-reply/{id}', ['as'=>'developer_contact_messages_reply', 'uses' => 'HomeController@developerContactReply']);
        Route::post('developer-contact-reply', ['as'=>'developer_contact_reply_send', 'uses' => 'HomeController@developerContactReplySave']);

        Route::get('developer-apartment-requests', ['as'=>'developer_apartment_requests', 'uses' => 'HomeController@developerApartmentRequests']);
    });

    //All user can access this route
    Route::get('payments', ['as'=>'payments', 'uses' => 'PaymentController@index']);
    Route::get('payments-data', ['as'=>'get_payments_data', 'uses' => 'PaymentController@paymentsData']);
    Route::get('payments-info/{trand_id}', ['as'=>'payment_info', 'uses' => 'PaymentController@paymentInfo']);
    //End all users access

    Route::group(['prefix'=>'u'], function(){
        Route::group(['prefix'=>'posts'], function(){
            Route::get('/', ['as'=>'my_ads', 'uses' => 'AdsController@myAds']);
            Route::get('create', ['as'=>'create_ad', 'uses' => 'AdsController@create']);
            Route::post('create', ['uses' => 'AdsController@store']);
            Route::post('delete', ['as'=>'delete_ads', 'uses' => 'AdsController@destroy']);
            Route::get('edit/{id}', ['as'=>'edit_ad', 'uses' => 'AdsController@edit']);
            Route::get('view/{id}', ['as'=>'view_ad', 'uses' => 'AdsController@view']);
            Route::post('edit/{id}', ['uses' => 'AdsController@update']);
            Route::get('my-lists', ['as'=>'my_ads', 'uses' => 'AdsController@myAds']);
            Route::get('favorite-lists', ['as'=>'favorite_ads', 'uses' => 'AdsController@favoriteAds']);
            //Upload ads image
            Route::post('upload-a-image', ['as'=>'upload_ads_image', 'uses' => 'AdsController@uploadAdsImage']);
            Route::post('upload-post-image', ['as'=>'upload_post_image', 'uses' => 'PostController@uploadPostImage']);
            //Delete media
            Route::post('delete-media', ['as'=>'delete_media', 'uses' => 'AdsController@deleteMedia']);
            Route::post('feature-media-creating', ['as'=>'feature_media_creating_ads', 'uses' => 'AdsController@featureMediaCreatingAds']);
            Route::get('append-media-image', ['as'=>'append_media_image', 'uses' => 'AdsController@appendMediaImage']);
            Route::get('append-post-media-image', ['as'=>'append_post_media_image', 'uses' => 'PostController@appendPostMediaImage']);
            Route::get('pending-lists', ['as'=>'pending_ads', 'uses' => 'AdsController@pendingAds']);
            Route::get('archive-lists', ['as'=>'favourite_ad', 'uses' => 'AdsController@create']);
            //Checkout payment
            Route::get('checkout/{transaction_id}', ['as'=>'payment_checkout', 'uses' => 'PaymentController@checkout']);
            Route::post('checkout/{transaction_id}', ['uses' => 'PaymentController@chargePayment']);
            //Payment success url
            // Route::post('checkout/{transaction_id}/payment-success', ['as'=>'payment_success_url','uses' => 'PaymentController@paymentSuccess']);
            Route::match(['get','post'],'checkout/{transaction_id}/payment-success',['as'=>'payment_success_url','uses' => 'PaymentController@paymentSuccess']);

            Route::post('checkout/{transaction_id}/paypal-notify', ['as'=>'paypal_notify_url','uses' => 'PaymentController@paypalNotify']);
            Route::get('reports-by/{slug}', ['as'=>'reports_by_ads', 'uses' => 'AdsController@reportsByAds']);
            Route::get('reports-by-agent/{slug}', ['as'=>'reports_by_agent', 'uses' => 'AgentController@reportsByAgents']);
            Route::get('profile', ['as'=>'profile', 'uses' => 'UserController@profile']);
            Route::get('profile/edit', ['as'=>'profile_edit', 'uses' => 'UserController@profileEdit']);
            Route::post('profile/edit', ['uses' => 'UserController@profileEditPost']);
            Route::get('profile/change-avatar', ['as'=>'change_avatar', 'uses' => 'UserController@changeAvatar']);
            Route::post('upload-avatar', ['as'=>'upload_avatar',  'uses' => 'UserController@uploadAvatar']);

            /**
             * Change Password route
             */
            Route::group(['prefix' => 'account'], function() {
                Route::get('change-password', ['as' => 'change_password', 'uses' => 'UserController@changePassword']);
                Route::post('change-password', 'UserController@changePasswordPost');
            });

        });
    });

    Route::get('logout', ['as'=>'logout', 'uses' => 'DashboardController@logout']);
});

// notification routes start

Route::get('notifications/mark-read', ['as'=>'mark_read', 'uses' => 'NotificationController@markRead']);
Route::get('all-notifications', ['as'=>'all_notifications','uses' => 'NotificationController@allNotifications']);
Route::get('all-notifications-json', [ 'uses' => 'NotificationController@allNotificationsjson']);
Route::post('notifications/delete', ['as'=>'delete_notification', 'uses' => 'NotificationController@delete']);
Route::get('get-latest-notifications', ['as'=>'get_latest_notifications', 'uses' => 'NotificationController@getLatestNotifications']);
Route::get('notifications-token-save', ['as'=>'notifications_token_save', 'uses' => 'NotificationController@notificationsTokenSave']);
// notification routes start

// blogs routes start

Route::group(['prefix'=>'blogs'], function(){
    Route::get('/listing', ['as'=>'blog_listing', 'uses' => 'BlogController@blogListing']);
    Route::get('/create', ['as'=>'create_blog', 'uses' => 'BlogController@create']);
    Route::post('/create', ['as'=>'store_blog', 'uses' => 'BlogController@store']);
    Route::post('/delete', ['as'=>'delete_blog', 'uses' => 'BlogController@delete']);
    Route::get('/edit/{blog_id}', ['as'=>'edit_blog', 'uses' => 'BlogController@edit']);
    Route::post('/update', ['as'=>'update_blog', 'uses' => 'BlogController@update']);
    // Route::get('/view-details/{slug}', ['as'=>'view_blog', 'uses' => 'BlogController@view']);
});

// blogs routes end

// developer section (29 nov 2018)

Route::group(['middleware' => 'only_developer_access'], function() {
    Route::group(['prefix'=>'developers'], function(){
        Route::get('settings', ['as' => 'developer_settings', 'uses'=>'AgentController@agentSetting']);  // used
        Route::get('setting-view', ['as' => 'developer_settings_view', 'uses'=>'AgentController@agentSettingView']); // used
        // Route::post('setting-update', ['as' => 'developer_setting_update', 'uses'=>'AgentController@agentSettingsUpdate']);
        // Route::post('update-profile', ['as' => 'update_developer_profile', 'uses'=>'AgentController@updateAgentProfile']);

        //8-10-2018
        Route::get('properties-list', ['as' => 'developer_properties_list', 'uses'=>'PropertyController@propertiesList']); // used
        // Route::match(['get','post'],'upload-property-image',['as'=>'developer_upload_property_image','uses' => 'HouseController@uploadPropertyImage']);
        // Route::match(['get','post'],'delete-property-image',['as'=>'developer_delete_property_media','uses' => 'HouseController@deletePropertyMedia']);
        // Route::match(['get','post'],'apend-property-image/{property_type}/{image_type}',['as'=>'developer_append_property_media','uses' => 'HouseController@appendPropertyMedia']);

        //House Property
        // Route::get('create-house', ['as' => 'developer_create_house', 'uses'=>'HouseController@createHouse']);
        // Route::post('create-house', ['as' => 'developer_create_house', 'uses'=>'HouseController@addHouse']);
        // Route::get('edit-house/{id}', ['as' => 'developer_edit_house', 'uses'=>'HouseController@editHouse']);
        // Route::post('edit-house/{id}', ['as' => 'developer_edit_house', 'uses'=>'HouseController@updateHouse']);
        // Route::post('delete-house', ['as' => 'developer_delete_house', 'uses'=>'HouseController@delete_house']);
        // Route::post('sold-house', ['as' => 'developer_sold_house', 'uses'=>'HouseController@sold_house']);
        Route::get('user-chat-messages', ['as' => 'developer_user_chat_messages', 'uses'=>'ChatController@user_chat_list']);
        //Apartment Property // used
        Route::get('create-apartment/{id?}', ['as' => 'developer_create_apartment', 'uses'=>'ApartmentController@createApartment']); // used
        Route::get('/apartment/create-tower/{id?}/{tower_id?}', ['as' => 'developer_create_tower', 'uses'=>'ApartmentController@createTower']); // used
        // Route::post('create-apartment', ['as' => 'developer_create_apartment', 'uses'=>'ApartmentController@addApartment']);
        Route::post('create-units', ['as' => 'developer_create_units', 'uses'=>'ApartmentController@createUnits']); // used
        // Route::get('refresh-captcha', ['as' => 'developer_refresh_captcha', 'uses'=>'PropertyController@captchaString']);

        Route::get('create-floor/{ad_id}/{id}', ['as' => 'developer_create_floor', 'uses'=>'ApartmentController@createFloor']); // used
        Route::post('create-floor/{id}', ['as' => 'create_floor', 'uses'=>'ApartmentController@addFloor']);
        Route::post('add-floor', ['as' => 'developer_add_floor', 'uses'=>'ApartmentController@addFloor']);
        Route::get('apend-floor-units/{id}', ['as' => 'developer_apend_floor_units', 'uses'=>'ApartmentController@appendFloorUnits']);
        // Route::get('create-units/{tower_id}/{id}/{floor_id}/{unit_id}', ['as' => 'developer_create_floor', 'uses'=>'ApartmentController@createUnits']); // used
        Route::get('create-units/{tower_id}/{id}/{unit_id}', ['as' => 'developer_create_floor', 'uses'=>'ApartmentController@createUnits']); // used
        Route::post('add-units', ['as' => 'developer_add_units', 'uses'=>'ApartmentController@addUnits']);

        // previous and edit links routes
        Route::get('edit-apartment/{id}/{type?}', ['as' => 'developer_edit_apartment', 'uses'=>'ApartmentController@editApartment']);
        Route::get('edit-floors/{id}/{type?}', ['as' => 'developer_edit_floors', 'uses'=>'ApartmentController@editFloors']); // used
        Route::get('edit-previous-units/{id}/{floor_id}/{unit_id}/{type?}', ['as' => 'developer_edit_previous_units', 'uses'=>'ApartmentController@editPreviousUnits']);

        Route::get('apend-apartment-units/{id}/{type}', ['as' => 'developer_apend_apartment_units', 'uses'=>'ApartmentController@appendApartmnetUnits']);
        Route::post('apartment-edit', ['as' => 'developer_apartment_edit', 'uses'=>'ApartmentController@apartmentEdit']);
        Route::post('floor-edit', ['as' => 'developer_floor_edit', 'uses'=>'ApartmentController@floorEdit']);
        Route::get('edit-units/{id}/{floor_id}/{unit_id}', ['as' => 'developer_edit_units', 'uses'=>'ApartmentController@editUnits']); // used
        Route::post('units-edit', ['as' => 'developer_units_edit', 'uses'=>'ApartmentController@unitsEdit']);
        // apartment list units sold/available status change
        Route::post('sold-unit-status-change', ['as' => 'developer_sold_unit_status_change', 'uses'=>'ApartmentController@soldUnitStatusChange']);

        // add credit routes
        // Route::get('credits-list', ['as' => 'developer_credit_list', 'uses'=>'CreditController@creditList']); // used
        // Route::post('add-credit', ['as' => 'developer_add_credit', 'uses'=>'CreditController@addCredit']);
        // Route::get('payment_checkout/{transaction_id}', ['as' => 'developer_credit_payment_checkout', 'uses'=>'CreditController@checkout']);
        // Route::post('charge-payment/{transaction_id}', ['as' => 'developer_credit_charge_payment', 'uses'=>'CreditController@chargePayment']);
        // Route::match(['get','post'],'payment_success_url/{transaction_id}/payment-success',['as'=>'developer_payment_success_url','uses' => 'CreditController@paymentSuccess']);
        // Route::match(['get','post'],'paypal_notify_url/{transaction_id}/paypal-notify',['as'=>'developer_paypal_notify_url','uses' => 'CreditController@paypalNotify']);

        // use credit routes
        // Route::get('add-credit-list', ['as' => 'developer_add_credit_list', 'uses'=>'CreditController@addCreditList']);
        // Route::get('add-property-credit/{ad_id}', ['as' => 'developer_add_property_credit', 'uses'=>'CreditController@addPropertyCredit']);
        // Route::post('add-credit-property', ['as' => 'developer_add_credit_property', 'uses'=>'CreditController@addCreditProperty']);

        // remove credits route
        // Route::get('remove-credits', ['as' => 'developer_remove_credits', 'uses'=>'CreditController@removeCredits']);

        // developer routes for getting one more apartment by admin approval

        Route::get('apartment-request', ['as' => 'developer_apartment_request', 'uses'=>'ApartmentController@apartmentRequest']);
        Route::post('insert-request-access-key', ['as' => 'insert_request_access_key', 'uses'=>'ApartmentController@insertRequestAccessKey']);

        // developer routes for getting one more apartment by admin approval

        // developer routes after login

        Route::match(['get','post'],'invite-agents',['as'=>'developer_invite_agents','uses' => 'DeveloperController@inviteAgents']);
        Route::match(['get','post'],'send-agent-invitation',['as'=>'send_agent_invitation','uses' => 'DeveloperController@sendAgentInvitation']);
        Route::match(['get','post'],'search-invite-agent/{value}',['as'=>'search_invite_agent','uses' => 'DeveloperController@searchInviteAgent']);
        Route::match(['get','post'],'invited-agents',['as'=>'developer_invited_agents','uses' => 'DeveloperController@invitedAgents']);
        Route::match(['get','post'],'delete-invited-agent',['as'=>'delete_invited_agent','uses' => 'DeveloperController@deleteInvitedAgent']);
        Route::match(['get','post'],'agent-details/{slug}',['as'=>'developer_agent_details','uses' => 'DeveloperController@agentDetails']);

        // assign property to agent routes

        Route::match(['get','post'],'assign-property/{ad_slug}',['as'=>'assign_property','uses' => 'DeveloperController@assignProperty']);
        Route::match(['get','post'],'assign-agent-property',['as'=>'assign_agent_property','uses' => 'DeveloperController@assignAgentProperty']);

        // agent view page routes
        Route::match(['get','post'],'chat-history/{agent_id}/{user_id}',['as'=>'show_chat_history','uses' => 'DeveloperController@chatHistory']);
        Route::match(['get','post'],'check-agent-status',['as'=>'check_agent_status','uses' => 'DeveloperController@checkAgentStatus']);

        // developer routes after login

        // developer new routes in third doc

        Route::match(['get','post'],'add-staff-number',['as'=>'add_staff_number','uses' => 'DeveloperController@addStaffNumber']);
        Route::match(['get','post'],'staff-number-add',['as'=>'staff_number_add','uses' => 'DeveloperController@staffNumberAdd']);
        Route::match(['get','post'],'add-staff',['as'=>'add_staff','uses' => 'DeveloperController@addStaff']);
        Route::match(['get','post'],'add-staff-member',['as'=>'add_staff_member','uses' => 'DeveloperController@addStaffMember']);
        Route::match(['get','post'],'assign-staff-property/{ad_slug}',['as'=>'assign_staff_property','uses' => 'DeveloperController@assignStaffProperty']);
        Route::match(['get','post'],'staff-assign-property',['as'=>'staff_assign_property','uses' => 'DeveloperController@staffAssignProperty']);
        Route::match(['get','post'],'added-staff-members',['as'=>'added_staff_members','uses' => 'DeveloperController@addedStaffMembers']);
        Route::match(['get','post'],'staff-details/{slug}',['as'=>'developer_staff_details','uses' => 'DeveloperController@agentDetails']);
        Route::match(['get','post'],'delete-staff-members',['as'=>'delete_staff_members','uses' => 'DeveloperController@deleteStaffMembers']);
        // Route::match(['get','post'],'check-exist-user-name',['as'=>'check_exist_user_name','uses' => 'DeveloperController@checkExistUserName']);

        // developer new routes in third doc
    });

});
Route::get('developers/{slug}', ['as' => 'developer_profile', 'uses'=>'AgentController@agentProfile']);
Route::get('/developer/contact-us', ['as' => 'developer_contact_us', 'uses'=>'UserController@developerContactUs']);
Route::post('/developer/contact-us', ['as' => 'developer_save_contact_us', 'uses'=>'UserController@saveContactUs']);
Route::get('/developer/all-staff-members/{ad_id}', ['as' => 'developer_all_staff_members', 'uses'=>'DeveloperController@allStaffMembers']);

// routes for developer's account

// routes for staff's account

Route::group(['middleware' => 'only_staff_access'], function() {
    Route::group(['prefix'=>'staff'],function() {
        Route::get('settings', ['as' => 'staff_settings', 'uses'=>'AgentController@agentSetting']);  // used
        Route::get('setting-view', ['as' => 'staff_settings_view', 'uses'=>'AgentController@agentSettingView']); // used
        Route::get('properties-list', ['as' => 'staff_properties_list', 'uses'=>'PropertyController@propertiesList']); // used
        Route::get('user-chat-messages', ['as' => 'staff_chat_messages', 'uses'=>'ChatController@user_chat_list']);
        Route::get('settings', ['as' => 'staff_settings', 'uses'=>'AgentController@agentSetting']);  // used
        Route::get('setting-view', ['as' => 'staff_settings_view', 'uses'=>'AgentController@agentSettingView']); // used
    });
});

Route::get('staff/{slug}', ['as' => 'staff_profile', 'uses'=>'AgentController@agentProfile']);
// routes for staff's account
