    function generate_option_from_json(jsonData, fromLoad){
        //Load Category Json Data To Brand Select
        if (fromLoad === 'category_to_brand'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> Select a bramd</option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
                }
                $('#brand_select').html(option);
                $('#brand_select').select2();
            }else {
                $('#brand_select').html('');
                $('#brand_select').select2();
            }
            $('#brand_loader').hide('slow');
        }else if(fromLoad === 'country_to_state'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> Select a State </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
                }
                $('#state_select').html(option);
                $('#state_select').select2();
            }else {
                $('#state_select').html('');
                $('#state_select').select2();
            }
            $('#state_loader').hide('slow');

        }else if(fromLoad === 'state_to_city'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> Select a City </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
                }
                $('#city_select').html(option);
                $('#city_select').select2();
            }else {
                $('#city_select').html('');
                $('#city_select').select2();
            }
            $('#city_loader').hide('slow');
        }
    }
    $(document).ready(function() {
        $('#phone').keyup(function(){
            $(this).val($(this).val().replace(/[^0-9]/g,""));
        });

        $(".notify img").attr("src", "<?php echo asset('/');?>/assets/img/notify.png");
        $(".footer-widget img").attr("src", "<?php echo asset('/');?>/assets/img/footer-logo.png");
        
    });
    $('[name="country"]').change(function(){
        var country_id = $(this).val();
        $('#state_loader').show();
        $.ajax({
            type : 'POST',
            url : "{{url('get-state-by-country') }}",
            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
            success : function (data) {
                generate_option_from_json(data, 'country_to_state');
            }
        });
    });

    $('[name="state"]').change(function(){
        var state_id = $(this).val();
        $('#city_loader').show();
        $.ajax({
            type : 'POST',
            url : "{{ route('get_city_by_state') }}",
            data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
            success : function (data) {
                generate_option_from_json(data, 'state_to_city');
            }
        });
    });
