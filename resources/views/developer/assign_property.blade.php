 @extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<style>
    .popover {
        top:-39px !important;
        left:749.961px !important;
        width:137px !important;
    }
    .popover .btn-primary {
        margin-right:5px;
    }
</style>
<div class="cms-inner-content">
    <div class="container">
        <div class="row">    
            @include('developer.developerInfoSection')
            <div class="col-md-9 col-sm-7 cms-border">
                <div class="tab-content agent-bio-data">
                    <div id="invite_agents" class="tab-pane fade in active">
                        <div class="cms-table">
                            <div class="invite_agents">
                                <h4>Assign Property</h4>
                                <!-- <h6>Hello User</h6> -->
                            </div>
                            <div class="table-responsive">
                                <input type="hidden" name="apartment_id" id="get_apartment_id" value="{{ $apartment['id'] }}">
                                <table class="table">
                                    <tbody id="get_agents_list">
                                        @if(count($invited_agents) > 0)
                                            @foreach($invited_agents as $key => $request)
                                                <tr>
                                                    <td class="column-one">
                                                        <div class="property-img">
                                                            @if(!empty(@$request['agent']['photo']))
                                                                <img src="{{asset('uploads/avatar/'.@$request['agent']['photo'])}}" alt="#" class="img-responsive"/>
                                                            @else
                                                                <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    
                                                    <td class="column-two"> 
                                                        <span>{{@$request['agent']['name']}}</span>
                                                        <span>{{substr(@$request['agent']['address'],0,80)}}</span>
                                                    </td>
                                                    <td class="column-three pos-raltv" rel="{{@$request['id']}}">
                                                        @if(!empty($apartment['assigned_agent_id']))
                                                            @if($apartment['assigned_agent_id'] == $request['agent']['id'])    
                                                                <a href="javascript:void(0);" class="btn btn-success small-btn pull-right">Assigned</a>
                                                            @else
                                                                <a href="javascript:void(0);" class="btn btn-danger small-btn pull-right">Assign</a>
                                                            @endif
                                                        @else
                                                            <a href="javascript:void(0);" class="btn btn-primary small-btn pull-right agent_assign_button" rel="{{@$request['agent']['id']}}" data-toggle="confirmation" >Assign</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <div class="col-md-12"><b>No Results found.</b></div>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script>
    
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Sucess!', options)
    @endif

    // open confirmation modal
    $('body').confirmation({
        selector: '[data-toggle="confirmation"]'
    });

    // assign property confirmation code
    $('.agent_assign_button').confirmation({
        onConfirm: function() {
            
            var current_selector = $(this);
            $(this).parent().addClass('current_selector');
            var agent_id = $(this).attr('rel');
            var apartment_id = $('#get_apartment_id').val();
            $.ajax({
                url : "{{ url('/developers/assign-agent-property') }}",
                type: "POST",
                data: { agent_id : agent_id, apartment_id : apartment_id, _token : '{{ csrf_token() }}' },
                success : function (data) {
                    if (data.success == 1){
                        
                        $('.column-three').html('<a href="javascript:void(0);" class="btn btn-danger small-btn pull-right">Assign</a>');
                        $('.current_selector').html('<a href="javascript:void(0);" class="btn btn-success small-btn pull-right">Assigned</a>');
                        $('.current_selector').removeClass('current_selector');

                        toastr.success(data.msg, '@lang('app.success')', toastr_options);
                    } else {
                        toastr.error('Something went wrong', '@lang('app.error')', toastr_options);
                    }
                },
                error:function() {
                    toastr.error('Something went wrong', '@lang('app.error')', toastr_options);
                }
            });
        },
        onCancel: function() { 
            // alert('not confirm');
        }
    });
    // open confirmation modal
    $('body').confirmation({
        selector: '[data-toggle="confirmation"]'
    });
</script>
@endsection