@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/datatables/jquery.dataTables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/datatables/dataTables.bootstrap.min.css')}}">
<link href="{{ asset('assets/plugins/datatables/responsive.dataTables.min.css') }}" rel="stylesheet"/>
<style>
    .dataTables_wrapper .dataTables_paginate .paginate_button{
        padding:0px !important;
    }
</style>
<div class="cms-inner-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="agnt-list-tbl">
                    <table id="table_id" class="display table cms-table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Joined At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($all_staff))
                                @foreach($all_staff as $key => $value)
                                    <tr>
                                        <td>
                                            <div class="property-img table-profile-pic">
                                                @if(!empty($value['staff']['photo']))
                                                    <img src="{{ asset('uploads/avatar/'.$value['staff']['photo']) }}" class="img-responsive" alt="Profile Pic" />
                                                @else
                                                    <img src="{{ asset('assets/img/default_user.png') }}" class="img-responsive" alt="Profile Pic" />
                                                @endif
                                            </div>
                                        </td>
                                        <td>{{$value['staff']['name']}}</td>
                                        <td>{{date('m-d-Y',strtotime($value['staff']['created_at'])) }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn action-btn dropdown-toggle" type="button" data-toggle="dropdown">Action<span class="caret"></span></button>
                                                <ul class="dropdown-menu" @if(Auth::check()) style="left:unset; right:153px; min-width:128px;" @endif>
                                                    <li>
                                                        @if(!empty($value['staff']['slug']))
                                                            <a href="{{url('staff/'.$value['staff']['slug'])}}" class="btn btn-primary small-btn"  id="">View Profile</a>
                                                        @else
                                                            <a href="javascript:void(0);" class="btn btn-primary small-btn"  id="">View Profile</a>
                                                        @endif
                                                    </li>
                                                    @if(!Auth::check())
                                                        <li>
                                                            <a href="" class="btn small-btn btn-success start_chat" data-agent-id ="{{$value['staff']['id']}}"  data-toggle="modal" data-target="#startChartModal">Start Chat</a>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- chat modal -->
<div id="startChartModal" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Start chat with Agent</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['id'=>'insertGuestUser', 'class' => '', 'files' => true , 'route'=> 'insert_guest_user' ]) }}
                <!-- {{csrf_field()}} -->
                <input type="hidden" name="agent_id" id="agent_id" value="">
                <div class="form-group {{ $errors->has('guestUserEmail')? 'has-error':'' }} email_error_div error_divs">
                    <label class="control-label">Email</label>
                    <input type="text" name="guestUserEmail" placeholder="Email" class="form-control" id="guestUserEmail" maxlength="255" value="{{@Auth::user()->email}}">
                    {!! $errors->has('guestUserEmail')? '
                    <p class="help-block">'.$errors->first('guestUserEmail').'</p>
                    ':'' !!}
                    <p class="help-block email_error_p error_ps"></p>
                </div>
                <div class="form-group {{ $errors->has('guestUserName')? 'has-error':'' }} name_error_div error_divs">
                    <label class="control-label">Name</label>
                    <input type="text" name="guestUserName" class="form-control" placeholder="Name" id="guestUserName" maxlength="255" value="{{@Auth::user()->name}}">
                    {!! $errors->has('guestUserName')? '
                    <p class="help-block">'.$errors->first('guestUserName').'</p>
                    ':'' !!}
                    <p class="help-block name_error_p error_ps"></p>
                </div>
                <div class="form-group {{ $errors->has('guestUserMessage')? 'has-error':'' }} message_error_div error_divs">
                    <label class="control-label">Message</label>
                    <textarea type="text" name="guestUserMessage" class="form-control" placeholder="Message.." id="guestUserMessage"></textarea>
                    {!! $errors->has('guestUserMessage')? '
                    <p class="help-block">'.$errors->first('guestUserMessage').'</p>
                    ':'' !!}
                    <p class="help-block message_error_p error_ps"></p>
                </div>
                <button type="button" class="btn btn-success" id="submitGuestUser">Submit</button>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal ends -->
@endsection
@section('page-js')
<script type="text/javascript" charset="utf8" src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript">
    $('#table_id').DataTable({
        pagingType: "simple",
        responsive: true,
        language: {
            paginate: {
                next: '<i class="fa fa-angle-double-right"></i>', 
                previous: '<i class="fa fa-angle-double-left"></i>'
            }
        }
    });
</script>
<script type="text/javascript">
    // $(document).ready( function () {
    //   $('#table_id').DataTable();
    //     language: {
    //       paginate: {
    //         next: '&#8594;', // or '→'
    //         previous: '&#8592;' // or '←' 
    //       }
    //     }
    // } );


    $('body').confirmation({
      selector: '[data-toggle="confirmation"]'
    });

    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif

    @if(session('error'))
        toastr.success('{{ session('error') }}', '<?php echo trans('app.error') ?>', toastr_options);
    @endif

    $(document).on('click','.start_chat',function(){
        var agent_id = $(this).attr('data-agent-id');
        $("#agent_id").val(agent_id);
    });

    // validations for send chat message modal
    $(document).on('click','#submitGuestUser',function() {
        
        var empty_error = 0;
        var email = $('#guestUserEmail').val();
        var name = $('#guestUserName').val();
        var message = $('#guestUserMessage').val();
        $('.error_divs').removeClass('has-error');
        $('.error_ps').text('');

        if(email == '') {
            $('.email_error_div').addClass('has-error');
            $('.email_error_p').text('Please enter your email address.');
            empty_error = 1;
        }

        if(name == '') {
            $('.name_error_div').addClass('has-error');
            $('.name_error_p').text('Please enter your name.');
            empty_error = 1;
        }

        if(message == '') {
            $('.message_error_div').addClass('has-error');
            $('.message_error_p').text('Please enter your message.');
            empty_error = 1;
        }
        
        var re = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        if(email != '') {

            if(!re.test(email)) {
                $('.email_error_div').addClass('has-error');
                $('.email_error_p').text('Please enter a valid email address.');
                empty_error = 1;
            }
        }

        if(empty_error > 0) {
            return false;
        }

        $('#insertGuestUser').submit();
    });
    // validations for send chat message modal
</script>
@endsection