@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="cms-inner-content">
    <div class="container">
        <div class="row">    
            @include('developer.developerInfoSection')
            <div class="col-md-9 col-sm-7 cms-border">
                <div class="tab-content agent-bio-data">
                    <div id="bio">
                        <h3>Add No. of Staff</h3>
                        <form id="addStaffForm" action="{{ url('/developers/staff-number-add') }}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group error_div about_error_div">
                                <div class="row">
                                    <label class="control-label col-sm-3">Staff's Description</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" placeholder="Enter staff description" name="staff_description" id="about" rows="5"></textarea>
                                    </div>
                                    <p class="about_error_p help-block"></p>
                                </div>
                            </div>
                            <div class="form-group error_div staff_error_div">
                                <div class="row">
                                    <label class="control-label col-sm-3">No. of Staff</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="no_of_staff" value="" name="no_of_staff" placeholder="Enter no. of staff" maxlength="6" />
                                    </div>
                                    <p class="staff_error_p help-block"></p>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary black-btn m-t-20" id="settings_save_btn">Next</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript">
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Success!', options)
    @endif

    // form validations
    // $(document).on('click','#settings_save_btn',function() {
    //     var description = $('#about').val();
    //     var no_of_staff = $('#no_of_staff').val();
    //     var is_error = 0;

    //     $('.error_div').removeClass('has-error');
    //     $('.help-block').text('');
    //     if(description == '') {

    //         $('.about_error_div').addClass('has-error');
    //         $('.about_error_p').text('Please add a description.');
    //         is_error = 1;
    //     }

    //     if(no_of_staff == '') {

    //         $('.staff_error_div').addClass('has-error');
    //         $('.staff_error_p').text('Please enter no. of staff.');
    //         is_error = 1;
    //     } else {
    //         if(!$.isNumeric(no_of_staff)){
    //             $('.staff_error_div').addClass('has-error');
    //             $('.staff_error_p').text('You can use number only.');
    //             is_error = 1;
    //         }
    //     }

    //     if(is_error > 0) {
    //         return false;
    //     }

    //     $('#addStaffForm').submit();
    // });
</script>
@endsection