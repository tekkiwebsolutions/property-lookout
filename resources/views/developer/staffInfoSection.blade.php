<div class="col-lg-3 col-md-3 col-sm-4 agent-detail-manual cms-avtar agent-cms-avtar">
    <div class="avtar">
        @if(!empty(Auth::user()->photo))
        <img src="{{asset('uploads/avatar/'.Auth::user()->photo)}}" alt="#" class="img-responsive"/>
        @else
        <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
        @endif
    </div>
    <ul class="agent_profiles text-center">
        <li><a href="@if(!empty($meta_data['facebook_url'])){{$meta_data['facebook_url']}}@else{{'https://www.facebook.com/'}}@endif"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
        <li><a href="@if(!empty($meta_data['twitter_url'])){{$meta_data['twitter_url']}}@else{{'https://www.twitter.com/'}}@endif"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
        <li><a href="@if(!empty($meta_data['linked_in_url'])){{$meta_data['linked_in_url']}}@else{{'https://www.linkedin.com/'}}@endif"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
    </ul>
    <h2>{{Auth::user()->name}}</h2>
    <div class="m-15">
        <span>+{{Auth::user()->phone}}</span>
        <span>{{Auth::user()->email}}</span>
    </div>
    <span>Account type: {{$current_package['name']}}</span>
    <?php  
        $ads_ids = \App\StaffAd::whereStaffId(Auth::user()->id)->pluck('ad_id');
        $apartment_number = \App\Ad::whereType('apartments')->whereTrash('0')->whereIn('id',$ads_ids)->count();
    ?>
    <span>Number of apartments: {{ @$apartment_number }}</span>
</div>