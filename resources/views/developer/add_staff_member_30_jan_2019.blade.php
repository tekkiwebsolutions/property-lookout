@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="cms-inner-content">
    <div class="container">
        <div class="row">    
            @include('developer.developerInfoSection')
            <div class="col-md-9 col-sm-7 cms-border">
                <div class="tab-content agent-bio-data">
                    <div id="staff_info">
                        <h3>Add Staff</h3>
                        <form id="addStaffMemberForm" action="{{ url('/developers/add-staff-member') }}" method="post">
                            {{csrf_field()}}
                            <div class="form-group error_div user_name_error_div {{ $errors->has('user_name')? 'has-error':'' }}">
                                <div class="row">
                                    <label class="control-label col-sm-4">UserName</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="user_name" placeholder="Enter User Name" maxlength="255" id="user_name" value="{{ old('user_name') }}"/>
                                    </div>
                                    {!! $errors->has('user_name')? '<p class="help-block">'.$errors->first('user_name').'</p>':'' !!}
                                </div>
                            </div>
                            <div class="form-group error_div email_error_div {{ $errors->has('email')? 'has-error':'' }}">
                                <div class="row">
                                    <label class="control-label col-sm-4">Email</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="email" placeholder="Enter Email Address" maxlength="255" id="email" value="{{ old('email') }}"/>
                                    </div>
                                    {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}
                                </div>
                            </div>
                            <div class="form-group error_div name_error_div {{ $errors->has('name')? 'has-error':'' }}">
                                <div class="row">
                                    <label class="control-label col-sm-4">Full Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="name" placeholder="Enter Full Name" maxlength="255" id="name" value="{{ old('name') }}"/>
                                    </div>
                                    {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
                                </div>
                            </div>
                            <div class="form-group error_div password_error_div {{ $errors->has('password')? 'has-error':'' }}">
                                <div class="row">
                                    <label class="control-label col-sm-4">Password</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="password" placeholder="Enter password" maxlength="255" id="password" value="{{ old('password') }}"/>
                                    </div>
                                    {!! $errors->has('password')? '<p class="help-block">'.$errors->first('password').'</p>':'' !!}
                                </div>
                            </div>
                            <div class="form-group error_div confirm_password_error_div {{ $errors->has('password_confirmation')? 'has-error':'' }}">
                                <div class="row">
                                    <label class="control-label col-sm-4">Confirm Password</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="password_confirmation" placeholder="Confirm your password" maxlength="255" id="password_confirmation" value="{{ old('password_confirmation') }}"/>
                                    </div>
                                    {!! $errors->has('password_confirmation')? '<p class="help-block">'.$errors->first('password_confirmation').'</p>':'' !!}
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary black-btn m-t-20" id="add_staff_button">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript">
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options);
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options);
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Success!', options);
    @endif

    // form validations
    // $(document).on('click','#add_staff_button',function() {
    //     var is_error = 0;
    //     var user_name = $('#user_name').val();
    //     var email = $('#email').val();
    //     var name = $('#name').val();
    //     var password = $('#password').val();
    //     var confirm_password = $('#password_confirmation').val();

    //     $('.error_div').removeClass('has-error');
    //     $('.help-block').text('');

    //     if(user_name == '') {

    //         $('.user_name_error_div').addClass('has-error');
    //         $('.user_name_error_p').text('Please enter username.');
    //         is_error = 1;
    //     } else {

    //         $.ajax({
    //             url : "{{ url('/developers/check-exist-user-name/') }}",
    //             type : "post",
    //             data : {user_name:user_name,type:"user_name", _token: "{{ csrf_token() }}"},
    //             success : function(data) {
    //                 if(data.success == '0') {
    //                     $('.user_name_error_div').addClass('has-error');
    //                     $('.user_name_error_p').text('This username has already been taken.');
    //                     is_error = 1;
    //                 }
    //             }
    //         });
    //     }

    //     if(email == '') {

    //         $('.email_error_div').addClass('has-error');
    //         $('.email_error_p').text('Please enter email address.');
    //         is_error = 1;
    //     } else {
    //         var re = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    //         if(!re.test(email)) {
    //             $('.email_error_div').addClass('has-error');
    //             $('.email_error_p').text('Please enter a valid email address.');
    //             is_error = 1;
    //         } else {
    //             $.ajax({
    //                 url : "{{ url('/developers/check-exist-user-name/') }}",
    //                 type : "post",
    //                 data : {email:email,type:"email", _token: "{{ csrf_token() }}"},
    //                 success : function(data) {
    //                     if(data.success == '0') {
    //                         $('.user_name_error_div').addClass('has-error');
    //                         $('.user_name_error_p').text('This username has already been taken.');
    //                         is_error = 1;
    //                     }
    //                 }
    //             });
    //         }
    //     }

    //     if(name == '') {

    //         $('.name_error_div').addClass('has-error');
    //         $('.name_error_p').text('Please enter full name.');
    //         is_error = 1;
    //     }

    //     if(password == '') {

    //         $('.password_error_div').addClass('has-error');
    //         $('.password_error_p').text('Please enter password.');
    //         is_error = 1;
    //     }

    //     if(password_confirmation == '') {

    //         $('.confirm_password_error_div').addClass('has-error');
    //         $('.confirm_password_error_p').text('Please confirm your password.');
    //         is_error = 1;
    //     }

    //     if(password != '' && password_confirmation != '') {
    //         if(password != password_confirmation) {
    //             $('.confirm_password_error_div').addClass('has-error');
    //             $('.confirm_password_error_p').text('Both passwords must match.');
    //             is_error = 1;
    //         }
    //     }

    //     if(is_error > 0) {
    //         return false;
    //     }

    //     $('#addStaffMemberForm').submit();
    // });
    // form validations
</script>
@endsection