<?php $url = Request::url(); ?>
<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 cms-avtar">
    <div class="avtar">
        @if(!empty(Auth::user()->photo))
            <img src="{{asset('uploads/avatar/'.Auth::user()->photo)}}" alt="#" class="img-responsive"/>
        @else
            <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
        @endif
    </div>
    <ul class="nav nav-pills cms-tabs">
        
        <!-- <li class="@if(str_contains($url,'developers/properties-list'))active @endif"><a @if(empty(Auth::user()->no_of_staff)) href="{{ url('/developers/add-staff-number') }}" @else href="{{ url('/developers/properties-list') }}" @endif><i class="fa fa-home" aria-hidden="true"></i>Home</a></li> -->
        <li class="@if(str_contains($url,'developers/properties-list'))active @endif"><a href="{{ url('/developers/properties-list') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>

        <!-- <li class="@if(str_contains($url,'developers/add-staff'))active @endif"><a @if(!empty(Auth::user()->no_of_staff)) @if($total_staff_added <= Auth::user()->no_of_staff) href="{{ url('/developers/add-staff') }}" @else href="javascript:void(0);" @endif @else href="{{ url('/developers/add-staff-number') }}" @endif><i class="fa fa-plus" aria-hidden="true"></i>Add Staff</a></li> -->
        <li class="@if(str_contains($url,'developers/add-staff'))active @endif"><a href="{{ url('/developers/add-staff') }}" ><i class="fa fa-plus" aria-hidden="true"></i>Add Staff</a></li>
        <!-- <li class="@if(str_contains($url,'developers/invite-agents'))active @endif"><a href="{{ url('/developers/invite-agents') }}"><i class="fa fa-lock" aria-hidden="true"></i>Invite Agents</a></li> -->
        
        <!-- <li class="@if(str_contains($url,'developers/invited-agents'))active @endif"><a href="{{ url('/developers/invited-agents') }}"><i class="fa fa-user" aria-hidden="true"></i>Invited Agents</a></li> -->

        <!-- <li class="@if(str_contains($url,'developers/added-staff-members'))active @endif"><a @if(!empty(Auth::user()->no_of_staff)) @if($total_staff_added <= Auth::user()->no_of_staff) href="{{ url('/developers/added-staff-members') }}" @else href="javascript:void(0);" @endif @else href="{{ url('/developers/add-staff-number') }}" @endif><i class="fa fa-user" aria-hidden="true"></i>Added Staff</a></li> -->
        <li class="@if(str_contains($url,'developers/added-staff-members'))active @endif"><a href="{{ url('/developers/added-staff-members') }}"><i class="fa fa-user" aria-hidden="true"></i>Added Staff</a></li>
    </ul>
</div>

