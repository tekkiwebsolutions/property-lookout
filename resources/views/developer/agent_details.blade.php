@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<?php use Carbon\Carbon; ?>
<style type="text/css">
    /*this style is kept in this file because it is only for this particular page and it creates disturbance for other pages*/
    .staff-invt{
        padding:30px 0;
    }
    .staff-invt li{
        width:100%; 
        font-size:20px; 
        font-weight:300; 
        float:left; 
        padding:0px; 
        margin:0px;
    }
    .staff-invt li a{
        color:#000000; 
        padding:5px 7px;
        float:left; 
        width:100%;}
    .staff-invt li a .fa {
        float:left; 
        width:20px; 
        text-align:center; 
        padding-top:5px;}
    .staff-invt li a:hover {
        background:#2d3e50; 
        color:#fff;}
    .column4 {
        width:26%;
    }
    .column-one {
        width:33%;
    }
    .align-right {
        text-align:right;
        margin-right:7px;
    }
    .cms-table .dropdown-menu {
        padding:10px 0px 10px 7px;
        min-width:175px;
    }
    .cms-table td li {
        margin:0px 4px;
    }
    .dropdown-menu > li > a {
        padding:3px 17px;
    }
    .popover-content {
        padding:5px 0px;
    }
    .popover {
        left:56.292px !important;
        top:-76px !important;
    }
    .popover-title, .cms-table .dropdown-menu .btn-xs {
        font-size:11px;
    }
    .cms-table .dropdown-menu .btn-xs {
        margin:0px 2px; 
        padding:4px 9px;
    }
</style>
<div class="cms-inner-content">
    <div class="container">
        <div class="row">    
            <div class="col-lg-3 col-md-3 col-sm-4 agent-detail-manual cms-avtar agent-cms-avtar">
                <div class="avtar">
                    @if(!empty($agent['photo']))
                        <img src="{{asset('uploads/avatar/'.$agent['photo'])}}" alt="#" class="img-responsive"/>
                    @else
                        <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
                    @endif
                </div>
                <ul class="agent_profiles text-center">
                    <li><a href="@if(!empty($meta_data['facebook_url'])){{$meta_data['facebook_url']}}@else{{'https://www.facebook.com/'}}@endif"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                    <li><a href="@if(!empty($meta_data['twitter_url'])){{$meta_data['twitter_url']}}@else{{'https://www.twitter.com/'}}@endif"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                    <li><a href="@if(!empty($meta_data['linked_in_url'])){{$meta_data['linked_in_url']}}@else{{'https://www.linkedin.com/'}}@endif"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                </ul>
                @if(!empty($agent['phone']))
                    <div class="contact_div" style="display:none;">

                        <button class="btn btn-block" id="onClickShowPhone" style="margin-bottom: 3%;font-size: 14px !important;">
                            <!-- <strong> <span id="ShowPhoneWrap">{{$agent['phone']}}</span> </strong> <br /> -->

                            <strong> <a href="callto:+{{$agent['phone']}}" id="ShowPhoneWrap">{{$agent['phone']}}</a> </strong> <br />
                        </button>
                    </div>
                    <div><a class="btn btn-primary contact-btn" href="javascript:void(0);" id="contact_btn">Contact</a></div>
                @endif
                <h2>{{$agent['name']}}</h2>
                <div class="m-15">
                    <span id="update_online_status"><strong>Online Status: </strong> @if(!empty($agent['last_activity_expires'])) @if($agent['last_activity_expires'] > Carbon::now()->setTimezone('Asia/Kolkata')) Online @else {{Carbon::parse($agent['last_activity_expires'])->diffForHumans()}} @endif @else N/A @endif </span>
                </div>
                <div class="m-15">
                    <span>{{$agent['email']}}</span>
                    <span>{{$agent['address']}}</span>
                    <span>{{$agent['country']['country_name']}}</span>
                </div>
                <span>Number of apartments : {{$agent['agent_apartment_count']}}</span>
            </div>
            <div class="col-md-9 col-sm-7 cms-border agents_credits">
                <div class="tab-content agent-bio-data">
                    <div id="invite_agents" class="tab-pane fade in active">
                        <div class="cms-table">
                            <div class="invite_agents">
                                <h4>Assigned Properties</h4>
                            </div>
                            <div class="table-responsive">
                                <table class="table cms-table cstm-tbl-cls agnt-prprty-tbl prprty-lst-tbl stf-dtl">
                                    @if(count($assigned_properties) > 0)
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Details</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach(@$assigned_properties as $key => $value)
                                                <tr class="three-btns-actn">
                                                    <td>
                                                        <div class="property-img">
                                                            @if($value['type'] == 'house')
                                                                <img src="{{asset('uploads/houseImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                                            @else
                                                                <img src="{{asset('uploads/apartmentImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td> 
                                                        <span>{{ @$value['title'] }}</span>
                                                        <span>{{ substr(@$value['address'],0,80) }}</span>
                                                    </td>
                                                    <td>
                                                        <a href="{{url('propertyShow/'.$value['type'].'/'.$value['slug'])}}" class="btn btn-primary small-btn">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <!-- <div class="dropdown">
                                                            <button class="btn action-btn dropdown-toggle" type="button" data-toggle="dropdown">Action<span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li>
                                                                    <a href="{{url('propertyShow/'.$value['type'].'/'.$value['slug'])}}" class="btn btn-primary small-btn">View Page</a>
                                                                </li>
                                                            </ul>
                                                        </div> -->
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    @else
                                        <div class="col-md-12"><b>No Apartment assigned to this staff member yet.</b></div>
                                    @endif
                                </table>
                            </div>
                        </div>
                        <div class="cms-table" style="margin-top:10px;">
                            <div class="invite_agents">
                                <h4>Last Activities</h4>
                            </div>
                            <div class="table-responsive" style="padding:10px !important">
                                <table id="table_id" class="display table cms-table ">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                            <th>Activity</th>
                                            <th>Type</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty(@$last_activities))
                                            @foreach(@$last_activities as $key => $value)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $value['description'] }}</td>
                                                <td>@if($value['type'] == 'property_available') Available @elseif($value['type'] == 'property_sold') Sold @endif</td>
                                                <td>{{ date('m-d-Y',strtotime($value['created_at'])) }} {{ date('h:i A',strtotime($value['created_at'])) }}</td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="cms-table" style="margin-top:10px;">
                            <div class="invite_agents">
                                <h4>Chat History</h4>
                            </div>
                            <div class="table-responsive" style="padding:10px !important">
                                <table id="chat_table_id" class="display table cms-table ">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                            <th>User Name</th>
                                            <th>User Email</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty(@$agent_chats))
                                            @foreach(@$agent_chats as $key => $value)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $value['user_detail']['name'] }}</td>
                                                    <td>{{ $value['user_detail']['email'] }}</td>
                                                    <td>
                                                        <?php 
                                                            $agent_id   = encrypt($value['Agent_id']);
                                                            $user_id    = encrypt($value['user_id']);
                                                        ?>
                                                        <a href="{{url('developers/chat-history/'.$agent_id.'/'.$user_id )}}" class="btn btn-primary small-btn approveAds" data-slug="" data-value="1">Show Chat</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript" charset="utf8" src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Sucess!', options)
    @endif

    // to show phone number div
    $(document).on('click','#contact_btn',function(){
        $(".contact_div").toggle();
    });

    // open confirmation modal
    $('body').confirmation({
        selector: '[data-toggle="confirmation"]'
    });

    // start data tables
    $(document).ready( function () {
        $('#table_id').DataTable({
            pagingType: "simple",
            responsive: true,
            language: {
                paginate: {
                    next: '<i class="fa fa-angle-double-right"></i>', 
                    previous: '<i class="fa fa-angle-double-left"></i>'
                }
            }
        });
        
        $('#chat_table_id').DataTable({
            pagingType: "simple",
            responsive: true,
            language: {
                paginate: {
                    next: '<i class="fa fa-angle-double-right"></i>', 
                    previous: '<i class="fa fa-angle-double-left"></i>'
                }
            }
        });
    });

    // function to check online status of agent every 10 seconds
    var checkOnline = setInterval(updateOnline, 10000);
    function updateOnline() {
        var agent_id = "{{$agent['id']}}";
        // ajax call to update login status after every 10 seconds
        $.ajax({
            url: "{{ url('/developers/check-agent-status') }}",
            type: "POST",
            data: {agent_id: agent_id, _token: "{{csrf_token()}}"},
            success:function(data) {
                if(data.success == 1) {
                    
                    $('#update_online_status').html('<strong>Online Status: </strong>'+data.status);
                }
            },
            error:function() {
                // alert("Oops. Some error occured. Please try again.");
            }
        });
    }
</script>
@endsection