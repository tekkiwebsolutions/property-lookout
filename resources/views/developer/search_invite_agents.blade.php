@if(count($agents) > 0)
    @foreach($agents as $key => $agent)
        <tr>
            <td class="column-one">
                <div class="property-img">
                    @if(!empty($agent['photo']))
                        <img src="{{asset('uploads/avatar/'.$agent['photo'])}}" alt="#" class="img-responsive"/>
                    @else
                        <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
                    @endif
                </div>
            </td>
            <td class="column-two"> 
                <span>{{$agent['name']}}</span>
                <span>{{substr($agent['address'],0,80)}}</span>
            </td>
            <td class="column-three btn-col">
                <a href="javascript:void(0);" class="btn @if(in_array($agent['id'],$invited_agents)) btn-success @else btn-primary @endif small-btn @if(!in_array($agent['id'],$invited_agents))send_request_button @endif" rel="{{$agent['id']}}">@if(in_array($agent['id'],$invited_agents)) Request Sent @else Send Request @endif</a>
                <a href="{{ url('/agent'.'/'.$agent['slug']) }}" class="btn btn-primary small-btn">View Profile</a>
            </td>
        </tr>
    @endforeach
@else
    <div class="col-md-12"><b>No Results found.</b></div>
@endif