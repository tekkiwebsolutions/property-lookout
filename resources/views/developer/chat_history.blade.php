@extends('layout.main')
<!-- @section('title') Chat History @parent @endsection -->
@section('title') @if( !empty($title) ) {{$title}} | @endif @parent @endsection
@section('main')
<style>
	.clearfix {
    	float: left;
    	width: 100%;
	}
	.chatcontainer{
	 	height: 400px;
	 	overflow-y: scroll;
	}
	.reciever_style{
		padding: 6px; 
		margin: 8px 0px !important; 
		width: 57%; 
		float: left; 
		border-radius: 5px; 
		background-color: rgb(245, 244, 244); 
		border: 1px solid rgb(227, 227, 227);
	}
	.sender_style{
		padding: 6px !important; 
		margin: 8px 0px !important; 
		width: 57%; 
		float: right; 
		border-radius: 5px; 
		background-color: rgb(44, 62, 80); 
		border: 1px solid rgb(227, 227, 227); 
		color: #ffffff !important;
	}
</style>

<div class="row mlr0">
   	<div class="page_wrapper">
      	<div class="container">
         	<div class="col-lg-12">
				<h2 class="single_page_heading">Chat History</h2>
				<div class="panel panel-default chat_panel clearfix">
					<div class="panel-heading clearfix">
	                  	<div class="float-left">
		                     <h4>Chat between {{$agent['name']}} and {{$user['name']}}</h4>
	                  	</div>
	              	</div>
	              	<div class="chatcontainer panel-body clearfix" id="chatcontainer">
						@if(!empty($chat_messages))
							@foreach($chat_messages as $key => $value)
								<div data-msgid="{{$value['id']}}" class="@if($value['sent_from'] == 'agent'){{'agent_div'}}@else{{'user_div'}}@endif">
									@if($value['sent_from'] == 'user')
										<p data-msgid="{{$value['id']}}" class="reciever_style"><b>User: </b>{{$value['Message']}}<span>{{date('d/m/Y',strtotime($value['created_at']))}}</span></p>
									@elseif($value['sent_from'] == 'agent')
										<p data-msgid="{{$value['id']}}" class="sender_style"><b>{{$agent['user_type'] == 'agent' ? 'Agent: ' : 'Staff: '}}</b>{{$value['Message']}}<span>{{date('d/m/Y',strtotime($value['created_at']))}}</span></p>
									@endif
								</div>
				        	@endforeach
			            @endif
      				</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('page-js')
<script type="text/javascript">
	$(function() {
		var wtf    = $('#chatcontainer');
		var height = wtf[0].scrollHeight;
		wtf.scrollTop(height);
	});

	setInterval(ajaxCall, 2500);
	var agent_id 	= "{{@$agent['id']}}"; 
	var user_id 	= "{{@$user['id']}}";
	function ajaxCall() {
		var last_id = $("#chatcontainer div:last-child").attr('data-msgid');
		$.ajax({
	        data: {agent_id:agent_id , user_id:user_id , window_type: 'developerWindow',last_id:last_id,_token : '{{ csrf_token() }}'},
	        type: "post",
	        url: "{{route('ajax_chat_after_interval')}}",
	        success: function(data){

				if(data.sent_from == 'agent'){
					var class_name = 'agent_div';
					var sender = 'Agent: ';
				}else{
					var class_name = 'user_div';
					var sender = 'User: ';
				}
				if(data.sent_from == 'agent'){
					var style_tags = 'padding: 6px; margin: 8px 0;width: 57%;float: right;border-radius: 5px;border: 1px solid #e3e3e3;background-color:#2c3e50;color: white;'
		
				}else if(data.sent_from == 'user'){
					var style_tags = 'padding: 6px;margin: 8px 0; width: 57%;float: left;border-radius: 5px;background-color: #f5f4f4;border: 1px solid #e3e3e3;'
				}

	            if(data != ''){
         			$(".chatcontainer").append('<div data-msgid="'+data.id+'" class="'+class_name+'"><p data-msgid="'+data.id+'" class="'+data.sent_from+'" style="'+style_tags+'"><b>'+sender+'</b>'+data.Message+'<span>'+data.sent_time+'</span></p></div>');

	            }
	            var wtf    = $('#chatcontainer');
		  		var height = wtf[0].scrollHeight;
		  		wtf.scrollTop(height);
	        }
		});
	}
</script>
@endsection

