@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="cms-inner-content">
    <div class="container">
        <div class="row">    
            @include('developer.developerInfoSection')
            <div class="col-md-9 col-sm-7 cms-border">
                <div class="tab-content agent-bio-data">
                    <div id="invite_agents" class="tab-pane fade in active">
                        <div class="cms-table">
                            <div class="invite_agents">
                                <h4>Invite Agents</h4>
                                <!-- <h6>Hello User</h6> -->
                            </div>
                            <div class="add_staff clearfix">
                                <h6>Invite Agent</h6>
                                <div class="search_agent">
                                    <input type="text" name="" class="" placeholder="Enter Agent Name" />
                                    <button type="button" id="agent_search_button">Search</button>
                                    <p class="warning_message" style="color:red;"></p>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody id="get_agents_list">
                                        @include('developer.search_invite_agents')
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript">
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Sucess!', options)
    @endif

    // agent send invitation script
    $(document).on('click','.send_request_button',function() {
        var agent_id = $(this).attr("rel");
        $.ajax({
            context : this,
            url : "{{ url('/developers/send-agent-invitation') }}",
            type: "POST",
            data: {agent_id : agent_id, _token : '{{ csrf_token() }}'},
            success: function(data) {
                // alert("yes");
                if(data.success == 1) {
                    $(this).text("Request Sent");
                    $(this).removeClass('send_request_button');
                    $(this).removeClass('btn-primary');
                    $(this).addClass('btn-success');
                    toastr.success(data.msg, '@lang('app.success')', toastr_options);  
                } else {
                    toastr.error('Something went wrong', '@lang('app.error')', toaster_options);
                }
            },
            error:function() {
                // alert("error");
                toastr.error('Something went wrong.', '@lang('app.error')', toastr_options);
            }
        });
    });

    // agent search function
    $(document).on('click','#agent_search_button',function() {
        var search_val = $(this).prev("input").val();
        if(search_val == '') {
            search_val = 'no_value';
        }
        $('#get_agents_list').load("{{ url('') }}"+'/'+'developers/search-invite-agent'+'/'+search_val);
    });
</script>
@endsection