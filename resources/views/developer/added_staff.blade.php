@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<style type="text/css">
    /*this style is kept in this file because it is only for this particular page and it creates disturbance for other pages*/
    .staff-invt{
        padding:30px 0;
    }
    .staff-invt li{
        width:100%; 
        font-size:20px; 
        font-weight:300; 
        float:left; 
        padding:0px; 
        margin:0px;
    }
    .staff-invt li a{
        color:#000000; 
        padding:5px 7px;
        float:left; 
        width:100%;
    }
    .staff-invt li a .fa {
        float:left; 
        width:20px; 
        text-align:center; 
        padding-top:5px;
    }
    .staff-invt li a:hover {
        background:#2d3e50; 
        color:#fff;
    }
    .column4 {
        width:26%;
    }
    .column-one {
        width:33%;
    }
    .align-right {
        text-align:right;
        margin-right:7px;
    }
    .cms-table .dropdown-menu {
        padding:10px 0px 10px 7px;
        min-width:175px;
    }
    .cms-table td li {
        margin:0px 4px;
    }
    .dropdown-menu > li > a {
        padding:3px 17px;
    }
    .popover-content {
        padding:5px 0px;
    }
    .popover {
        /*left:56.292px !important;*/
        /*top:-76px !important;*/
        width:137px !important;
    }
    .popover-title, .cms-table .dropdown-menu .btn-xs {
        font-size:11px;
    }
    .cms-table .dropdown-menu .btn-xs {
        margin:0px 2px; 
        padding:4px 9px;
    }
</style>
<div class="cms-inner-content">
    <div class="container">
        <div class="row">    
            @include('developer.developerInfoSection')
            <div class="col-md-9 col-sm-7 cms-border">
                <div class="tab-content agent-bio-data">
                    <div id="invite_agents" class="tab-pane fade in active">
                        <div class="cms-table">
                            <div class="invite_agents">
                                <h4>Added Staff Members</h4>
                            </div>
                            <div class="add-staff-flx">
                                <div class="add-staff-flx-inr">
                                    <h6>Added Staff Members</h6>
                                </div>
                                <div class="add-staff-flx-inr">
                                    <a href="{{ url('/developers/add-staff') }}" class="btn btn-primary">Add Staff Members</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        @if(count($added_members) > 0)
                                            @foreach($added_members as $key => $agent)
                                                <tr>
                                                    <td>
                                                        <div class="property-img table-profile-pic">
                                                            @if(!empty($agent['photo']))
                                                                <img src="{{asset('uploads/avatar/'.$agent['photo'])}}" alt="agent profile" class="img-responsive"/>
                                                            @else
                                                                <img src="{{asset('uploads/avatar/default_user.png')}}" alt="agent profile" class="img-responsive"/>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td> 
                                                        <span>{{ @$agent['name'] }}</span>
                                                        <!-- <span>{{ substr(@$agent['address'],0,80) }}</span> -->
                                                    </td>
                                                    <td>
                                                        <a href="{{ url('/developers/staff-details'.'/'.$agent['slug']) }}" class="btn btn-primary small-btn">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <a href="javascript:void(0);" class="btn small-btn btn-danger agentDeleteBtn" data-toggle="confirmation" rel="{{ $agent['id'] }}">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                        <!-- <div class="dropdown align-right">
                                                            <button class="btn action-btn dropdown-toggle float-right" type="button" data-toggle="dropdown">Action
                                                            <span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                view agent details
                                                                <li><a href="{{ url('/developers/staff-details'.'/'.$agent['slug']) }}" class="btn btn-primary small-btn">view</a></li>
                                                                delete agent request 
                                                                <li><a href="javascript:void(0);" class="btn small-btn btn-danger agentDeleteBtn" data-toggle="confirmation" rel="{{ $agent['id'] }}">Delete</a></li>
                                                            </ul>
                                                       </div> -->
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <div class="col-md-12"><b>No Staff Members added yet.</b></div>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript">
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Sucess!', options)
    @endif

    // delete agent confirmation code
    $('.agentDeleteBtn').confirmation({
        onConfirm: function() {
            
            var current_selector = $(this);
            var staff_id = $(this).attr('rel');
            $.ajax({
                url : "{{ url('/developers/delete-staff-members') }}",
                type: "POST",
                data: { staff_id : staff_id, _token : '{{ csrf_token() }}' },
                success : function (data) {
                    if (data.success == 1){
                        current_selector.closest('tr').hide('slow');
                        toastr.success(data.msg, '@lang('app.success')', toastr_options);
                    } else {
                        toastr.error('Some error occured.', 'Error!', toastr_options);
                    }
                }
            });
        },
        onCancel: function() { 
            // alert('not confirm');
        }
    });

    // open confirmation modal
    $('body').confirmation({
        selector: '[data-toggle="confirmation"]'
    });
</script>
@endsection