@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif 
@parent @endsection
@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/plugins/owl.carousel/assets/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('assets/paginate/src/jquery.paginate.css')}}" />

<style>
    .modal_text_style{

        font-family: 'Proxima Nova';
        font-size: 15px;
        line-height: 22px;
    }

    /*review start*/
    .rotate{
        -moz-transition: all 1s linear;
        -webkit-transition: all 1s linear;
        transition: all 1s linear;
    }

    .rotate.down{
        -moz-transform:rotate(180deg);
        -webkit-transform:rotate(180deg);
        transform:rotate(180deg);
    }
    .rotate{
        margin-left: 10px;
    }
    .review-section form #message {
        min-height: 60px !important;
    }
    .review-section .author-area {
        margin-top: 10px;
        margin-left: 9px;
    }
    .paginate-pagination{
        float: right;
    }

    /*review ends*/
    .des-img img{
        height:200px;
    }
    .outer {
        margin: 0 auto;
        max-width: 800px;
    }
</style>

@endsection
@section('main')

<div class="navigate-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="address">
                    <div class="title">
                        <h4>{{ucfirst($post['title'])}}</h4>
                    </div>
                    <address>
                        <div class="left">
                            <a href="javascript:void(0);"><i class="fa fa-map-marker"></i></a>
                        </div>
                        <div class="right">
                            <p>Posted By :<br/><small>{{$post['author']['name']}}</small></p>
                        </div>
                    </address>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="navigate-icons property_sold-icon">
                    <ul>
                        <!-- <li>
                            <div class="text">${{@$detail['price']}}</div>
                        </li> -->
                        <!-- <li class="apartment_link">
                            <a href="javascript:voud(0);">Go to Apartment Page</a>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pd-featured-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="javascript:void(0);" class="">

                    @if(!empty($post['feature_img']['media_name']) && file_exists(public_path('/uploads/images/').$post['feature_img']['media_name']))
                        <img src="{{asset('uploads/images/'.$post['feature_img']['media_name'])}}" class="img-responsive" width="400px" height="200px" alt="{{$post['title']}}">
                    @endif
                </a>
            </div>
        </div>
    </div>
</div>

<div class="description-box">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="content">
                    <div class="title">
                        <h3>Description</h3>
                        <p>{!! nl2br($post['post_content']) !!}</p>
                    </div>
                </div>
                <ul class="ad-action-list">
                    <!-- <button class="btn btn-primary black-btn" data-toggle="modal" data-target="#reportAdModal"><i class="fa fa-ban"></i> Report this property</button> -->
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="related_author_list clearfix cstm-author-list">
    <div class="container">
        <h4><b>AGENT</b></h4>
        <div class="row">
        @if(!empty($agents))
            @foreach($agents as $key => $value)
                <div class="col-md-3 col-sm-6 col-xs-12 author-area">
                    <div class="author-img">
                        <a href="javascript:void(0);">
                        @if(!empty($value['photo']))
                            <img src="{{asset('uploads/avatar/'.$value['photo'])}}" alt="#">
                        @else
                            <img src="{{asset('assets/img/default_user.png')}}" alt="#">
                        @endif
                        </a>
                    </div>
                    <div class="author-info">
                        <ul>
                            <li class="name"><a href="javascript:void(0);">{{$value['name']}}</a></li>
                            <li class="address">{{$value['address']}}</li>
                            <li>
                                <a href="{{url('agent/'.$value['slug'])}}" class="btn btn-primary agent-contact">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
            @endforeach
        @endif   
        </div>
    </div>
</div>

<!-- report modal starts -->
    
    <div class="modal fade" id="reportAdModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Is there something wrong with this Property?</h4>
                </div>
                <div class="modal-body">

                    <p>We're constantly working hard to assure that our properties meet high standards and we are very grateful for any kind of feedback from our users.</p>

                    <form>

                        <div class="form-group">
                            <label class="control-label">@lang('app.reason'):</label>
                            <select class="form-control" name="reason" id="report_reason">
                                <option value="">@lang('app.select_a_reason')</option>
                                <option value="unavailable">@lang('app.item_sold_unavailable')</option>
                                <option value="fraud">@lang('app.fraud')</option>
                                <option value="duplicate">@lang('app.duplicate')</option>
                                <option value="spam">@lang('app.spam')</option>
                                <option value="wrong_category">@lang('app.wrong_category')</option>
                                <option value="offensive">@lang('app.offensive')</option>
                                <option value="other">@lang('app.other')</option>
                            </select>

                            <div id="reason_info"></div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="control-label">@lang('app.email'):</label>
                            <input type="text" class="form-control" id="report_email" name="email">
                            <div id="email_info"></div>

                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">@lang('app.message'):</label>
                            <textarea class="form-control" id="report_message" name="message"></textarea>
                            <div id="message_info"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('app.close')</button>
                    <button type="button" class="btn btn-primary" id="report_ad">Report</button>
                </div>
            </div>
        </div>
    </div>
<!-- report modal ends -->

@endsection
@section('page-js')
<script src="{{ asset('assets/plugins/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{asset('assets/star/jquery.fontstar.js')}}"></script>
<script src="{{asset('assets/paginate/src/jquery.paginate.js')}}"></script>
<script type="text/javascript">
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif

    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 3,
                    nav: false
                },
                1000: {
                    items: 6,
                    nav: true,
                    loop: false,
                    margin: 20
                }
            }
        })
    })

    // review script start
    $(".rotate").click(function(){
        $(this).toggleClass("down")  ; 
    });

    $(document).on('click','.rotate',function(){
        var fd = '';
        $.ajax({
            url : '{{ route('refresh_captcha') }}',
            type: "GET",
            data: fd,
            cache: false,
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success : function (data) {
                // alert(data);
                $("#current_captcha").val(data);
                $(".captcha_string").text(data);
            }
        });
    });

    $('.star').fontstar({},function(value,self){

        console.log("hello "+value);
    });
    
    //call paginate
    $('#example').paginate();
    // review script ends

    // submit report against property
    $(function() {
        $('button#report_ad').click(function(){
            var reason = $('#report_reason').val();
            var email = $('#report_email').val();
            var message = $('#report_message').val();
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            var error = 0;
            if(reason.length < 1){
                $('#reason_info').html('<p class="text-danger">Reason required</p>');
                error++;
            }else {
                $('#reason_info').html('');
            }
            if(email.length < 1){
                $('#email_info').html('<p class="text-danger">Email required</p>');
                error++;
            }else {
                if ( ! regex.test(email)){
                    $('#email_info').html('<p class="text-danger">Valid email required</p>');
                    error++;
                }else {
                    $('#email_info').html('');
                }
            }
            if(message.length < 1){
                $('#message_info').html('<p class="text-danger">Message required</p>');
                error++;
            }else {
                $('#message_info').html('');
            }

            if (error < 1){
                $('#loadingOverlay').show();
                $.ajax({
                    type : 'POST',
                    url : '{{ route('report_ads_pos') }}',
                    data : { reason : reason, email: email,message:message, id:'{{ @$detail["id"] }}',  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        if (data.status == 1){
                            toastr.success(data.msg, '@lang('app.success')', toastr_options);
                        }else {
                            toastr.error(data.msg, '@lang('app.error')', toastr_options);
                        }
                        $('#report_reason').val('');
                        $('#report_email').val('');
                        $('#report_message').val('');
                        $('#reportAdModal').modal('hide');
                        $('#loadingOverlay').hide();
                    }
                });
            }
        });
    });
    // submit report against property
</script>
@endsection