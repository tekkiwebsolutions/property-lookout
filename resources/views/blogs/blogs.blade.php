@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('page-css')
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('main')
    <div class="container">
        <div id="wrapper">
            <div id="page-wrapper">
                @if( ! empty($title))
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"> {{ $title }}
                                @if(Auth::check() && Auth::user()->user_type == 'user')
                                    <?php $pack_permissions = packPermissions(Auth::user()->id); ?>
                                    @if($pack_permissions['blog_limit_reached'] == 0)
                                        <a href="{{ url('/blogs/create') }}" class="btn btn-info pull-right"> <i class="fa fa-floppy-o"></i> Create New Blog</a>
                                    @endif
                                @endif
                            </h1>
                        </div> <!-- /.col-lg-12 -->
                    </div> <!-- /.row -->
                @endif
                @include('admin.flash_msg')
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table table-bordered table-striped" id="jDataTable">
                            <thead>
                                <tr>
                                    <th>Sr. No.</th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($posts))
                                    @foreach($posts as $key => $blog)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $blog->title }}</td>
                                            <td>
                                                @if(!empty($blog['feature_img']['media_name']) && file_exists(public_path('/uploads/images/thumbs/').$blog['feature_img']['media_name']))
                                                    <img width="100px" height="80px" src="{{ asset('/uploads/images/thumbs/'.$blog['feature_img']['media_name']) }}">
                                                @else 
                                                    <img width="100px" height="80px" src="{{ asset('/assets/img/no_image.png') }}">
                                                @endif
                                            </td>
                                            <td>{{ $blog->created_at_datetime() }}</td>
                                            <td>
                                                <a href="{{ url('/blogs/view-details/'.$blog->slug) }}" class="btn btn-success"><i class="fa fa-eye"></i> </a>
                                                @if(Auth::check())
                                                    @if(Auth::user()->id == $blog['user_id'])
                                                        <a href="{{ url('/blogs/edit/'.getEncrypted($blog->id)) }}" class="btn btn-primary"><i class="fa fa-edit"></i> </a>
                                                        <a href="javascript:;" class="btn btn-danger deletePage" data-id="{{$blog->id}}"><i class="fa fa-trash"></i> </a>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>   <!-- /#page-wrapper -->
        </div>   <!-- /#wrapper -->
    </div> <!-- /#container -->
@endsection

@section('page-js')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#jDataTable').DataTable({
            });
        });
    
        $(document).ready(function() {
            $('body').on('click', '.deletePage', function (e) {
                if (!confirm("<?php echo trans('app.are_you_sure') ?>")) {
                    e.preventDefault();
                    return false;
                }

                var selector = $(this);
                var id = $(this).data('id');

                $.ajax({
                    type: 'POST',
                    url: "{{ url('/blogs/delete') }}",
                    data: {id: id, _token: '{{ csrf_token() }}'},
                    success: function (data) {
                        if (data.success == 1) {
                            selector.closest('tr').hide('slow');
                            var options = {closeButton: true};
                            toastr.success('success', '<?php echo trans('app.success') ?>', options)
                        } else {

                            toastr.success('error', 'some error occured', options)
                        }
                    }
                });
            });
        });
    
        var options = {closeButton : true};
        @if(session('success'))
            toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', options);
        @endif
    </script>
@endsection


