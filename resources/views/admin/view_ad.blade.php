@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('main')
    <div class="container">

        <div id="wrapper">

            @include('admin.sidebar_menu')

            <div id="page-wrapper">
                @if( ! empty($title))
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"> {{ $title }}  </h1>
                        </div> <!-- /.col-lg-12 -->
                    </div> <!-- /.row -->
                @endif

                @include('admin.flash_msg')

                <div class="row">
                    <div class="col-xs-12">

                        <div id="return_msg"></div>

                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Property status</th>
                                <td>
                                    <label>
                                        <input type="radio" name="account_status" value="1" data-ad-id="{{$ad->id}}" @if($ad->status == '1') checked="checked" @endif />
                                        @lang('app.active')
                                    </label>

                                    <label>
                                        <input type="radio" name="account_status" value="2" data-ad-id="{{$ad->id}}" @if($ad->status == '2') checked="checked" @endif />
                                        @lang('app.block')
                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <th>Title</th>
                                <td>{{ ucfirst($ad->title) }}</td>
                            </tr>
                            <tr>
                                <th>Agent Name</th>
                                <td>{{ ucfirst($ad->user->name) }}</td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{{ ucfirst($ad->description) }}</td>
                            </tr>
                            <tr>
                                <th>Type</th>
                                <td>{{ $ad->type }}</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>{{ $ad->country->country_name }}</td>
                            </tr>
                            <tr>
                                <th>State</th>
                                <td>{{ $ad->state->state_name }}</td>
                            </tr>
                            <tr>
                                <th>City</th>
                                <td>{{ $ad->city->city_name }}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>{{ $ad->address }}</td>
                            </tr>
                            <tr>
                                <th>Zip Code</th>
                                <td>{{ $ad->zip }}</td>
                            </tr>
                            <tr>
                                <th>Video Url</th>
                                <td><a href="{{$ad->video_url}}" target="_blank">{{$ad->video_url}}</a></td>
                            </tr>
                            @if($ad->type == 'house')
                                <tr>
                                    <th>Property status</th>
                                    <td>{{ $ad->property_status == 0 ? 'Available' : 'Sold'}}</td>
                                </tr>
                            @endif
                            @if($ad->price)
                                <tr>
                                    <th>Price</th>
                                    <td>{{ $ad->price }}</td>
                                </tr>
                            @endif
                            @if($ad->square_unit_space)
                                <tr>
                                    <th>Size</th>
                                    <td>{{ $ad->square_unit_space }} {{ $ad->unit_type }}</td>
                                </tr>
                            @endif
                            @if($ad->floor)
                            <tr>
                                <th>No. Of Floors</th>
                                <td>{{ $ad->floor }} Floors</td>
                            </tr>
                            @endif
                            @if($ad->units)
                                <tr>
                                    <th>Total Units</th>
                                    <td>{{ ($ad->floor?$ad->floor:0)*($ad->units?$ad->units:0) }} Units</td>
                                </tr>
                            @endif
                            @if($ad->type == 'apartment')
                                <tr>
                                    <th>Units Available</th>
                                    <td>{{ @$units_available }}</td>
                                </tr>
                            @endif
                            @if($ad->beds)
                                <tr>
                                    <th>Bedrooms</th>
                                    <td>{{ $ad->beds }} Bedrooms</td>
                                </tr>
                            @endif
                            @if($ad->attached_bath)
                                <tr>
                                    <th>Bathrooms</th>
                                    <td>{{ $ad->attached_bath }} Bathrooms</td>
                                </tr>
                            @endif
                            @if( $ad->no_of_parking )
                                <tr>
                                    <th>No. Of Parking</th>
                                    <td>{{ $ad->no_of_parking }}</td>
                                </tr>
                            @endif
                            @if($ad->cover_photo)
                                <tr>
                                    <th>Cover Photo</th>
                                    <td>
                                        @if($ad->type == 'house')
                                            <?php $path = '/uploads/houseImages'; ?>
                                        @else    
                                            <?php $path = '/uploads/apartmentImages'; ?>
                                        @endif
                                        @if(file_exists(public_path($path).'/'.$ad['cover_photo']))
                                            <img src="{{asset($path.'/'.$ad->cover_photo)}}" alt="Featured Image">
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>   <!-- /#page-wrapper -->
        </div>   <!-- /#wrapper -->
    </div> <!-- /#container -->
@endsection

@section('page-js')

    <script>

        $(function(){
            $(document).on('click','[name="account_status"]', function(){
                var ad_id = $(this).data('ad-id');
                var status = $(this).val();
                $.ajax({
                    type : 'POST',
                    url : '{{ route('change_ad_status') }}',
                    data: { ad_id : ad_id, status : status, _token : '{{csrf_token()}}' },
                    success : function(data){
                        $('#return_msg').html(data);
                    }
                });
            });
        })
    </script>

@endsection