@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="container">
    <div id="wrapper">
        @include('admin.sidebar_menu')
        <div id="page-wrapper">
            @if(session('error'))
                <div class="row">
                    <div class="col-lg-12">
                        <br />
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@lang('app.dashboard')</h1>
                </div>
            </div>
            <!-- dashboard panels start -->
            <div class="row">
                <!-- approved properties -->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12 ">
                                    <div class="huge">{{ $approved_ads }}</div>
                                    <div>Approved Properties</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- pending properties -->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $pending_ads }}</div>
                                    <div>Pending Properties</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- blocked properties -->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $blocked_ads }}</div>
                                    <div>Blocked Properties</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- @if($ten_contact_messages) -->
                <!-- total no of users in the website -->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $total_users }}</div>
                                    <div>@lang('app.users')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- total reports against properties and users -->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $total_reports }}</div>
                                    <div>@lang('app.reports')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- total properties sold -->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $properties_sold }}</div>
                                    <div>Properties Sold</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- total no of payments done -->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $total_payments }}</div>
                                    <div>@lang('app.success_payments')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- some of total payment amount -->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">  {{ $total_payments_amount }} <sup>{{ get_option('currency_sign') }}</sup></div>
                                    <div>@lang('app.total_payment')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- @endif -->

            </div>
            <!-- dashboard panels end -->

            @if($lUser->is_admin())
                <div class="row">
                    @if($ten_contact_messages)
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    @lang('app.latest_ten_contact_messages')
                                </div>
                                <div class="panel-body">
                                    @if($ten_contact_messages->count() > 0)
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>@lang('app.sender')</th>
                                                <th>@lang('app.message')</th>
                                            </tr>
                                            @foreach($ten_contact_messages as $message)
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-user"></i> {{ $message->name }} <br />
                                                        <i class="fa fa-envelope-o"></i> {{ $message->email }} <br />
                                                        <i class="fa fa-clock-o"></i> {{ $message->created_at->diffForHumans() }}
                                                    </td>
                                                    <td>{{ $message->message }}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    @else
                                        <div><h4>No contact messages to show!!</h4></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($reports)
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Latest 10 Property Reports
                                </div>
                                <div class="panel-body">

                                    @if($reports->count() > 0)
                                        <table class="table table-bordered table-striped table-responsive">
                                            <tr>
                                                <th>@lang('app.reason')</th>
                                                <th>@lang('app.email')</th>
                                                <th>@lang('app.message')</th>
                                                <th>@lang('app.ad_info')</th>
                                            </tr>

                                            @foreach($reports as $report)
                                                <tr>
                                                    <td>{{ $report->reason }}</td>
                                                    <td> {{ $report->email }}  </td>
                                                    <td>
                                                        {{ $report->message }}
                                                        <hr />
                                                        <p class="text-muted"> <i>@lang('app.date_time'): {{ $report->posting_datetime() }}</i></p>
                                                    </td>
                                                    <td>
                                                        @if($report->ad)
                                                            <a href="{{ url('propertyShow/'.$report->ad->type.'/'.$report->ad->slug) }}" target="_blank">View property</a>
                                                            <i class="clearfix"></i>
                                                            <a href="{{ route('reports_by_ads', $report->ad->slug) }}">
                                                                <i class="fa fa-exclamation-triangle"></i> @lang('app.reports') : {{ $report->ad->reports->count() }}
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    @else
                                        <div><h4>No reports to show!!</h4></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endif
        </div>
        <!-- /#page-wrapper -->
    </div>
</div> 
@endsection