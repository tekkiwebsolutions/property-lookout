@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="container">
    <div class="row">
        @include('admin.sidebar_menu')
        <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
            <div id="page-wrapper">
                @if( ! empty($title))
                <div class="inr-pg-ttl">
                    <h1 class="page-header"> {{ $title }}  </h1>
                </div>
                @endif
                @include('admin.flash_msg')
                <div class="row">
                    <div class="col-xs-12">
                        {{ Form::open(['class' => 'form-horizontal', 'method'=>'post', 'id'=>'replyContactMessage','route'=>'contact_messages_reply_send' ]) }}
                        <input type="hidden" name="id" value="{{$id}}">
                        <div class="form-group {{ $errors->has('email')? 'has-error':'' }}">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="email" value="{{$contact_us_details['email']}}" name="email" placeholder="@lang('app.email')" readonly>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('message')? 'has-error':'' }}">
                            <div class="col-sm-12">
                                <textarea name="message" id="message" class="form-control" rows="6" placeholder="Message..."></textarea>
                                {!! $errors->has('message')? '
                                <p class="help-block">'.$errors->first('message').'</p>
                                ':'' !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary">@lang('app.reply')</button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
</div>
<!-- /#container -->
@endsection
@section('page-js')
@endsection