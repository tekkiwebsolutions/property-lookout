@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
    <div class="container">
        <div class="row">
            @include('admin.sidebar_menu')
            <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h1 class="page-header"> {{ $title }}  </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            {{ Form::open(['class' => 'form-horizontal', 'method'=>'post', 'id'=>'replyContactMessage','route'=>'developer_contact_reply_send' ]) }}

                            <input type="hidden" name="id" value="{{ $id }}">
                            <input type="hidden" name="user_id" value="{{ $contact_us_details['user']['id'] }}">
                            <input type="hidden" name="type" value="{{ $contact_us_details['type'] }}">
                            <div class="form-group {{ $errors->has('email')? 'has-error':'' }}">
                                <label for="name" style="margin-left:15px;font-weight:bold;">Developer Email: </label>
                                <div class="col-xs-12">
                                    <input type="text" class="form-control" id="email" value="{{$contact_us_details['user']['email']}}" name="email" placeholder="@lang('app.email')" readonly>
                                </div>
                            </div>
                            <?php 
                                // get access key for user
                                $access_token = md5(uniqid($contact_us_details['user']['id'], true));
                            ?>
                            <div class="form-group {{ $errors->has('access_key')? 'has-error':'' }}">
                                <label for="name" style="margin-left:15px;font-weight:bold;">Access Key: </label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="access_key" value="{{$access_token}}" name="access_key" placeholder="" readonly>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('message')? 'has-error':'' }}">
                                <label for="name" style="margin-left:15px;font-weight:bold;">Admin Message: </label>
                                <div class="col-sm-12">
                                    <textarea name="message" id="message" class="form-control" rows="6" placeholder="Message..."></textarea>
                                    {!! $errors->has('message')? '<p class="help-block">'.$errors->first('message').'</p>':'' !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-9">
                                    <button type="submit" class="btn btn-primary">@lang('app.reply')</button>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>   <!-- /#page-wrapper -->
            </div>
        </div>   <!-- /#wrapper -->
    </div> <!-- /#container -->
@endsection

@section('page-js')
@endsection