@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<link rel="stylesheet" href="{{asset('assets/css/admin.css')}}">
<?php //prx($posts); ?>
    <div class="container">
        <div class="row">
            @include('admin.sidebar_menu')
            <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                <div id="page-wrapper">
                    @if(!empty($title))
                        <div class="inr-pg-ttl">
                            <h1 class="page-header"> {{ $title }}  </h1>
                        </div>
                    @endif
                    @include('admin.flash_msg')
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            {{ Form::open(['class' => 'form-horizontal', 'files'=>'true', 'id'=>'createPostForm' , 'url'=>'/dashboard/postsIndex?type=about-us' , 'method'=>'post']) }}
                                <input type="hidden" name="type" value="about-us">
                                <div class="form-group {{ $errors->has('title')? 'has-error':'' }}">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" class="form-control" id="title" value="@if(!empty($posts)){{$posts->title}}@endif" name="title" placeholder="@lang('app.title')" >
                                        {!! $errors->has('title')? '<p class="help-block">'.$errors->first('title').'</p>':'' !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('post_content')? 'has-error':'' }}">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <textarea name="post_content" id="post_content" class="form-control" rows="6">@if(!empty($posts)){{$posts->post_content }}@endif</textarea>
                                        {!! $errors->has('post_content')? '<p class="help-block">'.$errors->first('post_content').'</p>':'' !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="admn-abt-div profile-avatar">
                                            @if(!empty($posts->feature_image))
                                                <img src="{{asset('uploads/posts/'.$posts->feature_image)}}" class="img-thumbnail img-circle image_show" id="image_show">
                                            @else
                                                <img src="{{asset('assets/img/default_user.png')}}" class="img-thumbnail img-circle image_show" id="image_show">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="{{ $errors->has('photo')? 'has-error':'' }}">
                                            <input type="file" id="photo" name="photo" class="filestyle" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);"> 
                                            <div class="bootstrap-filestyle input-group">
                                                <span class="group-span-filestyle input-group-btn" tabindex="0">
                                                    <label for="photo" class="btn btn-primary ">
                                                        <span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span> 
                                                        <span class="buttonText choose_image"> Choose Image</span>
                                                    </label>
                                                </span>
                                            </div>
                                            <p class="image_error" style="color: red; font-size:10px;"></p>
                                            {!! $errors->has('photo')? '<p class="help-block">'.$errors->first('photo').'</p>':'' !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9">
                                        <!-- <button type="submit" class="btn btn-primary">@lang('app.create_new_post')</button> -->
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'post_content' );

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            console.log(reader);

            reader.onload = function (e) {
                $('.image_show').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    (function($) {
        $.fn.checkFileType = function(options) {
            var defaults = {
                allowedExtensions: [],
                success: function() {},
                error: function() {}
            };
            options = $.extend(defaults, options);
            return this.each(function() {
                $(this).on('change', function() {
                    var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);
                    if ($.inArray(extension, options.allowedExtensions) == -1) {
                        options.error();
                        $(this).focus();
                    } else {
                        options.success();
                        readURL(this);
                    }

                });
            });
        };

    })(jQuery);

    $(function() {
        $('#photo').checkFileType({
            allowedExtensions: ['jpg', 'jpeg','png'],
            success: function() {
                // alert('Success');
                $(".image_error").text('');

            },
            error: function() {
                // alert('Error');
                var loc = "{{asset('/assets/img/default_user.png')}}";
                $(".image_show").attr("src",loc);
                $(".image_error").text('Only ".jpg",".jpeg",".png" files are allowed.');
            }
        });
    });

</script>
@endsection