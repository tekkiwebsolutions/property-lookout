@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
 <link rel="stylesheet" href="{{asset('assets/css/admin.css')}}">

    <div class="container">
        <div class="row">
            @include('admin.sidebar_menu')
            <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="inr-pg-ttl">
                            <h1 class="page-header"> {{ $title }}  </h1>
                        </div>
                    @endif
                    @include('admin.flash_msg')
                    <div class="row">
                        <div class="col-xs-12">

                            {{ Form::open(['class' => 'form-horizontal', 'files'=>'true', 'id'=>'createPostForm' , 'url'=>'/dashboard/postsIndex?type=contact-us' , 'method'=>'post']) }}
                                <input type="hidden" name="type" value="contact-us">
                                <div class="form-group {{ $errors->has('address')? 'has-error':'' }}">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="address" value="@if(!empty($posts)){{$posts->address}}@endif" name="address" placeholder="@lang('app.address')" autocomplete="on" size="50" runat="server">

                                        {!! $errors->has('address')? '<p class="help-block">'.$errors->first('address').'</p>':'' !!}
                                         <input type="hidden" id="address2" name="address2" />
                                         <input type="hidden" id="lat" name="lat" />
                                         <input type="hidden" id="long" name="long" />
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('number')? 'has-error':'' }}">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="number" value="@if(!empty($posts)){{$posts->phone_number}}@endif" name="number" placeholder="Phone Number" >
                                        {!! $errors->has('number')? '<p class="help-block">'.$errors->first('number').'</p>':'' !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('email')? 'has-error':'' }}">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="email" value="@if(!empty($posts)){{$posts->email}}@endif" name="email" placeholder="@lang('app.email')" >
                                        {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9">
                                        <!-- <button type="submit" class="btn btn-primary">@lang('app.create_new_post')</button> -->
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            {{ Form::close() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{get_option('google_map_api_key')}}&libraries=places"></script>
<script type="text/javascript">
    function initialize() {
      var input = document.getElementById('address');
      var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('address2').value = place.name;
            document.getElementById('lat').value = place.geometry.location.lat();
            document.getElementById('long').value = place.geometry.location.lng();
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            console.log(reader);

            reader.onload = function (e) {
                $('.image_show').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    (function($) {
        $.fn.checkFileType = function(options) {
            var defaults = {
                allowedExtensions: [],
                success: function() {},
                error: function() {}
            };
            options = $.extend(defaults, options);
            return this.each(function() {
                $(this).on('change', function() {
                    var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);
                    if ($.inArray(extension, options.allowedExtensions) == -1) {
                        options.error();
                        $(this).focus();
                    } else {
                        options.success();
                        readURL(this);
                    }

                });

            });
        };

    })(jQuery);

    $(function() {
        $('#photo').checkFileType({
            allowedExtensions: ['jpg', 'jpeg','png'],
            success: function() {
                // alert('Success');
                $(".image_error").text('');

            },
            error: function() {
                // alert('Error');
                var loc = "{{asset('/assets/img/default_user.png')}}";
                $(".image_show").attr("src",loc);
                $(".image_error").text('Only ".jpg",".jpeg",".png" files are allowed.');

            }
        });

    });
</script>
@endsection