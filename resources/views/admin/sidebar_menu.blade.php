<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12">
    <div class="inner-right-sidebar">
        <ul class="sidebar-nav nav" id="side-menu">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-dashboard fa-fw"></i>
                    <span class="li-title"> @lang('app.dashboard')</span>
                </a>
            </li>
            <li class="hidden_div">
                <a href="javascript:void(0);">
                    <i class="fa fa-bullhorn"></i> 
                    <span class="li-title"> @lang('app.my_ads')</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('my_ads') }}">@lang('app.my_ads')</a>
                    </li>
                    <li>
                        <a href="{{ route('create_ad') }}">@lang('app.post_an_ad')</a>
                    </li>
                    <li>
                        <a href="{{ route('pending_ads') }}">@lang('app.pending_for_approval')</a>
                    </li>
                    <li>
                        <a href="{{ route('favorite_ads') }}">@lang('app.favourite_ads')</a>
                    </li>
                </ul>
            </li>
            @if($lUser->is_admin())
            <li class="hidden_div">
                <a href="{{ route('parent_categories') }}">
                    <i class="fa fa-list"></i> 
                    <span class="li-title"> @lang('app.categories')</span>
                </a>
            </li>
            <li class="hidden_div">
                <a href="{{ route('admin_brands') }}">
                    <i class="fa fa-adjust"></i> 
                    <span class="li-title"> @lang('app.brands')</span>
                </a>
            </li>
            <li class="">
                <a href="javascript:void(0);">
                    <i class="fa fa-bullhorn"></i> 
                    <span class="li-title"> Properties</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('approved_ads') }}">Approved Properties</a>
                    </li>
                    <li>
                        <a href="{{ route('admin_pending_ads') }}">@lang('app.pending_for_approval')</a>
                    </li>
                    <li>
                        <a href="{{ route('admin_blocked_ads') }}">Blocked Properties</a>
                    </li>
                </ul>
            </li>
            <li class="hidden_div">
                <a href="javascript:void(0);">
                    <i class="fa fa-rss-square"></i>
                    <span class="li-title"> @lang('app.blog')</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('posts') }}">@lang('app.posts')</a>
                    </li>
                    <li>
                        <a href="{{ route('create_new_post') }}">@lang('app.create_new_post')</a>
                    </li>
                </ul>
            </li>
            <li class="hidden_div">
                <a href="{{ route('pages') }}">
                    <i class="fa fa-file-word-o"></i>
                    <span class="li-title"> @lang('app.pages')</span>
                </a>
            </li>
            <li>
                <a href="{{ route('ad_reports') }}">
                    <i class="fa fa-exclamation"></i> 
                    <span class="li-title"> Property Reports</span>
                </a>
            </li>
            <li>
                <a href="{{ route('agent_reports') }}">
                    <i class="fa fa-exclamation"></i> 
                    <span class="li-title"> Agent Reports</span>
                </a>
            </li>
            <li>
                <a href="{{ route('users') }}">
                    <i class="fa fa-users"></i> 
                    <span class="li-title"> @lang('app.users')</span>
                </a>
            </li>
            <li class="hidden_div">
                <a href="javascript:void(0);">
                    <i class="fa fa-desktop fa-fw"></i>
                    <span class="li-title"> @lang('app.appearance')</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('theme_settings') }}">@lang('app.theme_settings')</a>
                    </li>
                    <li>
                        <a href="{{ route('modern_theme_settings') }}">@lang('app.modern_theme_settings')</a>
                    </li>
                    <li> 
                        <a href="{{ route('social_url_settings') }}">@lang('app.social_url')</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-map-marker"></i>
                    <span class="li-title"> @lang('app.locations')</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('country_list') }}">@lang('app.countries')</a>
                    </li>
                    <li>
                        <a href="{{ route('state_list') }}">@lang('app.states')</a>
                    </li>
                    <li>
                        <a href="{{ route('city_list') }}">@lang('app.cities')</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-reply-all"></i> 
                    <span class="li-title"> Developer Requests</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ url('/dashboard/developer-contact-messages') }}">Contact Messages</a>
                    </li>
                    <li>
                        <a href="{{ url('/dashboard/developer-apartment-requests') }}">Apartment Requests</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('contact_messages') }}">
                    <i class="fa fa-envelope-o"></i>
                    <span class="li-title"> @lang('app.contact_messages')</span>
                </a>
            </li>
            <li class="hidden_div">
                <a href="{{ route('monetization') }}">
                    <i class="fa fa-dollar"></i> 
                    <span class="li-title"> @lang('app.monetization')</span>
                </a>
            </li>
            <li class="">
                <a href="javascript:void(0);">
                    <i class="fa fa-wrench fa-fw"></i>
                    <span class="li-title"> @lang('app.settings')</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <!-- <li>
                        <a href="{{ route('general_settings') }}">@lang('app.general_settings')</a>
                    </li>
                    <li>
                        <a href="{{ route('ad_settings') }}">@lang('app.ad_settings_and_pricing')</a>
                    </li> -->
                    <li>
                        <a href="{{ route('payment_settings') }}">@lang('app.payment_settings')</a>
                    </li>
                    <!-- <li>
                        <a href="{{ route('language_settings') }}">@lang('app.language_settings')</a>
                    </li>
                    <li>
                        <a href="{{ route('file_storage_settings') }}">@lang('app.file_storage_settings')</a>
                    </li>
                    <li>
                        <a href="{{ route('social_settings') }}">@lang('app.social_settings')</a>
                    </li>
                    <li>
                        <a href="{{ route('blog_settings') }}">@lang('app.blog_settings')</a>
                    </li>
                    <li>
                        <a href="{{ route('other_settings') }}">@lang('app.other_settings')</a>
                    </li> -->
                </ul>
            </li>
            <li class="hidden_div">
                <a href="{{ route('administrators') }}">
                    <i class="fa fa-users"></i>
                    <span class="li-title"> @lang('app.administrators')</span>
                </a>
            </li>
            <li>
                <a href="{{url('/dashboard/postsIndex?type=about-us')}}">
                    <i class="fa fa-bullhorn"></i>
                    <span class="li-title"> About Us</span>
                </a>
            </li>
            <li>
                <a href="{{url('/dashboard/postsIndex?type=contact-us')}}">
                    <i class="fa fa-bullhorn"></i>
                    <span class="li-title"> Contact Us</span>
                </a>
            </li>
            @else
            <li class="hidden_div">
                <a href="javascript:void(0);">
                    <i class="fa fa-desktop fa-fw"></i>
                    <span class="li-title"> @lang('app.appearance')</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ url('dashboard/users/social-settings') }}">
                            <span class="li-title"> @lang('app.social_url')</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif
            @if(Auth::user()->user_type == 'user')
            <li class="hidden_div">
                <a href="{{ route('agent_chat_list') }}">
                    <i class="fa fa-money"></i>
                    <span class="li-title"> Chats</span>
                </a>
            </li>
            <li class="hidden_div">
                <a href="{{ route('packages') }}">
                    <i class="fa fa-money"></i>
                    <span class="li-title"> Package</span>
                </a>
            </li>
            @endif
            <li class="hidden_div">
                <a href="{{ route('packages') }}">
                    <i class="fa fa-money"></i> 
                    <span class="li-title"> Package</span>
                </a>
            </li>
            <li class="hidden_div">
                <a href="{{ route('payments') }}">
                    <i class="fa fa-money"></i> 
                    <span class="li-title"> @lang('app.payments')</span>
                </a>
            </li>
            <li>
                <a href="{{ route('profile') }}">
                    <i class="fa fa-user"></i> 
                    <span class="li-title"> @lang('app.profile')</span>
                </a>
            </li>
            <li>
                <a href="{{ route('change_password') }}">
                    <i class="fa fa-lock"></i>
                    <span class="li-title"> @lang('app.change_password')</span>
                </a>
            </li>
        </ul>
        
    </div>
</div>