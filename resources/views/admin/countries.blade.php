@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('page-css')
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('main')

    <div class="container">
        <div class="row">
            @include('admin.sidebar_menu')
            <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="inr-pg-ttl">
                            <h1 class="page-header"> {{ $title }}  </h1>
                        </div> <!-- /.col-lg-12 -->
                    @endif
                    @include('admin.flash_msg')
                    <table class="table table-bordered table-striped" id="jDataTable">
                        <thead>
                        <tr>
                            <th>@lang('app.country_name')</th>
                            <th>@lang('app.country_code')</th>
                        </tr>
                        </thead>
                    </table>
                </div>   <!-- /#page-wrapper -->
            </div>
        </div>   <!-- /#wrapper -->
    </div> <!-- /#container -->
@endsection

@section('page-js')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#jDataTable').DataTable({
                pagingType: "simple",
                processing: true,
                serverSide: true,
                ajax: '{{ route('get_countries_data') }}',
                "aaSorting": [],
                language: {
                   paginate: {
                       next: '<i class="fa fa-angle-double-right"></i>', 
                       previous: '<i class="fa fa-angle-double-left"></i>'
                   }
               }
            });
        });
    </script>
@endsection