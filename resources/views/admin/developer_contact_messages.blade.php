@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('page-css')
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/datatables/responsive.dataTables.min.css') }}" rel="stylesheet"/>
@stop
@section('main')
    <div class="container">
        <div class="row">
            @include('admin.sidebar_menu')
            <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="inr-pg-ttl">
                            <h1 class="page-header"> {{ $title }}  </h1>
                        </div>
                    @endif

                    @include('admin.flash_msg')
                    <table class="table table-bordered dtble-btn table-striped" id="jDataTable">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Person to address</th>
                                <th>Company Name</th>
                                <th>Contact Number</th>
                                <th>Other Contact</th>
                                <th>Cost</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        @if(!empty($contacts))
                            <tbody>
                                @foreach($contacts as $contact)
                                    <tr>
                                        <td>{{$contact['user']['email']}}</td>
                                        <td>{{$contact['person_to_address']}}</td>
                                        <td>{{$contact['company_name']}}</td>
                                        <td>{{$contact['contact_number']}}</td>
                                        <td>{{$contact['other_contact']}}</td>
                                        <td>{{$contact['cost']}}</td>
                                        <td>
                                            @if($contact['status'] == 0)
                                                <a href="{{ url('/dashboard/developer-contact-reply/'.getencrypted($contact['id'])) }}" class="btn btn-primary reply_button" >
                                                    <i class="fa fa-reply"></i> Send Access Key 
                                                </a>
                                            @else
                                                <a href="javascript:void(0);">Reply Sent</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach          
                            </tbody>
                        @endif
                    </table>
                </div>   <!-- /#page-wrapper -->
            </div>
        </div>   <!-- /#wrapper -->
    </div> <!-- /#container -->
@endsection

@section('page-js')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#jDataTable').DataTable({
                pagingType: "simple",
                responsive: true,
                language: {
                    paginate: {
                       next: '<i class="fa fa-angle-double-right"></i>', 
                       previous: '<i class="fa fa-angle-double-left"></i>'
                    }
                }
            });
        });
    </script>
@endsection