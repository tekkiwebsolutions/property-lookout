@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')

    <div class="container">
        <div class="row">
            @include('admin.sidebar_menu')
            <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="inr-pg-ttl">
                            <h1 class="page-header"> {{ $title }} </h1>
                        </div>
                    @endif

                    @include('admin.flash_msg')
                    @if($ads->total() > 0)
                        <table class="table table-bordered table-striped admin-tbl-cls">

                            @foreach($ads as $ad)
                                <tr>
                                    @if($ad->type == 'house')
                                        <?php $path = '/uploads/houseImages'; ?>
                                    @else
                                        <?php $path = '/uploads/apartmentImages'; ?>
                                    @endif
                                    <td width="100">
                                        @if(file_exists(public_path($path).'/'.$ad['cover_photo']))
                                            <img src="{{asset($path.'/'.$ad->cover_photo)}}" class="thumb-listing-table img-responsive" alt="Featured Image">
                                        @else
                                            <img src="{{asset('/assets/img/no_image.png')}}" class="thumb-listing-table img-responsive" alt="Featured Image">
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('/propertyShow/'.$ad->type.'/'.$ad->slug) }}" target="_blank">{{ $ad->title }}</a> 
                                            ({!! $ad->status_context() !!})
                                        <p class="text-muted">
                                            <i class="fa fa-map-marker"></i> 
                                            {!! $ad->full_address()  !!} <br />  
                                            <i class="fa fa-clock-o"></i>
                                            {{ $ad->posting_datetime()  }}
                                        </p>
                                    </td>

                                    <td>
                                        <a href="{{ route('reports_by_ads', $ad->slug) }}">
                                            <i class="fa fa-exclamation-triangle"></i> @lang('app.reports') : {{ $ad->reports->count() }}
                                        </a>
                                        <hr />
                                        <a href="{{ url('propertyShow/'.$ad->type.'/'.$ad->slug) }}" target="_blank" class="btn btn-primary" title="View">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        @if($ad->status ==1)
                                            <a href="javascript:;" class="btn btn-warning blockAds" data-slug="{{ $ad->slug }}" data-value="2" title="Block">
                                                <i class="fa fa-ban"></i>
                                            </a>
                                        @else
                                            <a href="javascript:;" class="btn btn-success approveAds" data-slug="{{ $ad->slug }}" data-value="1" title="Approve">
                                                <i class="fa fa-check-circle-o"></i>
                                            </a>
                                        @endif

                                        <a href="javascript:;" class="btn btn-danger deleteAds" data-slug="{{ $ad->slug }}" title="delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <div class="no-data-flex">
                            <i class="fa fa-frown-o fa-5x"></i>
                            <h4>There are no properties to show !</h4>
                        </div>
                    @endif
                    {!! $ads->links() !!}
                </div>   <!-- /#page-wrapper -->
            </div>
        </div>
    </div> <!-- /#container -->
@endsection

@section('page-js')

    <script>
        $(document).ready(function() {
            $('.deleteAds').on('click', function () {
                if (!confirm('{{ trans('app.are_you_sure') }}')) {
                    return '';
                }
                var selector = $(this);
                var slug = selector.data('slug');
                $.ajax({
                    url: '{{ route('delete_ads') }}',
                    type: "POST",
                    data: {slug: slug, _token: '{{ csrf_token() }}'},
                    success: function (data) {
                        if (data.success == 1) {
                            selector.closest('tr').hide('slow');
                            toastr.success(data.msg, '@lang('app.success')', toastr_options);
                        }
                    }
                });
            });

            $('.approveAds, .blockAds').on('click', function () {
                var selector = $(this);
                var slug = selector.data('slug');
                var value = selector.data('value');
                $.ajax({
                    url: '{{ route('ads_status_change') }}',
                    type: "POST",
                    data: {slug: slug, value: value, _token: '{{ csrf_token() }}'},
                    success: function (data) {
                        if (data.success == 1) {
                            selector.closest('tr').hide('slow');
                            toastr.success(data.msg, '@lang('app.success')', toastr_options);
                        }
                    }
                });
            });
        });

        @if(session('success'))
            toastr.success('{{ session('success') }}', '{{ trans('app.success') }}', toastr_options);
        @endif
        @if(session('error'))
            toastr.error('{{ session('error') }}', '{{ trans('app.success') }}', toastr_options);
        @endif
    </script>

@endsection