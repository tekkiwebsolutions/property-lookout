@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="container">
    <div class="row">
        @include('admin.sidebar_menu')
        <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
            <div id="page-wrapper">
                @if( ! empty($title))
                <div class="inr-pg-ttl">
                    <h1 class="page-header"> {{ $title }}  </h1>
                </div>
                @endif
                @include('admin.flash_msg')
                @if($reports->total() > 0)
                <div class="table-responsive">
                    <table class="table table-bordered table-striped admin-smple-tbl-cls">
                        <tr>
                            <th>@lang('app.reason')</th>
                            <th>@lang('app.email')</th>
                            <th>@lang('app.message')</th>
                            <th>Agent info</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        @foreach($reports as $report)
                        <tr>
                            <td>{{ $report->reason == 'wrong_category' ? 'Wrong Category' : ucfirst($report->reason) }}</td>
                            <td> {{ $report->email }}  </td>
                            <td>
                                {{ $report->message }}
                                <hr />
                                <p class="text-muted"> <i>@lang('app.date_time'): {{ $report->posting_datetime() }}</i></p>
                            </td>
                            <td>
                                @if($report->user)
                                <a href="{{ url('agent/'.$report->user->slug) }}" target="_blank">View Agent</a>
                                <i class="clearfix"></i>
                                <a href="{{ route('reports_by_agent', $report->user->slug) }}">
                                <i class="fa fa-exclamation-triangle"></i> @lang('app.reports') : {{ $report->user->reports->count() }}
                                </a>
                                @endif
                            </td>
                            <td>
                                <a href="javascript:;" class="btn btn-danger deleteReport" data-id="{{ $report->id }}"><i class="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                @else
                <div class="no-data-flex">
                    <i class="fa fa-frown-o fa-5x"></i>
                    <h4>No report to show !</h4>
                </div>
                @endif
                {!! $reports->links() !!}
            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
    <!-- /#wrapper -->
</div>
<!-- /#container -->
@endsection
@section('page-js')
<script>
    $(document).ready(function() {
        $('.deleteReport').on('click', function () {
            if (!confirm('{{ trans('app.are_you_sure') }}')) {
                return '';
            }
            var selector = $(this);
            var id = selector.data('id');
            $.ajax({
                url: '{{ route('delete_report') }}',
                type: "POST",
                data: {id: id, _token: '{{ csrf_token() }}'},
                success: function (data) {
                    if (data.success == 1) {
                        selector.closest('tr').hide('slow');
                        toastr.success(data.msg, '@lang('app.success')', toastr_options);
                    }
                }
            });
        });
    });
    
    @if(session('success'))
        toastr.success('{{ session('success') }}', '{{ trans('app.success') }}', toastr_options);
    @endif
    @if(session('error'))
        toastr.error('{{ session('error') }}', '{{ trans('app.success') }}', toastr_options);
    @endif
</script>
@endsection