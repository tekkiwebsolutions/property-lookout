@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('page-css')
    <li nk href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('main')

   <div class="container">

       <div id="wrapper">

           @include('admin.sidebar_menu')

           <div id="page-wrapper">
               @if( ! empty($title))
                   <div class="row">
                       <div class="col-lg-12">
                           <h1 class="page-header"> {{ $title }}  </h1>
                       </div> <!-- /.col-lg-12 -->
                   </div> <!-- /.row -->
               @endif

               @include('admin.flash_msg')

               <div class="row">
                   <div class="col-xs-12">
                       <div id="return_msg"></div>

                       <table class="table table-bordered table-striped" id="jDataTable">
                           <thead>
                               <tr>
                                   <th>@lang('app.name')</th>
                                   <th>@lang('app.via')</th>
                                   <th>@lang('app.email')</th>
                                   <th width="5%">Status</th>
                                   <th width="18%">@lang('app.created_at')</th>
                                   <th width="20%">@lang('app.actions')</th>
                               </tr>
                           </thead>
                           <tbody>
                               @if(!empty($users))
                                   @foreach($users as $user)
                                       <tr>
                                           <td><a href="{{url('/agent'.'/'.$user->slug)}}">{{$user->name}}</a></td>
                                           <td>{{$user->user_name}}</td>
                                           <td>{{$user->email}}</td>
                                           <?php 
                                               $status_val = '';
                                               if($user->plan_active_status == '0') {
                                                   $status_val =  'Payment Pending';
                                               } else {
                                                   if($user->active_status == '0') {
                                                       $status_val =  'Pending';
                                                   } elseif($user->active_status == '1') {
                                                       $status_val =  'Active';
                                                   } else {
                                                       $status_val =  'Blocked';
                                                   }
                                               }
                                           ?>
                                           <td>{{$status_val}}</td>
                                           <td>{{$user->signed_up_datetime()}}</td>
                                           <td>
                                               <a href="{{url('/agent'.'/'.$user->slug)}}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> view</a>
                                               @if($user->plan_active_status == '0')
                                                   <a href="jav * ascript:void(0);" title="Blocked" class="btn btn-danger btn-xs" style="margin-left:10px;"><i class="fa fa-ban"></i> Restricted</a>
                                               @else
                                                   @if($user->active_status == '0')
                                                       <a href="jav * ascript:void(0);" title="Pending" class="btn btn-warning btn-xs change_user_status" rel="{{$user->id}}" rel1="1" style="margin-left:10px;"><i class="fa fa-retweet"></i> Activate</a>
                                                   @elseif($user->active_status == '1')
                                                       <a href="jav * ascript:void(0);" title="Active" class="btn btn-success btn-xs change_user_status" rel="{{$user->id}}" rel1="2" style="margin-left:10px;"><i class="fa fa-retweet"></i> Active</a>
                                                   @else
                                                       <a href="jav * ascript:void(0);" title="Blocked" class="btn btn-danger btn-xs change_user_status" rel="{{$user->id}}" rel1="1" style="margin-left:10px;"><i class="fa fa-retweet"></i> Blocked</a>
                                                   @endif
                                               @endif
                                           </td>
                                       </tr>
                                   @endforeach
                               @endif
                           </tbody>
                       </table>
                   </div>
               </div>
           </div>   <!-- /#page-wrapper -->
       </div>   <!-- /#wrapper -->
   </div> <!-- /#container -->
@endsection

@section('page-js')
   <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
   <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
   <script>
       $(document).ready(function() {
           $('#jDataTable').DataTable({
               language: {
                   paginate: {
                       next: '<i class="fa fa-angle-double-right"></i>', 
                       previous: '<i class="fa fa-angle-double-left"></i>'
                   }
               }
               // processing: true,
               // serverSide: true,
               // ajax: '{{ route('get_users_data') }}',
               // "aaSorting": []
           });

           $(document).on('click','.agent-feature-btn', function(){
               var user_id = $(this).data('user-id');
               $that = $(this);
               $.ajax({
                   type : 'POST',
                   url : '{{ route('change_user_feature') }}',
                   data: { user_id : user_id, _token : '{{csrf_token()}}' },
                   success : function(data){
                       $that.html(data);
                   }
               });
           });

       });

       $(document).on('click','.change_user_status',function(){
           // document.getElementById('load').style.visibility="visible";
           var user_id = $(this).attr('rel');
           var status = $(this).attr('rel1');
           $.ajax({
               context : this,
               type : 'POST',
               url : '{{ route('change_user_status') }}',
               data: { user_id : user_id, status : status, _token : '{{csrf_token()}}' },
               success : function(data){
                   if(status == '1') {
                       $(this).attr('rel1','2');
                       $(this).html('<i class="fa fa-retweet"></i> Active</a>');
                       $(this).removeClass('btn-warning');
                       $(this).removeClass('btn-danger');
                       $(this).addClass('btn-success');
                       $(this).parent().prev().prev('td').html('Active');
                   } else {
                       $(this).attr('rel1','1');
                       $(this).html('<i class="fa fa-retweet"></i> Blocked</a>');
                       $(this).removeClass('btn-warning');
                       $(this).removeClass('btn-success');
                       $(this).addClass('btn-danger');
                       $(this).parent().prev().prev('td').html('Blocked');
                   }
                   // $('#return_msg').html(data);
                   // document.getElementById('load').style.visibility="hidden";
                   $("#return_msg").html(data);
                   setTimeout(function() { $("#return_msg").html(""); }, 3000);
               },
               error :function() {
                   // $('#return_msg').html('<p class="alert alert-success">Oops! Some error occured. Please Try again. </p>');
                   // document.getElementById('load').style.visibility="hidden"; 
                   $("#return_msg").html('<p class="alert alert-success">Oops! Some error occured. Please Try again. </p>');
                   setTimeout(function() { $("#return_msg").html(""); }, 3000);
               }
           });
       });
   </script>
@endsection