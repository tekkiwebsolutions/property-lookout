@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('page-css')
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/datatables/responsive.dataTables.min.css') }}" rel="stylesheet"/>
@stop

@section('main')
    <div class="container">
        <div class="row">
            @include('admin.sidebar_menu')
            <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="inr-pg-ttl">
                            <h1 class="page-header"> {{ $title }}  </h1>
                        </div>
                    @endif

                    @include('admin.flash_msg')    
                    <div class="user-page-tbl">
                        <div id="return_msg"></div>
                        <table class="table table-bordered table-striped" id="jDataTable">
                            <thead>
                                <tr>
                                    <th>@lang('app.name')</th>
                                    <th>@lang('app.via')</th>
                                    <th>@lang('app.email')</th>
                                    <th width="5%">Status</th>
                                    <th width="18%">@lang('app.created_at')</th>
                                    <th width="20%">@lang('app.actions')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>   <!-- /#page-wrapper -->
            </div>
        </div>   <!-- /#wrapper -->
    </div> <!-- /#container -->
@endsection
@section('page-js')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript">
        
        $(document).ready(function() {
            $('#jDataTable').DataTable({
                pagingType: "simple",
                processing: true,
                responsive: true,
                serverSide: true,
                ajax: '{{ route('get_users_data') }}',
                "aaSorting": [],
                language: {
                    paginate: {
                        next: '<i class="fa fa-angle-double-right"></i>', 
                        previous: '<i class="fa fa-angle-double-left"></i>'
                    }
                }
            });
        }); 

        $(document).ready(function() {
            $(document).on('click','.agent-feature-btn', function(){
                var user_id = $(this).data('user-id');
                $that = $(this);
                $.ajax({
                    type : 'POST',
                    url : '{{ route('change_user_feature') }}',
                    data: { user_id : user_id, _token : '{{csrf_token()}}' },
                    success : function(data){
                        $that.html(data);
                    }
                });
            });
        });

        $(document).on('click','.change_user_status',function(){
            // document.getElementById('load').style.visibility="visible";
            var user_id = $(this).attr('rel');
            var status = $(this).attr('rel1');
            $.ajax({
                context : this,
                type : 'POST',
                url : '{{ route('change_user_status') }}',
                data: { user_id : user_id, status : status, _token : '{{csrf_token()}}' },
                success : function(data){
                    if(status == '1') {
                        $(this).attr('rel1','2');
                        $(this).html('<i class="fa fa-retweet"></i> Active</a>');
                        $(this).removeClass('btn-warning');
                        $(this).removeClass('btn-danger');
                        $(this).addClass('btn-success');
                        $(this).parent().prev().prev('td').html('Active');
                    } else {
                        $(this).attr('rel1','1');
                        $(this).html('<i class="fa fa-retweet"></i> Blocked</a>');
                        $(this).removeClass('btn-warning');
                        $(this).removeClass('btn-success');
                        $(this).addClass('btn-danger');
                        $(this).parent().prev().prev('td').html('Blocked');
                    }
                    // $('#return_msg').html(data);
                    // document.getElementById('load').style.visibility="hidden";
                    $("#return_msg").html(data);
                    setTimeout(function() { $("#return_msg").html(""); }, 3000);
                },
                error :function() {
                    // $('#return_msg').html('<p class="alert alert-success">Oops! Some error occured. Please Try again. </p>');
                    // document.getElementById('load').style.visibility="hidden"; 
                    $("#return_msg").html('<p class="alert alert-success">Oops! Some error occured. Please Try again. </p>');
                    setTimeout(function() { $("#return_msg").html(""); }, 3000);
                }
            });
        });
    </script>
@endsection