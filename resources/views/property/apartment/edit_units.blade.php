@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<style>
    .floor_active{
        background-color: #333!important;
        color: white!important;
    }
</style>
<link rel="stylesheet" href="{{asset('assets/css/admin.css')}}">
<div class="cms-inner-content agent-bio-data">
    <div class="container">
        <h3 class="unit-title">Edit Units</h3>
        <ul class="create-units" id="change_floor_id" rel="{{@$apartment->id}}">
            <li>Floors</li>
            <!-- show all and selected floors -->
            @if(!empty(@$apartment) && !empty(@$apartment->floor))
                @for($i = 1; $i <= $apartment->floor;$i++)    
                    <li><a href="#" rel="{{$i}}" class="change_floor_setting @if($i == $floor_units['floor_id']) floor_active @endif change_floor_class" rel="{{$i}}">Floor {{$i}} settings</a></li>
                @endfor
            @endif
        </ul>
        <ul class="create-units" id="change_unit_id">
            <li>Units</li>
            <!-- show all and selected units -->
            @if(!empty(@$apartment) && !empty(@$apartment->units) )
                @for($i = 1; $i <= $apartment->units;$i++)
                    <li><a href="#" class="@if($i == $floor_units['unit_id']) floor_active @endif change_unit_class" rel="{{$i}}">{{$i}}</a></li>
                @endfor
            @endif
        </ul>
        <hr class="clearfix" />
        <form class="form-horizontal clearfix" id="createUnitForm" action="{{ route('units_edit') }}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="apartment_id" value="{{@$apartment->id}}">
            <input type="hidden" name="floor_id" value="{{@$floor_units['floor_id']}}">
            <input type="hidden" name="unit_id" value="{{@$floor_units['unit_id']}}">
            <input type="hidden" name="property_type" value="units" id="property_type">
            <input type="hidden" name="image_type" id="image_type">
            <input type="hidden" name="apartment_unit_id" id="apartment_unit_id" value="{{@$unit_details->id}}">
            <input type="hidden" name="image_count" id="image_count" value="{{@$unit_details->media_img_count}}">

            <div class="form-group {{ $errors->has('unit_status')? 'has-error':'' }}">
                <label class="control-label col-sm-3">Unit Status</label>
                <div class="col-sm-5">
                    <select id="property_type" name="unit_status" class="form-control select2" tabindex="6">
                        <option value="">Select status</option>
                        <option value="0" @if(old('unit_status') == 0) selected @endif @if(@$unit_details->property_status == 0) selected @endif>Available</option>
                        <option value="2" @if(old('unit_status') == 2) selected @endif @if(@$unit_details->property_status == 2) selected @endif>Reserved</option>
                        <option value="1" @if(old('unit_status') == 1) selected @endif @if(@$unit_details->property_status == 1) selected @endif>Sold</option>
                    </select>
                </div>
                {!! $errors->has('unit_status')? '<p class="help-block">'.$errors->first('unit_status').'</p>':'' !!}
            </div>
            <div class="form-group {{ $errors->has('name')? 'has-error':'' }}">
                <label class="control-label col-sm-3">Name</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" maxlength="255" name="name" value="{{ old('name') ? old('name') : @$unit_details->title }}" placeholder="Enter unit name" />
                </div>
                {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
            </div>
            <div class="form-group {{ $errors->has('description')? 'has-error':'' }}">
                <label class="control-label col-sm-3">Description</label>
                <div class="col-sm-5">
                    <textarea class="form-control" rows="4" name="description" placeholder="Enter description">{{ old('description') ? old('description') : @$unit_details->description }}</textarea>
                </div>
                {!! $errors->has('description')? '<p class="help-block">'.$errors->first('description').'</p>':'' !!}
            </div>
            <!-- <div class="form-group {{ $errors->has('images')? 'has-error':'' }}">
                <label class="control-label col-sm-3">Upload photos</label>
                <div class="col-sm-9 upload-image-scroll">
                    <div id="uploaded-ads-image-wrap">
                        @if(!empty($ads_images))
                        @foreach($ads_images as $img)
                            <div class="creating-ads-img-wrap">
                                <img src="{{ media_url($img, false) }}" class="img-responsive" />
                                <div class="img-action-wrap" id="{{ $img->id }}">
                                    <a href="javascript:void(0);" class="imgDeleteBtn"><i class="fa fa-trash-o"></i> </a>
                                </div>
                            </div>
                        @endforeach
                        @endif
                    </div>
                    <div class="file-upload-wrap">
                        <label>
                            <input type="file" name="images" id="images" style="display: none;" />
                            <i class="fa fa-cloud-upload"></i>
                            <p>@lang('app.upload_image')</p>
                            <div class="progress" style="display: none;"></div>
                        </label>
                    </div>
                    {!! $errors->has('images')? '
                    <p class="help-block">'.$errors->first('images').'</p>
                    ':'' !!}
                </div>
            </div> -->
            <div class="form-group {{ $errors->has('house_photos_check')? 'has-error':'' }}">
                <label class="control-label col-sm-3">Upload photos</label>
                <div class="col-sm-9">
                    <div class="existing_image">
                        @if(!empty(@$unit_details && !empty(@$unit_details->media_img)))
                            @foreach(@$unit_details->media_img as $key => $img)
                                @php $id = getEncrypted($img['id']) @endphp
                                <div class="creating-ads-img-wrap">
                                    <img src="{{ media_url_property_array($img, false) }}" class="img-responsive" />
                                    <div class="img-action-wrap" id="{{ $img['id'] }}">
                                        <a href="javascript:;" class="imgDeleteBtn"><i class="fa fa-trash-o"></i> </a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div id="uploaded-ads-image-wrap">
                        
                    </div>
                    <div class="file-upload-wrap">
                        <label>
                            <input type="hidden" name="house_photos_check" id="house_photos_check" value="{{!empty($unit_details->media_img) ? 'photo_uploaded' : ''}}" />
                            <input type="file" name="house_photos" id="house_photos" style="display: none;" />
                            <i class="fa fa-cloud-upload"></i>
                            <p>@lang('app.upload_image')</p>
                            <div class="progress" style="display: none;"></div>
                        </label>
                    </div>
                    <!-- {!! $errors->has('house_photos')? '<p class="help-block">'.$errors->first('house_photos').'</p>':'' !!} -->
                    {!! $errors->has('house_photos_check')? '<p class="help-block">Please select atleast one photo.</p>':'' !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('no_of_bathrooms')? 'has-error':'' }}">
                <label class="control-label col-sm-3">Number of bathrooms</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" maxlength="10" name="no_of_bathrooms" value="{{ old('no_of_bathrooms') ? old('no_of_bathrooms') : @$unit_details->attached_bath }}" placeholder="Enter no. of bathrooms" />
                </div>
                {!! $errors->has('no_of_bathrooms')? '<p class="help-block">'.$errors->first('no_of_bathrooms').'</p>':'' !!}
            </div>
            <div class="form-group {{ $errors->has('no_of_parking')? 'has-error':'' }}">
                <label class="control-label col-sm-3">Number of parking</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" maxlength="10" name="no_of_parking" value="{{ old('no_of_parking') ? old('no_of_parking') : @$unit_details->no_of_parking }}" placeholder="Enter no. of parking" />
                </div>
                {!! $errors->has('no_of_parking')? '<p class="help-block">'.$errors->first('no_of_parking').'</p>':'' !!}
            </div>
            <div class="form-group {{ $errors->has('no_of_bedrooms')? 'has-error':'' }}">
                <label class="control-label col-sm-3">Number of bedrooms</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" maxlength="10" name="no_of_bedrooms" value="{{ old('no_of_bedrooms') ? old('no_of_bedrooms') : @$unit_details->beds }}" placeholder="Enter no. of bedrooms" />
                </div>
                {!! $errors->has('no_of_bedrooms')? '<p class="help-block">'.$errors->first('no_of_bedrooms').'</p>':'' !!}
            </div>
            <div class="form-group {{ $errors->has('size')? 'has-error':'' }}">
                <label class="control-label col-sm-3">Size(Sqft.)</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" maxlength="20" name="size" value="{{ old('size') ? old('size') : @$unit_details->square_unit_space }}" placeholder="Enter size" />
                </div>
                {!! $errors->has('size')? '<p class="help-block">'.$errors->first('size').'</p>':'' !!}
            </div>
            <div class="form-group {{ $errors->has('price')? 'has-error':'' }}">
                <label class="control-label col-sm-3">Price</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" maxlength="10" name="price" value="{{ old('price') ? old('price') : @$unit_details->price }}" placeholder="Enter price" />
                </div>
                {!! $errors->has('price')? '<p class="help-block">'.$errors->first('price').'</p>':'' !!}
            </div>
            <!-- <div class="form-group {{ $errors->has('video_link')? 'has-error':'' }}">
                <label class="control-label col-sm-3">Video Link(optional)</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" maxlength="255" name="video_link" value="{{ old('video_link') ? old('video_link') : @$unit_details->video_url }}"/>
                </div>
                {!! $errors->has('video_link')? '<p class="help-block">'.$errors->first('video_link').'</p>':'' !!}
            </div> -->
            <div class="btns f-right">
                @if($floor_units['floor_id'] == 1 && $floor_units['unit_id'] == 1)
                    <a href="{{route ('edit_floors',[\Crypt::encrypt(@$apartment->id),'edit']) }}" class="btn btn-primary black-btn m-t-20">Previous</a>
                @else 
                    <a href="{{route ('edit_previous_units',[\Crypt::encrypt(@$apartment->id),\Crypt::encrypt(@$floor_units['floor_id']),\Crypt::encrypt(@$floor_units['unit_id']),'edit']) }}" class="btn btn-primary black-btn m-t-20">previous</a>
                @endif
                <button type="submit" class="btn btn-primary black-btn m-t-20">Next</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript">
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif

    // $("#images").change(function() {
    //     var fd = new FormData(document.querySelector("form#adsPostForm"));
    //     //$('#loadingOverlay').show();
    //     $('.progress').show();
    //     $.ajax({
    //         url : '{{ route('upload_ads_image') }}',
    //         type: "POST",
    //         data: fd,
    
    //         xhr: function() {
    //             var xhr = new window.XMLHttpRequest();
    //             xhr.upload.addEventListener("progress", function(evt) {
    //                 if (evt.lengthComputable) {
    //                     var percentComplete = evt.loaded / evt.total;
    //                     percentComplete = parseInt(percentComplete * 100);
    //                     //console.log(percentComplete);
    
    //                     var progress_bar = '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: '+percentComplete+'%">'+percentComplete+'% </div>';
    
    //                     if (percentComplete === 100) {
    //                         $('.progress').hide();
    //                     }
    //                 }
    //             }, false);
    
    //             return xhr;
    //         },
    
    //         cache: false,
    //         processData: false,  // tell jQuery not to process the data
    //         contentType: false,   // tell jQuery not to set contentType
    //         success : function (data) {
    //             //$('#loadingOverlay').hide('slow');
    //             if (data.success == 1){
    //                 $('#uploaded-ads-image-wrap').load('{{ route('append_media_image') }}');
    //             } else{
    //                 toastr.error(data.msg, '<?php echo trans('app.error') ?>', toastr_options);
    //             }
    //         }
    //     });
    // });
    
    
    // $('body').on('click', '.imgDeleteBtn', function(){
    //     //Get confirm from user
    //     if ( ! confirm('{{ trans('app.are_you_sure') }}')){
    //         return '';
    //     }
    
    //     var current_selector = $(this);
    //     var img_id = $(this).closest('.img-action-wrap').attr('id');
    //     $.ajax({
    //         url : '{{ route('delete_media') }}',
    //         type: "POST",
    //         data: { media_id : img_id, _token : '{{ csrf_token() }}' },
    //         success : function (data) {
    //             if (data.success == 1){
    //                 current_selector.closest('.creating-ads-img-wrap').hide('slow');
    //                 toastr.success(data.msg, '@lang('app.success')', toastr_options);
    //             }
    //         }
    //     });
    // });
    
    // $('body').on('click', '.imgFeatureBtn', function(){
    //     var img_id = $(this).closest('.img-action-wrap').attr('id');
    //     var current_selector = $(this);
    
    //     $.ajax({
    //         url : '{{ route('feature_media_creating_ads') }}',
    //         type: "POST",
    //         data: { media_id : img_id, _token : '{{ csrf_token() }}' },
    //         success : function (data) {
    //             if (data.success == 1){
    //                 $('.imgFeatureBtn').html('<i class="fa fa-star-o"></i>');
    //                 current_selector.html('<i class="fa fa-star"></i>');
    //                 toastr.success(data.msg, '@lang('app.success')', toastr_options);
    //             }
    //         }
    //     });
    // });

    $("#house_photos").change(function() {
        var media_count = $('.creating-ads-img-wrap').length;
        if(media_count < 30) {

            // document.getElementById('load').style.visibility="visible";
            $("#image_type").val('unit_images');
            var fd = new FormData(document.querySelector("form#createUnitForm"));
            $('.progress').show();
            $.ajax({
                url : '{{ route('upload_property_image') }}',
                type: "POST",
                data: fd,
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            //console.log(percentComplete);
                            var progress_bar = '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: '+percentComplete+'%">'+percentComplete+'% </div>';

                            if (percentComplete === 100) {
                                $('.progress').hide();
                            }
                        }
                    }, false);

                    return xhr;
                },
                cache: false,
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success : function (data) {
                    //$('#loadingOverlay').hide('slow');
                    if (data.success == 1){
                        
                        $('#uploaded-ads-image-wrap').load("{{ url('') }}"+'/'+'agents/apend-property-image'+'/'+data.property_type+'/'+data.image_type);
                        $("#house_photos_check").val("photo_uploaded");
                    } else{
                        toastr.error(data.msg, '<?php echo trans('app.error') ?>', toastr_options);
                    }
                    // document.getElementById('load').style.visibility="hidden";
                }
            });
        } else {
            swal("OOPS!","You cannot upload more than 30 images","error");
        }
    });

    $('body').on('click', '.imgDeleteBtn', function(){
        //Get confirmation from user
        if ( ! confirm('{{ trans('app.are_you_sure') }}')){
            return '';
        }
        // document.getElementById('load').style.visibility="visible";

        var current_selector = $(this);
        var img_id = $(this).closest('.img-action-wrap').attr('id');
        $.ajax({
            url : '{{ route('delete_property_media') }}',
            type: "POST",
            data: { media_id : img_id, _token : '{{ csrf_token() }}' },
            success : function (data) {
                if (data.success == 1){
                    // current_selector.closest('.creating-ads-img-wrap').hide('slow');
                    current_selector.closest('.creating-ads-img-wrap').remove();
                    var del_media_count = $('.creating-ads-img-wrap').length;
                    if(del_media_count < 1) {
                        $("#house_photos_check").val("");
                    }
                    toastr.success(data.msg, '@lang('app.success')', toastr_options);
                }
                // document.getElementById('load').style.visibility="hidden";
            }
        });
    });   

    $(document).on('click','.change_floor_setting',function() {
        var floor_id = $(this).attr('rel');
    });

    // change floor and its units section
    // $(document).on('click','.change_floor_class',function() {
    //     var floor_id = $(this).attr('rel');
    //     var apartment_id = $(this).parent().parent().attr('rel');
        
    //     $('#change_unit_id').load("{{ url('') }}"+'/'+'agents/get_units_by_floor'+'/'+floor_id);
    // });
    // change floor and its units section
</script>
@endsection