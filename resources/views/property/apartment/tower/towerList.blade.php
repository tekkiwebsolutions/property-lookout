@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<link rel="stylesheet" href="{{asset('assets/css/thumb.owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/thumb.owl.theme.default.min.css')}}">
<?php use App\ApartmentUnit; ?>
<style type="text/css">
    .modal-header{
    background-color:#306492;
    color:white;
    font-size:16px;
    font-weight:bolder !important;
    }
    .modal-header .close{
    color:white;
    font-size:16px;
    font-weight:bolder !important;
    }
</style>
<div class="cms-inner-content">
    <div class="container">
        <div class="row">
            @if(Auth::user()->user_type == 'user')
            @include('agent.agentInfoSection')
            @elseif(Auth::user()->user_type == 'developer')
            @include('developer.developerInfoSection')
            @elseif(Auth::user()->user_type == 'staff')
            @include('developer.staffInfoSection')
            @endif
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 cms-border">
                <?php 
                    $pack_permissions = packPermissions(Auth::user()->id);
                    ?>
                <div align="right" style="margin-bottom: 3%;">
                    @if(Auth::user()->user_type == 'user')
                    <a href="javascript:void(0);" style="font-size: 18px;font-weight: bold;">Create:</a>
                    <a href="{{route('create_house')}}" class="btn btn-primary small-btn">Properties</a>
                    @endif
                    @if(Auth::user()->user_type == 'developer')                        
                    @endif
                </div>
                
                <!-- Div for developer only -->
                @if(Auth::user()->user_type == 'developer')
        
                    @if(!empty(@$apartment_tower))
                    <div class="table-responsive">
                        <table class="table cms-table cstm-tbl-cls">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>No of Floor</th>
                                    <th>No of unit</th>
                                    <th>Area</th>
                                    <th>Ad Id</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(@$apartment_tower as $key => $value)
                                <?php $id = Crypt::encrypt($value['id']); 
                                      $ad_id = Crypt::encrypt($value['ad_id']);
                                ?>
                                <tr class="three-btns-actn">
                                    <td>
                                       <span>
                                           {{$value['title']}}
                                       </span>
                                    </td>
                                    <td> 
                                       <span>
                                           {{$value['no_of_floor']}}
                                       </span>
                                    </td>
                                    <td id="td_{{$value['id']}}">
                                       <span>
                                           {{$value['no_of_unit']}}
                                       </span>
                                    </td>
                                    <td>
                                        <span>
                                            {{$value['area']}}
                                        </span>
                                    </td>
                                    <td>
                                        <span>
                                            {{$value['ad_id']}}
                                        </span>
                                    </td>
                                    <td>
                                        <a href="{{ url('/developers/apartment/create-tower/') . '/' . $ad_id . '/' . $id }}" class="btn small-btn btn-success" title="Edit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        {{-- <a href="javascript:;" class="btn btn-primary small-btn" title="View Page">
                                            <i class="fa fa-eye"></i>
                                        </a> --}}
                                        <a href="javascript:;" class="btn small-btn btn-danger propertyDeleteBtn" title="Delete" id="{{$id}}" rel="{{$ad_id}}">
                                            <i class="fa fa-trash"></i>
                                        </a>                             
                                        <!-- <div class="dropdown">
                                            <button class="btn action-btn dropdown-toggle" type="button" data-toggle="dropdown">Action<span class="caret"></span></button>
                                            <ul class="dropdown-menu">

                                                <li>
                                                    <a href="{{ url('/developers/apartment/create-tower/') . '/' . $ad_id . '/' . $id }}" class="btn small-btn btn-success" title="Edit">
                                                    Edit
                                                    </a>
                                                </li>
                                                {{--
                                                <li>
                                                    <a href="javascript:;" class="btn btn-primary small-btn" title="View Page">
                                                    <i class="fa fa-eye"></i>
                                                    </a>
                                                </li>
                                                --}}
                                                <li>
                                                    <a href="javascript:;" class="btn small-btn btn-danger propertyDeleteBtn" title="Delete" id="{{$id}}" rel="{{$ad_id}}">
                                                    Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div> -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif

                    @if($towerCount < $no_of_tower)
                        <div class="table-responsive">
                            <table class="table cms-table cstm-tbl-cls">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href="{{route('developer_create_tower',[$ad_id])}}" class="first_apartment_class">
                                            <i class="fa fa-plus fa-4x"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>


@endsection
@section('page-js')
<script src="{{ asset('assets/js/thumb.owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#sold-unit').owlCarousel({
            loop: true,
               autoplay: false,
               dots:false,
               responsiveClass: true,
               nav: true,
               navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>'],
               responsive: {
                   0: {
                       items: 1,
                       nav: true
                   },
                   600: {
                       items: 1,
                       nav: true
                   },
                   1000: {
                       items: 1,
                       nav: true,
                   }
               }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#available-unit').owlCarousel({
            loop: true,
               autoplay: false,
               dots:false,
               responsiveClass: true,
               nav: true,
               navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>'],
               responsive: {
                   0: {
                       items: 1,
                       nav: true
                   },
                   600: {
                       items: 1,
                       nav: true
                   },
                   1000: {
                       items: 1,
                       nav: true,
                   }
               }
        });
    });
</script>

<script>
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Success!', options)
    @endif
    
    $('body').confirmation({
      placement: 'bottom',
      selector: '[data-toggle="confirmation"]'
    });
    $(document).ready(function(){
        // delete apartment button
        $('.propertyDeleteBtn').confirmation({
            onConfirm: function() {
                alert('confirm');
                //document.getElementById('load').style.visibility="visible";
                var current_selector = $(this);
                var tower_id = $(this).attr('id');
                var ad_id = $(this).attr('rel');
                $.ajax({
                    url : '{{ route('delete_tower') }}',
                    type: "POST",
                    data: { ad_id : ad_id,tower_id : tower_id, _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        if (data.success == 1){
                            current_selector.closest('tr').hide('slow');
                            toastr.success(data.msg, '@lang('app.success')', toastr_options);
                        }
                        document.getElementById('load').style.visibility="hidden";
                    },
                    error : function() {
                        document.getElementById('load').style.visibility="hidden";
                    }
                });
            },
            onCancel: function() { 
                // alert('not confirm');
            }
        });
    });
    
    // action to make a property (house for agent only) sold or available
    // $('.propertySoldbtn').confirmation({
    //     onConfirm: function() {
    //         // alert('confirm');
    //         var property_id = $(this).attr('id');
    //         var current_selector = $(this);
    //         $.ajax({
    //             url : '{{ route('sold_house') }}',
    //             type: "POST",
    //             data: { property_id : property_id, _token : '{{ csrf_token() }}' },
    //             success : function (data) {
    //                 if (data.success == 1){
    //                     $('#td_'+data.count).html('<p style="color:red;">Sold</p>');
    //                     toastr.success(data.msg, '@lang('app.success')', toastr_options);
    //                 }
    //             }
    //         });
    //     },
    //     onCancel: function() { 
    //       // alert('not confirm');
    
    //     }
    // });
    
    // // to make a unit for an apartment sold or available (for staff and invited agents only)
    // $('.unitSoldbtn').confirmation({
    //     onConfirm: function() {
    //         var unit_id = $(this).attr("rel");
    //         var type = $(this).attr('rel1');
    //         $.ajax({
    //             context : this,
    //             url : "{{ route('sold_unit_status_change') }}",
    //             type: "POST",
    //             data: {unit_id : unit_id, type: type, _token : '{{ csrf_token() }}'},
    //             success: function(data) {
    //                 if(data.success == 1) {
    //                     if(type == "sold") {
    //                         $(this).removeClass('green');
    //                         $(this).addClass('red');
    //                         $(this).attr('rel1','available'); // the new action to be done (i.e. make it available/green ?)
    //                     }else {
    //                         $(this).removeClass('red');
    //                         $(this).addClass('green');
    //                         $(this).attr('rel1','sold'); // the new action to be done (i.e. make it sold/red ?)
    //                     }
    //                 }
    //             },
    //             error:function() {
    //                 // alert("error");
    //                 // show error message here if unit is not available
    //             }
    //         });
    //     },
    //     onCancel: function() {
    //         // alert("cancelled");
    //         // if the developer clicks on no (for staff and invited agents only)
    //     }
    // });
    // 
    
    // $(document).ready(function() {
    //     //alert("test");
    //     $('.append_apartments_units_div').each(function() {
    //         //alert($(this).attr('rel'));
    //         var ad_id = $(this).attr('rel');
    //         var type = $(this).attr('rel1');
    //         $(this).load("{{ url('') }}"+'/'+'agents/apend-apartment-units'+"/"+ad_id+"/"+type);
    //     });
    // });
    
    $('.open-sold-box').slideUp();
    $('.sold-open').click(function(){
        //alert("here");
        $(this).closest('.three-btns-actn').next().toggle('.open-sold-box');
        $('.open-avail-box').slideUp();
    });

    $('.open-avail-box').slideUp();
    $('.available-open').click(function(){
        //alert("here");
        $(this).closest('.prprty-lst-tbl').find('.open-avail-box').toggle();
        $('.open-sold-box').slideUp(); 
    });

    $('.available-open').click(function(){
        //alert("here");
        $('.open-available-box').slideUp();
        $(this).closest('.three-btns-actn').next('.open-sold-box').toggle(); 
    });
    
    $(document).on('click','.apartment_already_requested',function() {
        
        swal("OOPS!","Apartment is already requested.","error");
        return false;
    
    });
    
    $(document).on('click','.apartment_new_request',function() {
        $('.email_get').val($('.developer_email').val());
        $('#createNewProject').modal('show');
    
    });
    $(document).on('click','.access_key_btn',function() {
    
        var data = $("#newApartmentForm").serialize();
        $.ajax({
            data: data,
            type: "post",
            url: "{{ url('/developers/insert-request-access-key') }}",
            success: function(data){
                if(data.status == 1){
                    // window.location.href = "{{url('')}}" + "/user/create"+'?id='+data.token+'&email='+data.email;
                    window.location.href = "{{url('/developers/create-apartment')}}"+'/'+data.contact_id; // create apartment page
    
                } else if(data.status == 2) {
                    $('#warning-message').addClass('has-error');
                    $('#warning-message').text(data.error);
                }else{
                    //console.log(data);
                    $('#warning-message').removeClass('has-error');
                    $('#warning-message').text("");
                     
                    $.each(data.error,function(key,value) {
    
                        $('#warning-message').addClass('has-error');
                        $('#warning-message').text(value);  
                    })
                }
            },
            errors:function(){
                alert('Something went wrong. Please try again.');
            }
        });
    });
</script>
@endsection