@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="cms-inner-content">
    <div class="container">
        <div class="row">
            @include('agent.agentInfoSection')
            <div class="col-md-9 col-sm-7 cms-border">
                <div align="right" style="margin-bottom: 3%;">
                    <a href="javascript:void(0);" style="font-size: 18px;font-weight: bold;">Create:</a>
                    <a href="{{route('create_house')}}" class="btn btn-primary small-btn">Properties</a>
                    <a href="{{route('create_apartment')}}" class="btn btn-primary small-btn">Appartments</a>    
                </div>
                <div class="table-responsive">
                    <table class="table cms-table cstm-tbl-cls">
                        <thead>
                            <tr>
                                <th>Properties</th>
                                <th>Space</th>
                                <th>Status</th>
                                <th>Details</th>
                                <th></th>
                            </tr>
                        </thead>
                        @if(!empty($properties))
                        <tbody>
                            @foreach($properties as $key => $value)
                            <?php $id = Crypt::encrypt($value['id']); ?>
                            <tr class="three-btns-actn">
                                <td class="columnOne">
                                    <div class="property-img">
                                        <img src="{{asset('uploads/houseImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                    </div>
                                    <div class="float-left">
                                        <span>{{$value['title']}}</span>
                                        <span>{{ucfirst($value['type'])}}</span>
                                        <h6>$ {{$value['price']}}</h6>
                                    </div>
                                </td>
                                <td class="columnTwo"> 
                                    <span>{{$value['beds']}} Bedrooms</span>
                                    <span>{{$value['attached_bath']}} Bathrooms</span>
                                    <span>{{$value['square_unit_space']}} sqft </span>
                                </td>
                                <td id="td_{{$value['id']}}">
                                    @if($value['property_status'] == 0)
                                    <p style="color:green;">{{'Available'}}</p>
                                    @else
                                    <p style="color:red;">{{'Sold'}}</p>
                                    @endif
                                </td>
                                <td>Not released for public confirmation needed</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn action-btn dropdown-toggle" type="button" data-toggle="dropdown">Action<span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            @if($value['property_status'] != 1 && $value['type'] == 'house')
                                            <li>
                                                <a href="javascript:void(0);" class="btn btn-primary small-btn propertySoldbtn sold-open" data-toggle="confirmation" id="{{$id}}">Sold</a>
                                            </li>
                                            @endif
                                            @if($value['type'] == 'apartments')
                                            <li>
                                                <a href="javascript:void(0);" class="btn btn-primary small-btn apartmentSoldbtn sold-open" data-toggle="confirmation" id="{{$id}}">Sold</a>
                                            </li>
                                            @endif
                                            <li style="@if($value['property_status'] == 1 && $value['type'] == 'house')float:initial; @endif">
                                                <a href="javascript:void(0);" class="btn small-btn btn-danger propertyDeleteBtn" data-toggle="confirmation" id="{{$id}}" >Delete</a>
                                            </li>
                                            @if($value['property_status'] != 1 && $value['type'] == 'house')
                                            <li>
                                                <a href="{{route ('edit_house',$id)}}" class="btn small-btn btn-success">Edit</a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <tr class="open-sold-box">
                                <td colspan="5">
                                    <div class="custom-clr-boxes">
                                        <div class="property-color">
                                            <ul>
                                                <li class="red">&nbsp;</li>
                                                <li class="green">&nbsp;</li>
                                                <li class="green">&nbsp;</li>
                                                <li class="green">&nbsp;</li>
                                                <li class="green">&nbsp;</li>
                                                <li class="green">&nbsp;</li>
                                                <li class="green">&nbsp;</li>
                                                <li class="green">&nbsp;</li>
                                                <li class="red">&nbsp;</li>
                                                <li class="green">&nbsp;</li>
                                                <li class="green">&nbsp;</li>
                                                <li class="green">&nbsp;</li>
                                            </ul>
                                            <ul>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                            </ul>
                                            <ul>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                            </ul>
                                            <ul>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                            </ul>
                                            <ul>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                            </ul>
                                            <ul>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                            </ul>
                                            <ul>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                            </ul>
                                            <ul>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('page-js')
<script>
    $('body').confirmation({
      selector: '[data-toggle="confirmation"]'
    });
</script>
<script> 
    $('.propertyDeleteBtn').confirmation({
          onConfirm: function() {
            // alert('confirm');
            var current_selector = $(this);
            var property_id = $(this).attr('id');
            $.ajax({
                url : '{{ route('delete_house') }}',
                type: "POST",
                data: { property_id : property_id, _token : '{{ csrf_token() }}' },
                success : function (data) {
                    if (data.success == 1){
                        current_selector.closest('tr').hide('slow');
                        toastr.success(data.msg, '@lang('app.success')', toastr_options);
                    }
                }
            });
          },
          onCancel: function() { 
            // alert('not confirm');
          }
    });
    
    $('.propertySoldbtn').confirmation({
        onConfirm: function() {
          // alert('confirm');
          var property_id = $(this).attr('id');
          var current_selector = $(this);
          $.ajax({
              url : '{{ route('sold_house') }}',
              type: "POST",
              data: { property_id : property_id, _token : '{{ csrf_token() }}' },
              success : function (data) {
                  if (data.success == 1){
                      $('#td_'+data.count).html('<p style="color:red;">Sold</p>');
                      toastr.success(data.msg, '@lang('app.success')', toastr_options);
                  }
              }
          });
        },
        onCancel: function() { 
          // alert('not confirm');
    
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        alert("test");
        $('.append_apartments_units_div').each(function() {
            alert($(this).attr('rel'));
        });
    });
</script>
<script type="text/javascript">
    $('.open-sold-box').slideUp();
    $('.sold-open').click(function(){
      $('.open-sold-box').slideUp();
      $(this).closest('.three-btns-actn').next('.open-sold-box').slideDown(); 
    });
</script>
@endsection