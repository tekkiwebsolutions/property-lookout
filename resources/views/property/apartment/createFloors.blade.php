@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<link rel="stylesheet" href="{{asset('assets/css/admin.css')}}">
<div class="cms-inner-content agent-bio-data">
    <div class="container">
        <h3 class="unit-title">Create Floors</h3>

        <!-- <ul class="create-units">
            <li>Units</li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">Floor 1 settings</a></li>
        </ul> -->
        <hr class="clearfix" />
        <?php //prx($apartment); ?>
        <form class="form-horizontal clearfix" id="addFloorUnitsForm" action="{{ route('add_floor') }}" method="post">
            {{csrf_field()}}
            <input type="hidden" name="apartment_id" value="{{$apartment_id}}">
            <div class="form-group">
                <label class="control-label col-sm-3">Enter no. of Floors</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="no_of_floors" name="no_of_floors" max="10" min="1" value="{{@$apartment ? @$apartment->floor : '1'}}" onKeyPress="if(this.value.length==2) return false;"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Enter no. of Units per Floor</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="no_of_units" name="no_of_units" max="12" min="1" value="{{@$apartment ? @$apartment->units : '1'}}" onKeyPress="if(this.value.length==2) return false;"/>
                </div>
            </div>

            <!-- <div class="btns">
                <a href="javascript:void;" class="btn btn-primary black-btn m-t-20 create_units_class">Create Units</a>
            </div>
            <div id="units_add_div">
                @if(!empty(@$apartment) && !empty(@$apartment->apartmentFloorsAll))
                    @foreach(@$apartment->apartmentFloorsAll as $key => $floor)
                        <div class="form-group">
                            <label class="control-label col-sm-3">Enter no. of Units for Floor {{$floor['floor_no']}}</label>
                            <div class="col-sm-5">
                                <input type="number" class="form-control unit_add_class" name="units[{{$floor['floor_no']}}]" max="12" min="1" value="{{$floor['no_of_units']}}"/>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div> -->
            <div class="btns f-right">
                <a href="{{route ('edit_apartment',\Crypt::encrypt($apartment_id))}}" class="btn btn-primary black-btn m-t-20">Previous</a>
                <button type="button" class="btn btn-primary black-btn m-t-20" id="submit_button">Next</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript">
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif

    // $(document).on('click','.create_units_class',function() {
    //     var no_of_floors = $("#no_of_floors").val();
    //     if(no_of_floors == '') {
    //         alert("Please add atleast one floor.")
    //     } else {
    //         $('#units_add_div').load("{{ url('') }}"+'/'+'agents/apend-floor-units'+'/'+no_of_floors);
    //         $('#submit_button').prop( "disabled", false );
    //     }
    // });

    // validate no of units and floors
    $(document).ready(function() {
        //$('#submit_button').prop( "disabled", true );
    });

    $(document).on('click','#submit_button',function() {
        
        if($('#no_of_floors').val() == '') {

            swal("OOPS!","Please enter No of Floors","error");
            //alert("Please enter No of Floors");
            return false;
        }

        // var unit_error;
        // $('.unit_add_class').each(function() {

        //     if($(this).val() == '') {

        //         alert("Please enter No Of Units.");
        //         unit_error = true;
        //     }
        // });

        // if(unit_error) {

        //     return false;
        // }
        if($('#no_of_units').val() == '') {

            swal("OOPS!","Please enter No of Units","error");
            //alert("Please enter No of Units");
            return false;
        }

        $('#addFloorUnitsForm').submit();
    });

    // enable button if edit floor page is open and user has filled units before
    $(document).ready(function() {
        if('{{@$apartment->apartmentFloorsAll}}' != '') {
            $('#submit_button').prop( "disabled", false );
        }
    });
</script>
@endsection