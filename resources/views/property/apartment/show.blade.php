

@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif
@parent @endsection
@section('page-css')
<!-- <link rel="stylesheet" href="{{ asset('assets/plugins/owl.carousel/assets/owl.carousel.css') }}"> -->
<link rel="stylesheet" href="{{asset('assets/paginate/src/jquery.paginate.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/thumb.owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/thumb.owl.theme.default.min.css')}}">
<style>
    .modal_text_style{
    font-family: 'Proxima Nova';
    font-size: 15px;
    line-height: 22px;
    }
    /*review start*/
    .rotate{
    -moz-transition: all 1s linear;
    -webkit-transition: all 1s linear;
    transition: all 1s linear;
    }
    .rotate.down{
    -moz-transform:rotate(180deg);
    -webkit-transform:rotate(180deg);
    transform:rotate(180deg);
    }
    .rotate{
    margin-left: 10px;
    }
    .review-section form #message {
    min-height: 60px !important;
    }
    .review-section .author-area {
    margin-top: 10px;
    margin-left: 9px;
    }
    .paginate-pagination{
    float: right;
    }
    /*review ends*/
    .des-img img{
    height:200px;
    }
    #unit-slider .owl-nav {
        display: block;
    }
</style>
@endsection
@section('main')
<div class="navigate-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="address">
                    @if(@$detail['title'])
                    <div class="title">
                        <h4>{{ucfirst($detail['title'])}}</h4>
                    </div>
                    @endif
                    @if(@$detail['address'])
                    <address>
                        <div class="left">
                            <a href="javascript:void(0);"><i class="fa fa-map-marker"></i></a>
                        </div>
                        <div class="right">
                            <p>{{$detail['address']}}<br/><small>{{$detail['country']['country_name']}}</small></p>
                        </div>
                    </address>
                    @endif
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                <div class="navigate-icons property_sold-icon">
                    <ul>
                        <?php $apart_values = showUnitsFloorsNumber($detail['id']); ?>
                        @if(@$apart_values['units'])
                            <li>
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <div class="text">{{ @$apart_values['units'] }}</div>
                            </li>
                        @endif
                        @if(@$apart_values['floors'])
                            <li>
                                <div class="icon"><img src="<?php echo asset('/');?>/assets/img/floor.png" alt="#"></div>
                                <div class="text">{{$apart_values['floors']}}</div>
                            </li>
                        @endif
                        @if(@$apart_values['units'])
                        <!-- <li>
                            <i class="fa fa-th" aria-hidden="true"></i>
                            <div class="text">{{ $apart_values['units']}} Units on each Floor</div>
                        </li> -->
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pd-featured-area">
<div class="container">
    <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
            <span class="sold_img img-sld side-slider-cstm" style="display:none;">
                @if($detail['property_status'] == 0)
                    <img src="{{asset('assets/img/available.png')}}" alt="#">
                @else
                    <img src="{{asset('assets/img/sold.png')}}" alt="#">
                @endif
            </span>
            <div id="big" class="owl-carousel owl-theme big-sldr">
                @if(!empty(@$detail['cover_photo']))
                    <div class="item">
                        <img src="{{asset('uploads/apartmentImages/'.$detail['cover_photo'])}}" class="img-responsive" alt="...">
                    </div>
                @endif
                @if(!empty($images))
                    @foreach($images as $key => $value)
                        <div class="item">
                            <img src="{{asset('uploads/apartmentImages/'.$value['media_name'])}}" class="img-responsive" alt="...">
                        </div>
                    @endforeach
                @endif
            </div>
            <div id="thumbs" class="owl-carousel owl-theme thumm-sldr">
                @if(!empty(@$detail['cover_photo']))
                    @if(file_exists(public_path("/uploads/apartmentImages/thumbs").'/'.$detail['cover_photo']))
                        <div class="item">
                            <img src="{{asset('uploads/apartmentImages/thumbs/'.$detail['cover_photo'])}}" class="img-responsive" alt="...">
                        </div>
                    @endif
                @endif
                @if(!empty($images))
                    @foreach($images as $key => $value)
                        <div class="item">
                            <img src="{{asset('uploads/apartmentImages/thumbs/'.$value['media_name'])}}" class="img-responsive" alt="...">
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 right">
            @if(!empty(count($tower)))
                <div id="unit-slider" class="owl-carousel owl-theme big-sldr">
                    @foreach($tower as $key => $value)
                        <div class="item">
                            <div class="unit-slider-inr">
                                <div class="text-center title">
                                    <h4>{{$value->title}}</h4>
                                </div>
                                <?php
                                    $apartment_units = \DB::table('apartment_units')->whereAdId($detail['id'])->whereTowerId($value->id)->select('id','slug','apartment_floor_id','apartment_unit_id','property_status')->get()->groupBy('apartment_floor_id');
                                    $apartment_units = json_decode(json_encode($apartment_units),true);
                                    $units_total = (int)$detail['floor']*(int)$detail['units'];
                                ?>
                                <div class="property-color apartmentshow" id="append_apartment_units_div" >
                                    <div class="apartmentshow-inner">
                                        @if(!empty(@$apartment_units))
                                            <?php
                                                $filled_floors = count(@$apartment_units); 
                                            ?>
                                            @foreach($apartment_units as $key => $floor)
                                                <!-- ul represents a floor -->
                                                <ul>
                                                    <?php $filled_units = count($floor); ?>
                                                    @foreach($floor as $key1 => $unit)
                                                        @if($unit['property_status'] == 0)
                                                            <!-- li represents a unit of thet floor (green => available, red => sold, transparent => unavailable) -->
                                                            <!-- available unit -->
                                                            <li class="green"><a href="{{ url('propertyShow/'.'units'.'/'.$unit['slug']) }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                                                        @elseif($unit['property_status'] == 1)
                                                            <!-- sold unit -->
                                                            <li class="red"><a href="{{ url('propertyShow/'.'units'.'/'.$unit['slug']) }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                                                        @else
                                                            <!-- not available unit -->
                                                            <li>&nbsp;</li>
                                                        @endif
                                                    @endforeach
                                                    <?php $left_units = (int)@$detail['units'] - (int)$filled_units; ?>
                                                    @for($k = 1; $k <= $left_units; $k++)
                                                        <li>&nbsp;</li>
                                                    @endfor
                                                </ul>
                                            @endforeach
                                            <?php $left_floors = (int)@$detail['floor'] - (int)$filled_floors; ?>
                                            @for($i = 1; $i <= $left_floors; $i++)
                                                <ul>
                                                    @for($j = 1; $j <= @$detail['units']; $j++)
                                                        <li>&nbsp;</li>
                                                    @endfor
                                                </ul>
                                            @endfor
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</div>
<div class="description-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-md-7 col-xs-12">
                <div class="content">
                    <div class="title">
                        <h3>Description</h3>
                        <p>{{ substr(ucfirst($detail['description']), 0, 300)}}...</p>
                        <div class="read-more">
                            <button type="text" class="btn btn-primary black-btn readMoreBtn">Read More</button>
                        </div>
                    </div>
                </div>
                <ul class="ad-action-list">
                    <button class="btn btn-primary black-btn" data-toggle="modal" data-target="#reportAdModal"><i class="fa fa-ban"></i> Report this property</button>
                </ul>
                <!-- old design viewing developer -->
                <!-- <div class="singl-agnt">
                    <h4>AGENT</h4>
                    <div class="singl-agnt-img-wrp">
                        <div class="img-or-nm">
                            <a href="{{ url('/agent'.'/'.$detail['user']['slug']) }}">
                            @if(!empty($detail['user']['photo']))
                            <img src="{{asset('uploads/avatar/'.$detail['user']['photo'])}}" alt="#">
                            @else
                            <img src="{{asset('assets/img/default_user.png')}}" alt="#">
                            @endif
                            </a>
                            <h4>{{ucfirst($detail['user']['name'])}}</h4>
                        </div>
                        <div class="agnt-btn-wrp">
                            <a href="{{ url('/agent'.'/'.$detail['user']['slug']) }}" class="btn btn-primary agent-contact">Contact</a>
                        </div>
                    </div>
                    <div class="see-agnt-btn-wrp">
                        <a href="{{ url('/frontend/agentsList') }}" class="btn btn-primary agent-contact">See all agents</a>
                    </div>
                </div> -->
                <!-- new design viewing staff member and all staff members attached with this apartment -->
                @if($staff)
                    <div class="singl-agnt">
                        <h4>AGENT</h4>
                        <div class="singl-agnt-img-wrp">
                            <div class="img-or-nm">
                                <a href="{{ url('/staff'.'/'.$staff['staff']['slug']) }}">
                                    @if(!empty($staff['staff']['photo']))
                                        <img src="{{asset('uploads/avatar/'.$staff['staff']['photo'])}}" alt="#">
                                    @else
                                        <img src="{{asset('assets/img/default_user.png')}}" alt="#">
                                    @endif
                                </a>
                                <h4>{{ucfirst($staff['staff']['name'])}}</h4>
                            </div>
                            <div class="agnt-btn-wrp">
                                <a href="{{ url('/staff'.'/'.$staff['staff']['slug']) }}" class="btn btn-primary agent-contact">Contact</a>
                            </div>
                        </div>
                        <div class="see-agnt-btn-wrp">
                            <a href="{{ url('/developer/all-staff-members/'.encrypt($detail['id'])) }}" class="btn btn-primary agent-contact">See all agents</a>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-lg-5 col-md-5 col-xs-5 col-xs-12">
                <div class="map-div-place" id="map"> </div>
            </div>
        </div>
    </div>
</div>
<div class="related_author_list clearfix cstm-author-list">
    <div class="container">
        <h4><b>OTHER AGENTS</b></h4>
        <div class="row">
            @if(!empty($agents))
                @foreach($agents as $key => $value)
                    <div class="col-md-3 col-sm-6 col-xs-12 author-area">
                        <div class="author-img">
                            <a href="{{url('agent/'.$value['slug'])}}">
                            @if(!empty($value['photo']))
                                <img src="{{asset('uploads/avatar/'.$value['photo'])}}" alt="#">
                            @else
                                <img src="{{asset('assets/img/default_user.png')}}" alt="#">
                            @endif
                            </a>
                        </div>
                        <div class="author-info">
                            <ul>
                                <li class="name"><a href="{{url('agent/'.$value['slug'])}}">{{$value['name']}}</a></li>
                                <li class="address">{{substr($value['address'],0,60)}}</li>
                                <li>
                                    <a href="{{url('agent/'.$value['slug'])}}" class="btn btn-primary agent-contact">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
@include('property.review')
<!-- MORE details model start -->
<div class="modal fade" id="readMoreModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            @if(@$detail['title'])
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ucfirst($detail['title'])}}</h4>
                </div>
            @endif
            <div class="modal-body">
                @if(@$detail['description'])
                    <h4 class="modal_text_style">
                        <b>Description:</b>
                        <p>{{ucfirst(@$detail['description'])}} </p>
                    </h4>
                    <br>
                @endif
                <?php $apart_values = showUnitsFloorsNumber($detail['id']); ?>
                <h4 class="modal_text_style">
                    <b>No. of floors:</b>
                    <p>{{$apart_values['floors'] ? $apart_values['floors'].' Floors': 'N/A' }}</p>
                </h4>
                <br>
                <h4 class="modal_text_style">
                    <b>No of Units:</b>
                    <p>{{ $apart_values['units'] ? $apart_values['units'] : 0 }} Units</p>
                </h4>
                <br>
                <!-- show latitude and longitude for the user -->
                @if(!empty(@$detail['latitude']) && !empty(@$detail['longitude']))
                    <h4 class="modal_text_style">
                        <b>Coordinate :</b>
                        <p>Latitude:{{@$detail['latitude']}}</p>
                        <p>Longitude:{{@$detail['longitude']}}</p>
                    </h4>
                    <br>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- MORE details model ends -->
<!-- report modal starts -->
<div class="modal fade" id="reportAdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Is there something wrong with this Apartment?</h4>
            </div>
            <div class="modal-body">
                <p>We're constantly working hard to assure that our properties meet high standards and we are very grateful for any kind of feedback from our users.</p>
                <form>
                    <div class="form-group">
                        <label class="control-label">@lang('app.reason'):</label>
                        <select class="form-control" name="reason" id="report_reason">
                            <option value="">@lang('app.select_a_reason')</option>
                            <option value="unavailable">@lang('app.item_sold_unavailable')</option>
                            <option value="fraud">@lang('app.fraud')</option>
                            <option value="duplicate">@lang('app.duplicate')</option>
                            <option value="spam">@lang('app.spam')</option>
                            <option value="wrong_category">@lang('app.wrong_category')</option>
                            <option value="offensive">@lang('app.offensive')</option>
                            <option value="other">@lang('app.other')</option>
                        </select>
                        <div id="reason_info"></div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label">@lang('app.email'):</label>
                        <input type="text" class="form-control" id="report_email" name="email" maxlength="255">
                        <div id="email_info"></div>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">@lang('app.message'):</label>
                        <textarea class="form-control" id="report_message" name="message"></textarea>
                        <div id="message_info"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('app.close')</button>
                <button type="button" class="btn btn-primary" id="report_ad">Report</button>
            </div>
        </div>
    </div>
</div>
<!-- report modal ends -->
@endsection
@section('page-js')
<!-- <script src="{{ asset('assets/plugins/owl.carousel/owl.carousel.min.js') }}"></script> -->
<script src="{{ asset('assets/js/thumb.owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/Thumb.index.js') }}"></script>
<script src="{{asset('assets/paginate/src/jquery.paginate.js')}}"></script>
<script src="{{ asset('assets/star/jquery.fontstar.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{get_option('google_map_api_key')}}&callback=initMap"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#unit-slider').owlCarousel({
            loop: true,
            // autoplay: false,
            autoplay: true,
            autoplayTimeout: 1000,
            dots:false,
            responsiveClass: true,
            nav: true,
            navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>'],
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: true
                },
                1000: {
                    items: 1,
                    nav: true,
                }
            }
        });
    });

    // to stop slider when mouse is over it and restart when mouse leaves
    var owlCarousel = $('.owl-carousel');
    owlCarousel.mouseover(function(){
      owlCarousel.trigger('stop.owl.autoplay');
    });

    owlCarousel.mouseleave(function(){
      owlCarousel.trigger('play.owl.autoplay',[1000]);
    });
    // to stop slider when mouse is over it and restart when mouse leaves
</script>
<script type="text/javascript">
    // to show available or sold tag not before page loads properly
    $(document).ready(function() {
        $('.sold_img').attr('style','display:block;');
        // var slider_items = $('#thumbs').find('.owl-stage-outer').find('.item').length;
        // if(slider_items < 8) {
        //     $('.owl-nav').hide();
        // }
    });

    $(document).on('click','.readMoreBtn',function(){
       $("#readMoreModal").modal('show');
    });

    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif

    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 3,
                    nav: false
                },
                1000: {
                    items: 6,
                    nav: true,
                    loop: false,
                    margin: 20
                }
            }
        });
    });

    // review script start
    $(".rotate").click(function(){
        $(this).toggleClass("down")  ;
    });

    $(document).on('click','.rotate',function(){
        var fd = '';
        $.ajax({
            url : '{{ route('refresh_captcha') }}',
            type: "GET",
            data: fd,
            cache: false,
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success : function (data) {
                // alert(data);
                $("#current_captcha").val(data);
                $(".captcha_string").text(data);
            }
        });
    });

    $('.star').fontstar({},function(value,self){

        console.log("hello "+value);
    });

    //call paginate
    $('#example').paginate();
    // review script ends

    // $(document).ready(function() {
    //     $('#append_apartment_units_div').load("{{ url('') }}"+'/'+'agents/apend-apartment-units'+"/{{@$detail['id']}}/{{@$detail['type']}}");
    // });

    // to show map according to lattiude and longitude of the given property
    // Initialize and add the map
    function initMap() {
        // the location of Uluru
        // The location of Uluru
        var latitude    = "{{$detail['latitude']}}";
        var longitude   = "{{$detail['longitude']}}";
        var uluru = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 4, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }
    // to show map according to lattiude and longitude of the given property

    // submit report against property
    $(function() {
        $('button#report_ad').click(function(){
            var reason = $('#report_reason').val();
            var email = $('#report_email').val();
            var message = $('#report_message').val();
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            var error = 0;
            if(reason.length < 1){
                $('#reason_info').html('<p class="text-danger">Please select a reason.</p>');
                error++;
            }else {
                $('#reason_info').html('');
            }
            if(email.length < 1){
                $('#email_info').html('<p class="text-danger">Please enter email address.</p>');
                error++;
            }else {
                if ( ! regex.test(email)){
                    $('#email_info').html('<p class="text-danger">Please enter a valid email address.</p>');
                    error++;
                }else {
                    $('#email_info').html('');
                }
            }
            if(message.length < 1){
                $('#message_info').html('<p class="text-danger">Please enter a message.</p>');
                error++;
            }else {
                $('#message_info').html('');
            }

            if (error < 1){
                $('#loadingOverlay').show();
                $.ajax({
                    type : 'POST',
                    url : '{{ route('report_ads_pos') }}',
                    data : { reason : reason, email: email,message:message, id:'{{ $detail["id"] }}',  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        if (data.status == 1){
                            toastr.success(data.msg, '@lang('app.success')', toastr_options);
                        }else {
                            toastr.error(data.msg, '@lang('app.error')', toastr_options);
                        }
                        $('#report_reason').val('');
                        $('#report_email').val('');
                        $('#report_message').val('');
                        $('#reportAdModal').modal('hide');
                        $('#loadingOverlay').hide();
                    }
                });
            }
        });

    });

    $(document).ready(function() {
        /*Add Width*/
        $(".apartmentshow-inner ul").each(function(index, el) {
            if($(this).find("li").length >= 12 && $(this).find("li").length <= 20)
            {
                $(this).css("width", "750px");
            } else if($(this).find("li").length > 20) {
                $(this).css("width", "1100px");
            }
        });
        /*Add Width*/
    });
    // submit report against property

    // validations for review form
    $(document).on('click','#review_button',function() {

        var empty_error = 0;
        var name = $('#name').val();
        var email = $('#email').val();
        var message = $('#message').val();
        var entered_captcha = $('#entered_captcha').val();
        var current_captcha = $('#current_captcha').val();
        $('.error_divs').removeClass('has-error');
        $('.error_ps').text('');

        if(name == '') {
            $('.name_div').addClass('has-error');
            $('.name_p').text('Please enter your name.');
            empty_error = 1;
        }

        if(email == '') {
            $('.email_div').addClass('has-error');
            $('.email_p').text('Please enter email address.');
            empty_error = 1;
        } else {
            var re = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            if(!re.test(email)) {
                $('.email_div').addClass('has-error');
                $('.email_p').text('Please enter a valid email address.');
                empty_error = 1;
            }
        }

        if(message == '') {
            $('.message_div').addClass('has-error');
            $('.message_p').text('Please enter message.');
            empty_error = 1;
        }else {
            if(message.length > 80) {
                $('.message_div').addClass('has-error');
                $('.message_p').text('Message should not be more than 80 characters.');
                empty_error = 1;
            }
        }

        if(entered_captcha == '') {
            $('.entered_captcha_div').addClass('has-error');
            $('.entered_captcha_p').text('Please enter captcha.');
            empty_error = 1;
        } else {

            if(entered_captcha != current_captcha) {
                $('.entered_captcha_div').addClass('has-error');
                $('.entered_captcha_p').text('Please enter correct captcha.');
                empty_error = 1;
            }
        }

        if(empty_error > 0) {
            return false;
        }
        // alert(empty_error);
        // return false;

        // $('#leaveReview').submit();
        var formData = $('#leaveReview').serializeArray();
        $('#loadingOverlay').show();
        $.ajax({
            type : 'POST',
            url : '{{ route('leave_review') }}',
            data : formData,
            success : function (data) {
                if (data.status == 1){
                    toastr.success('Review saved successfully.', '@lang('app.success')', toastr_options);
                    window.location.reload();
                }else {
                    toastr.error('Oops some error occured.', '@lang('app.error')', toastr_options);
                }
                $('#loadingOverlay').hide();
            }
        });
    });
    // validations for review form
</script>
@endsection