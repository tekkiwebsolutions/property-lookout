@extends('layout.main')
@section('title') @if( ! empty(@$title)) {{ @$title }} | @endif @parent @endsection
@section('main')
<link rel="stylesheet" href="{{asset('assets/css/admin.css')}}">
<div class="cms-inner-content agent-bio-data">
    <div class="container">
        <h3 class="unit-title">Edit Apartment</h3>
        <hr class="clearfix" />
        <?php //prx($apartment->media_img); ?>
        <!-- <form class="form-horizontal clearfix" id="createApartmentPost" action="{{ route('create_apartment')}}" method="post" enctype="multipart/form-data"> -->
        <form class="clearfix" id="createApartmentPost" action="{{ route('apartment_edit')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="property_type" value="apartments" id="property_type">
            <input type="hidden" name="ad_id" value="{{@$apartment->id ? @$apartment->id : ''}}" id="apartment_id">
            <input type="hidden" name="image_type" id="image_type">
            <div class="form-group {{ $errors->has('name')? 'has-error':'' }}">
                <div class="row">
                    <label class="control-label col-sm-3">Apartment name</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control"  name="name" id="name" maxlength="255" value="{{ old('name') ? old('name') : @$apartment->title }}" placeholder="Enter apartment name" />

                    </div>
                    {!! $errors->has('name')? '
                    <p class="help-block">'.$errors->first('name').'</p>
                    ':'' !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('no_of_tower')? 'has-error':'' }}">
                <div class="row">
                    <label class="control-label col-sm-3">No of tower</label>
                    <div class="col-sm-6">
                       <input class="form-control" id="no_of_tower" name="no_of_tower" placeholder="Enter No of Tower" value="{{ @$apartment?@$apartment->no_of_tower:old('no_of_tower') }}">
                    </div>
                     {!! $errors->has('no_of_tower')? '<p class="help-block">'.$errors->first('no_of_tower').'</p>':'' !!}
                </div>
            </div> 
            <div class="form-group {{ $errors->has('description')? 'has-error':'' }}">
                <div class="row">
                    <label class="control-label col-sm-3">Description</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" rows="4" id="description" name="description" placeholder="Enter description">{{ old('description') ? old('description') : @$apartment->description }}</textarea>
                    </div>
                    {!! $errors->has('description')? '
                    <p class="help-block">'.$errors->first('description').'</p>
                    ':'' !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address')? 'has-error':'' }} location_error_div">
                <div class="row">
                    <label class="control-label col-sm-3">Apartment address</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" rows="2" id="address" name="address" placeholder="Enter a location">{{ old('address') ? old('address') : @$apartment->address }}</textarea>
                        <input type="hidden" name="address2" id="address2" value="{{ old('address2') }}">
                    </div>
                    {!! $errors->has('address')? '
                    <p class="help-block">'.$errors->first('address').'</p>
                    ':'' !!}
                    <p class="location_error_p help-block"></p>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="control-label col-sm-3 col-xs-12">Coordinate </label>
                    <div class="col-md-3 col-sm-3 col-xs-12 {{ $errors->has('latitude')? 'has-error':'' }}">
                        <span>Latitude</span>
                        <input type="text" id="lat" name="latitude" class="form-control" readonly value="{{ old('latitude') ? old('latitude') : @$apartment->latitude }}"/>
                        {!! $errors->has('latitude')? '
                        <p class="help-block">'.$errors->first('latitude').'</p>
                        ':'' !!}
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 {{ $errors->has('longitude')? 'has-error':'' }}">
                        <span>Longitude</span>
                        <input type="text" id="long" name="longitude" class="form-control" readonly value="{{ old('longitude') ? old('longitude') : @$apartment->longitude }}"/>
                        {!! $errors->has('longitude')? '
                        <p class="help-block">'.$errors->first('longitude').'</p>
                        ':'' !!}
                    </div>
                </div>
            </div>
            <div class="form-group {{ $errors->has('zip')? 'has-error':'' }}">
                <div class="row">
                    <label class="control-label col-sm-3">Zip Code</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" name="zip" id="zip" value="{{ old('zip') ? old('zip') : @$apartment['zip'] }}" placeholder="Enter Zip Code" maxlength="6" placeholder="Enter zip code" />
                    </div>
                    {!! $errors->has('zip')? '
                    <p class="help-block">'.$errors->first('zip').'</p>
                    ':'' !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('country')? 'has-error':'' }}">
                <div class="row">
                    <label class="control-label col-sm-3">Country</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select id="country" name="country" class="form-control select2" tabindex="6">
                            <option value="">@lang('app.select_a_country')</option>
                            @foreach($countries as $country)
                            <option value="{{ $country->id }}" {{ old('country') == $country->id ? 'selected' :'' }} {{@$apartment->country_id == $country->id ? 'selected' : '' }}>{{ $country->country_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    {!! $errors->has('country')? '
                    <p class="help-block">'.$errors->first('country').'</p>
                    ':'' !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('state')? 'has-error':'' }}">
                <div class="row">
                    <label class="control-label col-sm-3">State</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control select2" id="state_select" name="state">
                            <option value="">Select State</option>
                            @if($previous_states->count() > 0)
                                @foreach($previous_states as $state)
                                    <option value="{{ $state->id }}" {{ old('state') == $state->id ? 'selected' :'' }} {{@$apartment->state_id == $state->id ? 'selected' : '' }}>{{ $state->state_name }}</option>
                                @endforeach
                            @endif
                        </select>
                        <p class="text-info">
                            <span id="state_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                        </p>
                    </div>
                    {!! $errors->has('state')? '
                    <p class="help-block">'.$errors->first('state').'</p>
                    ':'' !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('city')? 'has-error':'' }}">
                <div class="row">
                    <label class="control-label col-sm-3">City</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control select2" id="city_select" name="city">
                            <option value="">Select City</option>
                            @if($previous_cities->count() > 0)
                                @foreach($previous_cities as $city)
                                    <option value="{{ $city->id }}" {{ old('city') == $city->id ? 'selected':'' }} {{@$apartment->city_id == $city->id ? 'selected' : '' }}>{{ $city->city_name }}</option>
                                @endforeach
                            @endif
                        </select>
                        <p class="text-info">
                            <span id="city_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                        </p>
                    </div>
                    {!! $errors->has('city')? '
                    <p class="help-block">'.$errors->first('city').'</p>
                    ':'' !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('house_photos_check')? 'has-error':'' }}">
                <div class="row">
                    <label class="control-label col-sm-3">Upload photos</label>
                    <div class="col-sm-9">
                        <div class="existing_image">
                            @if(!empty(@$apartment && !empty(@$apartment->media_img)))
                            @foreach(@$apartment->media_img as $key => $img)
                            <div class="creating-ads-img-wrap">
                                <img src="{{ media_url_property_array($img, false) }}" class="img-responsive" />
                                <div class="img-action-wrap" id="{{ $img['id'] }}">
                                    <a href="javascript:;" class="imgDeleteBtn"><i class="fa fa-trash-o"></i> </a>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                        <div id="uploaded-ads-image-wrap">   
                        </div>
                        <div class="file-upload-wrap">
                            <label>
                                <input type="hidden" name="house_photos_check" id="house_photos_check" value="{{!empty($apartment->media_img ? 'photo_uploaded' : '')}}" />
                                <input type="file" name="house_photos" id="house_photos" style="display: none;" />
                                <i class="fa fa-cloud-upload"></i>
                                <p>@lang('app.upload_image')</p>
                                <div class="progress" style="display: none;"></div>
                            </label>
                        </div>
                        {!! $errors->has('house_photos_check')? '
                        <p class="help-block">Please select atleast one photo.</p>
                        ':'' !!}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="control-label col-sm-3">Cover Photo</label>
                    <div class="col-sm-9">
                        <div class="profile-avatar edt-aprtmnt-phto">
                            @if(!empty(@$apartment) && !empty(@$apartment->cover_photo)) 
                            <img src="{{asset('uploads/apartmentImages/'.@$apartment->cover_photo)}}" class="img-thumbnail img-circle image_show" id="image_show">
                            @else
                            <img src="{{asset('assets/img/default_user.png')}}" class="img-thumbnail img-circle image_show" id="image_show">
                            @endif
                        </div>
                        <div class="{{ $errors->has('cover_photo')? 'has-error':'' }}">
                            <input type="file" id="cover_photo" name="cover_photo" class="filestyle" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                            <div class="bootstrap-filestyle input-group">
                                <span class="group-span-filestyle input-group-btn" tabindex="0">
                                    <label for="cover_photo" class="btn btn-primary black-btn">
                                        <span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span> 
                                        <span class="buttonText choose_image">
                                            <b>Choose Cover Image</b>
                                        </span>
                                    </label>
                                </span>
                            </div>
                            <p class="image_error" style="color: red; font-size:10px;"></p>
                            {!! $errors->has('cover_photo')? '
                            <p class="help-block">'.$errors->first('cover_photo').'</p>
                            ':'' !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="btns f-right">
                <button type="submit" class="btn btn-primary black-btn m-t-20" id="submit_button">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{get_option('google_map_api_key')}}&libraries=places"></script>
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('address2').value = place.name;
            document.getElementById('lat').value = place.geometry.location.lat();
            document.getElementById('long').value = place.geometry.location.lng();
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            console.log(reader);
    
            reader.onload = function (e) {
                $('.image_show').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    (function($) {
        $.fn.checkFileType = function(options) {
            var defaults = {
                allowedExtensions: [],
                success: function() {},
                error: function() {}
            };
            options = $.extend(defaults, options);
            return this.each(function() {
                $(this).on('change', function() {
                    var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);
                    if ($.inArray(extension, options.allowedExtensions) == -1) {
                        options.error();
                        $(this).focus();
                    } else {
                        options.success();
                        readURL(this);
                    }
    
                });
            });
        };
    
    })(jQuery);
    
    $(function() {
        $('#cover_photo').checkFileType({
            allowedExtensions: ['jpg', 'jpeg','png'],
            success: function() {
                // alert('Success');
                $(".image_error").text('');
    
            },
            error: function() {
                // alert('Error');
                var loc = "{{asset('/assets/img/default_user.png')}}";
                $(".image_show").attr("src",loc);
                $(".image_error").text('Only ".jpg",".jpeg",".png" files are allowed.');
    
            }
        });
    });
    
    $("#house_photos").change(function() {
        var media_count = $('.creating-ads-img-wrap').length;
        if(media_count < 30) {


            // document.getElementById('load').style.visibility="visible";

            $("#image_type").val('apartment_images');
            var fd = new FormData(document.querySelector("form#createApartmentPost"));
            $('.progress').show();
            $.ajax({
                url : '{{ route('upload_property_image') }}',
                type: "POST",
                data: fd,
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            //console.log(percentComplete);
                            var progress_bar = '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: '+percentComplete+'%">'+percentComplete+'% </div>';
    
                            if (percentComplete === 100) {
                                $('.progress').hide();
                            }
                        }
                    }, false);
                    return xhr;
                },
                cache: false,
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success : function (data) {
                    //$('#loadingOverlay').hide('slow');
                    if (data.success == 1){
                        
                        $('#uploaded-ads-image-wrap').load("{{ url('') }}"+'/'+'agents/apend-property-image'+'/'+data.property_type+'/'+data.image_type);
                        $('"#house_photos_check').val("photo_uploaded");
                    } else{
                        toastr.error(data.msg, '<?php echo trans('app.error') ?>', toastr_options);
                    }
                    // document.getElementById('load').style.visibility="hidden";
                }
            });
        } else {
            swal("OOPS!","You cannot upload more than 30 images","error");
        }
    });
    
    $('body').on('click', '.imgDeleteBtn', function(){
        //Get confirm from user
        if ( ! confirm('{{ trans('app.are_you_sure') }}')){
            return '';
        }
        // document.getElementById('load').style.visibility="visible";

        var current_selector = $(this);
        var img_id = $(this).closest('.img-action-wrap').attr('id');
        $.ajax({
            url : '{{ route('delete_property_media') }}',
            type: "POST",
            data: { media_id : img_id, _token : '{{ csrf_token() }}' },
            success : function (data) {
                if (data.success == 1){
                    // current_selector.closest('.creating-ads-img-wrap').hide('slow');
                    current_selector.closest('.creating-ads-img-wrap').remove();
                    var del_media_count = $('.creating-ads-img-wrap').length;
                    if(del_media_count < 1) {
                        $('#house_photos_check').val("");
                    }
                    toastr.success(data.msg, '@lang('app.success')', toastr_options);
                }
                // document.getElementById('load').style.visibility="hidden";
            }
        });
    });     
    
    function generate_option_from_json(jsonData, fromLoad){
        //Load Category Json Data To Brand Select
        if (fromLoad === 'category_to_brand'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> <?php echo trans('app.select_a_brand') ?> </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
                }
                $('#brand_select').html(option);
                $('#brand_select').select2();
            }else {
                $('#brand_select').html('');
                $('#brand_select').select2();
            }
            $('#brand_loader').hide('slow');
        }else if(fromLoad === 'country_to_state'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> @lang('app.select_state') </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
                }
                $('#state_select').html(option);
                $('#state_select').select2();
            }else {
                $('#state_select').html('');
                $('#state_select').select2();
            }
            $('#state_loader').hide('slow');
    
        }else if(fromLoad === 'state_to_city'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> @lang('app.select_city') </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
                }
                $('#city_select').html(option);
                $('#city_select').select2();
            }else {
                $('#city_select').html('');
                $('#city_select').select2();
            }
            $('#city_loader').hide('slow');
        }
    }
    
    $(document).ready(function() {
        $('#phone').keyup(function(){
            $(this).val($(this).val().replace(/[^0-9]/g,""));
        });
    
        $(".notify img").attr("src", "<?php echo asset('/');?>/assets/img/notify.png");
        $(".footer-widget img").attr("src", "<?php echo asset('/');?>/assets/img/footer-logo.png");
    });
    
    $('[name="country"]').change(function(){
        var country_id = $(this).val();
        $('#state_loader').show();
        $.ajax({
            type : 'POST',
            url : '{{ route('get_state_by_country') }}',
            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
            success : function (data) {
                generate_option_from_json(data, 'country_to_state');
            }
        });
    });
    
    $('[name="state"]').change(function(){
        var state_id = $(this).val();
        $('#city_loader').show();
        $.ajax({
            type : 'POST',
            url : '{{ route('get_city_by_state') }}',
            data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
            success : function (data) {
                generate_option_from_json(data, 'state_to_city');
            }
        });
    });

    // scroll to top function
    function topScroll() {
        $("html, body").delay(200).animate({
            scrollTop: $('.container').offset().top 
        }, 200);
    }
    // scroll to top function

    $(document).on('click','#submit_button',function() {
        
        // $('.location_error_div').removeClass('has-error');
        // $('.location_error_p').text('');
        // var address_info = $('#address').val();
        // if(address_info == '') {
        //     $('.location_error_div').addClass('has-error');
        //     $('.location_error_p').text('Please select an address');
        //     topScroll();
        //     return false;
        // } else {
        //     var lat_info = $('#lat').val();
        //     var long_info = $('#long').val();
        //     if(lat_info == '' || long_info == '') {
        //         $('.location_error_div').addClass('has-error');
        //         $('.location_error_p').text('Please select a valid address');
        //         topScroll();
        //         return false;
        //     }
        // }
        $('#createApartmentPost').submit();
    });
</script>
@endsection