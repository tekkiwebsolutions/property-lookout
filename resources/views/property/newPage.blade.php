<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>@section('title') {{ get_option('site_title') }} @show</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @section('social-meta')
        <meta property="og:title" content="{{ get_option('site_title') }}">
        <meta property="og:description" content="{{ get_option('meta_description') }}">
        <meta property="og:url" content="{{ route('home') }}">
        <meta name="twitter:card" content="summary_large_image">
        <!--  Non-Essential, But Recommended -->
        <meta name="og:site_name" content="{{ get_option('site_name') }}">
        @show
        <style type="text/css">
            #load{
            width:100%;
            height:100%;
            position:fixed;
            z-index:9999;
            background:url("assets/img/loading.gif") no-repeat center center rgba(0,0,0,0.25)
            }
        </style>
        <div id="load"></div>
        <!-- bootstrap css -->
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-theme.min.css') }}">
        <!-- load page specific css -->
        <!-- main select2.css -->
        <link href="{{ asset('assets/select2-3.5.3/select2.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/select2-3.5.3/select2-bootstrap.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/plugins/nprogress/nprogress.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/fonts/stylesheet.css') }}">
        <!-- Conditional page load script -->
        @if(request()->segment(1) === 'dashboard')
            <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">
            <link rel="stylesheet" href="{{ asset('assets/plugins/metisMenu/dist/metisMenu.min.css') }}">
        @endif
        <!-- main style.css -->
        <?php $default_style = get_option('default_style'); ?>
        @if($default_style == 'default')
            <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        @else
            <link rel="stylesheet" href="{{ asset("assets/css/style-{$default_style}.css") }}">
        @endif
        @yield('page-css')
        @if(get_option('additional_css'))
            <style type="text/css">
                {{ get_option('additional_css') }}
            </style>
        @endif
        <script src="{{ asset('assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
        <!-- <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script> -->
        <!-- Font awesome 4.4.0 -->
        <link rel="stylesheet" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <!-- <script src="{{ asset('assets/js/jquery.panorama_viewer.js') }}"></script> -->
        <!-- <link rel="stylesheet" href="{{ asset('assets/css/panorama_viewer.css') }}"> -->
        <!-- <style>
            html {
                height: 100%;
            }

            body {
                background: #F1f1f2;
                padding: 0;
                text-align: center;
                font-family: 'open sans';
                position: relative;
                margin: 0;
                height: 100%;
            }
            
            #panorama-wrapper > .wrapper {
                height: auto !important;
                height: 100%;
                margin: 0 auto; 
                overflow: hidden;
            }
            
            #panorama-wrapper > a {
                text-decoration: none;
            }
            
            #panorama-wrapper > h1, h2 {
                width: 100%;
                float: left;
            }

            #panorama-wrapper > h1 {
                margin-top: 100px;
                color: #999;
                margin-bottom: 5px;
                font-size: 70px;
                font-weight: 100;
            }

            #panorama-wrapper > h2 {
                padding: 00px 20px 30px 20px;
                box-sizing: border-box;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                letter-spacing: 0px;
                color: #888;
                font-size: 20px;
                line-height: 160%;
                font-weight: 100;
                margin-top: 10px;
                margin-bottom: 0;
            }
            
            #panorama-wrapper > .pointer {
                color: #00B0FF;
                font-family: 'Pacifico';
                font-size: 24px;
                margin-top: 15px;
                position: absolute;
                top: 130px;
                right: -40px;
            }

            #panorama-wrapper > .pointer2 {
                color: #00B0FF;
                font-family: 'Pacifico';
                font-size: 24px;
                margin-top: 15px;
                position: absolute;
                top: 130px;
                left: -40px;
            }

            #panorama-wrapper > pre {
                margin: 80px auto;
            }

            #panorama-wrapper > pre code {
                padding: 35px;
                border-radius: 5px;
                font-size: 15px;
                background: rgba(0,0,0,0.1);
                border: rgba(0,0,0,0.05) 5px solid;
                max-width: 500px;
            }
            
            #panorama-wrapper > .main {
                float: left;
                width: 100%;
                margin: 0 auto;
            }
            
            #panorama-wrapper > .main h1 {
                padding:20px 50px 10px;
                float: left;
                width: 100%;
                font-size: 60px;
                box-sizing: border-box;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                font-weight: 100;
                margin: 0;
                padding-top: 55px;
                font-family: 'Open Sans';
                letter-spacing: -1px;
                text-transform: capitalize;
            }
            
            #panorama-wrapper > .main h1.demo1 {
                background: #1ABC9C;
            }
            
            #panorama-wrapper > .reload.bell {
                font-size: 12px;
                padding: 20px;
                width: 45px;
                text-align: center;
                height: 47px;
                border-radius: 50px;
                -webkit-border-radius: 50px;
                -moz-border-radius: 50px;
            }
            
            #panorama-wrapper > .reload.bell #notification {
                font-size: 25px;
                line-height: 140%;
            }
            
            #panorama-wrapper > .reload, .btn{
                display: inline-block;
                border-radius: 3px;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                display: inline-block;
                line-height: 100%;
                padding: 0.7em;
                text-decoration: none;
                width: 100px;
                line-height: 140%;
                font-size: 17px;
                font-family: Open Sans;
                font-weight: bold;
                -webkit-box-shadow: none;
                box-shadow: none;
                background-color: #4D90FE;
                background-image: -webkit-linear-gradient(top,#4D90FE,#4787ED);
                background-image: -webkit-moz-gradient(top,#4D90FE,#4787ED);
                background-image: linear-gradient(top,#4d90fe,#4787ed);
                border: 1px solid #3079ED;
                color: #FFF;
            }

            #panorama-wrapper > .reload:hover{
                background: #317af2;
            }

            #panorama-wrapper > .btn {
                width: 200px;
                -webkit-box-shadow: none;
                box-shadow: none;
                background-color: #4D90FE;
                background-image: -webkit-linear-gradient(top,#4D90FE,#4787ED);
                background-image: -moz-linear-gradient(top,#4D90FE,#4787ED);
                background-image: linear-gradient(top,#4d90fe,#4787ed);
                border: 1px solid #3079ED;
                color: #FFF;
            }

            #panorama-wrapper > .clear {
                width: auto;
            }

            #panorama-wrapper > .btn:hover, .btn:hover {
                background: #317af2;
            }

            #panorama-wrapper > .btns {
                float: left;
                width: 100%;
                margin: 50px auto;
            }

            #panorama-wrapper > .credit {
                text-align: center;
                color: #888;
                padding: 10px 10px;
                margin: 0 0 0 0;
                background: #f5f5f5;
                float: left;
                width: 100%;
            }

            #panorama-wrapper > .credit a {
                text-decoration: none;
                font-weight: bold;
                color: black;
            }
            
            #panorama-wrapper > .back {
                position: absolute;
                top: 0;
                left: 0;
                text-align: center;
                display: block;
                padding: 7px;
                width: 100%;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                background:#f5f5f5;
                font-weight: bold;
                font-size: 13px;
                color: #888;
                -webkit-transition: all 200ms ease-out;
                -moz-transition: all 200ms ease-out;
                -o-transition: all 200ms ease-out;
                transition: all 200ms ease-out;
            }

            #panorama-wrapper > .back:hover {
                background: #eee;
            }
            
            #panorama-wrapper > .page-container {
                float: left;
                width: 100%;
                margin: 0 auto 300px;
                position: relative;
            }

            #panorama-wrapper > .panorama {
                width: 100%;
                float: left;
                margin-top: -5px;
                height: 700px;
                position: relative;
            }
            
            #panorama-wrapper > .panorama .credit {
                background: rgba(0,0,0,0.2);
                color: white;
                font-size: 12px;
                text-align: center;
                position: absolute;
                bottom: 0;
                right: 0;
                box-sizing: border-box;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                float: right;
            }

            </style> -->
        <!-- <script>
            // Use $(window).load() on live site instead of document ready. This is for the purpose of running locally only
            $(document).ready(function(){
              $(".panorama").panorama_viewer({
                repeat: true
              });
            });
            </script> -->
        <!-- uncomment this section to view panoramic system -->
    </head>
    <body>
        <!-- remove this section to view penoramic system -->
        <div class="header-nav-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-5 width-100">
                        <div class="topContactInfo">
                            <ul class="nav nav-pills">
                                <li>
                                    @if(!Auth::check())
                                        <a href="{{url('frontend/agentsList')}}">
                                            <i class="fa fa-comments" aria-hidden="true"></i>
                                            Live Chat
                                        </a>
                                    @else
                                        @if(Auth::user()->user_type == 'user')
                                            <a href="{{ route('user_chat_messages')}}">
                                                <i class="fa fa-comments" aria-hidden="true"></i>
                                                Live Chat
                                            </a>
                                        @elseif(Auth::user()->user_type == 'developer')
                                            <a href="{{ route('developer_user_chat_messages')}}">
                                                <i class="fa fa-comments" aria-hidden="true"></i>
                                                Live Chat
                                            </a>
                                        @endif
                                    @endif
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-bell" aria-hidden="true"></i>
                                        Live Notifications
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-7 width-100">
                        <div class="topContactInfo">
                            <ul class="nav nav-pills">
                                @if(get_option('site_phone_number'))
                                <li>
                                    <a href="callto://+{{get_option('site_phone_number')}}">
                                        <i class="fa fa-phone"></i>
                                        +{{ get_option('site_phone_number') }}
                                    </a>
                                </li>
                                @endif
                                @if(get_option('site_email_address'))
                                <li>
                                    <a href="mailto:{{ get_option('site_email_address') }}">
                                        <i class="fa fa-envelope"></i>
                                        {{ get_option('site_email_address') }}
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        @if(Auth::check())
                            @if(Auth::user()->user_type == 'user' || Auth::user()->user_type == 'developer')
                                <div class="topContactInfo">
                                    <ul class="nav nav-pills navbar-right">
                                        <li>
                                            <a href="javascript:void(0);">
                                                <i class="fa fa-user"></i>
                                                @lang('app.hi'), {{ $logged_user->name }} 
                                            </a>
                                        </li>
                                        <!--<li>
                                            <a href="{{ route('dashboard') }}">
                                            <i class="fa fa-dashboard"></i>
                                            Dashboard </a>
                                            </li> -->
                                    </ul>
                                </div>
                            @else
                                <div class="topContactInfo">
                                    <ul class="nav nav-pills navbar-right">
                                        <li>
                                            <a href="{{ route('profile') }}">
                                                <i class="fa fa-user"></i>
                                                @lang('app.hi'), {{ $logged_user->name }} 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('dashboard') }}">
                                                <i class="fa fa-dashboard"></i>
                                                Dashboard 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}">
                                                <i class="fa fa-sign-out"></i>
                                                @lang('app.logout')
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @endif
                        @else
                            <div class="topContactInfo login_right">
                                <ul class="nav nav-pills navbar-right">
                                    @if($header_menu_pages->count() > 0)
                                        @foreach($header_menu_pages as $page)
                                            <li><a href="{{ route('single_page', $page->slug) }}">{{ $page->title }} </a></li>
                                        @endforeach
                                    @endif
                                    @if( ! Auth::check())
                                        <li><a href="{{ route('login') }}"> <i class="fa fa-lock"></i>  {{ trans('app.login') }}  </a>  </li>
                                    @endif
                                </ul>
                            </div>
                            <!--  {{ Form::open(['route'=>'login','class'=> 'navbar-form navbar-right', 'role'=> 'form']) }}
                                <div class="form-group">
                                   <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email address">
                                </div>
                                <div class="form-group">
                                   <input  type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-success">@lang('app.sign_in')</button> 
                                {{ Form::close() }} -->
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ route('home') }}">
                    @if(get_option('logo_settings') == 'show_site_name')
                        {{ get_option('site_name') }}
                    @else
                        @if(logo_url())
                            <img src="{{ logo_url() }}">
                        @else
                            {{ get_option('site_name') }}
                        @endif
                    @endif
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @if($header_menu_pages->count() > 0)
                            @foreach($header_menu_pages as $page)
                                <li><a href="{{ route('single_page', $page->slug) }}">{{ $page->title }} </a></li>
                            @endforeach
                        @endif
                        <?php 
                            if(Auth::check()){
                               if(Auth::user()->user_type == 'user' || Auth::user()->user_type == 'developer'){
                                     $header_menu = 1;
                               }else{
                                     $header_menu = 0;
                               }
                            }else{
                               $header_menu = 0;
                            }
                        ?>
                        @if($header_menu == 1)
                            @if(Auth::user()->user_type == 'user')    
                                <li><a href="{{ route('agent_properties_list') }}">Home</a></li>
                            @elseif(Auth::user()->user_type == 'developer')
                                <li><a href="{{ route('developer_properties_list') }}">Home</a></li>
                            @endif
                            @if(Auth::check()) 
                                @if(Auth::user()->user_type == 'user')
                                    <li><a href="{{url('agents/credits-list')}}">My Credits</a></li>
                                @elseif(Auth::user()->user_type == 'developer')
                                    <li><a href="{{url('developers/credits-list')}}">My Credits</a></li>
                                @endif 
                            @endif
                            <li><a href="{{url('/blog')}}"> Blogs</a></li>
                            <li class="dropdown hidden-xs">
                                <a href="#" class="dropdown-toggle notify" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php echo asset('/');?>/assets/img/notify.png" alt="#">Live Notifications</a>
                                <ul class="dropdown-menu notify-drop">
                                    <i class="fa fa-sort-asc" aria-hidden="true"></i>
                                    <div class="notify-drop-title">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">Notification</div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 text-right mark_notifications_read"><a href="" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom" title="tümü okundu.">Mark All as Read</a></div> <!-- function to mark all as read is kept in footer.blade.php in the same folder -->
                                        </div>
                                    </div>
                                    <!-- end notify title -->
                                    <!-- notify content -->
                                    <div class="drop-content" id="header_notifications">
                                        @if(!empty(@$header_notifications))
                                            @foreach(@$header_notifications as $notify)
                                                <li>
                                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                                    <div class="fleft">
                                                        <a href="{{url('agent/'.$notify->user->slug)}}" target="_blank"><b>
                                                            @if(!empty(@$notify->user))
                                                                @if(!empty(@$notify->user->name))
                                                                    {{ $notify->user->name }}
                                                                @elseif(!empty($notify->user->first_name) || !empty($notify->user->last_name))
                                                                    {{ @$notify->user->first_name.' '.@$notify->user->last_name }}
                                                                @endif
                                                            @endif
                                                        </b></a> {{ @$notify->type_text ? @$notify->type_text : '' }} 
                                                        <p>{{ @$notify->message ? substr(@$notify->message,0,40).'...' : '' }}</p>
                                                        <span> {{ @$notify->created_at->diffForHumans() }} | Applicant Tracking</span>
                                                    </div>
                                                    <a href="" class="rIcon pull-right"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                                </li>
                                                <hr class="clearfix">
                                            @endforeach
                                        @endif
                                        <!-- <li>
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <div class="fleft">
                                                <a href="#"><b>Jamie</b></a> has responded to your email 
                                                <p>Good morning Jospeph, Yes, I am inter...</p>
                                                <span>5 Hours Ago | Applicant Tracking</span>
                                            </div>
                                            <a href="" class="rIcon pull-right"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                        </li>
                                        <hr class="clearfix">
                                        <li>
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <div class="fleft">
                                                <a href=""><b>Jamie</b></a> has responded to your email 
                                                <p>Good morning Jospeph, Yes, I am inter...</p>
                                                <span>5 Hours Ago | Applicant Tracking</span>
                                            </div>
                                            <a href="" class="rIcon pull-right"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                        </li>
                                        <hr class="clearfix">
                                        <li>
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <div class="fleft">
                                                <a href=""><b>Jamie</b></a> has responded to your email 
                                                <p>Good morning Jospeph, Yes, I am inter...</p>
                                                <span>5 Hours Ago | Applicant Tracking</span>
                                            </div>
                                            <a href="" class="rIcon pull-right"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                        </li>
                                        <hr class="clearfix"> -->
                                    </div>
                                    <div class="notify-drop-footer text-center">
                                        <!-- <a href="../page/notification" class="allRead ">See All Notifications</a> -->
                                        <a href="{{ route('all_notifications') }}" class="allRead ">See All Notifications</a>
                                    </div>
                                </ul>
                            </li>
                            <li class="welcome">Welcome! You are logged in as</li>
                            <li class="dropdown admin-profile">
                                <a href="javascript:void(0);" class="dropdown-toggle admin-img" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                @if(!empty(Auth::user()->photo))
                                    <img src="{{asset('uploads/avatar/'.Auth::user()->photo)}}" alt="#" class="img-responsive"/>
                                @else
                                    <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
                                @endif
                                </a>
                                <ul class="dropdown-menu admin-dropdown">
                                    <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    <!-- plan upgrade button not in use now as there are only two packages left -->
                                    <!-- <a href="{{route('packages')}}" class="btn btn-primary small-btn">Upgrade</a> -->
                                    @if(Auth::user()->user_type == 'user')    
                                        <li><a href="{{url( '/agent'.'/'.Auth::user()->slug )}}"><i class="fa fa-user" aria-hidden="true"></i>@if(!empty(Auth::user()->name)){{Auth::user()->name}}@endif</a></li>
                                    @elseif(Auth::user()->user_type == 'developer')
                                        <li><a href="{{url( '/developers'.'/'.Auth::user()->slug )}}"><i class="fa fa-user" aria-hidden="true"></i>@if(!empty(Auth::user()->name)){{Auth::user()->name}}@endif</a></li>
                                    @endif
                                    @if(Auth::user()->user_type == 'user')    
                                        <li><a href="{{ route('settings_view') }}"><i class="fa fa-cog" aria-hidden="true"></i>Account Settings</a></li>
                                    @elseif(Auth::user()->user_type == 'developer')
                                        <li><a href="{{ route('developer_settings_view') }}"><i class="fa fa-cog" aria-hidden="true"></i>Account Settings</a></li>
                                    @endif
                                    <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a></li>
                                </ul>
                            </li>
                        @else
                            <li><a href="{{URL::to('/page/maintainance')}}">Maintenance</a></li>
                            <!-- <li><a href="{{URL::to('/page/properties')}}">@lang('app.properties')</a></li> -->
                            <li><a href="{{URL::to('/frontend/agentsList')}}">Agents</a></li>
                            <li><a href="{{URL::to('/page/about-us')}}">@lang('app.about_us')</a></li>
                            <li><a href="{{URL::to('/contact-us')}}"> @lang('app.contact_us')</a></li>
                            @if(Auth::check() && Auth::user()->user_type != 'admin')
                                <li><a href="{{url('/blog')}}"> Blogs</a></li>
                            @elseif(!Auth::check())
                                <li><a href="{{url('/blog')}}"> Blogs</a></li>
                            @endif
                            <li class="dropdown hidden-xs">
                                <a href="#" class="dropdown-toggle notify" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php echo asset('/');?>/assets/img/notify.png" alt="#">Live Notifications</a>
                                <ul class="dropdown-menu notify-drop">
                                    <i class="fa fa-sort-asc" aria-hidden="true"></i>
                                    <div class="notify-drop-title">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">Notification</div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 text-right"><a href="#" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom" title="tümü okundu.">Mark All as Read</a></div>
                                        </div>
                                    </div>
                                    <!-- end notify title -->
                                    <!-- notify content -->
                                    <div class="drop-content" id="header_notifications1">
                                        @if(!empty(@$header_notifications))
                                            @foreach(@$header_notifications as $notify)
                                                <li>
                                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                                    <div class="fleft">
                                                        <a href=""><b>{{ $notify->user->name }}</b></a> {{ $notify->type_text }} 
                                                        <p>{{ substr($notify->message,0,40).'...' }}</p>
                                                        <span>{{ $notify->created_at->diffForHumans() }} | Applicant Tracking</span>
                                                    </div>
                                                    <a href="" class="rIcon pull-right"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                                </li>
                                                <hr class="clearfix">
                                            @endforeach
                                        @endif
                                        <!-- <li>
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <div class="fleft">
                                                <a href=""><b>Jamie</b></a> has responded to your email 
                                                <p>Good morning Jospeph, Yes, I am inter...</p>
                                                <span>5 Hours Ago | Applicant Tracking</span>
                                            </div>
                                            <a href="" class="rIcon pull-right"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                        </li>
                                        <hr class="clearfix">
                                        <li>
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <div class="fleft">
                                                <a href=""><b>Jamie</b></a> has responded to your email 
                                                <p>Good morning Jospeph, Yes, I am inter...</p>
                                                <span>5 Hours Ago | Applicant Tracking</span>
                                            </div>
                                            <a href="" class="rIcon pull-right"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                        </li>
                                        <hr class="clearfix"> -->
                                    </div>
                                    <div class="notify-drop-footer text-center">
                                        <a href="javascript:void;" class="allRead ">See All Notifications</a>
                                    </div>
                                </ul>
                            </li>
                            <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <!-- remove this section to view penoramic system -->

<div class="cms-inner-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 agent-detail-manual cms-avtar agent-cms-avtar">
                <div class="avtar">
                    @if(!empty(Auth::user()->photo))
                        <img src="{{asset('uploads/avatar/'.Auth::user()->photo)}}" alt="#" class="img-responsive"/>
                    @else
                        <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
                    @endif
                </div>
                <ul class="agent_profiles text-center">
                    <li><a href="@if(!empty($meta_data['facebook_url'])){{$meta_data['facebook_url']}}@else{{'https://www.facebook.com/'}}@endif"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                    <li><a href="@if(!empty($meta_data['twitter_url'])){{$meta_data['twitter_url']}}@else{{'https://www.twitter.com/'}}@endif"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                    <li><a href="@if(!empty($meta_data['linked_in_url'])){{$meta_data['linked_in_url']}}@else{{'https://www.linkedin.com/'}}@endif"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                </ul>
                <!-- <div><a class="btn btn-primary contact-btn" href="#">Contact</a></div> -->
                <h2>{{Auth::user()->name}}</h2>
                <!-- <span>Sale Assocaite - (Property Lookout)</span> -->
                <div class="m-15">
                    <span>+{{Auth::user()->phone}}</span>
                    <span>{{Auth::user()->email}}</span>
                </div>
                <span>Accumulated visitors: 0</span>
                <span>Accumulated Rating: 0</span>
                <span>Account type: {{$current_package['name']}}</span>
                <?php  
                    
                    // get no of apartmnets and properties for users
                    $house_number = \App\Ad::whereUserId(Auth::user()->id)->whereType('house')->count(); 
                    $apartment_number = \App\Ad::whereUserId(Auth::user()->id)->whereType('apartments')->count(); 
                ?>
                @if(Auth::user()->user_type == 'user')
                    <span>Number of properties: {{ @$house_number }}</span>
                    <span>Number of apartments: {{ @$apartment_number }}</span> <!-- will be changed for agents as developers can assign their apartments to agents -->
                @elseif(Auth::user()->user_type == 'developer')
                    <span>Number of apartments: {{ @$apartment_number }}</span>
                @endif
            </div>


        </div>
    </div>
</div>
<!-- remove this section to view penoramic system -->
<div class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="footer-widget">
                        <img src="<?php echo asset('/');?>/assets/img/footer-logo.png" class="fleft" />
                        <p>{{ get_option('footer_about_us') }}</p>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-xs-12">
                    <div class="footer-widget footer-address clearfix">
                        <h4>@lang('app.contact_us')</h4>
                        <address>
                            <!-- <strong>{{ get_text_tpl(get_option('footer_company_name')) }}</strong> -->
                            @if(get_option('footer_address'))
                              
                                <i class="fa fa-map-marker"></i>
                                <span>
                                    {!! get_option('footer_address') !!}
                                </span>
                            @endif
                        </address>
                        <div class="footer-call">
                            @if(get_option('site_phone_number'))
                                <i class="fa fa-phone"></i>
                                <abbr title="Phone">{!! get_option('site_phone_number') !!}</abbr>
                            @endif
                        </div>

                        @if(get_option('site_email_address'))
                            <address>
                                <!-- <strong>@lang('app.email')</strong> -->
                                <i class="fa fa-envelope-o"></i>
                                <a href="mailto:{{ get_option('site_email_address') }}"> {{ get_option('site_email_address') }} </a>
                            </address>
                        @endif
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-xs-12">
                    <div class="footer-widget">
                        <h4>Contact With Us</h4>
                        <div class="newsletter">
                            <input type="text" name="" id="" placeholder="Your email address" />
                            <button type="submit" class="signupbtn">Sign Up</button>
                        </div>
                        <div class="social-icons">
                            <ul class="social-ul">
                                @if(get_option('facebook_url'))
                                    <li class="fb">
                                        <a href="{{ get_option('facebook_url') }}" target="_blank">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                @endif

                                @if(get_option('twitter_url'))
                                    <li class="tw">
                                        <a href="{{ get_option('twitter_url') }}" target="_blank">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                @endif
                                @if(get_option('linked_in_url'))
                                    <li class="inked">
                                        <a href="{{ get_option('linked_in_url') }}" target="_blank">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                @endif
                                @if(get_option('dribble_url'))
                                    <li class="dribl">
                                        <a href="{{ get_option('dribble_url') }}" target="_blank">
                                            <i class="fa fa-dribbble"></i>
                                        </a>
                                    </li>
                                @endif
                                @if(get_option('google_plus_url'))
                                    <li class="gplus">
                                        <a href="{{ get_option('google_plus_url') }}" target="_blank">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                    </li>
                                @endif
                                @if(get_option('youtube_url'))
                                    <li class="utub">
                                        <a href="{{ get_option('youtube_url') }}" target="_blank">
                                            <i class="fa fa-youtube"></i>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>{!! get_text_tpl(get_option('footer_left_text')) !!}</p>
                    <!-- <p class="pull-right"> {!! get_text_tpl(get_option('footer_right_text')) !!} </p> -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- remove this section to view penoramic system -->

<div id="loadingOverlay" style="display: none;">
    <div class="circleLoader"></div>
    <p>@lang('app.loading')...</p>
</div>


<!-- loader script start -->

<script type="text/javascript">
        document.onreadystatechange = function () {
            var state = document.readyState
            if (state == 'complete') {
                   document.getElementById('interactive');
                   document.getElementById('load').style.visibility="hidden";
            }
        }

</script>

<!-- loader script ends -->
<!-- <script src="{{ asset('assets/js/vendor/jquery-1.11.2.min.js') }}"></script> -->
<script src="{{ asset('assets/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/vendor/bootstrap-confirmation.min.js') }}"></script>
<script src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('assets/select2-3.5.3/select2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/nprogress/nprogress.js') }}"></script>
<script type="text/javascript">
    NProgress.start();
    NProgress.done();
</script>
<!-- Conditional page load script -->
@if(request()->segment(1) === 'dashboard')
    <script src="{{ asset('assets/plugins/metisMenu/dist/metisMenu.min.js') }}"></script>
    <script>
        $(function() {
            $('#side-menu').metisMenu();
        });
        // jQuery(function($) {
        //      var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
        //      $('ul a').each(function() {
        //       if (this.href === path) {
        //        $(this).addClass('active');
        //       }
        //      });
        //     });

    </script>
@endif
<script src="{{ asset('assets/js/main.js') }}"></script>
<script>
    var toastr_options = {closeButton : true};
</script>
@yield('page-js')


@if(get_option('additional_js'))
    {!! get_option('additional_js') !!}}
@endif
<script>
    $(document).on('click', '.ghuranti', function(){
        $('.themeqx-demo-chooser-wrap').toggleClass('open');
    });
</script>
<!-- mark all notifications as read -->
<script type="text/javascript">
    $(document).on('click','.mark_notifications_read',function(e) {
        e.preventDefault();
        
        $.ajax({
            url: "{{ url('notifications/mark-read') }}",
            type: "GET",
            success:function(data) {
                
                if(data.success == 1) {
                    toastr.success(data.msg, '@lang('app.success')', toastr_options);
                } else {
                    toastr.error(data.msg, '@lang('app.success')', toastr_options);
                }
            },
            error:function() {
                toastr.error(data.msg, '@lang('app.success')', toastr_options);
            }
        })
    })
</script>
<!-- mark all notifications as read -->

<!-- reload the latest notifications function after every 5 seconds -->

<script type="text/javascript">
    $(document).ready(function() {
        var myVar = setInterval(getLatestNotifications, 2000);

        function getLatestNotifications() {

            //alert("hello");
            $('#header_notifications').load('{{route("get_latest_notifications")}}');
            $('#header_notifications1').load('{{route("get_latest_notifications")}}');
        }
    });
</script>

<!-- reload the latest notifications function after every 5 seconds -->

<!-- remove credits from houses after every 29th of the month -->

<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({
            url: "{{ url('/agents/remove-credits') }}",
            type: "get",
            success:function() {
                console.log('success');
            },
            error:function() {
                console.log('error');
            }
        })
    });
</script>

<!-- remove credits from houses after every 29th of the month -->

</body>
</html>