<div class="review-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="title">
                    <h3>Reviews :</h3>
                </div>
                <div class="reviews clearfix" id="example">
                    @if(!empty($reviews))
                        @foreach($reviews as $key => $value)
                        <div>
                            <div class="author-area clearfix">
                                <div class="author-info">
                                    <ul>
                                        <li class="name"><a href="javascript:void(0);">{{$value['name']}}</a></li>
                                        <li class="address">{{$value['email']}}</li>
                                        <li class="star-reviews">
                                            @for ($i=0; $i < 5; $i++)
                                                @if($value['ratings'] <= $i)
                                                    <i class="fa fa-star-o" style="font-size:18px; color:grey"></i>
                                                @else
                                                    <i class="fa fa-star" style="font-size:18px; color:#ffbf58;"></i>
                                                @endif
                                            @endfor
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="review-text">
                                <p>{{$value['message']}}</p>
                            </div>
                        </div>
                        @endforeach
                    @else
                        <div>
                            <div class="author-area clearfix">
                                <div class="author-info">
                            
                                </div>
                            </div>
                            <div class="review-text">
                                <p>No Reviews Yet !</p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-4 right">
                <div class="title">
                    <center>
                        <h3>Leave a review</h3>
                    </center>
                </div>
                <form class="leaveReview" id="leaveReview" action="{{route('leave_review')}}" method="post">
                    {{csrf_field()}}
                    @if(isset($detail['type']))
                        <input type="hidden" name="ads_id" value="{{$detail['id']}}">
                        <input type="hidden" name="apartment_unit_id" value="0">
                    @else
                        <input type="hidden" name="ads_id" value="{{$detail['apartment']['id']}}">
                        <input type="hidden" name="apartment_unit_id" value="{{$detail['id']}}">
                    @endif
                    <div class="form-group {{ $errors->has('name')? 'has-error':'' }} error_divs name_div">
                        <input type="text" class="form-control" id="name" name="name" placeholder="@lang('app.enter_name')" value="{{ old('name') }}"/>
                        {!! $errors->has('name')? '
                            <p class="help-block">'.$errors->first('name').'</p>
                        ':'' !!}
                            <p class="help-block error_ps name_p"></p>
                    </div>
                    <div class="form-group {{ $errors->has('email')? 'has-error':'' }} error_divs email_div">
                        <input type="text" class="form-control" id="email" placeholder="@lang('app.enter_email_address')" name="email" value="{{ old('email') }}"  />
                        {!! $errors->has('email')? '
                            <p class="help-block">'.$errors->first('email').'</p>
                        ':'' !!}
                            <p class="help-block error_ps email_p"></p>
                    </div>
                    <div class="form-group {{ $errors->has('message')? 'has-error':'' }} error_divs message_div">
                        <textarea name="message" id="message" class="form-control" placeholder="@lang('app.message')">{{ old('message') }}</textarea>
                        {!! $errors->has('message')? '
                            <p class="help-block">'.$errors->first('message').'</p>
                        ':'' !!}
                            <p class="help-block error_ps message_p"></p>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <h5>Ratings</h5>
                            <select name="rating" class="star">
                                <option value="">--</option>
                                <option value="1" selected>1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                    </div>
                    <div class="cptch-div">
                        <h5>Captcha</h5>
                        <ul class="captcha">
                            <li>
                                <p>
                                    <label class="captcha_string">{{$captcha}}</label>
                                    <i class="fa fa-refresh rotate" aria-hidden="true"></i>
                                </p>
                            </li>
                            <li class="{{ $errors->has('entered_captcha')? 'has-error':'' }} error_divs entered_captcha_div"><input type="text" name="entered_captcha" id="entered_captcha"></li>
                        </ul>
                        <input type="hidden" name="current_captcha" id="current_captcha" value="{{$captcha}}">
                        <!-- <button type="submit" class="btn btn-primary black-btn"> @lang('app.send_message')</button> -->
                        <button type="button" class="btn btn-primary black-btn" id="review_button"> @lang('app.send_message')</button>
                        <div class="error_divs entered_captcha_div"><p class="help-block error_ps entered_captcha_p"></p></div>
                    </div>
                    {!! $errors->has('entered_captcha')?'<p class="help-block" style="color: red;">'.$errors->first('entered_captcha').'</p>':'' !!}
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
</script>

