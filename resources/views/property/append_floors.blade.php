@if(!empty($no_of_floors))
    @for($i = 1; $i <= $no_of_floors; $i++)
        <div class="form-group">
            <label class="control-label col-sm-3">Enter no. of Units for Floor {{$i}}</label>
            <div class="col-sm-5">
                <input type="number" class="form-control unit_add_class" name="units[{{$i}}]" max="12" min="1" value="1"/>
            </div>
        </div>
    @endfor
@endif