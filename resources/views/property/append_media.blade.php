@if(!empty($media_images))
    @foreach($media_images as $img)
        <div class="creating-ads-img-wrap">
            <img src="{{ media_url_property($img, false) }}" class="img-responsive" />
            <div class="img-action-wrap" id="{{ $img['id'] }}">
                <a href="javascript:;" class="imgDeleteBtn"><i class="fa fa-trash-o"></i> </a>
            </div>
        </div>
    @endforeach
@endif