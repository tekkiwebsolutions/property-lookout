@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif 
@parent @endsection
@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/plugins/owl.carousel/assets/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('assets/paginate/src/jquery.paginate.css')}}" />
<?php //prx($all_ads); ?>
<style>
    .modal_text_style{
    font-family: 'Proxima Nova';
    font-size: 15px;
    line-height: 22px;
    }
    /*review start*/
    .rotate{
    -moz-transition: all 1s linear;
    -webkit-transition: all 1s linear;
    transition: all 1s linear;
    }
    .rotate.down{
    -moz-transform:rotate(180deg);
    -webkit-transform:rotate(180deg);
    transform:rotate(180deg);
    }
    .rotate{
    margin-left: 10px;
    }
    .review-section form #message {
    min-height: 60px !important;
    }
    .review-section .author-area {
    margin-top: 10px;
    margin-left: 9px;
    }
    .paginate-pagination{
    float: right;
    }
    /*review ends*/
    .des-img img{
    height:200px;
    }
    .outer {
    margin: 0 auto;
    max-width: 800px;
    }
</style>
@endsection
@section('main')
<div class="navigate-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="address">
                    <div class="title">
                        <h4>{{ucfirst($detail['title'])}}</h4>
                        @if(!empty($detail['company_name']))
                        <h5><strong>On behalf of:</strong> {{$detail['company_name']}}</h5>
                        @endif
                    </div>
                    <address>
                        <div class="left">
                            <a href="javascript:void(0);"><i class="fa fa-map-marker"></i></a>
                        </div>
                        <div class="right">
                            <p>{{$detail['address']}}<br/><small>{{$detail['country']['country_name']}}</small></p>
                        </div>
                    </address>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="navigate-icons property_sold-icon">
                    <ul>
                        <li>
                            <div class="text">${{$detail['price']}}</div>
                        </li>
                        <li>
                            <i class="fa fa-arrows-alt" aria-hidden="true"></i>
                            <div class="text">{{$detail['square_unit_space']}} sqft</div>
                        </li>
                        <li>
                            <i class="fa fa-bed" aria-hidden="true"></i>
                            <div class="text">{{$detail['beds']}} Bedrooms</div>
                        </li>
                        <li>
                            <i class="fa fa-shower" aria-hidden="true"></i>
                            <div class="text">{{$detail['attached_bath']}} Bathrooms</div>
                        </li>
                        <li>
                            <i class="fa fa-car" aria-hidden="true"></i>
                            <div class="text">{{$detail['no_of_parking']}} Car Parking</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pd-featured-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="javascript:void(0);" class="">
                    <span class="sold_img">
                        <!-- @if($detail['property_status'] == 0)
                            <img src="<?php echo asset('/');?>/assets/img/available.png" alt="Available">
                            @elseif($detail['property_status'] == 1)
                            <img src="<?php echo asset('/');?>/assets/img/sold.png" alt="Sold">
                            @endif -->
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
<section class="property-slider">
    <div class="container">
        <div class="large-12 columns">
            <span class="sold_img img-sld" style="display:none;">
                @if($detail['property_status'] == 0)
                    <img src="{{asset('assets/img/available.png')}}" alt="Tag">
                @else
                    <img src="{{asset('assets/img/sold.png')}}" alt="Tag">
                @endif
            </span>
            <div id="big" class="owl-carousel owl-theme">
                @if(!empty(@$detail['cover_photo']))
                    <div class="item">
                        <img src="{{asset('uploads/houseImages/'.$detail['cover_photo'])}}" class="img-responsive bg-villa detail-page-img" alt="slider">
                    </div>
                @endif
                @if(!empty($images))
                    @foreach($images as $key => $value)
                        <div class="item">
                            <img src="{{asset('uploads/houseImages/'.$value['media_name'])}}" class="img-responsive bg-villa detail-page-img" alt="slider">
                        </div>
                    @endforeach
                @endif
            </div>
            <div id="thumbs" class="owl-carousel owl-theme thumm-sldr">
                @if(!empty(@$detail['cover_photo']))
                    @if(file_exists(public_path("/uploads/houseImages/thumbs").'/'.$detail['cover_photo']))
                        <div class="item">
                            <img src="{{asset('uploads/houseImages/thumbs/'.$detail['cover_photo'])}}" class="img-responsive" alt="slider">
                        </div>
                    @endif
                @endif
                @if(!empty($images))
                    @foreach($images as $key => $value)
                        <div class="item">
                            <img src="{{asset('uploads/houseImages/thumbs/'.$value['media_name'])}}" class="img-responsive" alt="slider">
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
<div class="description-box">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="content">
                    <div class="title">
                        <h3>Description</h3>
                        <p>{{ substr(ucfirst($detail['description']), 0, 300)}}...</p>
                        <div class="read-more">
                            <button type="text" class="btn btn-primary black-btn readMoreBtn">Read More</button>
                        </div>
                    </div>
                </div>
                <ul class="ad-action-list">
                    <li>
                        <button class="btn btn-primary black-btn" data-toggle="modal" data-target="#reportAdModal"><i class="fa fa-ban"></i> Report this property</button>
                    </li>
                </ul>
                <div class="singl-agnt">
                    <h4>AGENT</h4>
                    <div class="singl-agnt-img-wrp">
                        <div class="img-or-nm">
                            <a href="{{ url('/agent'.'/'.$detail['user']['slug']) }}">
                            @if(!empty($detail['user']['photo']))
                            <img src="{{asset('uploads/avatar/'.$detail['user']['photo'])}}" alt="#">
                            @else
                            <img src="{{asset('assets/img/default_user.png')}}" alt="#">
                            @endif
                            </a>
                            <h4>{{ucfirst($detail['user']['name'])}}</h4>
                        </div>
                        <div class="agnt-btn-wrp">
                            <a href="{{ url('/agent'.'/'.$detail['user']['slug']) }}" class="btn btn-primary agent-contact">Contact</a>
                        </div>
                    </div>
                    <!-- <div class="see-agnt-btn-wrp">
                        <a href="{{ url('/frontend/agentsList') }}" class="btn btn-primary agent-contact">See all agents</a>
                    </div> -->
                </div>
            </div>
            <div class="col-md-5">
                <div class="map-div-place" id="map">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="related_author_list clearfix cstm-author-list">
    <div class="container">
        <h4><b>OTHER AGENTS</b></h4>
        <div class="row">
            @if(!empty($agents))
            @foreach($agents as $key => $value)
            <div class="col-md-3 col-sm-6 col-xs-12 author-area">
                <div class="author-img">
                    <a href="{{url('agent/'.$value['slug'])}}">
                        @if(!empty($value['photo']))
                            <img src="{{asset('uploads/avatar/'.$value['photo'])}}" alt="#">
                        @else
                            <img src="{{asset('assets/img/default_user.png')}}" alt="#">
                        @endif
                    </a>
                </div>
                <div class="author-info">
                    <ul>
                        <li class="name"><a href="{{url('agent/'.$value['slug'])}}">{{$value['name']}}</a></li>
                        <li class="address">{{substr($value['address'],0,60)}}</li>
                        <li>
                            <a href="{{url('agent/'.$value['slug'])}}" class="btn btn-primary agent-contact">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
            @endforeach
            @endif   
        </div>
    </div>
</div>
@include('property.review')
<!-- MORE details model start -->
<div class="modal fade" id="readMoreModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{ucfirst($detail['title'])}}</h4>
            </div>
            <div class="modal-body">
                <h4 class="modal_text_style">
                    <b>Description:</b> 
                    <p>{{ucfirst($detail['description'])}} </p>
                </h4>
                <br>
                <h4 class="modal_text_style">
                    <b>No. of parking:</b>
                    <p>{{$detail['no_of_parking']}}</p>
                </h4>
                <br>
                <h4 class="modal_text_style">
                    <b>Property type:</b>
                    <p>{{$detail['property_type']['name']}}</p>
                </h4>
                <br>
                <h4 class="modal_text_style">
                    <b>Coordinate :</b>
                    <p>Latitude:{{$detail['latitude']}}</p>
                    <p>Longitude:{{$detail['longitude']}}</p>
                </h4>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- MORE details model ends -->
<!-- report modal starts -->
<div class="modal fade" id="reportAdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Is there something wrong with this Property?</h4>
            </div>
            <div class="modal-body">
                <p>We're constantly working hard to assure that our properties meet high standards and we are very grateful for any kind of feedback from our users.</p>
                <form>
                    <div class="form-group">
                        <label class="control-label">@lang('app.reason'):</label>
                        <select class="form-control" name="reason" id="report_reason">
                            <option value="">@lang('app.select_a_reason')</option>
                            <option value="unavailable">@lang('app.item_sold_unavailable')</option>
                            <option value="fraud">@lang('app.fraud')</option>
                            <option value="duplicate">@lang('app.duplicate')</option>
                            <option value="spam">@lang('app.spam')</option>
                            <option value="wrong_category">@lang('app.wrong_category')</option>
                            <option value="offensive">@lang('app.offensive')</option>
                            <option value="other">@lang('app.other')</option>
                        </select>
                        <div id="reason_info"></div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label">@lang('app.email'):</label>
                        <input type="text" class="form-control" id="report_email" name="email" maxlength="255">
                        <div id="email_info"></div>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">@lang('app.message'):</label>
                        <textarea class="form-control" id="report_message" name="message"></textarea>
                        <div id="message_info"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('app.close')</button>
                <button type="button" class="btn btn-primary" id="report_ad">Report</button>
            </div>
        </div>
    </div>
</div>
<!-- report modal ends -->
@endsection
@section('page-js')
<!-- <script src="{{ asset('assets/plugins/owl.carousel/owl.carousel.min.js') }}"></script> -->
<script src="{{ asset('assets/js/thumb.owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/Thumb.index.js') }}"></script>
<script src="{{ asset('assets/star/jquery.fontstar.js') }}"></script>
<script src="{{ asset('assets/paginate/src/jquery.paginate.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{get_option('google_map_api_key')}}&callback=initMap">
</script>
<script>
    // to show available or sold tag not before page loads properly
    $(document).ready(function() {
        $('.sold_img').attr('style','display:block;');
        var slider_items = $('#thumbs').find('.owl-stage-outer').find('.item').length;
        if(slider_items < 7) {
            $('#thumbs').find('.owl-nav').hide();
        }
    });

    $(document).on('click','.readMoreBtn',function(){
        $("#readMoreModal").modal('show');
    });

    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif

    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            // autoplayHoverPause: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 3,
                    nav: false
                },
                1000: {
                    items: 6,
                    nav: true,
                    loop: false,
                    margin: 20
                }
            }
        })
    });

    // to stop slider when mouse is over it and restart when mouse leaves
    var owlCarousel = $('.owl-carousel');
    owlCarousel.mouseover(function(){
      owlCarousel.trigger('stop.owl.autoplay');
    });

    owlCarousel.mouseleave(function(){
      owlCarousel.trigger('play.owl.autoplay',[1000]);
    });
    // to stop slider when mouse is over it and restart when mouse leaves

    // review script start
    $(".rotate").click(function(){
        $(this).toggleClass("down")  ; 
    });

    $(document).on('click','.rotate',function(){
        var fd = '';
        $.ajax({
            url : '{{ route('refresh_captcha') }}',
            type: "GET",
            data: fd,
            cache: false,
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success : function (data) {
                // alert(data);
                $("#current_captcha").val(data);
                $(".captcha_string").text(data);
            }
        });
    });

    $('.star').fontstar({},function(value,self){

        console.log("hello "+value);
    });
    
    //call paginate
    $('#example').paginate();

    // review script ends
    // to show map according to lattiude and longitude of the given property

    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        var latitude    = "{{$detail['latitude']}}";
        var longitude   = "{{$detail['longitude']}}";
        var uluru = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
        // The map, centered at Uluru
        var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 4, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }
    // to show map according to lattiude and longitude of the given property

    // submit report against property
    $(function() {
        $('button#report_ad').click(function(){
            var reason = $('#report_reason').val();
            var email = $('#report_email').val();
            var message = $('#report_message').val();
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            var error = 0;
            if(reason.length < 1){
                $('#reason_info').html('<p class="text-danger">Please select a reason.</p>');
                error++;
            }else {
                $('#reason_info').html('');
            }
            if(email.length < 1){
                $('#email_info').html('<p class="text-danger">Please enter email address.</p>');
                error++;
            }else {
                if ( ! regex.test(email)){
                    $('#email_info').html('<p class="text-danger">Please enter a valid email address.</p>');
                    error++;
                }else {
                    $('#email_info').html('');
                }
            }
            if(message.length < 1){
                $('#message_info').html('<p class="text-danger">Please enter a message.</p>');
                error++;
            }else {
                $('#message_info').html('');
            }

            if (error < 1){
                $('#loadingOverlay').show();
                $.ajax({
                    type : 'POST',
                    url : '{{ route('report_ads_pos') }}',
                    data : { reason : reason, email: email,message:message, id:'{{ $detail["id"] }}',  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        if (data.status == 1){
                            toastr.success(data.msg, '@lang('app.success')', toastr_options);
                        }else {
                            toastr.error(data.msg, '@lang('app.error')', toastr_options);
                        }
                        $('#report_reason').val('');
                        $('#report_email').val('');
                        $('#report_message').val('');
                        $('#reportAdModal').modal('hide');
                        $('#loadingOverlay').hide();
                    }
                });
            }
        });
    });
    // submit report against property

    // validations for review form
    $(document).on('click','#review_button',function() {
        
        var empty_error = 0;
        var name = $('#name').val();
        var email = $('#email').val();
        var message = $('#message').val();
        var entered_captcha = $('#entered_captcha').val();
        var current_captcha = $('#current_captcha').val();
        $('.error_divs').removeClass('has-error');
        $('.error_ps').text('');

        if(name == '') {
            $('.name_div').addClass('has-error');
            $('.name_p').text('Please enter your name.');
            empty_error = 1;
        }

        if(email == '') {
            $('.email_div').addClass('has-error');
            $('.email_p').text('Please enter email address.');
            empty_error = 1;
        } else {
            var re = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            if(!re.test(email)) {
                $('.email_div').addClass('has-error');
                $('.email_p').text('Please enter a valid email address.');
                empty_error = 1;
            }
        }

        if(message == '') {
            $('.message_div').addClass('has-error');
            $('.message_p').text('Please enter message.');
            empty_error = 1;
        }else {
            if(message.length > 80) {
                $('.message_div').addClass('has-error');
                $('.message_p').text('Message should not be more than 80 characters.');
                empty_error = 1;
            }
        }

        if(entered_captcha == '') {
            $('.entered_captcha_div').addClass('has-error');
            $('.entered_captcha_p').text('Please enter captcha.');
            empty_error = 1;
        } else {

            if(entered_captcha != current_captcha) {
                $('.entered_captcha_div').addClass('has-error');
                $('.entered_captcha_p').text('Please enter correct captcha.');
                empty_error = 1;
            }
        }

        if(empty_error > 0) {
            return false;
        }
        // alert(empty_error);
        // return false;
        
        // $('#leaveReview').submit();
        var formData = $('#leaveReview').serializeArray();
        $('#loadingOverlay').show();
        $.ajax({
            type : 'POST',
            url : '{{ route('leave_review') }}',
            data : formData,
            success : function (data) {
                if (data.status == 1){
                    toastr.success('Review saved successfully.', '@lang('app.success')', toastr_options);
                    window.location.reload();
                }else {
                    toastr.error('Oops some error occured.', '@lang('app.error')', toastr_options);
                }
                $('#loadingOverlay').hide();
            }
        });
    });
    // validations for review form
</script>
@endsection