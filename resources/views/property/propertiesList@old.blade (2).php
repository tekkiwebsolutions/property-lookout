@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<link rel="stylesheet" href="{{asset('assets/css/thumb.owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/thumb.owl.theme.default.min.css')}}">
<?php use App\ApartmentUnit; ?>
<style type="text/css">
    .modal-header{
        background-color:#306492;
        color:white;
        font-size:16px;
        font-weight:bolder !important;
    }
    .modal-header .close{
        color:white;
        font-size:16px;
        font-weight:bolder !important;
    }
</style>
<div class="cms-inner-content">
    <div class="container">
        <div class="row">
            @if(Auth::user()->user_type == 'user')
                @include('agent.agentInfoSection')
            @elseif(Auth::user()->user_type == 'developer')
                @include('developer.developerInfoSection')
            @elseif(Auth::user()->user_type == 'staff')
                @include('developer.staffInfoSection')
            @endif
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 cms-border">
                <?php 
                    $pack_permissions = packPermissions(Auth::user()->id);
                ?>
                <div align="right" style="margin-bottom: 3%;">
                    @if(Auth::user()->user_type == 'user')
                        <a href="javascript:void(0);" style="font-size: 18px;font-weight: bold;">Create:</a>
                        <a href="{{route('create_house')}}" class="btn btn-primary small-btn">Properties</a>
                    @endif
<<<<<<< HEAD
                        @if(Auth::user()->user_type == 'developer')                        
                    @endif
=======
>>>>>>> d8ae012839da132a95af7dd5726a656c02697bc0
                </div>
                <!-- Div for agent only -->
                @if(Auth::user()->user_type == 'user')
                    <div class="table-responsive">
                        @if(!empty(@$properties))
                        <table class="table cms-table cstm-tbl-cls agnt-prprty-tbl">
                            <thead>
                                <tr>
                                    <th>Properties</th>
                                    <th>Space</th>
                                    <th>Status</th>
                                    <th>Details</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(@$properties as $key => $value)
                                <?php $id = Crypt::encrypt($value['id']); ?>
                                <tr class="three-btns-actn">
                                    <td class="columnOne">
                                        <div class="property-img">
                                            @if($value['type'] == 'house')
                                            <img src="{{asset('uploads/houseImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                            @else
                                            <img src="{{asset('uploads/apartmentImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                            @endif
                                        </div>
                                        <div class="float-left">
                                            <span>{{substr($value['title'],0,20)}}</span>
                                            <span>{{ucfirst($value['type'])}}</span>
                                            @if($value['type'] == 'house')
                                            <h6>$ {{$value['price']}}</h6>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="columnTwo"> 
                                        @if($value['type'] == 'house')    
                                        <span>{{$value['beds']}} Bedrooms</span>
                                        <span>{{$value['attached_bath']}} Bathrooms</span>
                                        <span>{{$value['square_unit_space']}} sqft </span>
                                        @else
                                        <span>{{$value['floor']}} Floors</span>
                                        <span>{{($value['floor']?$value['floor']:0)*($value['units']?$value['units']:0)}} Units</span>
                                        <span>{{$value['available_units_count']}} Available</span>
                                        @endif
                                    </td>
                                    <td id="td_{{$value['id']}}">
                                        @if($value['property_status'] == 0)
                                        <p style="color:green;">{{'Available'}}</p>
                                        @else
                                        <p style="color:red;">{{'Sold'}}</p>
                                        @endif
                                    </td>
                                    @if($value['status'] == 0)
                                    <td>Not released for public confirmation needed</td>
                                    @else
                                    <td>
                                        <span>Views: {{ $value['view'] }}</span>
                                        @if($value['guest_review_count'] > 0)
                                        <span>Average Rating: {{ get_avrg_ratings($value['id']) }}/5 ({{ $value['guest_review_count'] }} reviews)</span>
                                        @endif
                                    </td>
                                    @endif
                                    <td>
                                        <div class="dropdown">
                                            @if($value['type'] == 'house' && $value['property_status'] == 1)
                                            <button class="btn action-btn" type="button">Sold</button>
                                            @else
                                            <button class="btn action-btn dropdown-toggle" type="button" data-toggle="dropdown">Action<span class="caret"></span></button>
                                            <ul class="dropdown-menu" @if($value['type'] == 'apartments') style="min-width:84px !important;" @endif>
                                            @if($value['property_status'] != 1 && $value['type'] == 'house')
                                            <li>
                                                <a href="javascript:void(0);" class="btn btn-primary small-btn propertySoldbtn" data-toggle="confirmation"  id="{{$id}}">Sold</a>
                                            </li>
                                            @elseif($value['property_status'] != 1 && $value['type'] == 'apartments')
                                            <li>
                                                <a href="javascript:void(0);" class="btn btn-primary small-btn sold-open" id="{{$id}}">Sold</a>
                                            </li>
                                            @endif
                                            @if($value['property_status'] != 1 && $value['type'] == 'house')
                                            <li style="@if($value['property_status'] == 1 && $value['type'] == 'house')float:initial; @endif">
                                                <a href="javascript:void(0);" class="btn small-btn btn-danger propertyDeleteBtn" data-toggle="confirmation" id="{{$id}}" >Delete</a>
                                            </li>
                                            <li>
                                                <a href="{{route ('edit_house',$id)}}" class="btn small-btn btn-success">Edit</a>
                                            </li>
                                            @endif
                                            </ul>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                @if(@$value['type'] == 'apartments')
                                <tr class="open-sold-box">
                                    <td colspan="5" class="padding-b-3">
                                        <div class="custom-clr-boxes">
                                            <div class="property-color append_apartments_units_div propertylist-m" rel="{{$value['id']}}" rel1="{{$value['type']}}">
                                                <!-- show apartment units for each apartment (function in helpers.php) -->
                                                {!! get_apartment_common_units($value['id'],$value['floor'],$value['units']) !!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <div class="col-md-12">
                            <h4>No Property added yet. Click on the "Create Property" button to start adding your properties.</h4>
                        </div>
                        @endif
                    </div>
                @endif
                <!-- Div for developer only -->
                @if(Auth::user()->user_type == 'developer')
                    @if(!empty(@$properties))
                        <table class="table cms-table cstm-tbl-cls">
                            <thead>
                                <tr>
                                    <th>Properties</th>
                                    <th>Space</th>
                                    <th>Status</th>
                                    <th>Details</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(@$properties as $key => $value)
                                <?php $id = Crypt::encrypt($value['id']); ?>
                                <tr class="three-btns-actn">
                                    <td class="columnOne">
                                        <div class="property-img">
                                            @if($value['type'] == 'house')
                                            <img src="{{asset('uploads/houseImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                            @else
                                            <img src="{{asset('uploads/apartmentImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                            @endif
                                        </div>
                                        <div class="float-left">
                                            <span>{{substr($value['title'],0,20)}}</span>
                                            <span>{{ucfirst($value['type'])}}</span>
                                            @if($value['type'] == 'house')
                                            <h6>$ {{$value['price']}}</h6>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="columnTwo"> 
                                        @if($value['type'] == 'house')    
                                        <span>{{$value['beds']}} Bedrooms</span>
                                        <span>{{$value['attached_bath']}} Bathrooms</span>
                                        <span>{{$value['square_unit_space']}} sqft </span>
                                        @else
                                        <span>{{$value['floor']}} Floors</span>
                                        <span>{{($value['floor']?$value['floor']:0)*($value['units']?$value['units']:0)}} Units</span>
                                        <span>{{$value['available_units_count']}} Available</span>
                                        @endif
                                    </td>
                                    <td id="td_{{$value['id']}}">
                                        @if($value['property_status'] == 0)
                                        <p style="color:green;">{{'Available'}}</p>
                                        @else
                                        <p style="color:red;">{{'Sold'}}</p>
                                        @endif
<<<<<<< HEAD
                                    </td>
                                    @if($value['status'] == 0)
                                    <td>Not released for public confirmation needed</td>
=======
                                        <li>
                                            <a href="{{ route('apartment_tower',[$id]) }}" class="btn btn-primary small-btn">Towers</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mbl-actn-btn">
                                    <a href="javascript:;" class="btn btn-primary small-btn propertySoldbtn" title="Sold">
                                    <i class="fa fa-cart-plus"></i>
                                    </a>
                                    <a href="javascript:;" class="btn btn-primary small-btn" title="View Page">
                                    <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="javascript:;" class="btn btn-info small-btn" title="Add staff">
                                    <i class="fa fa-user-plus"></i>
                                    </a>
                                    <a href="javascript:;" class="btn small-btn btn-danger propertyDeleteBtn" title="View Page">
                                    <i class="fa fa-trash"></i>
                                    </a>
                                    <a href="javascript:;" class="btn small-btn btn-success" title="Edit">
                                    <i class="fa fa-pencil"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @if(@$value['type'] == 'apartments')
                        <tr class="open-sold-box" style="display:none;">
                            <td colspan="5" class="padding-b-3">
                                <div class="custom-clr-boxes">
                                    <div class="property-color append_apartments_units_div propertylist-m" rel="{{$value['id']}}" rel1="{{$value['type']}}">
                                        <!-- show apartment units for each apartment (function in helpers.php) -->
                                        {!! get_apartment_common_units($value['id'],$value['floor'],$value['units']) !!}
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
                @else
                <div class="table-responsive">
                    <table class="table cms-table cstm-tbl-cls">
                        <tbody>
                            <tr>
                                <td>
                                    <a href="{{route('developer_create_apartment')}}" class="first_apartment_class">
                                    <i class="fa fa-plus fa-4x"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                @endif
                @if(!empty(@$properties))
                <div class="table-responsive">
                    <table class="table cms-table cstm-tbl-cls">
                        <tbody>
                            <tr>
                                <td>
                                    <!-- if developer has already requested and admin has not responded yet and 24 hour period is not over too then show error message -->
                                    @if(!empty($developer_request))
                                    <a href="javascript:void(0);" class="apartment_already_requested">
                                    <i class="fa fa-plus fa-4x"></i>
                                    </a>
>>>>>>> d8ae012839da132a95af7dd5726a656c02697bc0
                                    @else
                                    <td>
                                        <span>Views: {{ $value['view'] }}</span>
                                        @if($value['guest_review_count'] > 0)
                                        <span>Average Rating: {{ get_avrg_ratings($value['id']) }}/5 ({{ $value['guest_review_count'] }} reviews)</span>
                                        @endif
                                    </td>
                                    @endif
                                    <td>
                                        <div class="dropdown dsktp-actn-btn">
                                            <button class="btn action-btn dropdown-toggle" type="button" data-toggle="dropdown">Action<span class="caret"></span></button>
                                            <ul class="dropdown-menu" style="width:292px;">
                                                @if($value['property_status'] != 1 && $value['type'] == 'house')
                                                <li>
                                                    <a href="javascript:void(0);" class="btn btn-primary small-btn propertySoldbtn" data-toggle="confirmation"  id="{{$id}}">Sold</a>
                                                </li>
                                                @elseif($value['property_status'] != 1 && $value['type'] == 'apartments')
                                                <li>
                                                    <a href="{{url('propertyShow/'.$value['type'].'/'.$value['slug'])}}" class="btn btn-primary small-btn" id="{{$id}}">View Page</a>
                                                </li>
                                                <li>
                                                    <a href="{{url('developers/assign-staff-property'.'/'.$value['slug'])}}" class="btn btn-info small-btn" id="{{$id}}">Add staff</a>
                                                </li>
                                                @endif
                                                <!-- <li style="@if($value['property_status'] == 1 && $value['type'] == 'house')float:initial; @endif">
                                                    <a href="javascript:void(0);" class="btn small-btn btn-danger propertyDeleteBtn" data-toggle="confirmation" id="{{$id}}" >Delete</a>
                                                    </li> -->
                                                @if($value['property_status'] != 1 && $value['type'] == 'house')
                                                <li>
                                                    <a href="{{route ('edit_house',$id)}}" class="btn small-btn btn-success">Edit</a>
                                                </li>
                                                @elseif($value['property_status'] != 1 && $value['type'] == 'apartments')
                                                <li>
                                                    <a href="{{ route('edit_apartment',[$id,'edit']) }}" class="btn small-btn btn-success">Edit</a>
                                                </li>
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="mbl-actn-btn">
                                            <a href="javascript:;" class="btn btn-primary small-btn propertySoldbtn" title="Sold">
                                            <i class="fa fa-cart-plus"></i>
                                            </a>
                                            <a href="javascript:;" class="btn btn-primary small-btn" title="View Page">
                                            <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="javascript:;" class="btn btn-info small-btn" title="Add staff">
                                            <i class="fa fa-user-plus"></i>
                                            </a>
                                            <a href="javascript:;" class="btn small-btn btn-danger propertyDeleteBtn" title="View Page">
                                            <i class="fa fa-trash"></i>
                                            </a>
                                            <a href="javascript:;" class="btn small-btn btn-success" title="Edit">
                                            <i class="fa fa-pencil"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @if(@$value['type'] == 'apartments')
                                <tr class="open-sold-box" style="display:none;">
                                    <td colspan="5" class="padding-b-3">
                                        <div class="custom-clr-boxes">
                                            <div class="property-color append_apartments_units_div propertylist-m" rel="{{$value['id']}}" rel1="{{$value['type']}}">
                                                <!-- show apartment units for each apartment (function in helpers.php) -->
                                                {!! get_apartment_common_units($value['id'],$value['floor'],$value['units']) !!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="table-responsive">
                            <table class="table cms-table cstm-tbl-cls">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href="{{route('developer_create_apartment')}}" class="first_apartment_class">
                                            <i class="fa fa-plus fa-4x"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    @endif
                    @if(!empty(@$properties))
                        <div class="table-responsive">
                            <table class="table cms-table cstm-tbl-cls">
                                <tbody>
                                    <tr>
                                        <td>
                                            <!-- if developer has already requested and admin has not responded yet and 24 hour period is not over too then show error message -->
                                            @if(!empty($developer_request))
                                            <a href="javascript:void(0);" class="apartment_already_requested">
                                            <i class="fa fa-plus fa-4x"></i>
                                            </a>
                                            @else
                                            <!-- if user has access key or wants to contact admin for a new apartment request (for developer only) -->
                                            <a href="javascript:void(0);" class="apartment_new_request">
                                            <i class="fa fa-plus fa-4x"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    @endif
                @endif
                
                <!-- Div for staff only -->
                @if(Auth::user()->user_type == 'staff')
                    <div class="table-responsive">
                        @if(!empty(@$properties))
<<<<<<< HEAD
                        <table class="table cms-table cstm-tbl-cls agnt-prprty-tbl prprty-lst-tbl">
                            <thead>
                                <tr>
                                    <th>Properties</th>
                                    <th>Space</th>
                                    <th>Status</th>
                                    <th>Details</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(@$properties as $key => $value)
                                <?php $id = Crypt::encrypt($value['id']); ?>
                                <tr class="three-btns-actn">
                                    <td>
                                        <div class="property-img">
                                            @if($value['type'] == 'house')
                                                <img src="{{asset('uploads/houseImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                            @else
                                                <img src="{{asset('uploads/apartmentImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                            @endif
                                        </div>
                                        <div class="float-left">
                                            <span>{{substr($value['title'],0,20)}}</span>
                                            <span>{{ucfirst($value['type'])}}</span>
                                            @if($value['type'] == 'house')
                                                <h6>$ {{$value['price']}}</h6>
                                            @endif
                                        </div>
                                    </td>
                                    <td> 
                                        @if($value['type'] == 'house')    
                                            <span>{{$value['beds']}} Bedrooms</span>
                                            <span>{{$value['attached_bath']}} Bathrooms</span>
                                            <span>{{$value['square_unit_space']}} sqft </span>
                                        @else
                                            <span>{{$value['floor']}} Floors</span>
                                            <span>{{($value['floor']?$value['floor']:0)*($value['units']?$value['units']:0)}} Units</span>
                                            <span>{{$value['available_units_count']}} Available</span>
                                        @endif
                                    </td>
                                    <td id="td_{{$value['id']}}">
                                        @if($value['property_status'] == 0)
                                            <p style="color:green;">{{'Available'}}</p>
                                        @else
                                            <p style="color:red;">{{'Sold'}}</p>
                                        @endif
                                    </td>
                                    @if($value['status'] == 0)
                                        <td>Not released for public confirmation needed</td>
                                    @else
                                    <td>
                                        <span>Views: {{ $value['view'] }}</span>
                                        @if($value['guest_review_count'] > 0)
                                            <span>Average Rating: {{ get_avrg_ratings($value['id']) }}/5 ({{ $value['guest_review_count'] }} reviews)</span>
                                        @endif
                                    </td>
                                    @endif
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn action-btn dropdown-toggle" type="button" data-toggle="dropdown">Action<span class="caret"></span></button>
                                            <ul class="dropdown-menu" @if($value['type'] == 'apartments')  @endif>
                                                <li>
                                                    <a href="javascript:void(0);" class="btn btn-danger small-btn sold-open" id="{{$id}}">Sold</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);" class="btn btn-success small-btn available-open" id="{{$id}}">Available</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @if(@$value['type'] == 'apartments')
                                <tr class="open-sold-box" style="display:none;">
                                    <td colspan="5">
                                        <div class="custom-clr-boxes">
                                            <div class="sold-unit owl-carousel owl-theme big-sldr">
                                                <div class="item">
                                                    <div class="unit-slider-inr">
                                                        <div class="text-center title">
                                                            <h4>Sold Tower 2</h4>
                                                        </div>
                                                        <div class="property-color append_apartments_units_div propertylist-m" rel="{{$value['id']}}" rel1="{{$value['type']}}">
                                                            
                                                            <ul>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                            </ul>
                                                            <ul>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                                <li>&nbsp;</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="open-avail-box" style="display:none;">
                                    <td colspan="5">
                                        <div class="custom-clr-boxes">
                                            <div class="available-unit owl-carousel owl-theme big-sldr">
                                                @if(!empty(@$value['no_of_tower']) && !empty(@$value['towers']))
                                                    @foreach(@$value['towers'] as $tower)
                                                        <div class="item">
                                                            <div class="unit-slider-inr">
                                                                <div class="text-center title">
                                                                    <h4>Available Tower 1</h4>
                                                                </div>
                                                                <div class="property-color append_apartments_units_div propertylist-m" rel="{{$value['id']}}" rel1="{{$value['type']}}">
                                                                    <!-- show apartment units for each apartment (function in helpers.php) -->
                                                                    <!-- {!! get_apartment_common_avail_units($value['id'],$tower['id'],$tower['no_of_floor'],$tower['no_of_unit']) !!} -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
=======
                            <table class="table cms-table cstm-tbl-cls agnt-prprty-tbl prprty-lst-tbl">
                                <thead>
                                    <tr>
                                        <th>Properties</th>
                                        <th>Space</th>
                                        <th>Status</th>
                                        <th>Details</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(@$properties as $key => $value)
                                        <?php $id = Crypt::encrypt($value['id']); ?>
                                        <tr class="three-btns-actn">
                                            <td>
                                                <div class="property-img">
                                                    @if($value['type'] == 'house')
                                                        <img src="{{asset('uploads/houseImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                                    @else
                                                        <img src="{{asset('uploads/apartmentImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                                    @endif
                                                </div>
                                                <div class="float-left">
                                                    <span>{{substr($value['title'],0,20)}}</span>
                                                    <span>{{ucfirst($value['type'])}}</span>
                                                    @if($value['type'] == 'house')
                                                        <h6>$ {{$value['price']}}</h6>
                                                    @endif
                                                </div>
                                            </td>
                                            <td> 
                                                @if($value['type'] == 'house')    
                                                    <span>{{$value['beds']}} Bedrooms</span>
                                                    <span>{{$value['attached_bath']}} Bathrooms</span>
                                                    <span>{{$value['square_unit_space']}} sqft </span>
                                                @else
                                                    <span>{{$value['floor']}} Floors</span>
                                                    <span>{{($value['floor']?$value['floor']:0)*($value['units']?$value['units']:0)}} Units</span>
                                                    <span>{{$value['available_units_count']}} Available</span>
                                                @endif
                                            </td>
                                            <td id="td_{{$value['id']}}">
                                                @if($value['property_status'] == 0)
                                                    <p style="color:green;">{{'Available'}}</p>
                                                @else
                                                    <p style="color:red;">{{'Sold'}}</p>
                                                @endif
                                            </td>
                                            @if($value['status'] == 0)
                                                <td>Not released for public confirmation needed</td>
                                            @else
                                                <td>
                                                    <span>Views: {{ $value['view'] }}</span>
                                                    @if($value['guest_review_count'] > 0)
                                                        <span>Average Rating: {{ get_avrg_ratings($value['id']) }}/5 ({{ $value['guest_review_count'] }} reviews)</span>
                                                    @endif
                                                </td>
                                            @endif
                                            <td>
                                                <div class="dropdown">
                                                    <button class="btn action-btn dropdown-toggle" type="button" data-toggle="dropdown">Action<span class="caret"></span></button>
                                                    <ul class="dropdown-menu" @if($value['type'] == 'apartments') @endif>
                                                        <li>
                                                            <a href="javascript:void(0);" class="btn btn-danger small-btn sold-open" id="{{$id}}" rel="{{count($value['towers'])}}">Sold</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" class="btn btn-success small-btn available-open" id="{{$id}}" rel="{{count($value['towers'])}}">Available</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        @if(@$value['type'] == 'apartments')
                                            <tr class="open-sold-box" style="display:none;">
                                                <td colspan="5">
                                                    <div class="custom-clr-boxes">
                                                        <div class="sold-unit owl-carousel owl-theme big-sldr">
                                                            @if(!empty(@$value['no_of_tower']) && !empty(@$value['towers']))
                                                                @foreach(@$value['towers'] as $tower)
                                                                    <div class="item">
                                                                        <div class="unit-slider-inr">
                                                                            <div class="text-center title">
                                                                                <h4>{{ $tower['title'] }}</h4>
                                                                            </div>
                                                                            <div class="property-color append_apartments_units_div propertylist-m" rel="{{$value['id']}}" rel1="{{$value['type']}}">
                                                                                <!-- show apartment units for each apartment (function in helpers.php)-->
                                                                                {!! get_apartment_common_units($value['id'],$tower['id'],$tower['no_of_floor'],$tower['no_of_unit']) !!} 
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                            <!-- <div class="item">
                                                                <div class="unit-slider-inr">
                                                                    <div class="text-center title">
                                                                        <h4>Sold Tower 2</h4>
                                                                    </div>
                                                                    <div class="property-color append_apartments_units_div propertylist-m" rel="{{$value['id']}}" rel1="{{$value['type']}}">
                                                                        
                                                                        <ul>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                        </ul>
                                                                        <ul>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                            <li>&nbsp;</li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="open-avail-box" style="display:none;">
                                                <td colspan="5">
                                                    <div class="custom-clr-boxes">
                                                        <div class="available-unit owl-carousel owl-theme big-sldr">
                                                            @if(!empty(@$value['no_of_tower']) && !empty(@$value['towers']))
                                                                @foreach(@$value['towers'] as $tower)
                                                                    <div class="item">
                                                                        <div class="unit-slider-inr">
                                                                            <div class="text-center title">
                                                                                <h4>Available Tower 1</h4>
                                                                            </div>
                                                                            <div class="property-color append_apartments_units_div propertylist-m" rel="{{$value['id']}}" rel1="{{$value['type']}}">
                                                                                <!-- show apartment units for each apartment (function in helpers.php) -->
                                                                                {!! get_apartment_common_avail_units($value['id'],$tower['id'],$tower['no_of_floor'],$tower['no_of_unit']) !!}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
>>>>>>> d8ae012839da132a95af7dd5726a656c02697bc0
                        @else
                            <div class="col-md-12">
                                <h4>No apartment is assigned to you yet!!</h4>
                            </div>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- modal for developer to ask for a new access key, first developer has to fill contact form, then he gets access key, then put the access key and is redirected to create apartment page -->
<div class="modal fade" id="createNewProject" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create a new project</h5>
                <button type="button" class="close" style="line-height:0 !important" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="newApartmentForm" class="form-group">
                {{csrf_field()}}
                <div class="modal-body">
                    <p></p>
                    <p>To create a new project, please contact us with the same way when registering for your developer account. If you have acquired an access key already, please enter it here.</p>
                    <hr>
                    <label for="access-key" class="col-form-label" style="padding:5px 0px;">Your Email:</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}" readonly>
                    <label for="access-key" class="col-form-label" style="padding:5px 0px;">Access Key:</label>
                    <!-- login user id to check user's contact request -->
                    <input type="hidden" name="login_user" value="{{ Auth::user()->id }}" id="login_user">
                    <!-- to check if request has been sent by developer -->
                    <input type="hidden" name="request_id" class="request_id" value="{{ $available_request?$available_request['id']:'0' }}">
                    <input type="text" class="form-control" id="access-key" name="access_key" maxlength="255">
                    <p style="color:red" id="warning-message"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="submit" class="btn btn-primary access_key_btn">Submit</button>
                    <!-- this button takes the developer to the contact us page -->
                    @if(!empty($available_request))
                        <a href="javascript:void(0);" id="contact-us" class="btn btn-danger apartment_already_requested">Contact Us</a>
                    @else
                        <a href="{{ url('/developer/contact-us'.'?id='.encrypt(Auth::user()->id).'&email='.Auth::user()->email.'&value=apartment_request') }}" id="contact-us" class="btn btn-danger">Contact Us</a>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
<!-- contact us create new apartment modal -->
@endsection
@section('page-js')
<script src="{{ asset('assets/js/thumb.owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.sold-unit').owlCarousel({
            loop: true,
            autoplay: false,
            dots:false,
            responsiveClass: true,
            nav: true,
            navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>'],
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: true
                },
                1000: {
                    items: 1,
                    nav: true,
                }
            }
        });
    });

    $(document).ready(function() {
        $('.available-unit').owlCarousel({
            loop: true,
            autoplay: false,
            dots:false,
            responsiveClass: true,
            nav: true,
            navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>'],
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: true
                },
                1000: {
                    items: 1,
                    nav: true,
                }
            }
        });
    });

    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Success!', options)
    @endif
    
    $('body').confirmation({
        placement: 'bottom',
        selector: '[data-toggle="confirmation"]'
    });
    
    // delete apartment button
    $('.propertyDeleteBtn').confirmation({
        onConfirm: function() {
            // alert('confirm');
            // document.getElementById('load').style.visibility="visible";
            var current_selector = $(this);
            var property_id = $(this).attr('id');
            $.ajax({
                url : '{{ route('delete_house') }}',
                type: "POST",
                data: { property_id : property_id, _token : '{{ csrf_token() }}' },
                success : function (data) {
                    if (data.success == 1){
                        current_selector.closest('tr').hide('slow');
                        toastr.success(data.msg, '@lang('app.success')', toastr_options);
                    }
                    // document.getElementById('load').style.visibility="hidden";
                },
                error : function() {
                    // document.getElementById('load').style.visibility="hidden";
                }
            });
        },
        onCancel: function() { 
            // alert('not confirm');
        }
    });
    
    // action to make a property (house for agent only) sold or available
    $('.propertySoldbtn').confirmation({
        onConfirm: function() {
            // alert('confirm');
            var property_id = $(this).attr('id');
            var current_selector = $(this);
            $.ajax({
                url : '{{ route('sold_house') }}',
                type: "POST",
                data: { property_id : property_id, _token : '{{ csrf_token() }}' },
                success : function (data) {
                    if (data.success == 1){
                        $('#td_'+data.count).html('<p style="color:red;">Sold</p>');
                        toastr.success(data.msg, '@lang('app.success')', toastr_options);
                    }
                }
            });
        },
        onCancel: function() { 
          // alert('not confirm');
    
        }
    });
    
    // to make a unit for an apartment sold or available (for staff and invited agents only)
    $('.unitSoldbtn').confirmation({
        onConfirm: function() {
            var unit_id = $(this).attr("rel");
            var type = $(this).attr('rel1');
            $.ajax({
                context : this,
                url : "{{ route('sold_unit_status_change') }}",
                type: "POST",
                data: {unit_id : unit_id, type: type, _token : '{{ csrf_token() }}'},
                success: function(data) {
                    if(data.success == 1) {
<<<<<<< HEAD
                        $(this).removeClass('green');
                        $(this).addClass('red');
                        $(this).attr('rel1','available'); // the new action to be done (i.e. make it available/green ?)
=======
                        $(this).replaceWith('<li class="red">&nbsp;</li>');
                        // $(this).removeClass('green');
                        // $(this).addClass('red');
                        // $(this).attr('rel1','available'); // the new action to be done (i.e. make it available/green ?)
>>>>>>> d8ae012839da132a95af7dd5726a656c02697bc0
                    }
                },
                error:function() {
                    // alert("error");
                    // show error message here if unit is not available
                }
            });
        },
        onCancel: function() {
            // alert("cancelled");
            // if the developer clicks on no (for staff and invited agents only)
        }
    });

    $('.unitAvailbtn').confirmation({
        onConfirm: function() {
            var unit_id = $(this).attr("rel");
            var type = $(this).attr('rel1');
            $.ajax({
                context : this,
<<<<<<< HEAD
                
=======
                url: "{{ route('avail_unit_status_change')}}",
>>>>>>> d8ae012839da132a95af7dd5726a656c02697bc0
                type: "POST",
                data: {unit_id : unit_id, type: type, _token : '{{ csrf_token() }}'},
                success: function(data) {
                    if(data.success == 1) {
<<<<<<< HEAD
                        $(this).removeClass('red');
                        $(this).addClass('green');
                        $(this).attr('rel1','sold'); // the new action to be done (i.e. make it sold/red ?)
=======
                        $(this).replaceWith('<li class="green">&nbsp;</li>');
                        // $(this).removeClass('red');
                        // $(this).addClass('green');
                        // $(this).attr('rel1','sold'); // the new action to be done (i.e. make it sold/red ?)
>>>>>>> d8ae012839da132a95af7dd5726a656c02697bc0
                    }
                },
                error:function() {
                    // alert("error");
                    // show error message here if unit is not available
                }
            });
        },
        onCancel: function() {
            // alert("cancelled");
            // if the developer clicks on no (for staff and invited agents only)
        }
    });
    // 
    
    // $(document).ready(function() {
    //     //alert("test");
    //     $('.append_apartments_units_div').each(function() {
    //         //alert($(this).attr('rel'));
    //         var ad_id = $(this).attr('rel');
    //         var type = $(this).attr('rel1');
    //         $(this).load("{{ url('') }}"+'/'+'agents/apend-apartment-units'+"/"+ad_id+"/"+type);
    //     });
    // });
    
    // $('.open-sold-box').slideUp();
    // $('.sold-open').click(function(){
    //     //alert("here");
    //     $(this).closest('.three-btns-actn').next().toggle('.open-sold-box');
    //     $('.open-avail-box').slideUp();
    // });

    // $('.open-avail-box').slideUp();
    // $('.available-open').click(function(){
    //     //alert("here");
    //     $(this).closest('.prprty-lst-tbl').find('.open-avail-box').toggle();
    //     $('.open-sold-box').slideUp(); 
    // });

    // $('.available-open').click(function(){
    //     //alert("here");
    //     $('.open-available-box').slideUp();
    //     $(this).closest('.three-btns-actn').next('.open-sold-box').toggle(); 
    // });

    $(document).on('click','.sold-open',function() {
        $('.open-avail-box').hide();
<<<<<<< HEAD
        $(this).closest('.three-btns-actn').next('.open-sold-box').show();
=======
        if($(this).attr('rel') <2) {
            $(this).closest('.three-btns-actn').next('.open-sold-box').find('.owl-nav').attr('style','display:none !important;');
        }
        $(this).closest('.three-btns-actn').next('.open-sold-box').toggle();
>>>>>>> d8ae012839da132a95af7dd5726a656c02697bc0
    });

    $(document).on('click','.available-open',function() {
        $('.open-sold-box').hide();
<<<<<<< HEAD
        $(this).closest('.prprty-lst-tbl').find('.open-avail-box').show();
=======
        // $(this).closest('.prprty-lst-tbl').find('.open-avail-box').toggle();
        if($(this).attr('rel') <2) {
            $(this).closest('.three-btns-actn').next('.open-sold-box').next('.open-avail-box').find('.owl-nav').attr('style','display:none !important;');
        }
        $(this).closest('.three-btns-actn').next('.open-sold-box').next('.open-avail-box').toggle();
>>>>>>> d8ae012839da132a95af7dd5726a656c02697bc0
    });
    
    $(document).on('click','.apartment_already_requested',function() {
        swal("OOPS!","Apartment is already requested.","error");
        return false;
    });
    
    $(document).on('click','.apartment_new_request',function() {
        $('.email_get').val($('.developer_email').val());
        $('#createNewProject').modal('show');
    });

    $(document).on('click','.access_key_btn',function() {
        var data = $("#newApartmentForm").serialize();
        $.ajax({
            data: data,
            type: "post",
            url: "{{ url('/developers/insert-request-access-key') }}",
            success: function(data){
                if(data.status == 1){
                    // window.location.href = "{{url('')}}" + "/user/create"+'?id='+data.token+'&email='+data.email;
                    window.location.href = "{{url('/developers/create-apartment')}}"+'/'+data.contact_id; // create apartment page
    
                } else if(data.status == 2) {
                    $('#warning-message').addClass('has-error');
                    $('#warning-message').text(data.error);
                }else{
                    $('#warning-message').removeClass('has-error');
                    $('#warning-message').text("");
                     
                    $.each(data.error,function(key,value) {
                        $('#warning-message').addClass('has-error');
                        $('#warning-message').text(value);  
                    })
                }
            },
            errors:function(){
                alert('Something went wrong. Please try again.');
            }
        });
    });
</script>
@endsection