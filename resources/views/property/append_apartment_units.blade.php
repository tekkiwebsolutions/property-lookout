@if(!empty(@$apartment_units))
    <?php $filled_floors = count(@$apartment_units); ?>
    @foreach($apartment_units as $key => $floor)
        <ul>
            <?php $filled_units = count($floor); ?>
            @foreach($floor as $key1 => $unit)
                @if($unit['property_status'] == 0)    
                    <li class="green unitSoldbtn" data-toggle="confirmation">&nbsp;</li>
                @elseif($unit['property_status'] == 1)
                    <li class="red">&nbsp;</li>
                @else
                    <li>&nbsp;</li>
                @endif
            @endforeach
            <?php $left_units = @$detail['units']-$filled_units; ?>
            @for($k = 1; $k <= $left_units; $k++)
                <li>&nbsp;</li>
            @endfor
        </ul>
    @endforeach
    <?php $left_floors = @$detail['floor']-$filled_floors; ?>
    @for($i = 1; $i <= $left_floors; $i++)
        <ul>
            @for($j = 1; $j <= @$detail['units']; $j++)
                <li>&nbsp;</li>
            @endfor
        </ul>
    @endfor
@endif