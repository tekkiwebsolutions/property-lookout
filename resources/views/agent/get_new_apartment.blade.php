@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<style type="text/css">
    .active_plans{
        margin-bottom:30px;
        -webkit-box-shadow: 0px 0px 11px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 0px 0px 11px 0px rgba(0,0,0,0.3); 
        transition: 0.6s;
        box-shadow: 0px 0px 11px 0px rgba(0,0,0,0.3); 
        background: lightgray; border-radius:20px; 
        padding:30px 0; 
        border:2px solid #cacaca; 
        text-align: center; 
    }
    .select-btn1 .btn {
        padding: 5px 40px;
        margin-top:10px;
        border: 2px solid #33aa58;
        color: #33aa58;
        font-weight: 600;
        border-radius: 30px;
        font-size: 20px;
        text-transform: uppercase;
        background-color: white;
    }
    .access_key {
        padding: 5px 10px;
        width:300px;
    }
</style>
<?php 
   
    if(Auth::check()){
        if(Auth::user()->user_type == 'developer'){
            $agent_email = Auth::user()->email;
        }else{
            $agent_email = '';
        }
    }else{
        $agent_email = '';
    }

?>
<div class="row mlr0">
    <div class="page_wrapper page-@if(!empty($page->id)){{ $page->id }}@endif">
        <div class="modern-top-intoduce-section banner package_banner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <h2>Unlock Apartment</h2>
                    </div>
                </div>
            </div>
        </div>
        <form class="submitPackage" id="submitPackage" method="post">
            {{csrf_field()}}
            <div class="container">
                <div class="package_email">
                    <div class="input-group email_div">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="email"  type="text" class="form-control email" name="email" placeholder="Email" value="@if(!empty($agent_email)){{$agent_email}}@endif"  @if(!empty($agent_email)){{'readonly'}}@endif>
                    </div>
                    <p class="help-block email_p" style="color: red;"></p>
                </div>
                <input type="hidden" name="login_user" value="{{ Auth::user()->id }}" id="login_user">
                <input type="hidden" name="request_id" class="request_id" value="{{ $available_request?$available_request['id']:'0' }}">

            </div>
            <div class="pricing_plan">
                <h2><b>Unlock Apartment</b></h2>
                <!-- <p>Writers and stars of Veep have responded incredulously to the news an Australian politician<br /> -->
                    <!-- required preinstalled stitches way email client, calendar, mapping program. -->
                <!-- </p> -->
                <p>Get one more apartment from us:</p>
                <p>If you are a developer and have added apartment, but want to add more apartments,<br> 
                then Please contact us through the Contact Us button below.<br> 
                An access key will be provided to you and after that 
                you can use the access key to add a new apartment.</p>
                <div class="plan_detail">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3"></div>
                                <div class="col-sm-6">
                                    <div class="plans">
                                        <h5></h5>
                                        <h4></h4>
                                        <h6></h6>
                                        <div class="select-btn">
                                            <!-- if user has already asked for access token, contact us will not work -->
                                            @if(!empty($available_request))
                                                <a class="btn request_already_sent" rel="" rel1="developer" data-plan-id="" href="javascript:void(0);" data-plan-status="0" >Contact Us</a>
                                            @else
                                                <a class="btn" rel="" rel1="developer" data-plan-id="" href="{{ url('/developer/contact-us'.'?id='.encrypt(Auth::user()->id).'&email='.Auth::user()->email.'&value=apartment_request') }}" data-plan-status="0" >Contact Us</a>
                                            @endif
                                        </div>
                                        <div class="access_key_div" rel="">
                                            <p>Do you have an access key?</p>
                                            <input class="access_key" type="text" name="access_key">
                                            <div class="select-btn1">
                                                <input type="button" value="submit" class="btn access_key_btn">
                                            </div>
                                        </div>
                                        <p class="help-block access_key_p" style="color: red;"></p>
                                    </div>
                                </div>
                            <div class="col-sm-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Modal start -->
<!-- Modal ends -->
@endsection
@section('page-js')
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript">
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Sucess!', options)
    @endif

    // access key token scripts
    $(document).on('click','.access_key_btn',function() {
 
        var access_key = $(this).parent().prev().val();
        $(".admin_access_token").val(access_key);
        var data = $("#submitPackage").serialize();
        $.ajax({
            data: data,
            type: "post",
            url: "{{ url('/developers/insert-request-access-key') }}",
            success: function(data){
                if(data.status == 1){
                    // window.location.href = "{{url('')}}" + "/user/create"+'?id='+data.token+'&email='+data.email;
                    window.location.href = "{{url('/developers/create-apartment')}}"+'/'+data.contact_id; // create apartment page

                } else if(data.status == 2) {
                    $('.access_key_div').addClass('has-error');
                    $('.access_key_p').text(data.error);
                }else{
                    //console.log(data);
                    $('.email_div').removeClass('has-error');
                    $('.email_p').text("");
                    $('.access_key_div').removeClass('has-error');
                    $('.access_key_p').text("");
                    $.each(data.error, function(key, value){
                        
                        $('.'+key+'_div').addClass('has-error');
                        $('.'+key+'_p').text(value);
                    });  
                }
            },
            errors:function(){
               // alert('error');
            }
        });
    });

    $(document).on('click','.request_already_sent',function() {

        swal("OOPS!","You already have an access key.","error");
        return false;
    });
</script>
@endsection