@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/datatables/responsive.dataTables.min.css') }}" rel="stylesheet"/>
<style type="text/css">
    .caret_style{
        position: absolute;
        top: 33px;
        right: 23px;
        font-size: 18px;
    }
</style>
<div class="cms-inner-content">
    <div class="container">
        <div class="row">
            @include('agent.agentInfoSection')
            <div class="col-md-9 col-sm-7 cms-border agentcredits-list agents_credits">
                <div class="credit-div-wrp">
                    <div class="crdt-flx">
                        <div class="crdt-flx-inr">
                            <div class="credit-div-inr">
                                <strong>Total Available Credits:</strong>
                                <small>{{ (int)@$user_credit_data['total_available_credit'] }} Credits</small>
                            </div>
                        </div>
                        <div class="crdt-flx-inr">
                            <a href="javascript:void(0);" class="btn btn-primary small-btn open-add-credit">Get Credits</a>
                        </div>
                        <div class="crdt-flx-inr">
                            @if(date('d') < 29)
                                @if(!empty((int)@$user_credit_data['total_available_credit']))
                                    <a href="{{ url('/agents/add-credit-list') }}" class="btn btn-primary small-btn">Add Credits</a>
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="entr-add-credit">
                        <div class="row">
                            <form id="addCreditForm" action="{{url('agents/add-credit')}}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="select-style">
                                            <label>Select Credits</label>
                                            <select class="credit_select_class" name="user_credits">
                                                <option value="">Select Credits</option>
                                                <option value="1">$1 : 1 Credit</option>
                                                <option value="5">$5 : 5 Credits</option>
                                                <option value="10">$10 : 10 Credits</option>
                                                <option value="25">$25 : 25 Credits</option>
                                                <option value="50">$50 : 50 Credits</option>
                                                <option value="100">$100 : 100 Credits(Premium)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="select-style">
                                            <label>Select Payment Method</label>
                                            <select class="payment_select_class" name="payment_method">
                                                <option value="paypal">Paypal</option>
                                                <option value="stripe">Stripe</option>
                                            </select>
                                            <i class="fa fa-caret-down caret_style"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6">
                                    <div class="btn-div">
                                        <a href="javascript:void();" class="btn btn-primary sv-crdit-btn save_credit_btn">Proceed To Pay</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="">
                    <table id="table_id" class="display table-bordered table cms-table ">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Credits</th>
                                <th>Type</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty(@$user_credits))
                            @foreach(@$user_credits as $key => $value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ (int)$value['amount'] }}</td>
                                <td>@if($value['type'] == 'added') Credit Added @elseif($value['type'] == 'used') Credit Used @endif</td>
                                <td>{{ date('m-d-Y',strtotime($value['created_at'])) }} {{ date('h:i A',strtotime($value['created_at'])) }}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modals Starts -->

<!-- Modals Ends -->
@endsection
@section('page-js')
<script type="text/javascript" src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript">
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif

    $('.entr-add-credit').hide();
    $('.open-add-credit').click(function(){
        $('.entr-add-credit').toggle();
    });

    // $(document).ready( function () {
    //     $('#table_id').DataTable();
    // } );

    $('#table_id').DataTable({
        pagingType: "simple",
        responsive: true,
        language: {
          paginate: {
            next: '<i class="fa fa-angle-double-right"></i>',
            previous: '<i class="fa fa-angle-double-left"></i>' 
          }
        }
    });

    // submit form when credit is added
    $(document).on('click','.save_credit_btn',function() {
        var credit = $('.credit_select_class').val();
        if(credit == '') {
            swal("OOPS!","Please select credit","error");
            return false;
        } else {
            $('#addCreditForm').submit();
        }
    });
</script>
@endsection