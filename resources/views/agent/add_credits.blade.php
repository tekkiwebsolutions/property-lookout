@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<link rel="stylesheet" href="{{asset('assets/css/admin.css')}}">
<div class="cms-inner-content agent-bio-data">
    <div class="container">
        <h3 class="unit-title">Add Credits To Property</h3>
        <br><br><p  style="color:red;"><span>*</span>The property will be featured till 28th of this Month. Select the highest Credit to make your property Premium and show it on the top.(You can add credits only once for Apartment)</p>
        <hr class="clearfix" />
        <form class="clearfix" id="addCreditForm" action="{{url('agents/add-credit-property')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="ad_id" value="{{ $ad_id }}">
            <input type="hidden" name="type" value="used">
            <div class="form-group">
                <div class="row">
                    <label class="control-label col-sm-3">Property Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" disabled value="{{@$property->title}}"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="control-label col-sm-3">Select Credits</label>
                    <div class="col-sm-5">
                        <select class="credit_select_class form-control" name="amount">
                            <option value="">Select Credits</option>
                            <option value="1">1 Credit</option>
                            <option value="5">5 Credits</option>
                            <option value="10">10 Credits</option>
                            <option value="25">25 Credits</option>
                            <option value="50">50 Credits</option>
                            <option value="100">100 Credits(Premium)</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="btns f-right">
                <a href="{{ url('/agents/add-credit-list') }}" class="btn btn-primary black-btn m-t-20">Cancel</a>
                <a href="javascript:void(0);" class="btn btn-primary black-btn m-t-20 save_credit_btn">Add Credits</a>
            </div>
        </form>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript">
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif
    @if(session('error'))
        toastr.error('{{ session('error') }}', '<?php echo trans('app.error') ?>', toastr_options);
    @endif

    // submit form when credit is added
    $(document).on('click','.save_credit_btn',function() {
        var credit = $('.credit_select_class').val();
        if(credit == '') {
            swal("OOPS!","Please select credit","error");
            return false;
        } else {
            $('#addCreditForm').submit();
        }
    });
</script>
@endsection