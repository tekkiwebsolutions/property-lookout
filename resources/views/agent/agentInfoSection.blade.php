<div class="col-lg-3 col-md-3 col-sm-4 agent-detail-manual cms-avtar agent-cms-avtar">
    <div class="avtar">
        @if(!empty(Auth::user()->photo))
        <img src="{{asset('uploads/avatar/'.Auth::user()->photo)}}" alt="#" class="img-responsive"/>
        @else
        <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
        @endif
    </div>
    <ul class="agent_profiles text-center">
        <li><a href="@if(!empty($meta_data['facebook_url'])){{$meta_data['facebook_url']}}@else{{'https://www.facebook.com/'}}@endif"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
        <li><a href="@if(!empty($meta_data['twitter_url'])){{$meta_data['twitter_url']}}@else{{'https://www.twitter.com/'}}@endif"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
        <li><a href="@if(!empty($meta_data['linked_in_url'])){{$meta_data['linked_in_url']}}@else{{'https://www.linkedin.com/'}}@endif"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
    </ul>
    <!-- <div><a class="btn btn-primary contact-btn" href="#">Contact</a></div> -->
    <h2>{{Auth::user()->name}}</h2>
    <!-- <span>Sale Assocaite - (Property Lookout)</span> -->
    <div class="m-15">
        <span>+{{Auth::user()->phone}}</span>
        <span>{{Auth::user()->email}}</span>
    </div>
    <!-- <span>Accumulated visitors: 0</span> -->
    <!-- <span>Accumulated Rating: 0</span> -->
    <span>Account type: {{$current_package['name']}}</span>
    <?php  
        // get no of apartmnets and properties for users
        $house_number = \App\Ad::whereUserId(Auth::user()->id)->whereType('house')->whereTrash('0')->count(); 
        $apartment_number = \App\Ad::where('assigned_agent_id',Auth::user()->id)->whereType('apartments')->count(); 
        ?>
    @if(Auth::user()->user_type == 'user')
    <span>Number of properties: {{ @$house_number }}</span>
    <!-- <span>Number of apartments: {{ @$apartment_number }}</span> will be changed for agents as developers can assign their apartments to agents -->
    @elseif(Auth::user()->user_type == 'developer')
    <span>Number of apartments: {{ @$apartment_number }}</span>
    @endif
</div>