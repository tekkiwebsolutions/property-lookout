@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | 
@endif 
@parent 
@endsection
@section('main')
<?php 
    $window_type = encrypt('agentWindow');
?>

<link rel="stylesheet" href="{{asset('assets/css/admin.css')}}">
<div class="container">
    <div id="wrapper">
        @include('admin.sidebar_menu')
        <div id="page-wrapper" style="min-height: 272px;">
            @if( ! empty($title))
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> {{ $title }}  </h1>
                    </div> <!-- /.col-lg-12 -->
                </div> <!-- /.row -->
            @endif
            @include('admin.flash_msg')
            <div class="row">
                <div class="col-xs-12">
                        <table class="table table-bordered table-striped table-responsive">
                            @if(!empty($users))
                                @foreach($users as $key =>$value)
                                    <tr>
                                        <td>
                                            <h5>{{$value['user_detail']['email']}}</h5>
                                        </td>
                                        <td>
                                            <h5>{{$value['user_detail']['name']}}</h5>
                                        </td>
                                        <?php 
                                            $agent_id   = encrypt($value['Agent_id']);
                                            $user_id    = encrypt($value['user_id']);
                                        ?>
                                        <td>
                                            <a href="{{ url('chat-room/'.$agent_id.'/'.$user_id.'/'.$window_type )}}" class="btn btn-success approveAds" data-slug="" data-value="1">Start Chat</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                </div>
            </div>
        </div>   <!-- /#page-wrapper -->
    </div>   <!-- /#wrapper -->
</div> <!-- /#container -->
@endsection

@section('page-js')

    <script>
     
    </script>

    <script>
        @if(session('success'))
            toastr.success('{{ session('success') }}', '{{ trans('app.success') }}', toastr_options);
        @endif
        @if(session('error'))
            toastr.error('{{ session('error') }}', '{{ trans('app.success') }}', toastr_options);
        @endif
    </script>

@endsection