<div class="col-md-3 col-sm-5 cms-avtar">
    <div class="avtar">
        @if(!empty(Auth::user()->photo))
            <img src="{{asset('uploads/avatar/'.Auth::user()->photo)}}" alt="#" class="img-responsive"/>
        @else
            <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
        @endif
    </div>
    <ul class="nav nav-pills cms-tabs">
        <li class="active" id="general_tab"><a data-toggle="pill" href="#general"><i class="fa fa-cog" aria-hidden="true"></i>General</a></li>
        <li id="sec_tab"><a data-toggle="pill" href="#security"><i class="fa fa-lock" aria-hidden="true"></i>Security</a></li>
        <li><a data-toggle="pill" href="#bio"><i class="fa fa-user" aria-hidden="true"></i>Bio</a></li>
    </ul>
</div>

