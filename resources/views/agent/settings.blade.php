@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<style type="text/css">
    .file {
        visibility: hidden;
        position: absolute;
    }
</style>
<div class="cms-inner-content">
    <div class="container">
        <div class="row">
            @include('agent.sideTab')
            <div class="col-md-8 col-sm-7 cms-border">
                <div class="tab-content agent-bio-data">
                    <div id="general" class="tab-pane fade in active">
                        <h3>General Account Setting</h3>
                        <form class="form-horizontal" id="updateProfile" action="{{route('update_agent_profile')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group {{ $errors->has('name')? 'has-error':'' }}">
                                <label class="control-label col-sm-3">Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  value="{{Auth::user()->name}}" name="name" maxlength="255" placeholder="Enter Full Name"/>
                                </div>
                                {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Email</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  value="{{Auth::user()->email}}" name="email" readonly placeholder="Enter email address" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Gender</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="gender">
                                    <option value="male" @if(Auth::user()->gender == 'male'){{'selected'}}@endif>Male</option>
                                    <option value="female" @if(Auth::user()->gender == 'female'){{'selected'}}@endif>Female</option>
                                    <option value="third_gender" @if(Auth::user()->gender == 'third_gender'){{'selected'}}@endif>Third Gender</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('phone')? 'has-error':'' }}">
                                <label class="control-label col-sm-3">Mobile No.</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="phone" value="{{Auth::user()->phone}}" maxlength="12" placeholder="Enter mobile number" />
                                </div>
                                {!! $errors->has('phone')? '<p class="help-block">'.$errors->first('phone').'</p>':'' !!}
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Website</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="website" value="{{Auth::user()->website}}" maxlength="255" placeholder="Enter website" />
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('address')? 'has-error':'' }}">
                                <label class="control-label col-sm-3">Address</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" rows="2" name="address" placeholder="Enter Address">{{Auth::user()->address}}</textarea>
                                </div>
                                {!! $errors->has('address')? '<p class="help-block">'.$errors->first('address').'</p>':'' !!}
                            </div>
                            <div class="form-group {{ $errors->has('country')? 'has-error':'' }}">
                                <label class="control-label col-sm-3">Country</label>
                                <div class="col-sm-6">
                                    <select id="country" name="country" class="form-control select2" tabindex="6">
                                        <option value="">@lang('app.select_a_country')</option>
                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}" @if(Auth::user()->country_id == $country->id){{'selected'}} @endif>{{ $country->country_name }}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->has('country')? '
                                    <p class="help-block">'.$errors->first('country').'</p>
                                    ':'' !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('state')? 'has-error':'' }}">
                                <label class="control-label col-sm-3">State</label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" id="state_select" name="state">
                                        <option value="">Select state</option>
                                        @if(!empty(Auth::user()->state_id))
                                            @if($previous_states->count() > 0)
                                                @foreach($previous_states as $state)
                                                    <option value="{{ $state->id }}"  @if(Auth::user()->state_id == $state->id){{'selected'}} @endif>{{ $state->state_name }}</option>
                                                @endforeach
                                            @endif
                                        @endif
                                    </select>
                                    {!! $errors->has('state')? '
                                    <p class="help-block">'.$errors->first('state').'</p>
                                    ':'' !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('city')? 'has-error':'' }}">
                                <label class="control-label col-sm-3">City</label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" id="city_select" name="city">
                                        <option value="">Select City</option>
                                        @if(!empty(Auth::user()->city_id))
                                            @if($previous_cities->count() > 0)
                                                @foreach($previous_cities as $city)
                                                    <option value="{{ $city->id }}"  @if(Auth::user()->city_id == $city->id){{'selected'}} @endif>{{ $city->city_name }}</option>
                                                @endforeach
                                            @endif
                                        @endif
                                    </select>
                                    {!! $errors->has('city')? '
                                    <p class="help-block">'.$errors->first('city').'</p>
                                    ':'' !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('postal_code')? 'has-error':'' }}">
                                <label class="control-label col-sm-3">Postal Code</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="postal_code" value="{{Auth::user()->postal_code}}" maxlength="6" placeholder="Enter zip code" />
                                </div>
                                {!! $errors->has('postal_code')? '<p class="help-block">'.$errors->first('postal_code').'</p>':'' !!}
                            </div>
                            <div class="form-group {{ $errors->has('company')? 'has-error':'' }}">
                                <label class="control-label col-sm-3">Company</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="company" value="{{Auth::user()->company}}"  maxlength="255" placeholder="Enter your company name" />
                                </div>
                                {!! $errors->has('company')? '<p class="help-block">'.$errors->first('company').'</p>':'' !!}
                            </div>
                            <div class="form-group {{ $errors->has('photo')? 'has-error':'' }}">
                                <label class="control-label col-sm-3">Image</label>
                                <input type="file" name="photo" class="file profileImageAgent" id="profileImage">
                                <div class="input-group col-sm-6 pl-pr">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                    <input type="text" class="form-control input-lg" disabled placeholder="Upload profile photo" value="{{Auth::user()->photo}}">
                                    <span class="input-group-btn">
                                    <button class="browse btn btn-primary input-lg" type="button" style="height: 16%;"><i class="glyphicon glyphicon-folder-open"></i></button>
                                    </span>
                                </div>
                                {!! $errors->has('photo')? '
                                <p class="help-block">'.$errors->first('photo').'</p>
                                ':'' !!}
                                <p class="help-block" id="error_size" style="color:red;"></p>
                            </div>
                            @if(Auth::user()->user_type == 'staff')
                                <div class="form-group {{ $errors->has('agent_card')? 'has-error':'' }}">
                                    <label class="control-label col-sm-3">Contact Photo</label>
                                    <input type="file" name="agent_card" tabindex="17" class="file">
                                    <div class="input-group col-sm-6 pl-pr">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                        <input type="text" class="form-control input-lg" disabled value="{{@Auth::user()->agent_card_photo}}" placeholder="Upload Contract photo">
                                        <span class="input-group-btn">
                                            <button class="browse btn btn-primary input-lg" type="button" style="height: 16%;"><i class="glyphicon glyphicon-folder-open"></i></button>
                                        </span>
                                    </div>
                                    {!! $errors->has('agent_card')? '<p class="help-block">'.$errors->first('agent_card').'</p>':'' !!}
                                </div>
                            @endif
                            <button type="submit" class="btn btn-primary black-btn m-t-20 saveEditProfile">Save Changes</button>
                        </form>
                    </div>
                    <div id="security" class="tab-pane fade">
                        <h3>Security Setting</h3>
                        <form class="form-horizontal">
                            {{csrf_field()}}
                            <div class="form-group old_password_div">
                                <label class="control-label col-sm-4">Current Password</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="current_password" name="old_password" placeholder="Current Password" maxlength="255"/>
                                    <p class="help-block old_password_p" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group new_password_div">
                                <label class="control-label col-sm-4">New Password</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password" maxlength="255"/>
                                    <p class="help-block new_password_p" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group new_password_confirmation_div">
                                <label class="control-label col-sm-4">Confirm Password</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="confirm_password" name="new_password_confirmation" placeholder="Confirm Password" maxlength="255"/>
                                    <p class="help-block new_password_confirmation_p" style="color: red;"></p>
                                </div>
                            </div>
                            <a href="#" class="btn btn-primary black-btn m-t-20" id="change_password">Change Password</a>
                        </form>
                    </div>
                    <div id="bio" class="tab-pane fade">
                        <h3>Bio Setting</h3>
                        <form class="form-horizontal">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label class="control-label col-sm-3">@if(Auth::user()->user_type == 'user') Agent’s @elseif(Auth::user()->user_type == 'developer') Developer's @endif About</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" placeholder="About Us" name="about" id="about" rows="5">{{ @$user_meta['about'] }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Facebook</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="facebook_url" value="{{ @$user_meta['facebook_url'] }}" name="facebook_url" placeholder="@lang('app.facebook_url')" maxlength="255"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">LinkedIn</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="linked_in_url" value="{{ @$user_meta['linked_in_url'] }}" name="linked_in_url" placeholder="@lang('app.linked_in_url')" maxlength="255"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Twitter</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="twitter_url" value="{{ @$user_meta['twitter_url'] }}" name="twitter_url" placeholder="@lang('app.twitter_url')" maxlength="255"/>
                                </div>
                            </div>
                            <a href="#" class="btn btn-primary black-btn m-t-20" id="settings_save_btn">Save Changes</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript">
    $(document).ready(function () {
        if(window.location.href.indexOf("security") > -1) {
            // alert("your url contains the name sec.");
            $("#general_tab" ).removeClass( 'active' );
            $("#sec_tab" ).addClass( 'active' );
            $("#general").removeClass('in active');
            $("#security").addClass('in active');
        }
    });

    $(document).on('change','.profileImageAgent',function(){
        files = this.files;
        size = files[0].size;
        //max size 50kb => 50*1000
        if( size > 500*1000){
            $("#error_size").text('Please upload less than 500kb file');
            $(".saveEditProfile").attr('disabled',true);
            return false;
        }else{
            $("#error_size").text('');
            $(".saveEditProfile").attr('disabled',false);
        }
        return true;
    });

    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Success!', options)
    @endif

    $(document).on('click', '.browse', function(){
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });
    $(document).on('change', '.file', function(){
        console.log($(this).val());
        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });

    $('#settings_save_btn').click(function(e){
        e.preventDefault();
        var this_btn = $(this);
        this_btn.attr('disabled', 'disabled');
        var form_data = this_btn.closest('form').serialize();
        $.ajax({
            url : '{{ url('agents/setting-update') }}',
            type: "POST",
            data: form_data,
            success : function (data) {            
                if (data.success == 1){
                    this_btn.removeAttr('disabled');
                    toastr.success(data.msg, '@lang('app.success')', toastr_options);
                }
            }
       });
    });

    $('#change_password').click(function(e){
        e.preventDefault();
        var this_btn = $(this);
        this_btn.attr('disabled', 'disabled');
        var form_data = this_btn.closest('form').serialize();
        $.ajax({
            url : "{{ url('dashboard/u/posts/account/change-password') }}",
            type: "POST",
            data: form_data,
            success : function (data) {            
                this_btn.removeAttr('disabled');
                if (data.success == 1){
                    toastr.success(data.msg, '@lang('app.success')', toastr_options);
                    window.location.href = "{{url('')}}"+'/'+'agents/settings';
    
                }else if(data.success == 0){
                    // this_btn.removeAttr('disabled');
                    toastr.error(data.msg, '@lang('app.error')', toastr_options);
                    window.location.href = "{{url('')}}"+'/'+'agents/settings';
                }
                $.each(data.error, function(key, value){
                    $('.'+key+'_div').addClass('has-error');
                    $('.'+key+'_p').text(value);
                });  
            }
       });
    });

    function generate_option_from_json(jsonData, fromLoad){
        //Load Category Json Data To Brand Select
        if (fromLoad === 'category_to_brand'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> <?php echo trans('app.select_a_brand') ?> </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
                }
                $('#brand_select').html(option);
                $('#brand_select').select2();
            }else {
                $('#brand_select').html('');
                $('#brand_select').select2();
            }
            $('#brand_loader').hide('slow');
        }else if(fromLoad === 'country_to_state'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> @lang('app.select_state') </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
                }
                $('#state_select').html(option);
                $('#state_select').select2();
            }else {
                $('#state_select').html('');
                $('#state_select').select2();
            }
            $('#state_loader').hide('slow');
    
        }else if(fromLoad === 'state_to_city'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> @lang('app.select_city') </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
                }
                $('#city_select').html(option);
                $('#city_select').select2();
            }else {
                $('#city_select').html('');
                $('#city_select').select2();
            }
            $('#city_loader').hide('slow');
        }
    }
    $(document).ready(function() {
        $('#phone').keyup(function(){
            $(this).val($(this).val().replace(/[^0-9]/g,""));
        });
    
        $(".notify img").attr("src", "<?php echo asset('/');?>/assets/img/notify.png");
        $(".footer-widget img").attr("src", "<?php echo asset('/');?>/assets/img/footer-logo.png");
    });
    $('[name="country"]').change(function(){
        var country_id = $(this).val();
        $('#state_loader').show();
        $.ajax({
            type : 'POST',
            url : '{{ route('get_state_by_country') }}',
            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
            success : function (data) {
                generate_option_from_json(data, 'country_to_state');
            }
        });
    });
    
    $('[name="state"]').change(function(){
        var state_id = $(this).val();
        $('#city_loader').show();
        $.ajax({
            type : 'POST',
            url : '{{ route('get_city_by_state') }}',
            data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
            success : function (data) {
                generate_option_from_json(data, 'state_to_city');
            }
        });
    });
</script>
@endsection