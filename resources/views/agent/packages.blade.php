@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<style type="text/css">
    .active_plans{
        margin-bottom:30px;
        -webkit-box-shadow: 0px 0px 11px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 0px 0px 11px 0px rgba(0,0,0,0.3); 
        transition: 0.6s;
        box-shadow: 0px 0px 11px 0px rgba(0,0,0,0.3); 
        background: lightgray; 
        border-radius:20px; 
        padding:30px 0; 
        border:2px solid #cacaca; 
        text-align: center; 
    }
    .select-btn1 .btn {
        padding: 5px 40px;
        margin-top:10px;
        border: 2px solid #33aa58;
        color: #33aa58;
        font-weight: 600;
        border-radius: 30px;
        font-size: 20px;
        text-transform: uppercase;
        background-color: white;
    }

    input:focus {
        outline: none;
    }
    /*
    21 Dec
    .plans {
        min-height: 756px !important;
    }*/
    .plan_detail .row {display:block;}
    .access_key_div input{
        border: 1px solid #ccc;
        width: 67%;
        border-radius: 40px;
        padding: 6px 15px;
        box-sizing: border-box;
        margin-bottom:10px;
    }
    .Developerer-price {height:54px;}
</style>
<?php 
   
    if(Auth::check()){
        if(Auth::user()->user_type == 'user'){
            $agent_email = Auth::user()->email;
        }else{
            $agent_email = '';
        }
    }else{
        $agent_email = '';
    }

?>
<div class="row mlr0">
    <div class="page_wrapper page-@if(!empty($page->id)){{ $page->id }}@endif">
        <div class="modern-top-intoduce-section banner package_banner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <h2>Choose Packages</h2>
                    </div>
                </div>
            </div>
        </div>
        <form class="submitPackage" id="submitPackage" method="post">
            {{csrf_field()}}
            <div class="container">
                <div class="package_email">
                    <div class="input-group email_div">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="email" type="text" class="form-control email" name="email" placeholder="Email" value="{{$email ? $email : ''}}"  @if(!empty($email)){{'readonly'}}@endif>
                        <!--<input id="email"  type="text" class="form-control email" name="email" placeholder="Email" value="@if(!empty($agent_email)){{$agent_email}}@endif"  @if(!empty($agent_email)){{'readonly'}}@endif>-->
                    </div>
                    <p class="help-block email_p" style="color: red;"></p>
                </div>
                <input type="hidden" name="plan_id" class="plan_id">
                <input type="hidden" name="login_user" value="@if(Auth::check()){{Auth::user()->id}}@endif" id="login_user">
                <input type="hidden" name="expiry_date" value="@if(!empty($current_package_detail)){{$current_package_detail['user_package']['expiry_date']}}@endif" id="expiry_date">
                <input type="hidden" name="current_pack_name" value="@if(!empty($current_package_detail)){{$current_package_detail['user_package']['package']['name']}}@endif" id="current_pack_name">
                <input type="hidden" name="admin_access_token" class="admin_access_token">
                <input type="hidden" name="user_type" class="user_type">

            </div>
            <div class="pricing_plan">
                <h2><b>Pricing Plan</b></h2>
                <p>Choose a package that fits your needs:</p>
                <p class="container">Agent package is meant for real estate agents who are not bound to a company and is free to be in charge of other properties. Registered agents will always<br> have an agent license and will be required upon registration in this platform. Developer package is meant for real-estate developers who want to monitor<br> their staff in the platform to entertain customer enquiries and also handle the status of properties similar to agents. Developers will have to contact<br> us and fill out the required fields in the form below.</p>
                <div class="plan_detail">
                    <div class="container">
                        <div class="plan-flx">
                            @foreach($packages as $key => $value)
                                <div class="@if(!empty($active_package_id))
                                    @if($active_package_id == $value['id']){{'active_plans'}} @else{{'plans'}} @endif @else{{'plans'}} @endif">
                                    <h5>{{$value['name']}}</h5>
                                    @if($value['name'] == 'Agent')<h4>${{$value['price']}}</h4>@else <div class="Developerer-price">&nbsp;&nbsp;</div> @endif
                                    <h6>{{$value['sub-title']}}</h6>
                                    {!! $value['description'] !!}
                                    <?php 
                                        if($value['name'] == 'Advanced'){
                                            $plan = 'advance_btn';
                                        }elseif($value['name'] == 'Premium'){
                                            $plan = 'premium_btn';
                                        }elseif($value['name'] == 'Basic'){
                                            $plan = 'basic_btn';
                                        }elseif($value['name'] == 'Agent'){
                                            $plan = 'agent_btn';
                                        }elseif($value['name'] == 'Developer'){
                                            $plan = 'developer_btn';
                                        }
                                    ?>
                                    @if($value['name'] == 'Agent')
                                        <div class="row">
                                            <input type="radio" name="charge_payment_method" group="charge_payment_method" value="charge_automatically" required="">
                                            Reccuring payment
                                            <input type="radio" name="charge_payment_method" group="charge_payment_method" required="" value="manual" checked="">
                                            Manual Payment
                                            </br>
                                            <p class="help-block charge_payment_method_p" style="color: red;"></p>
                                        </div>
                                        <div class="select-btn">
                                            <a class="btn @if(Auth::check()){{'plan_login_btn'}}@else{{'plan_button'}}@endif @if($value['name'] == 'Advanced'){{'advance_select'}}@elseif($value['name'] == 'Premium'){{'premium_select'}}@endif{{$plan}} @if(!empty($email) && empty($register_type))email_given_check @endif" rel="{{$plan}}" rel1="user" data-plan-id="{{$value['id']}}" href="javascript:void(0);" data-plan-status="0" >Purchase</a>
                                        </div>
                                    @elseif($value['name'] == 'Developer')
                                        <div class="select-btn">
                                            <a class="btn @if(Auth::check()){{'plan_login_btn'}}@else{{'plan_button'}}@endif @if($value['name'] == 'Advanced'){{'advance_select'}}@elseif($value['name'] == 'Premium'){{'premium_select'}}@endif{{$plan}}" rel="{{$plan}}" rel1="developer" data-plan-id="{{$value['id']}}" href="javascript:void(0);" data-plan-status="0" >Contact Us</a>
                                        </div>
                                    @endif
                                    <div class="access_key_div" rel="{{$value['id']}}">
                                        <p>Do you have an access key?</p>
                                        <input class="access_key" type="text" name="access_key">
                                        <div class="select-btn1">
                                            <input type="button" value="submit" class="btn access_key_btn" data-plan-id="{{$value['id']}}">
                                        </div>
                                    </div>
                                    <p class="help-block access_key_p" style="color: red;"></p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Modal start -->

<div class="modal" id="confirmModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirm</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <form class="updatePackage" id="updatePackage" action="{{route('update_package')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="plan_id_m" id="plan_id_m" class="plan_id_m">
                <input type="hidden" name="login_user" value="@if(Auth::check()){{Auth::user()->id}}@endif" id="login_user">
                <input type="hidden" name="plan_expire_or_not" value="@if(Auth::check()){{Auth::user()->current_active_plan_id}}@endif">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary confirmPlan">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="updateMessage">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Success</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                Your Plan updated Successfully!
            </div>
            <div class="modal-footer">
                <button type="buuton" class="btn btn-primary okMessage" data-dimiss="modal">ok</button>
            </div>
        </div>
    </div>
</div>

   <!-- Modal ends -->
@endsection
@section('page-js')
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript">
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Sucess!', options)
    @endif

    $(document).on('click','.plan_button',function(){
        //alert($(this).attr('class'));
        if($(this).hasClass('email_given_check')) {
            
            swal("Oops","Please enter the access key for registration","error");
        } else {

            var email = $("#email").val();
            var type = $(this).attr('data-plan-id');
            $(".plan_id").val(type);
            var user_type = $(this).attr('rel1');
            $(".user_type").val(user_type);
            var data = $("#submitPackage").serialize();
            $('.access_key_p').text('');
            $.ajax({
                data: data,
                type: "post",
                url: "{{route('insert_package')}}",
                success: function(data){
                    if(data.status == 1){
                        // window.location.href = "{{url('')}}" + "/user/create"+'?id='+data.token+'&email='+data.email;
                        if(data.plan_id == 5) {

                            window.location.href = "{{url('')}}" + "/developer/contact-us"+'?id='+data.token+'&email='+data.email; // send user to payment checkout page
                        } else {
                        
                            // alert("here");
                            window.location.href = "{{url('')}}" + "/user/register-payment"+'?id='+data.token+'&email='+data.email; // send user to payment checkout page
                        }   

                    }else{
                        $.each(data.error, function(key, value){
                            $('.'+key+'_div').addClass('has-error');
                            $('.'+key+'_p').text(value);
                            topScroll();
                        });  
                    }
                },
                errors:function(){
                    // alert('error');
                }
            });
        }
    });

    $(document).on('click','.plan_login_btn',function(){
        var email = $("#email").val();
        var type = $(this).attr('data-plan-id');
        var expiry_date = $("#expiry_date").val();
        var current_pack_name = $("#current_pack_name").val();
        var class_btn = $(this).attr('rel');
        $(".confirmPlan").attr('rel',class_btn);
        $(".plan_id_m").val(type);
        var plan_exp_or_not = $("#plan_expire_or_not").val();;

        if(plan_exp_or_not != 0 && plan_exp_or_not != ''){
            // if plan not expire
            if(new Date() <= new Date(expiry_date))
            {
                $("#confirmModal").modal('show');
                $("#confirmModal .modal-body").html('You currently use '+current_pack_name+' do you want to change your plan ? if yes please click confirm.');
           
                $("#confirmModal .confirmPlan").on('click',function(){
                    var class_btn_modal =  $(this).attr('rel');
                    $("."+class_btn_modal).attr('data-plan-status',1);
                    // $("#rconfirmModal").modal('hide');
                });
            }
        }else{
            // When user plan expires
            var data = $("#updatePackage").serialize();
            $.ajax({
                data: data,
                type: "post",
                url: "{{route('update_package')}}",
                success: function(data){
                    if(data.status == 1){
                        $("#updateMessage").modal('show');
                        $("#updateMessage .okMessage").on('click',function(){
                            window.location.href = "{{route('packages')}}";
                        });
                    }
                },
                errors:function(){
                    // alert('error');
                }
            });
        } 
    });

    // access key token scripts
    $(document).on('click','.access_key_btn',function() {
 
        // clear the errors 
        $('.email_div').removeClass('has-error');
        $('.email_p').text("");
        $('.access_key_div').removeClass('has-error');
        $('.access_key_p').text("");
        
        var access_key = $(this).parent().prev().val();
        $(".admin_access_token").val(access_key);
        var type = $(this).attr('data-plan-id');
        $(".plan_id").val(type);
        var data = $("#submitPackage").serialize();
        $.ajax({
            data: data,
            type: "post",
            url: "{{ url('/agent/insert-access-key') }}",
            success: function(data){
                if(data.status == 1){
                    // window.location.href = "{{url('')}}" + "/user/create"+'?id='+data.token+'&email='+data.email;
                    window.location.href = "{{url('')}}" + "/user/create"+'?id='+data.token+'&email='+data.email; // send user to payment checkout page

                } else if(data.status == 2) {

                    $('.email_div').addClass('has-error');
                    $('.email_p').text(data.error);
                    topScroll();

                } else if(data.status == 3) {

                    $('.access_key_div').each(function() {
                        var rel = $(this).attr('rel');
                        if(rel == data.plan_id) {
                            $(this).addClass('has-error');
                            $(this).next('.access_key_p').text(data.error);
                        }
                    });

                }else{
                    
                    $.each(data.error, function(key, value){
                        if(key == 'email') {

                            $('.'+key+'_div').addClass('has-error');
                            $('.'+key+'_p').text(value);
                            topScroll();
                        } 
                        if(key == 'admin_access_token') {
                            $('.access_key_div').each(function() {
                                var rel = $(this).attr('rel');
                                if(rel == data.plan_id) {
                                    $(this).addClass('has-error');
                                    $(this).next('.access_key_p').text("The access key is required.");
                                }
                            });
                        }
                    });  
                }
            },
            errors:function(){
               // alert('error');
            }
        });
    });
    // access key token scripts

    // scroll to top function
    function topScroll() {
        $("html, body").delay(200).animate({
            scrollTop: $('.container').offset().top 
        }, 200);
    }
    // scroll to top function
</script>

@endsection