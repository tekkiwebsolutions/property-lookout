@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="modern-top-intoduce-section banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2>Account Setting</h2>
            </div>
        </div>
    </div>
</div>
<div class="about_us single-agent_detail">
    <div class="container">
        <div class="row">
            @if(Auth::user()->user_type == 'user')
                
                @include('agent.agentInfoSection')

            @elseif(Auth::user()->user_type == 'developer')
                
                @include('developer.developerInfoSection')
            
            @elseif(Auth::user()->user_type == 'staff')

                @include('developer.staffInfoSection')
                    
            @endif
            <div class="col-md-9 col-sm-7 cms-border agent-bio-data">
                <h3>Account Setting</h3>
                <div class="fld-lbl-nm-wrp">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <p class="fld-lbl">Name</p>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <p class="fld-nm">{{Auth::user()->name}}</p>
                        </div>
                    </div>
                </div>
                <div class="fld-lbl-nm-wrp">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <p class="fld-lbl">Email</p>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <p class="fld-nm">{{Auth::user()->email}}</p>
                        </div>
                    </div>
                </div>
                <div class="fld-lbl-nm-wrp">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <p class="fld-lbl">Gender</p>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <p class="fld-nm">@if(Auth::user()->gender == 'third_gender'){{'Third Gender'}}@else{{ucfirst(Auth::user()->gender)}}@endif</p>
                        </div>
                    </div>
                </div>
                <div class="fld-lbl-nm-wrp">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <p class="fld-lbl">Mobile No.</p>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <p class="fld-nm">+{{Auth::user()->phone}}</p>
                        </div>
                    </div>
                </div>
                <div class="fld-lbl-nm-wrp">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <p class="fld-lbl">Website</p>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <p class="fld-nm">{{Auth::user()->website}}</p>
                        </div>
                    </div>
                </div>
                <div class="fld-lbl-nm-wrp">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <p class="fld-lbl">Address</p>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <p class="fld-nm">{{Auth::user()->address}}</p>
                        </div>
                    </div>
                </div>
                <div class="fld-lbl-nm-wrp">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <p class="fld-lbl">Country</p>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <p class="fld-nm">{{$user_detail['country']['country_name']}}</p>
                        </div>
                    </div>
                </div>
                <div class="fld-lbl-nm-wrp">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <p class="fld-lbl">Postal code</p>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <p class="fld-nm">{{Auth::user()->postal_code}}</p>
                        </div>
                    </div>
                </div>
                <div class="fld-lbl-nm-wrp">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <p class="fld-lbl">Company</p>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <p class="fld-nm">{{Auth::user()->company}}</p>
                        </div>
                    </div>
                </div>
                <h2>About</h2>
                <p>@if(!empty($meta_data['about'])){{$meta_data['about']}}@else{{'.....'}}@endif</p>
                <div class="btns">
                    @if(Auth::user()->user_type == 'user')
                        <?php $profile_url = route('agent_settings'); $password_url = url('/agents/settings#security'); ?>
                    @elseif(Auth::user()->user_type == 'developer')
                        <?php $profile_url = route('developer_settings'); $password_url = url('/developers/settings#security'); ?>
                    @elseif(Auth::user()->user_type == 'staff')
                        <?php $profile_url = route('staff_settings'); $password_url = url('/developers/settings#security'); ?>
                    @endif
                    <a href="{{$profile_url}}" class="btn btn-primary black-btn pull-left m-t-20">Edit Profile</a>
                    <a href="{{$password_url}}" class="btn btn-primary black-btn pull-right m-t-20">Change Password</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')

@endsection