@extends('layout.main')
<?php use App\ApartmentUnit; ?>
<link rel="stylesheet" href="{{ asset('assets/plugins/owl.carousel/assets/owl.carousel.css') }}">
<style type="text/css">
    .ads-thumbnail .property_image{
        height: 200px;
    }
</style>
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')

<div class="row mlr0">
    <div class="page_wrapper page-@if(!empty($page->id)){{ $page->id }}@endif">
        <div class="modern-top-intoduce-section banner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <h2>@if($agent['user_type'] == 'user') Agent @elseif($agent['user_type'] == 'developer') Developer @elseif($agent['user_type'] == 'staff') Staff @endif Detail</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="about_us single-agent_detail">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 agent-avtar">
                        <div class="avtar">
                            @if(!empty($agent['photo']))
                                <img src="{{asset('uploads/avatar/'.$agent['photo'])}}" alt="#" class="img-responsive"/>
                            @else
                                <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
                            @endif
                        </div>
                        <ul class="agent_profiles text-center">
                            <li><a href="@if(!empty($meta_data['facebook_url'])){{$meta_data['facebook_url']}}@else{{'https://www.facebook.com/'}}@endif"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="@if(!empty($meta_data['twitter_url'])){{$meta_data['twitter_url']}}@else{{'https://www.twitter.com/'}}@endif"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="@if(!empty($meta_data['linked_in_url'])){{$meta_data['linked_in_url']}}@else{{'https://www.linkedin.com/'}}@endif"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                        </ul>
                        <div class="contact_div" style="display:none;">
                            @if($agent['phone'])
                                <button class="btn btn-block" id="onClickShowPhone" @if($agent['user_type'] == 'developer') style="margin-top: 3%;margin-bottom: 3%;" @endif @if(Auth::check()) style="margin-bottom: 3%" @endif>
                                    <!-- <strong> <span id="ShowPhoneWrap">{{$agent['phone']}}</span> </strong> <br /> -->
                                    <strong> <a href="callto:+{{$agent['phone']}}" id="ShowPhoneWrap">{{$agent['phone']}}</a> </strong> <br/>
                                </button>
                            @endif
                            @if(!Auth::check())
                                @if($agent['user_type'] == 'user' || $agent['user_type'] == 'staff')
                                    <button href="javascript:void(0);" class="btn btn-success start_chat" data-agent-id ="{{$agent['id']}}"  @if($agent['id'] != @Auth::user()->id) data-toggle="modal" data-target="#startChartModal" @endif style="padding-left: 35%;padding-right: 40%;margin-top: 3%;margin-bottom: 3%;">Start Chat</button>
                                @endif
                            @endif
                        </div>
                        @if($agent['phone'] || !Auth::check())
                            <div><a class="btn btn-primary contact-btn" href="javascript:void(0);" id="contact_btn">Contact</a></div>
                        @endif
                    </div>
                    <div class="col-md-6 col-sm-6 agent-detail-manual">
                        <h2>{{$agent['name']}}</h2>
                        <ul>
                            @if($agent['phone'])<li><a href="callto:+{{$agent['phone']}}">+{{$agent['phone']}}</a></li>@endif
                            <li><a href="mailto:{{$agent['email']}}">{{$agent['email']}}</a></li>
                            @if($agent['address'])<li>{{$agent['address']}}</li>@endif
                            @if($agent['website'])<li><a href="#">@if(!empty($agent['website'])){{$agent['website']}}@endif</a></li>@endif
                        </ul>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Gender:</td>
                                    <td>@if($agent['gender'] == 'third_gender'){{'Third Gender'}}@else{{ucfirst($agent['gender'])}}@endif</td>
                                </tr>
                                @if($agent['country'])
                                    <tr>
                                        <td>Country:</td>
                                        <td>{{$agent['country']['country_name']}}</td>
                                    </tr>
                                @endif
                                @if($agent['company'])
                                    <tr>
                                        <td>Company:</td>
                                        <td>{{$agent['company']}}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="aggnt-desc">
                    <h2>About {{ $agent['name'] }}</h2>
                    <p>@if(!empty($meta_data['about'])){{$meta_data['about']}}@else{{'.....'}}@endif</p>
                    @if($agent['user_type'] == 'user')
                        @if(Auth::check())
                            @if($agent['id'] != Auth::user()->id || Auth::user()->user_type == 'admin')
                                <button class="btn btn-primary black-btn" data-toggle="modal" data-target="#reportAdModal"><i class="fa fa-ban"></i> Report against agent <!-- Report against {{$agent['name']}} --></button>
                            @endif
                        @else
                            <button class="btn btn-primary black-btn" data-toggle="modal" data-target="#reportAdModal"><i class="fa fa-ban"></i> Report against agent <!-- Report against {{$agent['name']}} --></button>
                        @endif
                    @endif
                </div>
            </div>
        </div>

        @if($properties->count() > 0)
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="carousel-header">
                            <h3>@lang('app.new_urgent_ads')</h3>
                            <h6>Listing properties by this agent</h6>
                        </div>
                        <div class="themeqx_new_regular_ads_wrap themeqx-carousel-ads">
                            @foreach($properties as $ad)
                            <?php $apart_values = showUnitsFloorsNumber($ad->id); ?>
                            <div>
                                <div itemscope itemtype="http://schema.org/Product" class="ads-item-thumbnail ad-box-{{$ad->price_plan}}">
                                    <div class="ads-thumbnail">
                                        <a href="{{url('propertyShow/'.$ad->type.'/'.$ad->slug)}}">
                                            @if($ad->type == 'house')
                                                <img itemprop="image"  src="{{asset('uploads/houseImages/'.$ad->cover_photo) }}" class="img-responsive property_image" alt="{{ $ad->title }}">
                                            @else
                                                <img itemprop="image"  src="{{asset('uploads/apartmentImages/'.$ad->cover_photo) }}" class="img-responsive property_image" alt="{{ $ad->title }}">
                                            @endif

                                            <div class="img-hdr">
                                                <span style="@if($ad->property_status == 0)background:#33aa58;@elseif($ad->property_status == 1)background:#f26c61 @endif" class="modern-sale-rent-indicator">
                                                    @if($ad->property_status == 0) 
                                                        {{'Available'}}
                                                    @elseif($ad->property_status == 1)
                                                        {{'Sold'}}
                                                    @endif
                                                </span>
                                                <p class="date-posted" style="color:red;"> <i class="fa fa-clock-o"></i> {{ $ad->created_at->diffForHumans() }}</p>
                                            </div>
                                            <div class="img-ftr">
                                                <span class="avatar">
                                                    @if(!empty($agent['photo']))
                                                        <img src="{{asset('uploads/avatar/'.$agent['photo'])}}" alt="#">
                                                    @else
                                                        <img src="{{asset('assets/img/default_user.png')}}" alt="#">
                                                    @endif
                                                    <span class="user_name">{{$agent['name']}}</span>
                                                    <span class="modern-img-indicator">
                                                        @if(! empty($ad->video_url))
                                                            <span>Video</span>
                                                        @else
                                                            <span>{{ $ad->media_img->count() }} Photos</span>
                                                        @endif
                                                    </span>
                                                </span>

                                                <span class="price">
                                                    @if($ad->type == 'apartments')
                                                        <b itemprop="price" content="unit_wise"> Unit Wise </b>
                                                    @else
                                                        <b itemprop="price" content="{{$ad->price}}"> $ {{$ad->price}} </b>
                                                    @endif
                                                </span>
                                                <!-- </div> -->
                                            </div>
                                        </a>
                                    </div>
                                    <div class="caption">
                                        <h4><a href="{{url('propertyShow/'.$ad->type.'/'.$ad->slug)}}" title="{{ $ad->title }}"><span itemprop=name>{{ str_limit($ad->title, 40) }} </span></a></h4>
                                        @if($ad->city)
                                            <a class="location text-muted" href="{{ route('listing', ['city' => $ad->city->id]) }}"><i class="fa fa-map-marker"></i> {{ $ad->city->city_name }} </a>
                                        @endif

                                        <hr/>

                                        <table class="table table-responsive property-box-info">
                                            <tr>
                                                <td> 
                                                    <i class="fa fa-building"></i> {{ ucfirst($ad->type) }} 
                                                </td>

                                                @if($ad->type == 'house')
                                                    <td>
                                                        <i class="fa fa-arrows-alt "></i>  {{ $ad->square_unit_space.' '.$ad->unit_type }}
                                                    </td>
                                                @else 
                                                    <td>
                                                        <div class="icon"><img src="<?php echo asset('/');?>/assets/img/floor.png" alt="#"></div>

                                                        {{$apart_values['floors'] ? $apart_values['floors'].' Floors': 'N/A' }}

                                                    </td>
                                                @endif

                                                <td>
                                                    <span class="view"><i class="fa fa-eye" aria-hidden="true"></i>{{$ad->view}}</span>
                                                </td>
                                            </tr>

                                            @if($ad->type == 'house')
                                                <tr>
                                                    @if($ad->beds)
                                                        <td><i class="fa fa-bed"></i> {{ $ad->beds.' '.trans('app.bedrooms') }}</td>
                                                    @endif
                                                    @if($ad->attached_bath)
                                                        <td><i class="fa fa-shower"></i> {{ $ad->attached_bath.' Bathrooms'}}</td>
                                                    @endif
                                                </tr>
                                            @else
                                                <tr>
                                                    <td><i class="fa fa-home"></i> {{ $apart_values['units'] ? $apart_values['units'] : 0 }} Units</td>
                                                    <?php $available_units = ApartmentUnit::whereAdId($ad->id)->Where('property_status',0)->count(); ?>
                                                    <td><i class="fa fa-check"></i> {{ $available_units }} Available</td>
                                                </tr>
                                            @endif

                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="container">
                <div class="alert alert-warning">
                    <h2><i class="fa fa-info-circle"></i> Properties Not Posted By {{$agent['name']}} Yet !</h2>
                </div>
            </div>
        @endif

    </div>
</div>
<!-- Modals Starts -->
<!-- chat modal -->
<div id="startChartModal" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Start Chat with Agent</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['id'=>'insertGuestUser', 'class' => '', 'files' => true , 'route'=> 'insert_guest_user' ]) }}
                <!-- {{csrf_field()}} -->
                <input type="hidden" name="agent_id" id="agent_id" value="">
                <div class="form-group {{ $errors->has('guestUserEmail')? 'has-error':'' }} email_error_div error_divs">
                    <label class="control-label">Email</label>
                    <input type="text" name="guestUserEmail" placeholder="Email" class="form-control" id="guestUserEmail" maxlength="255">
                    {!! $errors->has('guestUserEmail')? '
                    <p class="help-block">'.$errors->first('guestUserEmail').'</p>
                    ':'' !!}
                    <p class="help-block email_error_p error_ps"></p>
                </div>
                <div class="form-group {{ $errors->has('guestUserName')? 'has-error':'' }} name_error_div error_divs">
                    <label class="control-label">Name</label>
                    <input type="text" name="guestUserName" class="form-control" placeholder="Name" id="guestUserName" maxlength="255">
                    {!! $errors->has('guestUserName')? '
                    <p class="help-block">'.$errors->first('guestUserName').'</p>
                    ':'' !!}
                    <p class="help-block name_error_p error_ps"></p>
                </div>
                <div class="form-group {{ $errors->has('guestUserMessage')? 'has-error':'' }} message_error_div error_divs">
                    <label class="control-label">Message</label>
                    <textarea type="text" name="guestUserMessage" class="form-control" placeholder="Message.." id="guestUserMessage"></textarea>
                    {!! $errors->has('guestUserMessage')? '
                    <p class="help-block">'.$errors->first('guestUserMessage').'</p>
                    ':'' !!}
                    <p class="help-block message_error_p error_ps"></p>
                </div>
                <button type="submit" class="btn btn-success" id="submitGuestUser">Submit</button>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- chat modal ends -->
<!-- Report Modal start -->
<div class="modal fade" id="reportAdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Is there something wrong with this Agent?</h4>
            </div>
            <div class="modal-body">
                <p>We're constantly working hard to assure that our properties meet high standards and we are very grateful for any kind of feedback from our users.</p>
                <form>
                    <div class="form-group">
                        <label class="control-label">@lang('app.reason'):</label>
                        <select class="form-control" name="reason" id="report_reason">
                            <option value="">@lang('app.select_a_reason')</option>
                            <!-- <option value="unavailable">@lang('app.item_sold_unavailable')</option> -->
                            <option value="fraud">@lang('app.fraud')</option>
                            <option value="duplicate">@lang('app.duplicate')</option>
                            <option value="spam">@lang('app.spam')</option>
                            <!-- <option value="wrong_category">@lang('app.wrong_category')</option> -->
                            <option value="offensive">@lang('app.offensive')</option>
                            <option value="other">@lang('app.other')</option>
                        </select>
                        <div id="reason_info"></div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label">@lang('app.email'):</label>
                        <input type="text" class="form-control" id="report_email" name="email" maxlength="255">
                        <div id="email_info"></div>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">@lang('app.message'):</label>
                        <textarea class="form-control" id="report_message" name="message"></textarea>
                        <div id="message_info"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('app.close')</button>
                <button type="button" class="btn btn-primary" id="report_ad">Report</button>
            </div>
        </div>
    </div>
</div>
<!-- Report Modal Ends -->
<!-- Modals Ends -->
@endsection
@section('page-js')
<script src="{{ asset('assets/plugins/owl.carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript">
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif

    @if(session('error'))
        toastr.success('{{ session('error') }}', '<?php echo trans('app.error') ?>', toastr_options);
    @endif
    
    $(document).on('click','.start_chat',function(){
        var agent_id = $(this).attr('data-agent-id');
        $("#agent_id").val(agent_id);
    });
    
    $(document).on('click','#contact_btn',function(){
        $(".contact_div").toggle();
    });
    
    $(document).ready(function(){
        $(".themeqx_new_regular_ads_wrap").owlCarousel({
            loop: true,
            autoplay : true,
            autoplayTimeout : 2000,
            margin:10,
            autoplayHoverPause : true,
            responsiveClass:false,
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    loop:true
                },
                767:{
                    items:2,
                    nav:false,
                    loop:true
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:true
                }
            },
            navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>']
        });
    });
    
    // submit report against property 
    $(function() {
        $('button#report_ad').click(function(){
            var reason = $('#report_reason').val();
            var email = $('#report_email').val();
            var message = $('#report_message').val();
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    
            var error = 0;
            if(reason.length < 1){
                $('#reason_info').html('<p class="text-danger">Please select a reason.</p>');
                error++;
            }else {
                $('#reason_info').html('');
            }
            if(email.length < 1){
                $('#email_info').html('<p class="text-danger">Please enter email address.</p>');
                error++;
            }else {
                if ( ! regex.test(email)){
                    $('#email_info').html('<p class="text-danger">Please enter a valid email address.</p>');
                    error++;
                }else {
                    $('#email_info').html('');
                }
            }
            if(message.length < 1){
                $('#message_info').html('<p class="text-danger">Please enter a message.</p>');
                error++;
            }else {
                $('#message_info').html('');
            }
    
            if (error < 1){
                $('#loadingOverlay').show();
                $.ajax({
                    type : 'POST',
                    url : '{{ route('report_ads_pos') }}',
                    data : {report_for:'agent' ,reason : reason, email: email,message:message, id:'{{ $agent["id"] }}',  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        if (data.status == 1){
                            toastr.success(data.msg, '@lang('app.success')', toastr_options);
                        }else {
                            toastr.error(data.msg, '@lang('app.error')', toastr_options);
                        }
                        $('#report_reason').val('');
                        $('#report_email').val('');
                        $('#report_message').val('');
                        $('#reportAdModal').modal('hide');
                        $('#loadingOverlay').hide();
                    }
                });
            }
        });
    });
    // submit report against property
    
    // validations for send chat message modal
    $(document).on('click','#submitGuestUser',function() {
        
        var empty_error = 0;
        var email = $('#guestUserEmail').val();
        var name = $('#guestUserName').val();
        var message = $('#guestUserMessage').val();
        $('.error_divs').removeClass('has-error');
        $('.error_ps').text('');
    
        if(email == '') {
            $('.email_error_div').addClass('has-error');
            $('.email_error_p').text('Please enter your email address.');
            empty_error = 1;
        }
    
        if(name == '') {
            $('.name_error_div').addClass('has-error');
            $('.name_error_p').text('Please enter your name.');
            empty_error = 1;
        }
    
        if(message == '') {
            $('.message_error_div').addClass('has-error');
            $('.message_error_p').text('Please enter your message.');
            empty_error = 1;
        }
        
        var re = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        if(email != '') {
    
            if(!re.test(email)) {
                $('.email_error_div').addClass('has-error');
                $('.email_error_p').text('Please enter a valid email address.');
                empty_error = 1;
            }
        }
    
        if(empty_error > 0) {
            return false;
        }
    
        $('#insertGuestUser').submit();
    });
    // validations for send chat message modal
</script>
@endsection