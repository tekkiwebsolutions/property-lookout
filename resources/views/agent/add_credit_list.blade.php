@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<?php use App\ApartmentUnit; ?>
<div class="cms-inner-content">
    <div class="container">
        <div class="row">
            @include('agent.agentInfoSection')
            <div class="col-md-9 col-sm-7 cms-border">
                <div align="left" style="margin-bottom: 3%;">
                    <h4 style="font-weight:bold;">Add Credits To Property</h4>   
                </div>
                <div class="table-responsive">
                    <table class="table cms-table cstm-tbl-cls agnt-prprty-tbl prprty-lst-tbl">
                        @if(!empty($properties))
                        <thead>
                            <tr>
                                <th></th>
                                <th>Property Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($properties as $key => $value)
                            <?php $id = Crypt::encrypt($value['id']); ?>
                            <tr class="three-btns-actn">
                                <td>
                                    <div class="property-img">
                                        @if($value['type'] == 'house')
                                            <img src="{{asset('uploads/houseImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                        @else
                                            <img src="{{asset('uploads/apartmentImages/'.$value['cover_photo'])}}" class="img-responsive" />
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    <div class="float-left">
                                        <span>{{$value['title']}}</span>
                                        <span>{{ucfirst($value['type'])}}</span>
                                    </div>
                                </td>
                                <td id="td_{{$value['id']}}">
                                    @if($value['property_status'] == 0)
                                    <p style="color:green;">{{'Available'}}</p>
                                    @else
                                    <p style="color:red;">{{'Sold'}}</p>
                                    @endif
                                </td>
                                <td>
                                    @if($value['type'] == 'house')
                                        @if(!empty($value['credits']))
                                            <a href="javascript:void(0);" class="btn btn-success small-btn" id="{{$id}}">{{ $value['credits'] }} Credits Added</a>
                                        @else
                                            @if(date('d') < 29 && @$user_credit_data['total_available_credit'] > 0)
                                                <a href="{{ url('/agents/add-property-credit'.'/'.getEncrypted($value['id'])) }}" class="btn btn-primary small-btn" id="{{$id}}">Add Credits</a>
                                            @endif
                                        @endif
                                    @elseif($value['type'] == 'apartments')
                                        @if($value['credit_added'] == 1)
                                            <?php 
                                                $credit_amount = \App\UserCredit::whereAdId($value['id'])->value('amount');
                                            ?>
                                            <a href="javascript:void(0);" class="btn btn-success small-btn" id="{{$id}}">{{ (int)@$credit_amount }} Credits Added</a>
                                        @else
                                            @if(date('d') < 29 && @$user_credit_data['total_available_credit'] > 0)
                                                <a href="{{ url('/agents/add-property-credit'.'/'.getEncrypted($value['id'])) }}" class="btn btn-primary small-btn" id="{{$id}}">Add Credits</a>
                                            @endif
                                        @endif
                                    @endif    
                                </td>
                            </tr> 
                            @endforeach
                        </tbody>
                        @else
                            <tbody>
                                <tr>
                                    <td colspan="3"><h4>No property added yet. Please add a property to get started.</h4></td>
                                </tr>
                            </tbody>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script>
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif
    @if(session('error'))
        toastr.error('{{ session('error') }}', '<?php echo trans('app.error') ?>', toastr_options);
    @endif
</script>
@endsection