@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/datatables/responsive.dataTables.min.css') }}" rel="stylesheet"/>
<div class="cms-inner-content">
    <div class="container">
        <div class="row">
            @include('agent.agentInfoSection')
            <div class="col-md-9 col-sm-7 cms-border agentcredits-list agents_credits">
                <table id="table_id" class="display table table-bordered cms-table">
                    <thead>
                        <tr>
                            <th>Sr. No.</th>
                            <th>Reason</th>
                            <th>From</th>
                            <th>Message</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($reports))
                            <?php $count = 1; ?>
                            @foreach($reports as $report)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{ $report->reason }}</td>
                                    <td> {{ $report->email }}  </td>
                                    <td>{{ $report->message }}</td>
                                    <td> {{ $report->posting_datetime() }}</td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modals Starts -->

<!-- Modals Ends -->
@endsection
@section('page-js')
<script type="text/javascript" src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script>
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif

    $('.entr-add-credit').hide();
    $('.open-add-credit').click(function(){
        $('.entr-add-credit').toggle();
    });

    $(document).ready( function () {
        $('#table_id').DataTable({
            pagingType: "simple",
            responsive: true,
            language: {
              paginate: {
                next: '<i class="fa fa-angle-double-right"></i>',
                previous: '<i class="fa fa-angle-double-left"></i>' 
              }
            }
        });
    } );

    // submit form when credit is added
    $(document).on('click','.save_credit_btn',function() {
        var credit = $('.credit_select_class').val();
        if(credit == '') {
            swal("OOPS!","Please select credit","error");
            return false;
        } else {
            $('#addCreditForm').submit();
        }
    });
</script>
@endsection