@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<?php 
    $window_type = encrypt('agentWindow');
    ?>
<div class="page_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="single_page_heading">Inbox</h2>
                @if(!empty($users))
                <div class="notification all_messages">
                    @foreach($users as $key =>$value)
                    <div class="ntfction-flx">
                        <div class="ntfction-flx-inr text-left">
                            <div class="notification-content">
                                <span class="title">
                                    <i class="fa fa-envelope"></i>
                                    <p>{{$value['user_detail']['name']}} </p>
                                </span>
                            </div>
                        </div>
                        <div class="ntfction-flx-inr text-center">
                            <div class="notification-content">
                                {{$value['user_detail']['email']}}
                                <?php 
                                    $agent_id   = encrypt($value['Agent_id']);
                                    $user_id    = encrypt($value['user_id']);
                                    ?>
                            </div>
                        </div>
                        <div class="ntfction-flx-inr text-right">
                            <div class="notification-content">
                                <a href="{{url('chat-room/'.$agent_id.'/'.$user_id.'/'.$window_type )}}" class="btn btn-primary small-btn approveAds" data-slug="" data-value="1">Start Chat</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @else
                <div class="ntfction-flx">
                    <div class="ntfction-flx-inr text-center">
                        <h4>No Message received yet!!</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script>
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif
</script>
@endsection