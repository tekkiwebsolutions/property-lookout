@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<style type="text/css">
    #map {
        height: 360px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
</style>
<div class="modern-top-intoduce-section banner">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h2>@lang('app.contact_with_us')</h2>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="contact_detail cntct-flx">
        <div class="contact-box-flx">
            <div class="contact-col">
                <i class="fa fa-map-marker"></i>
                <address>
                    {{ $posts['address'] }}
                </address>
            </div>
        </div>
        <div class="contact-box-flx">
            <div class="contact-col">
                <i class="fa fa-phone"></i>
                <span title="Phone">{{ $posts['phone_number'] }}</span>
            </div>
        </div>
        <div class="contact-box-flx">
            <div class="contact-col">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <address>
                    <a href="mailto:{{ $posts['email'] }}"> {{ $posts['email'] }} </a>
                </address>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="contact_form clearfix">
            <div class="col-sm-6">
                <!-- contact us form for property request and developer registration (This page is for developers account only) -->
                <!-- {!! Form::open() !!} -->
                <form action="{{ url('/developer/contact-us') }}" method="POST" id="contactUsForm">
                    {{csrf_field()}}
                <input type="hidden" name="user_id" value="{{@Auth::user()->id ? @Auth::user()->id : @$user->id}}">
                <input type="hidden" name="type" value="{{ $value }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('person_to_address')? 'has-error':'' }} error_divs person_to_address_div">
                            <label for="name">Person to address:</label>
                            <input type="text" class="form-control" id="person_to_address" name="person_to_address" placeholder="Enter person to address" value="{{ old('person_to_address') }}" maxlength="255"/>
                            {!! $errors->has('person_to_address')? '
                                <p class="help-block">'.$errors->first('person_to_address').'</p>
                            ':'' !!}
                                <p class="help-block error_ps person_to_address_p"></p>
                        </div>
                        <div class="form-group {{ $errors->has('company_name')? 'has-error':'' }} error_divs company_name_div">
                            <label for="name">Company Name:</label>
                            <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Enter Company Name" value="{{ old('company_name') }}" maxlength="255"/>
                            {!! $errors->has('company_name')? '
                                <p class="help-block">'.$errors->first('company_name').'</p>
                            ':'' !!}
                                <p class="help-block error_ps company_name_p"></p>
                        </div>
                        <div class="form-group {{ $errors->has('contact_number')? 'has-error':'' }} error_divs contact_number_div">
                            <label for="name">Contact Number:</label>
                            <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Enter Contact Number" value="{{ old('contact_number') }}" maxlength="10" />
                            {!! $errors->has('contact_number')? '
                                <p class="help-block">'.$errors->first('contact_number').'</p>
                            ':'' !!}
                                <p class="help-block error_ps contact_number_p"></p>
                        </div>
                        <div class="form-group {{ $errors->has('other_contact')? 'has-error':'' }} error_divs other_contact_div">
                            <label for="name">Other ways to contact(social media, email address etc.):</label>
                            <input type="text" class="form-control" id="other_contact" name="other_contact" placeholder="Enter Other Contacts" value="{{ old('other_contact') }}" maxlength="255"/>
                            {!! $errors->has('other_contact')? '
                                <p class="help-block">'.$errors->first('other_contact').'</p>
                            ':'' !!}
                                <p class="help-block error_ps other_contact_p"></p>
                        </div>
                        <div class="form-group {{ $errors->has('cost')? 'has-error':'' }} error_divs cost_div">
                            <label for="name">The cost of the specific project:</label>
                            <input type="text" class="form-control" id="cost" name="cost" placeholder="Enter Cost" value="{{ old('cost') }}" maxlength="10" />
                            {!! $errors->has('cost')? '
                                <p class="help-block">'.$errors->first('cost').'</p>
                            ':'' !!}
                                <p class="help-block error_ps cost_p"></p>
                        </div>
                        <div class="form-group {{ $errors->has('short_description')? 'has-error':'' }} error_divs short_description_div">
                            <label for="name">Short Description of the company</label>
                            <textarea name="short_description" id="short_description" class="form-control" placeholder="Enter Short Description">{{ old('short_description') }}</textarea>
                            {!! $errors->has('short_description')? '
                                <p class="help-block">'.$errors->first('short_description').'</p>
                            ':'' !!}
                                <p class="help-block error_ps short_description_p"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary black-btn" id="btnContactUs"> @lang('app.send_message')</button>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-sm-6">
                <!-- {!! get_option('google_map_embedded_code') !!} -->
                <div id="map"></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{get_option('google_map_api_key')}}&callback=initMap">
</script>
<script>
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif

    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        var latitude    = "{{$posts['latitude']}}";
        var longitude   = "{{$posts['longitude']}}";
        var uluru = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 4, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }

    // validations for contact us form
    $(document).on('click','#btnContactUs',function() {
        
        var empty_error = 0;
        var person_to_address = $('#person_to_address').val();
        var company_name = $('#company_name').val();
        var contact_number = $('#contact_number').val();
        var other_contact = $('#other_contact').val();
        var cost = $('#cost').val();
        var short_description = $('#short_description').val();
        $('.error_divs').removeClass('has-error');
        $('.error_ps').text('');

        if(person_to_address == '') {
            $('.person_to_address_div').addClass('has-error');
            $('.person_to_address_p').text('Please enter person to address.');
            empty_error = 1;
        }

        if(company_name == '') {
            $('.company_name_div').addClass('has-error');
            $('.company_name_p').text('Please enter company name.');
            empty_error = 1;
        }

        if(contact_number == '') {
            $('.contact_number_div').addClass('has-error');
            $('.contact_number_p').text('Please enter contact number.');
            empty_error = 1;
        } else {

            if(!$.isNumeric(contact_number) || contact_number.length != 10) {
                $('.contact_number_div').addClass('has-error');
                $('.contact_number_p').text('Please enter a valid contact number.');
                empty_error = 1;
            }
        }

        if(other_contact == '') {
            $('.other_contact_div').addClass('has-error');
            $('.other_contact_p').text('Please enter other contacts.');
            empty_error = 1;
        }

        if(cost == '') {
            $('.cost_div').addClass('has-error');
            $('.cost_p').text('Please enter cost.');
            empty_error = 1;
        } else {

            if(!$.isNumeric(cost)) {
                $('.cost_div').addClass('has-error');
                $('.cost_p').text('Please enter cost in digits only.');
                empty_error = 1;
            }
        }

        if(short_description == '') {
            $('.short_description_div').addClass('has-error');
            $('.short_description_p').text('Please enter short description.');
            empty_error = 1;
        } else {

            if(short_description.length < 80) {
                $('.short_description_div').addClass('has-error');
                $('.short_description_p').text('Minimum 80 characters are required.');
                empty_error = 1;
            }
        }

        if(empty_error > 0) {
            return false;
        }
        // alert(empty_error);
        // return false;

        $('#contactUsForm').submit();
    });
    // validations for contact us form
</script>
@endsection