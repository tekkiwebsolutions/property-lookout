@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="cms-inner-content">
    <div class="container">
        <div class="row">    
            @include('agent.agentInfoSection')
            <div class="col-md-9 col-sm-7 cms-border">
                <div class="tab-content agent-bio-data">
                    <div id="invite_agents" class="tab-pane fade in active">
                        <div class="cms-table">
                            <div class="invite_agents">
                                <h4>Developer Requests</h4>
                                <!-- <h6>Hello User</h6> -->
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody id="get_agents_list">
                                        @if(count($pending_requests) > 0)
                                            @foreach($pending_requests as $key => $request)
                                                <tr>
                                                    <td class="column-one">
                                                        <div class="property-img">
                                                            @if(!empty(@$request['developer']['photo']))
                                                                <img src="{{asset('uploads/avatar/'.@$request['developer']['photo'])}}" alt="#" class="img-responsive"/>
                                                            @else
                                                                <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    
                                                    <td class="column-two"> 
                                                        <span>{{@$request['developer']['name']}}</span>
                                                        <span>{{substr(@$request['developer']['address'],0,80)}}</span>
                                                    </td>
                                                    <td class="column-three" rel="{{@$request['id']}}">
                                                        @if(@$request['accept_status'] == 0)
                                                            <a href="javascript:void(0);" class="btn btn-primary small-btn accept_cancel_request pull-right" rel="2" style="margin-left:10px;">Deny</a>
                                                            <a href="javascript:void(0);" class="btn btn-primary small-btn accept_cancel_request pull-right" rel="1">Accept</a>
                                                        @elseif(@$request['accept_status'] == 1)
                                                            <a href="javascript:void(0);" class="btn btn-success small-btn pull-right" rel="1">Accepted</a>
                                                        @elseif(@$request['accept_status'] == 2)
                                                            <a href="javascript:void(0);" class="btn btn-danger small-btn pull-right" rel="2">Denied</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <div class="col-md-12"><b>No Results found.</b></div>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script>
    
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Sucess!', options)
    @endif

    // agent send invitation script
    $(document).on('click','.accept_cancel_request',function() {
        var request_id = $(this).parent().attr("rel");
        var accept_val = $(this).attr("rel");
        $.ajax({
            context : this,
            url : "{{ url('/agents/accept-cancel-request') }}",
            type: "POST",
            data: {request_id : request_id,accept_val:accept_val, _token : '{{ csrf_token() }}'},
            success: function(data) {
                // alert("yes");
                if(data.success == 1) {
                    
                    if(data.accept_val == '1') {

                        $(this).text("Accepted");
                        $(this).removeClass("accept_cancel_request");
                        $(this).removeClass("btn-primary");
                        $(this).addClass("btn-success");
                        $(this).prev().remove();
                    } else {
                        $(this).text("Denied");
                        $(this).removeClass("accept_cancel_request");
                        $(this).removeClass("btn-primary");
                        $(this).addClass("btn-danger");
                        $(this).next().remove();
                    }
                    toastr.success(data.msg, '@lang('app.success')', toastr_options); 
                } else {

                    toastr.error('Something went wrong.', '@lang('app.error')', toastr_options);
                }
            },
            error:function() {
                // alert("error");
                toastr.error('Something went wrong.', '@lang('app.error')', toastr_options);
            }
        });
    });

    // agent search function
    $(document).on('click','#agent_search_button',function() {
        var search_val = $(this).prev("input").val();
        if(search_val == '') {
            search_val = 'no_value';
        }
        $('#get_agents_list').load("{{ url('') }}"+'/'+'developers/search-invite-agent'+'/'+search_val);
    });

</script>
@endsection