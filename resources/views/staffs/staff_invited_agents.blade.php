@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<?php use App\ApartmentUnit; ?>
<style type="text/css">
   .modal-header{
   background-color:#306492;
   color:white;
   font-size:16px;
   font-weight:bolder !important;
   }
   .modal-header .close{color:white; font-size:16px; font-weight:bolder !important;}
   .staff-invt{padding:30px 0;}
   .staff-invt li{width:100%; font-size:20px; font-weight:300; float:left; padding:0px; margin:0px;}
   .staff-invt li a{color:#000000; padding:5px 7px;float:left; width:100%;}
   .staff-invt li a .fa {float:left; width:20px; text-align:center; padding-top:5px;}
   .staff-invt li a:hover {background:#2d3e50; color:#fff;}
   .column4 {width:26%;}
   .column-one {width:33%;}
   .align-right {text-align:right}
   .cms-table .dropdown-menu {padding:10px 0px 10px 7px;}
   .cms-table td li {margin:0px 4px;}
   .dropdown-menu > li > a {padding:3px 17px;}
   .popover-content {padding:5px 0px;}
   .popover {left:118.292px !important;}
   .popover-title, .cms-table .dropdown-menu .btn-xs {font-size:11px;}
   .cms-table .dropdown-menu .btn-xs {margin:0px 2px; padding:4px 9px;}
</style>
<div class="cms-inner-content">
   <div class="container">
      <div class="row">
         <div class="col-lg-3 col-md-3 col-sm-4 agent-detail-manual cms-avtar agent-cms-avtar">
            <div class="avtar">
               <img src="http://localhost/Projects/Laravel/property-lookout/uploads/avatar/1540354682x5yty-img-3900-copy.jpg" />
            </div>
            <div class="staff-invt">
               <ul>
                  <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i> Staff Info</a></li>
                  <li><a href="#"><i class="fa fa-lock" aria-hidden="true"></i> Invite Agents</a></li>
                  <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> Staff / Invited Agents</a></li>
               </ul>
            </div>
         </div>
         <div class="col-md-9 col-sm-7 cms-border">
            <div class="table-responsive">
               <table class="table cms-table">
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Action</th>
                        <th class="column4">
                           <a href="#" class="btn btn-primary small-btn">Invite Agent</a>
                           <a href="#" class="btn btn-primary small-btn">Add New</a>
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td class="column-one">
                           <div class="property-img">
                              <img src="http://localhost/Projects/Laravel/property-lookout/uploads/avatar/1540354682x5yty-img-3900-copy.jpg" class="img-responsive" />
                           </div>
                           <div class="float-left">
                              <span>Agent 1</span>
                           </div>
                        </td>
                        <td class="columnTwo"> 
                           NA
                        </td>
                        <td>Not released for public confirmation needed</td>
                        <td>
                           <div class="dropdown align-right">
                              <button class="btn action-btn dropdown-toggle float-right" type="button" data-toggle="dropdown">Action
                              <span class="caret"></span></button>
                              <ul class="dropdown-menu">
                                 <li><a href="#" class="btn btn-primary small-btn">view</a></li>
                                 <li><a href="/property-lookout/agent-edit-cms" class="btn small-btn btn-success">Edit</a></li>
                                 <li><a href="#" class="btn small-btn btn-danger" data-toggle="confirmation">Delete</a></li>
                              </ul>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td class="column-one">
                           <div class="property-img">
                              <img src="http://localhost/Projects/Laravel/property-lookout/uploads/avatar/1540354682x5yty-img-3900-copy.jpg" class="img-responsive" />
                           </div>
                           <div class="float-left">
                              <span>Agent 2</span>
                           </div>
                        </td>
                        <td class="columnTwo"> 
                           NA
                        </td>
                        <td>Not released for public confirmation needed</td>
                        <td>
                           <div class="dropdown align-right">
                              <button class="btn action-btn dropdown-toggle float-right" type="button" data-toggle="dropdown">Action
                              <span class="caret"></span></button>
                              <ul class="dropdown-menu">
                                 <li><a href="#" class="btn btn-primary small-btn">view</a></li>
                                 <li><a href="/property-lookout/agent-edit-cms" class="btn small-btn btn-success">Edit</a></li>
                                 <li><a href="#" class="btn small-btn btn-danger" data-toggle="confirmation">Delete</a></li>
                              </ul>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td class="column-one">
                           <div class="property-img">
                              <img src="http://localhost/Projects/Laravel/property-lookout/uploads/avatar/1540354682x5yty-img-3900-copy.jpg" class="img-responsive" />
                           </div>
                           <div class="float-left">
                              <span>Agent 3</span>
                           </div>
                        </td>
                        <td class="columnTwo"> 
                           NA
                        </td>
                        <td>Not released for public confirmation needed</td>
                        <td>
                           <div class="dropdown align-right">
                              <button class="btn action-btn dropdown-toggle float-right" type="button" data-toggle="dropdown">Action
                              <span class="caret"></span></button>
                              <ul class="dropdown-menu">
                                 <li><a href="#" class="btn btn-primary small-btn">view</a></li>
                                 <li><a href="/property-lookout/agent-edit-cms" class="btn small-btn btn-success">Edit</a></li>
                                 <li><a href="#" class="btn small-btn btn-danger" data-toggle="confirmation">Delete</a></li>
                              </ul>
                           </div>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal for developer to ask for a new access key, first developer has to fill contact form, then he gets access key, then put the access key and is redirected to create apartment page -->
{{-- 
<div class="modal fade" id="createNewProject" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Create a new project</h5>
            <button type="button" class="close" style="line-height:0 !important" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form id="newApartmentForm" class="form-group">
            {{csrf_field()}}
            <div class="modal-body">
               <p></p>
               <p>To create a new project, please contact us with the same way when registering for your developer account. If you have acquired an access key already, please enter it here.</p>
               <hr>
               <label for="access-key" class="col-form-label" style="padding:5px 0px;">Your Email:</label>
               <input type="text" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}" readonly>
               <label for="access-key" class="col-form-label" style="padding:5px 0px;">Access Key:</label>
               login user id to check user's contact request
               <input type="hidden" name="login_user" value="{{ Auth::user()->id }}" id="login_user">
               <!-- to check if request has been sent by developer -->
               <input type="hidden" name="request_id" class="request_id" value="{{ $available_request?$available_request['id']:'0' }}">
               <input type="text" class="form-control" id="access-key" name="access_key" maxlength="255">
               <p style="color:red" id="warning-message"></p>
            </div>
            <div class="modal-footer">
               <button type="button" id="submit" class="btn btn-primary access_key_btn">Submit</button>
               <!-- this button takes the developer to the contact us page -->
               @if(!empty($available_request))
               <a href="javascript:void(0);" id="contact-us" class="btn btn-danger apartment_already_requested">Contact Us</a>
               @else
               <a href="{{ url('/developer/contact-us'.'?id='.encrypt(Auth::user()->id).'&email='.Auth::user()->email.'&value=apartment_request') }}" id="contact-us" class="btn btn-danger">Contact Us</a>
               @endif
            </div>
         </form>
      </div>
   </div>
</div>
--}}
<!-- contact us create new apartment modal -->
@endsection
@section('page-js')
<script>
   $('body').confirmation({
     selector: '[data-toggle="confirmation"]'
   });
</script>
@endsection