@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<style type="text/css">
    .file {
        visibility: hidden;
        position: absolute;
    }
    .column-one { width:20%; }
    .column-two { width:50%; }
    .column-three { width:40%; }
    .invite_agents h4{ font-size: 22px; color:#2d3e50; float: left; padding-left:15px;}
    .invite_agents h6{ float: right; color:#000000; padding-right:15px; }
    .table-responsive{ width:100%; }
    .invite_agents{ background: #f5f4f4; float: left; width:100%; }
    .add_staff{border-top:1px solid #ddd; padding: 20px 0px 10px 0px;}
    .add_staff h6 {float:left; font-size:15px; color:#000000; padding-left:15px; font-weight: 400;}
    .search_agent{ float: right; padding-right:15px; }
    .search_agent input{border:1px solid #d1d0d0; padding:8px 13px;}
    .search_agent button{ background: #213140; border:0; color:#fff; padding:9px 15px; margin-left: -4px; }
</style>
<div class="cms-inner-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-5 cms-avtar">
                <div class="avtar">
                    @if(!empty(Auth::user()->photo))
                        <img src="{{asset('uploads/avatar/'.Auth::user()->photo)}}" alt="#" class="img-responsive"/>
                    @else
                        <img src="{{asset('uploads/avatar/default_user.png')}}" alt="#" class="img-responsive"/>
                    @endif
                </div>
                <ul class="nav nav-pills cms-tabs">
                    <li class="active"><a data-toggle="pill" href="#staff_info"><i class="fa fa-cog" aria-hidden="true"></i>Staff Info</a></li>
                    <li><a data-toggle="pill" href="#invite_agents"><i class="fa fa-lock" aria-hidden="true"></i>Invite Agents</a></li>
                    <li><a data-toggle="pill" href="#bio"><i class="fa fa-user" aria-hidden="true"></i>Staff / Invited Agents</a></li>
                </ul>
            </div>
            <div class="col-md-9 col-sm-7 cms-border">
                <div class="tab-content agent-bio-data">
                    <div id="staff_info" class="tab-pane fade in active">
                        <h3>Add Staff</h3>
                        <form class="form-horizontal" id="updateProfile" action="{{route('update_agent_profile')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label class="control-label col-sm-3">UserName</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  value="{{Auth::user()->name}}" name="name" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Email</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  value="{{Auth::user()->email}}" name="email" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  value="{{Auth::user()->email}}" name="email" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Password</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="phone" value="{{Auth::user()->phone}}" />
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary black-btn m-t-20">Add</button>
                        </form>
                    </div>
                    <div id="invite_agents" class="tab-pane fade">
                        <div class="cms-table">
                            <div class="invite_agents">
                                <h4>Invite Agents</h4>
                                <h6>Hello User</h6>
                            </div>
                            <div class="add_staff clearfix">
                                <h6>Add Staff & invite Agent</h6>
                                <div class="search_agent">
                                    <input type="text" name="" class="" placeholder="Enter UserName" />
                                    <button type="button">Search</button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td class="column-one">
                                                <div class="property-img">
                                                    <img src="http://localhost/property-lookout//assets/img/slider-1.jpg" class="img-responsive" />
                                                </div>
                                            </td>
                                            <td class="column-two"> 
                                                <span>Agent 1</span>
                                                <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                            </td>
                                            <td class="column-three">
                                                <a href="#" class="btn btn-primary small-btn">Send Request</a>
                                                <a href="#" class="btn btn-primary small-btn">View Profile</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="column-one">
                                                <div class="property-img">
                                                    <img src="http://localhost/property-lookout//assets/img/slider-1.jpg" class="img-responsive" />
                                                </div>
                                            </td>
                                            <td class="column-two"> 
                                                <span>Agent 1</span>
                                                <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                            </td>
                                            <td class="column-three">
                                                <a href="#" class="btn btn-primary small-btn">Send Request</a>
                                                <a href="#" class="btn btn-primary small-btn">View Profile</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="bio" class="tab-pane fade">
                        <h3>Staff</h3>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Staff's Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" placeholder="About Us" name="about" id="about" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">No. of Staff</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="facebook_url" value="{{ @$user_meta['facebook_url'] }}" name="facebook_url" placeholder="@lang('app.facebook_url')"/>
                                </div>
                            </div>
                          
                            <a href="#" class="btn btn-primary black-btn m-t-20" id="settings_save_btn">Next</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript">
    $(document).ready(function () {
        if(window.location.href.indexOf("security") > -1) {
            // alert("your url contains the name sec.");
            $("#general_tab" ).removeClass( 'active' );
            $("#sec_tab" ).addClass( 'active' );
            $("#general").removeClass('in active');
            $("#security").addClass('in active');
        }
    });
</script>
<script>
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', options)
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', options)
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Success!', options)
    @endif
</script>
<script type="text/javascript">
    $(document).on('click', '.browse', function(){
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });
    $(document).on('change', '.file', function(){
        console.log($(this).val());
        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
</script>
<script>
    $('#settings_save_btn').click(function(e){
        e.preventDefault();
        var this_btn = $(this);
        this_btn.attr('disabled', 'disabled');
        var form_data = this_btn.closest('form').serialize();
        $.ajax({
            url : '{{ url('agents/setting-update') }}',
            type: "POST",
            data: form_data,
            success : function (data) {            
                if (data.success == 1){
                    this_btn.removeAttr('disabled');
                    toastr.success(data.msg, '@lang('app.success')', toastr_options);
                }
            }
       });
    });
</script>
<script>
    $('#change_password').click(function(e){
        e.preventDefault();
        var this_btn = $(this);
        this_btn.attr('disabled', 'disabled');
        var form_data = this_btn.closest('form').serialize();
        $.ajax({
            url : "{{ url('dashboard/u/posts/account/change-password') }}",
            type: "POST",
            data: form_data,
            success : function (data) {            
                this_btn.removeAttr('disabled');
                if (data.success == 1){
                    toastr.success(data.msg, '@lang('app.success')', toastr_options);
                    window.location.href = "{{url('')}}"+'/'+'agents/settings';
    
                }else if(data.success == 0){
                    // this_btn.removeAttr('disabled');
                    toastr.error(data.msg, '@lang('app.error')', toastr_options);
                    window.location.href = "{{url('')}}"+'/'+'agents/settings';
                }
                $.each(data.error, function(key, value){
                    $('.'+key+'_div').addClass('has-error');
                    $('.'+key+'_p').text(value);
                });  
            }
       });
    });
</script>
<script>
    function generate_option_from_json(jsonData, fromLoad){
        //Load Category Json Data To Brand Select
        if (fromLoad === 'category_to_brand'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> <?php echo trans('app.select_a_brand') ?> </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
                }
                $('#brand_select').html(option);
                $('#brand_select').select2();
            }else {
                $('#brand_select').html('');
                $('#brand_select').select2();
            }
            $('#brand_loader').hide('slow');
        }else if(fromLoad === 'country_to_state'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> @lang('app.select_state') </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
                }
                $('#state_select').html(option);
                $('#state_select').select2();
            }else {
                $('#state_select').html('');
                $('#state_select').select2();
            }
            $('#state_loader').hide('slow');
    
        }else if(fromLoad === 'state_to_city'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> @lang('app.select_city') </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
                }
                $('#city_select').html(option);
                $('#city_select').select2();
            }else {
                $('#city_select').html('');
                $('#city_select').select2();
            }
            $('#city_loader').hide('slow');
        }
    }
    $(document).ready(function() {
        $('#phone').keyup(function(){
            $(this).val($(this).val().replace(/[^0-9]/g,""));
        });
    
        $(".notify img").attr("src", "<?php echo asset('/');?>/assets/img/notify.png");
        $(".footer-widget img").attr("src", "<?php echo asset('/');?>/assets/img/footer-logo.png");
    });
    $('[name="country"]').change(function(){
        var country_id = $(this).val();
        $('#state_loader').show();
        $.ajax({
            type : 'POST',
            url : '{{ route('get_state_by_country') }}',
            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
            success : function (data) {
                generate_option_from_json(data, 'country_to_state');
            }
        });
    });
    
    $('[name="state"]').change(function(){
        var state_id = $(this).val();
        $('#city_loader').show();
        $.ajax({
            type : 'POST',
            url : '{{ route('get_city_by_state') }}',
            data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
            success : function (data) {
                generate_option_from_json(data, 'state_to_city');
            }
        });
    });
</script>
@endsection