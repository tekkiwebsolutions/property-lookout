@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<?php use Carbon\Carbon; ?>
<div class="row mlr0">
    <div class="page_wrapper page-{{ @$page->id }}">
        <div class="container">
            <div class="col-lg-12">
                <h2 class="single_page_heading">Notification</h2>
                
                <div class="notification clearfix">
                    <span id="notification-data">
                        @include('theme.notification-render')
                    </span>
                    @if(!empty($notifications))
                        <div class="learn_more show_more">
                            <a class="btn btn-primary read" href="javascript:void(0);">Read More</a>                        
                        </div>
                    @else
                        <div id="no-noti"><h4>No notification to show!!</h4></div>
                    @endif
                </div>
                <div class="learn_more show_more">
                    <a class="btn btn-primary read" href="javascript:void(0);">Read More</a>                        
                </div>
            </div>
        </div>
        <!-- {!! @$page->post_content !!}  -->        
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript">
   @if(session('success'))
       toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
   @endif

    // hide and show previous notifications
    var page = 1;
    $(document).ready(function() {
        loadMoreData(page);
        $('.hidden_previous_notifications').hide();
    });
    
    $(document).on('click','.show_more',function() {
        // $('.hidden_previous_notifications').show();
        // //$('.hidden_previous_notifications').removeStyle('display','block');
        // $(this).addClass('show_less');
        // $(this).removeClass('show_more');
        // $(this).html('<a class="btn btn-primary" href="javascript:void(0);">Read Less</a>');
        page++;
        loadMoreData(page);
    });

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function loadMoreData(page){
        var url = "{{url('')}}";
        var token = getCookie('TOKEN');
        $.ajax({
            url: url + '/all-notifications-json?page=' + page + '&token=' + token,
            type: "get",
            // beforeSend: function()
            // {
            //     $('.ajax-load').show();
            // }
        })
        .done(function(data)
        {
            if(data.html == ""){
                $('.read').remove();
                $('.show_more').html('<span class="btn btn-primary">No more records found</span>');
                return;
            } else {
                $('#no-noti').hide();
            }
            
            //$('.ajax-load').hide();
            $("#notification-data").append(data.html);
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
            alert('server not responding...');

        });
    }
    // $(document).on('click','.show_less',function() {
    //     $('.hidden_previous_notifications').hide();
    //     $(this).addClass('show_more');
    //     $(this).removeClass('show_less');
    //     $(this).html('<a class="btn btn-primary" href="javascript:void(0);">Read More</a>');
    // });

    // hide and show previous notifications

    // delete notification
    $(document).on('click','.deleteNotification',function() {
        var notify_id = $(this).parent().parent().attr('rel');
        var token = getCookie('TOKEN');
        $.ajax({
            context : this,
            url : "{{ url('notifications/delete') }}",
            type : "POST",
            data : {notify_id : notify_id, _token : '{{csrf_token()}}',token: token},
            success:function(data) {

                if(data.success == 1) {
                    //alert($(this).parent().parent().parent().parent().attr('class'));
                    $(this).parent().parent().parent().parent().remove();
                    toastr.success(data.msg, '@lang('app.success')', toastr_options);
                } else {
                    
                    console.log($(this).html());
                    $(this).parent().parent().parent().html('<h4>Oops Some Error Occured.</h4>');
                    toastr.error(data.msg, '@lang('app.error')', toastr_options);
                }
            },
            error:function(data) {
                
                toastr.error(data.msg, '@lang('app.error')', toastr_options);
            }
        });
    });
    // delete notification
</script>
@endsection