@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<link rel="stylesheet" href="{{asset('assets/css/admin.css')}}">
<div class="cms-inner-content agent-bio-data">
   <div class="container">
      <h3 class="unit-title">Create Units</h3>
      <ul class="create-units">
         <li>Units</li>
         <li><a href="#">1</a></li>
         <li><a href="#">2</a></li>
         <li><a href="#">3</a></li>
         <li><a href="#">4</a></li>
         <li><a href="#">Floor 1 settings</a></li>
      </ul>
      <hr class="clearfix" />
      <form class="form-horizontal clearfix" id="adsPostForm">
         {{csrf_field()}}
         <div class="form-group">
            <label class="control-label col-sm-3">Name</label>
            <div class="col-sm-5">
               <input type="text" class="form-control" />
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-sm-3">Description</label>
            <div class="col-sm-5">
               <textarea class="form-control" rows="4"></textarea>
            </div>
         </div>
         <div class="form-group {{ $errors->has('images')? 'has-error':'' }}">
            <label class="control-label col-sm-3">Upload photos</label>
            <div class="col-sm-9 upload-image-scroll">
                <div id="uploaded-ads-image-wrap">
                    @if($ads_images->count() > 0)
                        @foreach($ads_images as $img)
                            <div class="creating-ads-img-wrap">
                                <img src="{{ media_url($img, false) }}" class="img-responsive" />
                                <div class="img-action-wrap" id="{{ $img->id }}">
                                    <a href="javascript:void(0);" class="imgDeleteBtn"><i class="fa fa-trash-o"></i> </a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="file-upload-wrap">
                    <label>
                        <input type="file" name="images" id="images" style="display: none;" />
                        <i class="fa fa-cloud-upload"></i>
                        <p>@lang('app.upload_image')</p>
                        <div class="progress" style="display: none;"></div>
                    </label>
                </div>
                {!! $errors->has('images')? '<p class="help-block">'.$errors->first('images').'</p>':'' !!}
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-sm-3">Number of bathrooms</label>
            <div class="col-sm-2">
               <input type="text" class="form-control" />
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-sm-3">Number of parking</label>
            <div class="col-sm-2">
               <input type="text" class="form-control" />
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-sm-3">Number of bedrooms</label>
            <div class="col-sm-2">
               <input type="text" class="form-control" />
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-sm-3">Size(Sqft.)</label>
            <div class="col-sm-2">
               <input type="text" class="form-control" />
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-sm-3">Price</label>
            <div class="col-sm-2">
               <input type="text" class="form-control" />
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-sm-3">Video Link(optional)</label>
            <div class="col-sm-5">
               <input type="text" class="form-control" />
            </div>
         </div>
         <div class="btns f-right">
            <a href="#" class="btn btn-primary black-btn m-t-20">Previous</a>
            <a href="#" class="btn btn-primary black-btn m-t-20">Next</a>
         </div>
      </form>
   </div>
</div>
@endsection
@section('page-js')
  <script>
        @if(session('success'))
            toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
        @endif
    </script>
    <script type="text/javascript">

            $("#images").change(function() {
                var fd = new FormData(document.querySelector("form#adsPostForm"));
                //$('#loadingOverlay').show();
                $('.progress').show();
                $.ajax({
                    url : '{{ route('upload_ads_image') }}',
                    type: "POST",
                    data: fd,

                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                //console.log(percentComplete);

                                var progress_bar = '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: '+percentComplete+'%">'+percentComplete+'% </div>';

                                if (percentComplete === 100) {
                                    $('.progress').hide();
                                }
                            }
                        }, false);

                        return xhr;
                    },

                    cache: false,
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,   // tell jQuery not to set contentType
                    success : function (data) {
                        //$('#loadingOverlay').hide('slow');
                        if (data.success == 1){
                            $('#uploaded-ads-image-wrap').load('{{ route('append_media_image') }}');
                        } else{
                            toastr.error(data.msg, '<?php echo trans('app.error') ?>', toastr_options);
                        }
                    }
                });
            });


            $('body').on('click', '.imgDeleteBtn', function(){
                //Get confirm from user
                if ( ! confirm('{{ trans('app.are_you_sure') }}')){
                    return '';
                }

                var current_selector = $(this);
                var img_id = $(this).closest('.img-action-wrap').attr('id');
                $.ajax({
                    url : '{{ route('delete_media') }}',
                    type: "POST",
                    data: { media_id : img_id, _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        if (data.success == 1){
                            current_selector.closest('.creating-ads-img-wrap').hide('slow');
                            toastr.success(data.msg, '@lang('app.success')', toastr_options);
                        }
                    }
                });
            });
            
            $('body').on('click', '.imgFeatureBtn', function(){
                var img_id = $(this).closest('.img-action-wrap').attr('id');
                var current_selector = $(this);

                $.ajax({
                    url : '{{ route('feature_media_creating_ads') }}',
                    type: "POST",
                    data: { media_id : img_id, _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        if (data.success == 1){
                            $('.imgFeatureBtn').html('<i class="fa fa-star-o"></i>');
                            current_selector.html('<i class="fa fa-star"></i>');
                            toastr.success(data.msg, '@lang('app.success')', toastr_options);
                        }
                    }
                });
            });
    </script>
@endsection