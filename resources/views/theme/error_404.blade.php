@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('main')

    <div class="container">    
        <!-- <div class="bg-white error-404-wrap">
            <h1>
                Oops!</h1>
            <h2>
                404 Not Found</h2>
            <div class="error-details">
                Sorry, an error has occured, Requested page not found!
            </div>
        </div> -->
        <div class="error-flx-prnt">
            <h3 class="nthng-fnd">Ohh ! Nothing Found</h3>
            <h1 class="err">404</h1>
            <h3 class="sry">Sorry...</h3>
            <p class="undr-sry">The page you are looking for does not exist or another error occurred.</p>
            <a href="{{ url('/') }}" class="btn btn-custom bk-t-hm">Back To Home</a>
        </div>
    </div>

@endsection
