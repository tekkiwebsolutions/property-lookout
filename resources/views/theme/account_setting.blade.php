@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="modern-top-intoduce-section banner">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 col-lg-12">
            <h2>Account Setting</h2>
         </div>
      </div>
   </div>
</div>
<div class="about_us single-agent_detail">
   <div class="container">
      <div class="row clearfix">
         <div class="col-md-3 col-sm-5 agent-avtar">
            <div class="avtar"><img src="<?php echo asset('/');?>/assets/img/marcus.png" alt="#"></div>
            <ul class="agent_profiles text-center">
               <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
               <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
               <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
            </ul>
           <ul class="account-type">
               <li>Basic</li>
               <li>5 Apartments creation limit</li>
               <li>50 Properties creation limit</li>
               <li>1 Blog limit</li>
               <li>3 minutes video limit</li>
           </ul>
         </div>
         <div class="col-md-9 col-sm-7 cms-border agent-bio-data">
            <h3>Account Setting</h3>
            <div class="form-group clearfix">
                <label class="control-label col-sm-3 col-xs-4">Name</label>
                <div class="col-sm-6 col-xs-8">
                   <p>Marcus Marrio</p>
                </div>
             </div>
             <div class="form-group clearfix">
                <label class="control-label col-sm-3 col-xs-4">Email</label>
                <div class="col-sm-6 col-xs-8">
                   <p>marrio.sa@gmail.com </p>
                </div>
             </div>
             <div class="form-group clearfix">
                <label class="control-label col-sm-3 col-xs-4">Gender</label>
                <div class="col-sm-6 col-xs-8">
                   <p>Male</p>
                </div>
             </div>
             <div class="form-group clearfix">
                <label class="control-label col-sm-3 col-xs-4">Mobile No.</label>
                <div class="col-sm-6 col-xs-8">
                   <p>+60194526523</p>
                </div>
             </div>
             <div class="form-group clearfix">
                <label class="control-label col-sm-3 col-xs-4">Website</label>
                <div class="col-sm-6 col-xs-8">
                   <p>www.marcusmarrio.com.my </p>
                </div>
             </div>
              <div class="form-group clearfix">
                <label class="control-label col-sm-3 col-xs-4">Address</label>
                <div class="col-sm-6 col-xs-8">
                   <p>lot 4452, Taman Damansara, Kuching, sarawak</p>
                </div>
             </div>
              <div class="form-group clearfix">
                <label class="control-label col-sm-3 col-xs-4">Country</label>
                <div class="col-sm-6 col-xs-8">
                   <p>Malaysia</p>
                </div>
             </div>
             <div class="form-group clearfix">
                <label class="control-label col-sm-3 col-xs-4">Postal code</label>
                <div class="col-sm-6 col-xs-8">
                   <p>93150</p>
                </div>
             </div>
              <div class="form-group clearfix">
                <label class="control-label col-sm-3 col-xs-4">Company</label>
                <div class="col-sm-6 col-xs-8">
                   <p>ABC Estate</p>
                </div>
             </div>
             <h2>About</h2>
             <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
             <div class="btns">
                <a href="#" class="btn btn-primary black-btn m-t-20">Edit Profile</a>
                <a href="#" class="btn btn-primary black-btn m-t-20">Change Password</a>
            </div>
         </div>
      </div>
     
   </div>
</div>
@endsection
@section('page-js')
<script></script>
@endsection