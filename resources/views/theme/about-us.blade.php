@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="row mlr0">
    <div class="page_wrapper page-{{ $page->id }}">
        <div class="modern-top-intoduce-section banner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>About Us</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="about_us">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <h6>Introduction</h6>
                        <h2>{{$posts['title']}}</h2>
                        <p>{!! substr($posts['post_content'], 0, 230) !!}...</p>
                        <div class="learn_more about_more"><a class="btn btn-primary about_us_btn" href="javascript:void(0);">Read More</a></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <img class="img-responsive" src="{{asset('uploads/posts/'.$posts['feature_image'])}}" alt="image"/>  
                    </div>
                </div>
            </div>
        </div>
        <div class="company_work">
            <div class="container">
                <div class="d-flx">
                    <div class="flx-inr">
                        <i class="fa fa-home" aria-hidden="true"></i>
                        <div class="col-text">
                            <h4>{{ @$complete_projects }}</h4>
                            <h5>Complete Project</h5>
                        </div>
                    </div>
                    <div class="flx-inr">
                        <i class="fa fa-key" aria-hidden="true"></i>
                        <div class="col-text">
                            <h4>{{ @$property_sold }}</h4>
                            <h5>Property Sold</h5>
                        </div>
                    </div>
                    <div class="flx-inr">
                        <i class="fa fa-smile-o" aria-hidden="true"></i>
                        <div class="col-text">
                            <h4>{{ @$happy_clients }}</h4>
                            <h5>Happy Clients</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="meet_agent">
            <div class="container">
                <h2>MEET OUR AGENT</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta est,<br /> itaque inventore, eum, hic ullam quos sint dolore doloremque rerum enim?
                </p>
                <div class="agnt-flx">
                    @foreach($users as $key => $value)
                    <div class="agnt-flxinr">
                        <div class="agent-detail">
                            <a href="@if(!empty($value->slug)){{url('agent/'.$value->slug)}}@endif">
                            @if(!empty($value->photo))
                            <img src="{{asset('uploads/avatar/'.$value->photo)}}" class="img-responsive" />
                            @else
                            <img src="http://www.gravatar.com/avatar/1aedb8d9dc4751e229a335e371db8058?s=200&d=mm&r=g" class="img-responsive"/>
                            @endif
                            </a>
                            <div class="detail">
                                <h5>{{$value->name}}</h5>
                                <span>Agent at the property sale. </span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal start -->
<div id="aboutUsModal" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{$posts['title']}}</h4>
            </div>
            <div class="modal-body">
                {!! $posts['post_content'] !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal ends -->
@endsection
@section('page-js')
<script type="text/javascript">
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif
    
    $(document).on('click','.about_us_btn',function(){
    
        $("#aboutUsModal").modal('show');
    
    });
</script>
@endsection