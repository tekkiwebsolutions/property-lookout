@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="row mlr0">
   <div class="page_wrapper page-{{ $page->id }}">
      <div class="container">
        <div class="row">
           <div class="col-sm-6 agent-property">
                <div class="avtar"><img src="http://localhost/realestate/assets/img/marcus.png" /></div>
                  <div class="float-left">
                      <h4>Marcus Marrio</h4>
                      <span>Sale Assocaite - (Property Lookout)</span>
                      <a href="callto:+60194526523"><i class="fa fa-phone m-r-7" aria-hidden="true"></i>+60194526523</a>
                  </div>                    
               </div>
               <div class="col-sm-6 pull-right">
                  <ul class="agent_profiles text-right">
                     <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                     <li><a class="btn btn-primary contact-btn" href="#">Contact</a></li>
                  </ul>
               </div>
        </div>
        <div class="row">
            <div class="col-sm-12 selling-property">
               <div class="carousel-header">
                  <h3>Properties I am Selling</h3>
                  <h6>Properties Selling by location or properties near you </h6>
               </div>

            </div>
          </div>
          
      </div>
      <!-- {!! $page->post_content !!}  -->        
   </div>
</div>
@endsection
@section('page-js')
<script>
   @if(session('success'))
       toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
   @endif
</script>
@endsection