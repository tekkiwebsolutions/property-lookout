@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="cms-inner-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 agent-detail-manual cms-avtar agent-cms-avtar">
                <div class="avtar">
                    <img src="http://localhost/property-lookout//assets/img/marcus.png" />
                </div>
                <ul class="agent_profiles text-center">
                    <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                </ul>
                <div><a class="btn btn-primary contact-btn" href="#">Contact</a></div>
                <h2>Marcus Marrio</h2>
                <span>Sale Assocaite - (Property Lookout)</span>
                <div class="m-15">
                    <span>+60194526523</span>
                    <span>marrio.sa@gmail.com</span>
                </div>
                <span>Accumulated visitors:</span>
                <span>Accumulated Rating:</span>
                <span>Account type:</span>
                <span>Number of apartments:</span>
                <span>Number of properties:</span>
            </div>
            <div class="col-md-9 col-sm-7 cms-border">
                <div class="table-responsive cms-edit-table">
                    <table class="table cms-table">
                        <thead>
                            <tr> 
                                <th>Properties</th>
                                <th>Space</th>
                                <th>Date</th>
                                <th><a href="#" class="btn btn-primary small-btn">Add New</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="columnOne">
                                    <div class="property-img">
                                        <img src="http://localhost/property-lookout//assets/img/slider-1.jpg" class="img-responsive" />
                                    </div>
                                    <div class="float-left">
                                        <span>Charming West Beautiful Villa</span>
                                        <span>Bungalow</span>
                                        <h6>$4,00,000</h6>
                                    </div>
                                </td>
                                <td class="columnTwo"> 
                                    <span>5 Bedrooms</span>
                                    <span>5 Bathrooms</span>
                                    <span>4000 sqft </span>
                                </td>
                                <td>Not released for public confirmation needed</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn action-btn dropdown-toggle" type="button" data-toggle="dropdown">Action
                                        <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a tabindex="-1" href="#" class="btn btn-primary small-btn">Sold</a></li>
                                            <li class="dropdown-submenu">
                                                <a tabindex="-1" href="#" class="btn small-btn btn-success test">Edit</a>
                                                <ul class="dropdown-menu">
                                                    <div class="property-color">
                                                        <ul>
                                                            <li class="red" data-toggle="confirmation">&nbsp;</li>
                                                            <li class="green">&nbsp;</li>
                                                            <li class="green">&nbsp;</li>
                                                            <li class="green">&nbsp;</li>
                                                            <li class="green">&nbsp;</li>
                                                            <li class="green">&nbsp;</li>
                                                            <li class="green">&nbsp;</li>
                                                            <li class="green">&nbsp;</li>
                                                            <li class="red">&nbsp;</li>
                                                            <li class="green">&nbsp;</li>
                                                            <li class="green">&nbsp;</li>
                                                            <li class="green">&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                            <li>&nbsp;</li>
                                                        </ul>
                                                    </div>
                                                </ul>
                                            </li>
                                            <li><a href="#" tabindex="-1" class="btn small-btn btn-danger">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script>
    $('body').confirmation({
        selector: '[data-toggle="confirmation"]'
    });
</script>
<script>
    $(document).ready(function(){
        $('.dropdown-submenu a.test').on("click", function(e){
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    ssss});
</script>
@endsection