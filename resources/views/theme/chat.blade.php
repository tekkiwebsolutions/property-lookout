@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="row mlr0">
   <div class="page_wrapper">
      <div class="container">
         <div class="col-lg-12">
            <h2 class="single_page_heading">Chat</h2>
            <div class="panel panel-default chat_panel clearfix">
              <div class="panel-heading clearfix">
                 <div class="avtar">
                     <img src="http://localhost/property-lookout//assets/img/marcus.png" />
                  </div>
                  <div class="float-left">
                     <h4>Gregory</h4>
                     <h6>View profile</h6>
                  </div>
              </div>
              <div class="panel-body clearfix">
               <div class="recieved-msg">
                 <div class="avtar">
                     <img src="http://localhost/property-lookout//assets/img/marcus.png" />
                  </div>
                  <div class="received_withd_msg">
                     <p>I would like to negotitate about the apartment at Selangor. The indoors look fine!</p>
                     <span class="time-date"> 11:01 AM    |    June 9</span>
                  </div>
               </div>
                <div class="send-msg">
                  <div class="received_withd_msg">
                     <p>Hello Gregory! What would you like to talk about?</p>
                     <span class="time-date"> 11:01 AM    |    June 9</span>
                  </div>
               </div>
               <div class="recieved-msg">
                 <div class="avtar">
                     <img src="http://localhost/property-lookout//assets/img/marcus.png" />
                  </div>
                  <div class="received_withd_msg">
                     <p>I would like to negotitate about the apartment at Selangor. The indoors look fine!</p>
                     <span class="time-date"> 11:01 AM    |    June 9</span>
                  </div>
               </div>
                <div class="send-msg">
                  <div class="received_withd_msg">
                     <p>Hello Gregory! What would you like to talk about?</p>
                     <span class="time-date"> 11:01 AM    |    June 9</span>
                  </div>
               </div>
               <div class="input_msg_write">
                  <input type="text" class="write_msg form-control" placeholder="Write a message" />
                  <button type="button" class="btn btn-primary f-right black-btn">Send</button>
               </div>
              </div>
            </div>
         </div>
      </div>
              
   </div>
</div>
@endsection
@section('page-js')
<script>
   @if(session('success'))
       toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
   @endif
</script>
@endsection