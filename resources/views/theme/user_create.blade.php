@extends('layout.main')
@section('title') Register | @parent @endsection
@section('main')

<style type="text/css">
    .dropdown {
        margin: 0;
        padding: 0;
        border-bottom: 0px solid #DDD;
    }

    .dropdown span {
        display:inline-block;
        width: 80px;
    }
    .file {
          visibility: hidden;
          position: absolute;
    }
  </style>

<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-xs-12">
            <div class="login">
                <!-- <h3 class="authTitle">Sign up or <a href="{{ route('login') }}">@lang('app.login')</a></h3> -->
                <h3 class="authTitle">Complete Your Registeration!</h3>
                @if(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                {{ Form::open(['route'=>'user.store', 'role'=> 'form','files' => true]) }}
                    <input type="hidden" name="user_id" value="{{$userId}}">
                    <input type="hidden" name="package" value="{{$package_purchase->package->name}}">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <h5><b>Package Purchased : </b>@if(!empty($package_purchase)){{$package_purchase->package->name}}@endif</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group {{ $errors->has('first_name')? 'has-error':'' }} ">
                                <input type="text" name="first_name" id="first_name" class="form-control" value="{{ old('first_name') ? old('first_name') : $user['contact_query']['person_to_address'] }}" placeholder="First Name" tabindex="1" maxlength="255">
                                {!! $errors->has('first_name')? '<p class="help-block">'.$errors->first('first_name').'</p>':'' !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group {{ $errors->has('middle_name')? 'has-error':'' }} ">
                                <input type="text" name="middle_name" id="middle_name" class="form-control" value="{{ old('middle_name') }}" placeholder="Middle Name" tabindex="1" maxlength="255">
                                {!! $errors->has('middle_name')? '<p class="help-block">'.$errors->first('middle_name').'</p>':'' !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group {{ $errors->has('last_name')? 'has-error':'' }} ">
                                <input type="text" name="last_name" id="last_name" class="form-control"  value="{{ old('last_name') }}" placeholder="Last Name" tabindex="2" maxlength="255">
                                {!! $errors->has('last_name')? '<p class="help-block">'.$errors->first('last_name').'</p>':'' !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('email')? 'has-error':'' }} ">
                        <input type="email" name="email" id="email" class="form-control" value="@if(!empty($userEmail)){{$userEmail}}@endif" placeholder="Email Address" tabindex="3" readonly>
                        {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}

                    </div>

                    <div class="form-group {{ $errors->has('phone')? 'has-error':'' }}">
                        <input type="text" name="phone" id="phone" class="form-control" value="{{ old('phone') ? old('phone') : $user['contact_query']['contact_number'] }}" placeholder="Phone Number" tabindex="4" maxlength="10">
                        {!! $errors->has('phone')? '<p class="help-block">'.$errors->first('phone').'</p>':'' !!}
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group {{ $errors->has('gender')? 'has-error':'' }}">
                                <select id="gender" name="gender" class="form-control select2" tabindex="5">
                                    <option value="">Select Gender</option>
                                    <option value="male" @if(old('gender') == 'male') selected @endif>Male</option>
                                    <option value="female" @if(old('gender') == 'female') selected @endif>Fe-Male</option>
                                    <option value="third_gender" @if(old('gender') == 'third_gender') selected @endif>Third Gender</option>
                                </select>
                                {!! $errors->has('gender')? '<p class="help-block">'.$errors->first('gender').'</p>':'' !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group {{ $errors->has('user_name')? 'has-error':'' }}">
                                <input type="user_name" name="user_name" id="user_name" class="form-control" placeholder="Username" tabindex="6" value="{{ old('user_name') }}" maxlength="255">
                                {!! $errors->has('user_name')? '<p class="help-block">'.$errors->first('user_name').'</p>':'' !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group {{ $errors->has('password')? 'has-error':'' }}">
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" tabindex="7" maxlength="255">
                                {!! $errors->has('password')? '<p class="help-block">'.$errors->first('password').'</p>':'' !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group {{ $errors->has('password_confirmation')? 'has-error':'' }}">
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password" tabindex="8" maxlength="255">
                                {!! $errors->has('password_confirmation')? '<p class="help-block">'.$errors->first('password_confirmation').'</p>':'' !!}

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('address')? 'has-error':'' }}">
                                <textarea name="address" id="address" class="form-control" placeholder="Address(Work/Home)" tabindex="9">{{ old('address') }}</textarea>
                                {!! $errors->has('address')? '<p class="help-block">'.$errors->first('address').'</p>':'' !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group {{ $errors->has('country')? 'has-error':'' }}">
                                <select id="country" name="country" class="form-control select2" tabindex="10">
                                    <option value="">@lang('app.select_a_country')</option>
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}" {{ old('country') == $country->id ? 'selected' :'' }}>{{ $country->country_name }}</option>
                                    @endforeach
                                </select>
                                {!! $errors->has('country')? '<p class="help-block">'.$errors->first('country').'</p>':'' !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group  {{ $errors->has('state')? 'has-error':'' }}">
                                    <select class="form-control select2" id="state_select" name="state" tabindex="11">
                                        @if($previous_states->count() > 0)
                                            @foreach($previous_states as $state)
                                                <option value="{{ $state->id }}" {{ old('state') == $state->id ? 'selected' :'' }}>{{ $state->state_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    {!! $errors->has('state')? '<p class="help-block">'.$errors->first('state').'</p>':'' !!}
                                    <p class="text-info">
                                        <span id="state_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                                    </p>
                                
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group  {{ $errors->has('city')? 'has-error':'' }}">
                                <select class="form-control select2" id="city_select" name="city" tabindex="12">
                                    @if($previous_cities->count() > 0)
                                        @foreach($previous_cities as $city)
                                            <option value="{{ $city->id }}" {{ old('city') == $city->id ? 'selected':'' }}>{{ $city->city_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                {!! $errors->has('city')? '<p class="help-block">'.$errors->first('city').'</p>':'' !!}
                                <p class="text-info">
                                    <span id="city_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                                </p>
                            </div>
                        </div>
                    </div>                        
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group {{ $errors->has('postal_code')? 'has-error':'' }}">
                                <input type="text" name="postal_code" id="postal_code" class="form-control" placeholder="Enter Postal code" tabindex="13" value="{{ old('postal_code') }}" maxlength="6">
                                {!! $errors->has('postal_code')? '<p class="help-block">'.$errors->first('postal_code').'</p>':'' !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group {{ $errors->has('company')? 'has-error':'' }}">
                                <input type="text" name="company" id="company" class="form-control" placeholder="Enter company name" tabindex="14" value="{{ old('company') ? old('company') : $user['contact_query']['company_name'] }}" maxlength="255">
                                {!! $errors->has('company')? '<p class="help-block">'.$errors->first('company').'</p>':'' !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('description')? 'has-error':'' }}">
                                <textarea name="description" id="description" class="form-control" placeholder="Enter description" tabindex="15">{{ old('description') }}</textarea>
                                {!! $errors->has('description')? '<p class="help-block">'.$errors->first('description').'</p>':'' !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="profile-avatar">
                                <img src="{{asset('assets/img/default_user.png')}}" class="img-thumbnail img-circle image_show" id="image_show">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="{{ $errors->has('photo')? 'has-error':'' }}">
                                <input type="file" id="photo" name="photo" class="filestyle" tabindex="-1" tabindex="16" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                                <div class="bootstrap-filestyle input-group">
                                    <span class="group-span-filestyle input-group-btn" tabindex="0">
                                        <label for="photo" class="btn btn-primary ">
                                            <span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span> 
                                            <span class="buttonText choose_image">Choose Image</span>
                                        </label>
                                    </span>
                                        <!-- <input type="text" class="form-control " placeholder="" disabled="">  -->
                                </div>
                                <p class="image_error" style="color: red; font-size:10px;"></p>
                                {!! $errors->has('photo')? '<p class="help-block">'.$errors->first('photo').'</p>':'' !!}
                            </div>
                        </div>
                    </div>
                    @if($package_purchase->package->name == 'Agent')
                        <h3></h3>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <!-- <input type="file" name="agent_card" id="agent_card"> -->
                                <div class="form-group {{ $errors->has('agent_card')? 'has-error':'' }}">
                                    <input type="file" name="agent_card" tabindex="17" class="file">
                                    <div class="input-group col-xs-12">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                        <input type="text" class="form-control input-lg" disabled placeholder="Upload agent Card">
                                        <span class="input-group-btn">
                                            <button class="browse btn btn-primary input-lg" type="button" style="height: 16%;"><i class="glyphicon glyphicon-folder-open"></i>  Choose File</button>
                                        </span>
                                    </div>
                                    {!! $errors->has('agent_card')? '<p class="help-block">'.$errors->first('agent_card').'</p>':'' !!}
                                </div>   
                            </div>
                        </div>
                        <h3></h3>
                        <div class="row">
                              <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group {{ $errors->has('agent_number')? 'has-error':'' }}">
                                    <input type="text" name="agent_number" id="agent_number" class="form-control" placeholder="Enter Agent Number" tabindex="18" value="{{ old('agent_number') }}" maxlength="255">
                                    {!! $errors->has('agent_number')? '<p class="help-block">'.$errors->first('agent_number').'</p>':'' !!}
                                </div>
                            </div>
                        </div>
                    @endif
                    <h3></h3>

                    <div class="row  {{ $errors->has('agree')? 'has-error':'' }}">
                        <div class="col-xs-4 col-sm-3 col-md-3">
                            <span class="button-checkbox">
                                <label><input type="checkbox" name="agree" value="1" tabindex="19" /> I Agree </label>
                            </span>
                        </div>
                        <div class="col-xs-8 col-sm-9 col-md-9">
                            <b>By clicking <strong class="label label-primary">Register</strong>, you agree to the <a href="{{ route('single_page', 'terms-and-condition') }}" target="_blank">Terms and Conditions</a> set out by this site, including our Cookie Use.</b>
                        </div>
                        <div class="col-sm-12">
                            {!! $errors->has('agree')? '<p class="help-block">You must agree with terms and condition</p>':'' !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12"><input type="submit" value="Register" class="btn btn-primary m-t-20 btn-block btn-lg" tabindex="20"></div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript">
    $(document).on('click', '.browse', function(){
      var file = $(this).parent().parent().parent().find('.file');
      file.trigger('click');
    });
    $(document).on('change', '.file', function(){
      $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            console.log(reader);

            reader.onload = function (e) {
                $('.image_show').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    (function($) {
        $.fn.checkFileType = function(options) {
            var defaults = {
                allowedExtensions: [],
                success: function() {},
                error: function() {}
            };
              options = $.extend(defaults, options);
            return this.each(function() {
                $(this).on('change', function() {
                    var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);
                    if ($.inArray(extension, options.allowedExtensions) == -1) {
                        options.error();
                        $(this).focus();
                    } else {
                        options.success();
                        readURL(this);
                    }

                });

            });
        };

    })(jQuery);

    $(function() {
        $('#photo').checkFileType({
            allowedExtensions: ['jpg', 'jpeg','png'],
            success: function() {
                // alert('Success');
                $(".image_error").text('');

            },
            error: function() {
                // alert('Error');
                var loc = "{{asset('/assets/img/default_user.png')}}";
                $(".image_show").attr("src",loc);
                $(".image_error").text('Only ".jpg",".jpeg",".png" files are allowed.');

            }
        });

    });

    $(document).ready(function() {
        $('#phone').keyup(function(){
            $(this).val($(this).val().replace(/[^0-9]/g,""));
        });
   
        $(".notify img").attr("src", "<?php echo asset('/');?>/assets/img/notify.png");
        $(".footer-widget img").attr("src", "<?php echo asset('/');?>/assets/img/footer-logo.png");
    });
   
    function generate_option_from_json(jsonData, fromLoad){
        //Load Category Json Data To Brand Select
        if (fromLoad === 'category_to_brand'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> <?php echo trans('app.select_a_brand') ?> </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
                }
                $('#brand_select').html(option);
                $('#brand_select').select2();
            }else {
                $('#brand_select').html('');
                $('#brand_select').select2();
            }
            $('#brand_loader').hide('slow');
        }else if(fromLoad === 'country_to_state'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> @lang('app.select_state') </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
                }
                $('#state_select').html(option);
                $('#state_select').select2();
            }else {
                $('#state_select').html('');
                $('#state_select').select2();
            }
            $('#state_loader').hide('slow');

        }else if(fromLoad === 'state_to_city'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> @lang('app.select_city') </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
                }
                $('#city_select').html(option);
                $('#city_select').select2();
            }else {
                $('#city_select').html('');
                $('#city_select').select2();
            }
            $('#city_loader').hide('slow');
        }
    }
    $(document).ready(function() {
        $('#phone').keyup(function(){
            $(this).val($(this).val().replace(/[^0-9]/g,""));
        });

        $(".notify img").attr("src", "<?php echo asset('/');?>/assets/img/notify.png");
        $(".footer-widget img").attr("src", "<?php echo asset('/');?>/assets/img/footer-logo.png");
        
    });

    $('[name="country"]').change(function(){
        var country_id = $(this).val();
        $('#state_loader').show();
        $.ajax({
            type : 'POST',
            url : '{{ route('get_state_by_country') }}',
            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
            success : function (data) {
                generate_option_from_json(data, 'country_to_state');
            }
        });
    });

    $('[name="state"]').change(function(){
        var state_id = $(this).val();
        $('#city_loader').show();
        $.ajax({
            type : 'POST',
            url : '{{ route('get_city_by_state') }}',
            data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
            success : function (data) {
                generate_option_from_json(data, 'state_to_city');
            }
        });
    });

    $(document).on('click','.register_user_button',function() {
        var user_name = $('#user_name').val();
        var last_name = $('#last_name').val();
        var mobile = $('#mobile_number').val();
        if(user_name == '') {
            swal("Oops","Please enter username","error");
        }
        if(last_name == '') {
            swal("Oops","Please enter last name","error");
        }
        if(mobile == '') {
            swal("Oops","Please enter mobile number","error");
        }
    }); 
</script>
@endsection
   