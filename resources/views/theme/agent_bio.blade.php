@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="cms-inner-content">
   <div class="container">
      <div class="row">
         <div class="col-md-3 col-sm-5 cms-avtar">
            <div class="avtar">
               <img src="http://localhost/property-lookout//assets/img/marcus.png" />
            </div>
            <ul class="nav nav-pills cms-tabs">
               <li class="active"><a data-toggle="pill" href="#general"><i class="fa fa-cog" aria-hidden="true"></i>General</a></li>
               <li><a data-toggle="pill" href="#security"><i class="fa fa-lock" aria-hidden="true"></i>Security</a></li>
               <li><a data-toggle="pill" href="#bio"><i class="fa fa-user" aria-hidden="true"></i>Bio</a></li>
            </ul>
         </div>
         <div class="col-md-9 col-sm-7 cms-border">
            <div class="tab-content agent-bio-data">
               <div id="general" class="tab-pane fade in active">
                  <h3>General Account Setting</h3>
                  <form class="form-horizontal">
                     <div class="form-group">
                        <label class="control-label col-sm-3">Name</label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">Email</label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">Gender</label>
                        <div class="col-sm-6">
                          <select class="form-control">
                              <option value="">Male</option>
                              <option value="">Female</option>
                          </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">Mobile No.</label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" />
                        </div>
                     </div>
                       <div class="form-group">
                        <label class="control-label col-sm-3">Website</label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">Address</label>
                        <div class="col-sm-6">
                           <textarea class="form-control" rows="2"></textarea>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">Country</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" />
                        </div>
                     </div>
                      <div class="form-group">
                        <label class="control-label col-sm-3">Postal Code</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">Company</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" />
                        </div>
                     </div>
                     <a href="#" class="btn btn-primary black-btn m-t-20">Save Changes</a>
                  </form>
               </div>
               <div id="security" class="tab-pane fade">
                   <h3>Security Setting</h3>
                  <form class="form-horizontal">
                     <div class="form-group">
                        <label class="control-label col-sm-3">Current Password</label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">New Password</label>
                       <div class="col-sm-6">
                           <input type="text" class="form-control" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">Confirm Password</label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" />
                        </div>
                     </div>
                     <a href="#" class="btn btn-primary black-btn m-t-20">Save Changes</a>
                  </form>
               </div>
               <div id="bio" class="tab-pane fade">
                  <h3>Bio Setting</h3>
                  <form class="form-horizontal">
                     <div class="form-group">
                        <label class="control-label col-sm-3">Agent’s About</label>
                        <div class="col-sm-9">
                           <textarea class="form-control" placeholder="About Us" rows="5"></textarea>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">Facebook</label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">LinkedIn</label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">Twitter</label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" />
                        </div>
                     </div>
                     <a href="#" class="btn btn-primary black-btn m-t-20">Save Changes</a>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('page-js')
<script></script>
@endsection