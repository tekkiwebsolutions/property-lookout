@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="modern-top-intoduce-section">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="modern-top-hom-cat-section">
               <div class="modern-home-search-bar-wrap">
                  <div class="search-wrapper advance_search">
                     <h3> <i class="fa fa-home"></i> @lang('app.find_your_property')</h3>
                     <h6>@lang('app.million_properties')</h6>
                     <form class="form-inline" action="{{ route('listing') }}" method="get">
                        <div class="form-group row-width8">
                           <input type="text" class="form-control" name="q" value="{{ request('q') }}" placeholder="@lang('app.search___')" />
                        </div>
                        <button type="submit" class="btn theme-btn"> <i class="fa fa-search"></i> Search</button>
                        <div class="row row-width">
                           <div class="col-md-3 col-sm-6">
                              <div class="form-group">
                                 <select class="form-control select2" name="country">
                                    <option value="">@lang('app.select_a_country')</option>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <select class="form-control select2" id="state_select" name="state">
                                    <option value=""> @lang('app.select_state') </option>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <input type="number" class="form-control" name="min_price" placeholder="Price From">
                              </div>
                           </div>
                           <div class="col-md-3 col-sm-6">
                              <div class="form-group">
                                 <select class="form-control select2" id="city_select" name="city">
                                    <option value="">City</option>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <
                                 <input id="area" type="text" size="50" placeholder="Enter Area" autocomplete="on" runat="server" class="form-control" name="area"/>  
                                 <input type="hidden" id="area2" name="area2" />
                                 <input type="hidden" id="lat" name="lat" />
                                 <input type="hidden" id="long" name="long" />
                              </div>
                              <div class="form-group">
                                 <input type="number" class="form-control" name="max_price" placeholder="Price to">
                              </div>
                           </div>
                           <div class="col-md-3 col-sm-6">
                              <div class="form-group w-70">
                                 <pre>Sqft</pre>
                              </div>
                              <div class="form-group w-70">
                                 <select class="form-control select2" id="" name="">
                                    <option value="">Sq.</option>
                                    <option value="">4</option>
                                    <option value="">4</option>
                                    <option value="">4</option>
                                 </select>
                              </div>
                              <div class="form-group w-70">
                                 <select class="form-control select2" id="" name="">
                                    <option value="">Ft.</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                 </select>
                              </div>
                              <div class="form-group w-70">
                                 <pre>Bedroom</pre>
                              </div>
                              <div class="form-group w-70">
                                 <select class="form-control select2" id="" name="bedroom_start">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                 </select>
                              </div>
                              <div class="form-group w-70">
                                 <select class="form-control select2" id="" name="bedroom_end">
                                    <option value="5">5+</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                 </select>
                              </div>
                              <div class="form-group w-70">
                                 <pre>Bathroom</pre>
                              </div>
                              <div class="form-group w-70">
                                 <select class="form-control select2" id="" name="bathroom_start">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                 </select>
                              </div>
                              <div class="form-group w-70">
                                 <select class="form-control select2" id="" name="bathroom_end">
                                    <option value="5">5+</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-3 col-sm-6">
                              <div class="form-group">
                                 <select class="form-control select2" name="property_type">
                                    <option value="">@lang('app.property_type')</option>
                                    <option value="apartment">@lang('app.apartment')</option>
                                    <option value="condos">@lang('app.condos')</option>
                                    <option value="house">@lang('app.house')</option>
                                    <option value="land">@lang('app.land')</option>
                                    <option value="commercial_space">@lang('app.commercial_space')</option>
                                    <option value="villa">@lang('app.villa')</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </form>
                     <!-- <div class="or-search"> OR </div>
                        <a href="{{ route('listing') }}" class="btn btn-info btn-lg"><i class="fa fa-search-plus"></i> @lang('app.try_advance_search')</a> -->
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
</div>
<div class="container">
   <div class="row">
      <div class="col-md-8 ad-box-grid-view property_list">
         <h3>property Listing</h3>
         <h6>Property Listing by location or properties near you </h6>
         <div itemscope="" itemtype="http://schema.org/Product" class="ads-item-thumbnail ad-box-regular">
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-0">
                  <div class="ads-thumbnail">
                     <!-- <a href="http://localhost/property-lookout/ad/exclusive-flat-in-cheap-price-in-rome"> -->
                     <a href="javascript:void;">
                        <!-- <img itemprop="image" src="http://localhost/property-lookout/uploads/images/thumbs/1483211248y81bw-home-1237469-640.jpg" class="img-responsive" alt="Exclusive flat in cheap price in Rome"> -->
                        <img itemprop="image" src="{{asset('uploads/images/thumbs/1483211248y81bw-home-1237469-640.jpg')}}" class="img-responsive property_image" alt="Exclusive flat in cheap price in Rome">
                        <span class="modern-sale-rent-indicator">
                        For&nbsp;Sale
                        </span>
                        <p class="date-posted text-muted"> <i class="fa fa-clock-o"></i> 1 year ago</p>
                        <div class="img-ftr">
                           <span class="price"> <b itemprop="price" content="8000.00"> USD 8,000.00 </b></span>
                           <span class="avatar">
                              <img src="{{asset('assets/img/avtar.jpg')}}" alt="#">
                              <span class="user_name">Grant Barron</span>
                              <span class="modern-img-indicator">
                              <span>2&nbsp;Photos</span>
                              </span>
                           </span>
                        </div>
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="caption">
                     <!-- <h4><a href="http://localhost/property-lookout/ad/exclusive-flat-in-cheap-price-in-rome" title="Exclusive flat in cheap price in Rome"><span itemprop="name">Exclusive flat in cheap price in Rome </span></a></h4> -->
                     <h4><a href="javascript:void;" title="Exclusive flat in cheap price in Rome"><span itemprop="name">Exclusive flat in cheap price in Rome </span></a></h4>
                     <!-- <a class="location text-muted" href="http://localhost/property-lookout/listing?city=22855"> <i class="fa fa-map-marker"></i> Rome </a> -->
                     <a class="location text-muted" href="javascript:void;"> <i class="fa fa-map-marker"></i> Rome </a>
                     <hr>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting…</p>
                     <table class="table table-responsive property-box-info">
                        <tbody>
                           <tr>
                              <td><i class="fa fa-building"></i> Apartment </td>
                              <td><i class="fa fa-arrows-alt "></i>  1000 sqft</td>
                              <td><span class="view"><i class="fa fa-eye" aria-hidden="true"></i>450</span></td>
                           </tr>
                           <tr>
                              <td><i class="fa fa-bed"></i> 3 Bedrooms</td>
                              <td><img src="{{asset('assets/img/floor.png')}}"> 18th Floor </td>
                           </tr>
                        </tbody>
                     </table>
                     <div class="property-btn">
                        <a href="#" class="btn btn-primary black-btn green-btn">Unit available</a>
                        <a href="#" class="btn btn-primary black-btn">Details</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-0">
                  <div class="ads-thumbnail">
                     <!-- <a href="http://localhost/property-lookout/ad/exclusive-flat-in-cheap-price-in-rome"> -->
                     <a href="javascript:void;">
                        <!-- <img itemprop="image" src="http://localhost/property-lookout/uploads/images/thumbs/1483211248y81bw-home-1237469-640.jpg" class="img-responsive" alt="Exclusive flat in cheap price in Rome"> -->
                        <img itemprop="image" src="{{asset('uploads/images/thumbs/1483211248y81bw-home-1237469-640.jpg')}}" class="img-responsive property_image" alt="Exclusive flat in cheap price in Rome">
                        <span class="modern-sale-rent-indicator">
                        For&nbsp;Sale
                        </span>
                        <p class="date-posted text-muted"> <i class="fa fa-clock-o"></i> 1 year ago</p>
                        <div class="img-ftr">
                           <span class="price"> <b itemprop="price" content="8000.00"> USD 8,000.00 </b></span>
                           <span class="avatar">
                              <img src="{{asset('assets/img/avtar.jpg')}}" alt="#">
                              <span class="user_name">Grant Barron</span>
                              <span class="modern-img-indicator">
                              <span>2&nbsp;Photos</span>
                              </span>
                           </span>
                        </div>
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="caption">
                     <!-- <h4><a href="http://localhost/property-lookout/ad/exclusive-flat-in-cheap-price-in-rome" title="Exclusive flat in cheap price in Rome"><span itemprop="name">Exclusive flat in cheap price in Rome </span></a></h4> -->
                     <h4><a href="javascript:void;" title="Exclusive flat in cheap price in Rome"><span itemprop="name">Exclusive flat in cheap price in Rome </span></a></h4>
                     <!-- <a class="location text-muted" href="http://localhost/property-lookout/listing?city=22855"> <i class="fa fa-map-marker"></i> Rome </a> -->
                     <a class="location text-muted" href="javascript:void;"> <i class="fa fa-map-marker"></i> Rome </a>
                     <hr>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting…</p>
                     <table class="table table-responsive property-box-info">
                        <tbody>
                           <tr>
                              <td><i class="fa fa-building"></i> Apartment </td>
                              <td><i class="fa fa-arrows-alt "></i>  1000 sqft</td>
                              <td><span class="view"><i class="fa fa-eye" aria-hidden="true"></i>450</span></td>
                           </tr>
                           <tr>
                              <td><i class="fa fa-bed"></i> 3 Bedrooms</td>
                              <td><img src="{{asset('assets/img/floor.png')}}"> 18th Floor </td>
                           </tr>
                        </tbody>
                     </table>
                     <div class="property-btn">
                        <a href="#" class="btn btn-primary black-btn green-btn">Unit available</a>
                        <a href="#" class="btn btn-primary black-btn">Details</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-0">
                  <div class="ads-thumbnail">
                     <!-- <a href="http://localhost/property-lookout/ad/exclusive-flat-in-cheap-price-in-rome"> -->
                     <a href="javascript:void;">
                        <!-- <img itemprop="image" src="http://localhost/property-lookout/uploads/images/thumbs/1483211248y81bw-home-1237469-640.jpg" class="img-responsive" alt="Exclusive flat in cheap price in Rome"> -->
                        <img itemprop="image" src="{{asset('uploads/images/thumbs/1483211248y81bw-home-1237469-640.jpg')}}" class="img-responsive property_image" alt="Exclusive flat in cheap price in Rome">
                        <span class="modern-sale-rent-indicator">
                        For&nbsp;Sale
                        </span>
                        <p class="date-posted text-muted"> <i class="fa fa-clock-o"></i> 1 year ago</p>
                        <div class="img-ftr">
                           <span class="price"> <b itemprop="price" content="8000.00"> USD 8,000.00 </b></span>
                           <span class="avatar">
                              <img src="{{asset('assets/img/avtar.jpg')}}" alt="#">
                              <span class="user_name">Grant Barron</span>
                              <span class="modern-img-indicator">
                              <span>2&nbsp;Photos</span>
                              </span>
                           </span>
                        </div>
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="caption">
                     <!-- <h4><a href="http://localhost/property-lookout/ad/exclusive-flat-in-cheap-price-in-rome" title="Exclusive flat in cheap price in Rome"><span itemprop="name">Exclusive flat in cheap price in Rome </span></a></h4> -->
                     <h4><a href="javascript:void;" title="Exclusive flat in cheap price in Rome"><span itemprop="name">Exclusive flat in cheap price in Rome </span></a></h4>
                     <!-- <a class="location text-muted" href="http://localhost/property-lookout/listing?city=22855"> <i class="fa fa-map-marker"></i> Rome </a> -->
                     <a class="location text-muted" href="javascript:void;"> <i class="fa fa-map-marker"></i> Rome </a>
                     <hr>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting…</p>
                     <table class="table table-responsive property-box-info">
                        <tbody>
                           <tr>
                              <td><i class="fa fa-building"></i> Apartment </td>
                              <td><i class="fa fa-arrows-alt "></i>  1000 sqft</td>
                              <td><span class="view"><i class="fa fa-eye" aria-hidden="true"></i>450</span></td>
                           </tr>
                           <tr>
                              <td><i class="fa fa-bed"></i> 3 Bedrooms</td>
                              <td><img src="{{asset('assets/img/floor.png')}}"> 18th Floor </td>
                           </tr>
                        </tbody>
                     </table>
                     <div class="property-btn">
                        <a href="#" class="btn btn-primary black-btn green-btn">Unit available</a>
                        <a href="#" class="btn btn-primary black-btn">Details</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="latest_listings">
            <h3>Latest Listings</h3>
            <div class="latest-list">
               <div class="latest_listing_img">
                  <a href="#/"><img src="<?php echo asset('/');?>/assets/img/slider-5.jpg" class="img-responsive" alt="#"></a>
               </div>
               <div class="latest_listing_des">
                  <h6><a href="#">Charming West Beautiful Villa </a></h6>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting…</p>
                  <span>215 Terry Lane </span>
               </div>
            </div>
             <div class="latest-list">
               <div class="latest_listing_img">
                  <a href="#/"><img src="<?php echo asset('/');?>/assets/img/slider-5.jpg" class="img-responsive" alt="#"></a>
               </div>
               <div class="latest_listing_des">
                  <h6><a href="#">Charming West Beautiful Villa </a></h6>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting…</p>
                  <span>215 Terry Lane </span>
               </div>
            </div>
             <div class="latest-list">
               <div class="latest_listing_img">
                  <a href="#/"><img src="<?php echo asset('/');?>/assets/img/slider-5.jpg" class="img-responsive" alt="#"></a>
               </div>
               <div class="latest_listing_des">
                  <h6><a href="#">Charming West Beautiful Villa </a></h6>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting…</p>
                  <span>215 Terry Lane </span>
               </div>
            </div>
         </div>
         <div class="feature_agents clearfix">
            <h3>Feature Agents</h3>
            <div class="author-area clearfix">
               <div class="author-img">
                  <a href="#/"><img src="<?php echo asset('/');?>/assets/img/marcus.png" alt="#"></a>
               </div>
               <div class="author-info">
                  <ul>
                     <li class="name"><a href="#/">Gregory</a></li>
                     <li class="address">United Estate, USA</li>
                     <li class="call">+1 (123) 456-852</li>
                  </ul>
               </div>
            </div>
            <div class="author-area clearfix">
               <div class="author-img">
                  <a href="#/"><img src="<?php echo asset('/');?>/assets/img/marcus.png" alt="#"></a>
               </div>
               <div class="author-info">
                  <ul>
                     <li class="name"><a href="#/">Gregory</a></li>
                     <li class="address">United Estate, USA</li>
                     <li class="call">+1 (123) 456-852</li>
                  </ul>
               </div>
            </div>
            <div class="author-area clearfix">
               <div class="author-img">
                  <a href="#/"><img src="<?php echo asset('/');?>/assets/img/marcus.png" alt="#"></a>
               </div>
               <div class="author-info">
                  <ul>
                     <li class="name"><a href="#/">Gregory</a></li>
                     <li class="address">United Estate, USA</li>
                     <li class="call">+1 (123) 456-852</li>
                  </ul>
               </div>
            </div>
            <div class="author-area clearfix">
               <div class="author-img">
                  <a href="#/"><img src="<?php echo asset('/');?>/assets/img/marcus.png" alt="#"></a>
               </div>
               <div class="author-info">
                  <ul>
                     <li class="name"><a href="#/">Gregory</a></li>
                     <li class="address">United Estate, USA</li>
                     <li class="call">+1 (123) 456-852</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('page-js')
@endsection