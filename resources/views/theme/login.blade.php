@extends('layout.main')
@section('title') Log-In | @parent @endsection
@section('main')
<div class="container">
    <div class="row">

        <div class="col-sm-6 col-sm-offset-3 col-xs-12">
            <div class="login">
                <h3 class="authTitle">Login or <a href="{{ route('user.create') }}">Sign up</a></h3>
                @if(get_option('enable_social_login') == 1)
                <div class="row row-sm-offset-3 socialButtons">
                    @if(get_option('enable_facebook_login') == 1)
                    @endif
                    @if(get_option('enable_google_login') == 1)
                    @endif
                </div>
                <div class="row row-sm-offset-3 loginOr">
                    <div class="col-xs-12">
                        <hr class="hrOr">

                    </div>
                </div>
                @endif
                <div class="row row-sm-offset-3">
                    <div class="col-xs-12">
                        {{ Form::open(['class'=> 'loginForm', 'autocomplete'=> 'off']) }}
                        <div class="input-group {{ $errors->has('email')? 'has-error':'' }}">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email address">
                        </div>
                        {!! $errors->has('email')? '

                        <p class="help-block">'.$errors->first('email').'</p>
                        ':'' !!}
                        <span class="help-block"></span>
                        <div class="input-group {{ $errors->has('password')? 'has-error':'' }}">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input  type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                        {!! $errors->has('password')? '
                        <p class="help-block">'.$errors->first('password').'</p>
                        ':'' !!}
                        <span class="help-block"></span>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="login-flx">
                    <div class="login-flxinr chekbx-div">
                        <input type="checkbox" value="remember-me">Remember Me
                    </div>
                    <div class="login-flxinr forgt-div">
                        <a href="#" data-toggle="modal" data-target="#forgotPasswordEmail"> @lang('app.forgot_password')</a>
                    </div>
                </div>
                <p class="new_register">Are you an agent or developer wanting to join the platform?&nbsp;<a href="{{ route('packages') }}">Register</a></p>
                <!-- <div class="login-flx">
                    <div class="login-flxinr chekbx-div">
                        <input type="checkbox" value="remember-me">Remember Me
                    </div>
                    <div class="login-flxinr forgt-div">
                        <a href="#" data-toggle="modal" data-target="#forgotPasswordEmail"> @lang('app.forgot_password')</a>
                    </div>
                </div>
                <p class="new_register">Are you an agent wanting to join the platform?&nbsp;<a href="{{ route('packages') }}">Register</a></p> -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="forgotPasswordEmail" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <!-- {!! Form::open(['route'=>'send_reset_link']) !!} -->
            <!-- <form id="reset_password_link">
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">@lang('app.enter_email_to_reset_password')</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="email" class="control-label">@lang('app.email'):</label>
                        <input type="text" class="form-control" id="email" name="email">
                        <div id="email_info"><p class="show_email_error" style="color:red;"></p></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('app.close')</button>
                    <button type="button" class="btn btn-primary" id="send_reset_link">@lang('app.send_reset_link')</button>
                </div> -->

                <!-- {!! Form::open(['route'=>'send_reset_link']) !!} -->
            <form id="reset_password_link">
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">@lang('app.enter_email_to_reset_password')</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="email" class="control-label">@lang('app.email'):</label>
                        <input type="text" class="form-control" id="email" name="email">
                        <div id="email_info"><p class="show_email_error" style="color:red;"></p></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default otlin-btn pull-right" data-dismiss="modal">@lang('app.close')</button>
                    <button type="button" class="btn btn-primary black-btn pull-left" id="send_reset_link">@lang('app.send_reset_link')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script>
    var options = {closeButton : true};
    @if(session('error'))
        toastr.error('{{ session('error') }}', 'Error!', toastr_options);
    @endif
    @if(session('warning'))
        toastr.warning('{{ session('warning') }}', 'Warning!', toastr_options);
    @endif
    @if(session('success'))
        toastr.success('{{ session('success') }}', 'Success!', toastr_options);
    @endif

    $(document).on('click','#send_reset_link',function() {
        $('#send_reset_link').prop('disabled',true);
        var form_data = $('#reset_password_link').serializeArray();
        $.ajax({
            url: "{{ route('send_reset_link') }}",
            type: "post",
            data: form_data,
            success: function(data) {
                if(data.success == '1') {
                    var msg = 'Reset Password link has been sent to your registered email. Please reset your password there.';
                    toastr.success(msg, '@lang('app.success')', toastr_options);
                    $('#email').val('');
                    $('.show_email_error').text('');
                    $('#send_reset_link').prop('disabled',false);
                    $('#forgotPasswordEmail').modal('hide');
                } else {
                    $('.show_email_error').text(data.error);
                    $('#send_reset_link').prop('disabled',false);
                }
            }

        })
    });
</script>
@endsection