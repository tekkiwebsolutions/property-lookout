<?php use Carbon\Carbon; ?>
@if(!empty(@$notifications))
    @foreach(@$notifications as $notify)
        <div class="notify-n clearfix">
            <ul class="notify-icons">
                @if(Auth::check())
                    <li rel="{{ $notify['id'] }}"><a href="javascript:void(0);"><i class="fa fa-times deleteNotification" aria-hidden="true"></i></a></li>
                @endif
            </ul>
            <div class="notify-des">
                @if(!empty(@$notify['user']))
                    <p><a href=""><b>
                    @if(!empty(@$notify['user']['name']))
                        {{ @$notify['user']['name'] }}
                    @elseif(!empty(@$notify['user']['first_name']) || !empty(@$notify['user']['last_name']))
                        {{ @$notify['user']['first_name'].' '.@$notify['user']['last_name'] }}
                    @endif
                    </b></a> {{ $notify['type_text'] ? $notify['type_text'] : '' }}</p>
                @endif
                @if(!empty($notify['message']))
                    <p>{{ $notify['message'] }}</p>
                @endif
                <span>{{ @$notify['created_at'] }} | Applicant Tracking</span>
            </div>
            <div class="pull-right"><em>{{ Carbon::parse($notify['created_at'])->format('d/m/y') }}</em></div>
        </div>
    @endforeach
@endif
