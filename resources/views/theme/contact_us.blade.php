@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<style type="text/css">
    #map {
        height: 360px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
    }
</style>
<div class="modern-top-intoduce-section banner">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h2>@lang('app.contact_with_us')</h2>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="contact_detail cntct-flx">
        <div class="contact-box-flx">
            <div class="contact-col">
                <i class="fa fa-map-marker"></i>
                <address>
                    {{ $posts['address'] }}
                </address>
            </div>
        </div>
        <div class="contact-box-flx">
            <div class="contact-col">
               <i class="fa fa-phone"></i>
               <span title="Phone">{{ $posts['phone_number'] }}</span>
            </div>
        </div>
        <div class="contact-box-flx">
            <div class="contact-col">
               <i class="fa fa-envelope" aria-hidden="true"></i>
               <address>
                  <a href="mailto:{{ $posts['email'] }}"> {{ $posts['email'] }} </a>
               </address>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="contact_form clearfix">
            <div class="col-sm-6">
            <!-- {!! Form::open() !!} -->
            <form action="{{ url('/contact-us') }}" method="POST" id="contactUsForm">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('name')? 'has-error':'' }} error_divs name_error_div">
                            <input type="text" class="form-control" id="name" name="name" placeholder="@lang('app.enter_name')" value="{{ old('name') }}"/>
                                {!! $errors->has('name')? '
                                    <p class="help-block">'.$errors->first('name').'</p>
                                ':'' !!}
                                <p class="error_ps help-block name_error_p"></p>
                        </div>
                        <div class="form-group {{ $errors->has('email')? 'has-error':'' }} error_divs email_error_div">
                            <input type="email" class="form-control" id="email" placeholder="@lang('app.enter_email_address')" name="email" value="{{ old('email') }}"/>
                            {!! $errors->has('email')? '
                                <p class="help-block">'.$errors->first('email').'</p>
                            ':'' !!}
                                <p class="error_ps help-block email_error_p"></p>
                        </div>
                        <div class="form-group {{ $errors->has('message')? 'has-error':'' }} error_divs message_error_div">
                            <textarea name="message" id="message" class="form-control" placeholder="@lang('app.message')">{{ old('message') }}</textarea>
                            {!! $errors->has('message')? '
                                <p class="help-block">'.$errors->first('message').'</p>
                            ':'' !!}
                                <p class="error_ps help-block message_error_p"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary black-btn" id="btnContactUs"> @lang('app.send_message')</button>
                    </div>
                </div>
            </form>
            </div>
            <div class="col-sm-6">
                <div id="map"></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{get_option('google_map_api_key')}}&callback=initMap">
</script>
<script>
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif

    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        var latitude    = "{{$posts['latitude']}}";
        var longitude   = "{{$posts['longitude']}}";
        var uluru = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
        // The map, centered at Uluru
        var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 4, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }

    // validations for contact us form
    $(document).on('click','#btnContactUs',function() {
        
        var empty_error = 0;
        var email = $('#email').val();
        var name = $('#name').val();
        var message = $('#message').val();
        $('.error_divs').removeClass('has-error');
        $('.error_ps').text('');

        if(email == '') {
            $('.email_error_div').addClass('has-error');
            $('.email_error_p').text('Please enter your email address.');
            empty_error = 1;
        }

        if(name == '') {
            $('.name_error_div').addClass('has-error');
            $('.name_error_p').text('Please enter your name.');
            empty_error = 1;
        }

        if(message == '') {
            $('.message_error_div').addClass('has-error');
            $('.message_error_p').text('Please enter your message.');
            empty_error = 1;
        }
        
        var re = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        if(email != '') {

            if(!re.test(email)) {
                $('.email_error_div').addClass('has-error');
                $('.email_error_p').text('Please enter a valid email address.');
                empty_error = 1;
            }
        }

        if(empty_error > 0) {
            return false;
        }

        $('#contactUsForm').submit();
    });
    // validations for contact us form
</script>
@endsection