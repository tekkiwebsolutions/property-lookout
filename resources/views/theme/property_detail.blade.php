@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif 
@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/plugins/owl.carousel/assets/owl.carousel.css') }}">
@endsection
@parent @endsection
@section('main')
<div class="navigate-area">
   <div class="container">
      <div class="row">
         <div class="col-sm-8">
            <div class="address">
               <div class="title">
                  <h4>Richardo</h4>
               </div>
               <address>
                  <div class="left">
                     <a href="#/"><i class="fa fa-map-marker"></i></a>
                  </div>
                  <div class="right">
                     <p>Lebuhraya Presekutuan, Bangsar South, 59200, Kuala Lumpur <br/><small>Malaysia</small></p>
                  </div>
               </address>
            </div>
         </div>
         <div class="col-sm-4">
            <div class="navigate-icons">
               <ul>
                  <li>
                     <i class="fa fa-home" aria-hidden="true"></i>
                     <div class="text">88</div>
                  </li>
                  <li>
                     <div class="icon"><img src="<?php echo asset('/');?>/assets/img/floor.png" alt="#"></div>
                     <div class="text">8</div>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="pd-featured-area">
   <div class="container">
      <div class="row">
         <div class="col-md-7">
            <a href="/"><img src="<?php echo asset('/');?>/assets/img/pd-story-img.jpg" alt="#"></a>
         </div>
         <div class="col-md-5 right">
            <center class="title">
               <h4>Units in Richardo</h4>
            </center>
            <div class="property-color">
               <ul>
                  <li class="red">&nbsp;</li>
                  <li class="green">&nbsp;</li>
                  <li class="green">&nbsp;</li>
                  <li class="green">&nbsp;</li>
                  <li class="green">&nbsp;</li>
                  <li class="green">&nbsp;</li>
                  <li class="green">&nbsp;</li>
                  <li class="green">&nbsp;</li>
                  <li class="red">&nbsp;</li>
                  <li class="green">&nbsp;</li>
                  <li class="green">&nbsp;</li>
                  <li class="green">&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
                  <li>&nbsp;</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<section class="property-slider">
   <div class="container">
      <div class="large-12 columns">
         <div class="owl-carousel owl-theme">
            <div class="item">
               <img src="<?php echo asset('/');?>/assets/img/slider-1.jpg" alt="#">
            </div>
            <div class="item">
               <img src="<?php echo asset('/');?>/assets/img/slider-2.jpg" alt="#">
            </div>
            <div class="item">
               <img src="<?php echo asset('/');?>/assets/img/slider-3.jpg" alt="#">
            </div>
            <div class="item">
               <img src="<?php echo asset('/');?>/assets/img/slider-4.jpg" alt="#">
            </div>
            <div class="item">
               <img src="<?php echo asset('/');?>/assets/img/slider-5.jpg" alt="#">
            </div>
            <div class="item">
               <img src="<?php echo asset('/');?>/assets/img/slider-1.jpg" alt="#">
            </div>
            <div class="item">
               <img src="<?php echo asset('/');?>/assets/img/slider-2.jpg" alt="#">
            </div>
            <div class="item">
               <img src="<?php echo asset('/');?>/assets/img/slider-3.jpg" alt="#">
            </div>
            <div class="item">
               <img src="<?php echo asset('/');?>/assets/img/slider-4.jpg" alt="#">
            </div>
            <div class="item">
               <img src="<?php echo asset('/');?>/assets/img/slider-5.jpg" alt="#">
            </div>
         </div>
      </div>
   </div>
</section>
<div class="description-box">
   <div class="container">
      <div class="row">
         <div class="col-md-7">
            <div class="content">
               <div class="title">
                  <h3>Description</h3>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br/>
                     It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br/>
                     It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.
                  </p>
                  <div class="read-more">
                     <button type="text" class="btn btn-primary black-btn" >Read More</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-5">
            <div class="des-img">
               <img src="<?php echo asset('/');?>/assets/img/des-img.jpg" alt="#">
            </div>
         </div>
      </div>
   </div>
</div>
<div class="review-section">
   <div class="container">
      <div class="row">
         <div class="col-md-8">
            <div class="title">
               <h3>Reviews :</h3>
            </div>
            <div class="reviews clearfix">
               <div class="review-text">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
               </div>
               <div class="author-area clearfix">
                  <div class="author-img">
                     <a href="#/"><img src="<?php echo asset('/');?>/assets/img/marcus.png" alt="#"></a>
                  </div>
                  <div class="author-info">
                     <ul>
                        <li class="name"><a href="#/">Gregory</a></li>
                        <li class="address">United Estate, USA</li>
                        <li class="star-reviews">
                           <img src="<?php echo asset('/');?>/assets/img/stars-reviews.png" alt="#">
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4 right">
            <div class="title">
               <center>
                  <h3>Leave a review</h3>
               </center>
            </div>
            {!! Form::open() !!}
            <div class="form-group {{ $errors->has('name')? 'has-error':'' }}">
               <!-- <label for="name">@lang('app.name')</label> -->
               <input type="text" class="form-control" id="name" name="name" placeholder="@lang('app.enter_name')" value="{{ old('name') }}" required="required" />
               {!! $errors->has('name')? '
               <p class="help-block">'.$errors->first('name').'</p>
               ':'' !!}
            </div>
            <div class="form-group {{ $errors->has('email')? 'has-error':'' }}">
               <!-- <label for="email">@lang('app.email_address')</label> -->
               <input type="email" class="form-control" id="email" placeholder="@lang('app.enter_email_address')" name="email" value="{{ old('email') }}" required="required" />
               {!! $errors->has('email')? '
               <p class="help-block">'.$errors->first('email').'</p>
               ':'' !!}
            </div>
            <div class="form-group {{ $errors->has('message')? 'has-error':'' }}">
               <!-- <label for="name">@lang('app.message')</label> -->
               <textarea name="message" id="message" class="form-control" required="required" placeholder="@lang('app.message')">{{ old('message') }}</textarea>
               {!! $errors->has('message')? '
               <p class="help-block">'.$errors->first('message').'</p>
               ':'' !!}
            </div>
            <div class="col-md-12">
               <div class="row">
                  <div class="star-rating">
                     <p>Rating: <img src="<?php echo asset('/');?>/assets/img/stars-reviews.png" alt="#"></p>
                  </div>
               </div>
            </div>
            <div class="col-md-12">
               <div class="row">
                  <ul class="captcha">
                     <li>
                        <p>Captcha</p>
                     </li>
                     <li><input type="text"></li>
                  </ul>
                  <button type="submit" class="btn btn-primary black-btn"> @lang('app.send_message')</button>
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('page-js')
<!-- <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script> -->
<script src="{{ asset('assets/plugins/owl.carousel/owl.carousel.min.js') }}"></script>
<script>
   @if(session('success'))
       toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
   @endif
</script>
<script>
   $(document).ready(function() {
     $('.owl-carousel').owlCarousel({
       loop: true,
       margin: 10,
       autoplay: true,
       autoplayTimeout: 1000,
       responsiveClass: true,
       responsive: {
         0: {
           items: 1,
           nav: false
         },
         600: {
           items: 3,
           nav: false
         },
         1000: {
           items: 6,
           nav: true,
           loop: false,
           margin: 20
         }
       }
     })
   })
</script>
@endsection