@extends('layout.main')
<link rel="stylesheet" href="{{ asset('assets/plugins/owl.carousel/assets/owl.carousel.css') }}">
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="row mlr0">
    <div class="page_wrapper page-@if(!empty($page->id)){{ $page->id }}@endif">
        <div class="modern-top-intoduce-section banner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <h2>Agent Detail</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="about_us single-agent_detail">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-3 col-sm-4 agent-avtar">
                        <div class="avtar"><img src="<?php echo asset('/');?>/assets/img/marcus.png" alt="#"></div>
                        <ul class="agent_profiles text-center">
                            <li><a href="javascript:;"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:;"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:;"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                        </ul>
                        <div><a class="btn btn-primary contact-btn" href="#">Contact</a></div>
                    </div>
                    <div class="col-md-6 col-sm-6 agent-detail-manual">
                        <h2>Marcus Marrio</h2>
                        <span>Sale Assocaite - (Property Lookout)</span>
                        <ul>
                            <li><a href="callto:+60194526523">+60194526523</a></li>
                            <li><a href="mailto:marrio.sa@gmail.com">marrio.sa@gmail.com</a></li>
                            <li>Lot 4452, Taman Damansara, Kuching, Sarawak</li>
                            <li><a href="javascript:;">www.marcusmarrio.com.my</a></li>
                        </ul>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Gender:</td>
                                    <td>Male</td>
                                </tr>
                                <tr>
                                    <td>Country:</td>
                                    <td>Malaysia</td>
                                </tr>
                                <tr>
                                    <td>Company:</td>
                                    <td>ABC Estate</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <h2>About Marcus Marrio</h2>
                <p>Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with &#39;real&#39; content. This is required when, for example, the final text is not yet available. Dummy text is also known as &#39;fill text&#39;. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a &#39;ready-made&#39; text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>
                <p>Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with &#39;real&#39; content. This is required when, for example, the final text is not yet available. Dummy text is also known as &#39;fill text&#39;. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a &#39;ready-made&#39; text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>
            </div>
        </div>
        @if($urgent_ads->count() > 0)
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="carousel-header">
                        <h3>Properties I am Selling</h3>
                        <h6>Properties Selling by location or properties near you </h6>
                    </div>
                    <div class="themeqx_new_regular_ads_wrap themeqx-carousel-ads">
                        @foreach($urgent_ads as $ad)
                        <div>
                            <div itemscope itemtype="http://schema.org/Product" class="ads-item-thumbnail ad-box-{{$ad->price_plan}}">
                                <div class="ads-thumbnail">
                                    <a href="{{ route('single_ad', $ad->slug) }}">
                                        <img itemprop="image"  src="{{ media_url($ad->feature_img) }}" class="img-responsive" alt="{{ $ad->title }}">
                                        @if($ad->purpose)
                                        <span class="modern-sale-rent-indicator">
                                        {{ 'For&nbsp;'.ucfirst($ad->purpose) }}
                                        </span>
                                        @endif
                                        <span class="estate_category">
                                        Modern Villa
                                        </span>
                                        <p class="date-posted"> <i class="fa fa-clock-o"></i> {{ $ad->created_at->diffForHumans() }}</p>
                                        <p class="price"> <span itemprop="price" content="{{$ad->price}}"> {{ themeqx_price_ng($ad) }} </span></p>
                                        <div class="avtar">
                                            <img src="assets/img/avtar.jpg" alt="#">
                                            <span class="user_name">Grant Barron</span>
                                            <span class="modern-img-indicator">
                                            @if(! empty($ad->video_url))
                                            <span>Video</span>
                                            @else
                                            <span>{{ $ad->media_img->count() }}&nbsp;Photos</span>
                                            @endif
                                            </span>
                                        </div>
                                    </a>
                                </div>
                                <div class="caption">
                                    <h4><a href="{{ route('single_ad', $ad->slug) }}" title="{{ $ad->title }}"><span itemprop=name>{{ str_limit($ad->title, 40) }} </span></a></h4>
                                    @if($ad->city)
                                    <a class="location text-muted" href="{{ route('listing', ['city' => $ad->city->id]) }}"> <i class="fa fa-map-marker"></i> {{ $ad->city->city_name }} </a>
                                    @endif
                                    <hr/>
                                    <table class="table table-responsive property-box-info">
                                        <tr>
                                            <td> <i class="fa fa-building"></i> {{ ucfirst($ad->type) }} </td>
                                            <td><i class="fa fa-arrows-alt "></i>  {{ $ad->square_unit_space.' '.$ad->unit_type }}</td>
                                            <td><span class="view"><i class="fa fa-eye" aria-hidden="true"></i>250</span></td>
                                        </tr>
                                        @if($ad->beds)
                                        <tr>
                                            <td><i class="fa fa-bed"></i> {{ $ad->beds.' '.trans('app.bedrooms') }}</td>
                                            <td><img src="assets/img/floor.png" />{{ $ad->floor.' '.trans('app.floor') }} </td>
                                        </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- themeqx_new_premium_ads_wrap -->
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
@section('page-js')
<script src="{{ asset('assets/plugins/owl.carousel/owl.carousel.min.js') }}"></script>
<script>
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif
</script>
<script>
    $(document).ready(function(){
     $(".themeqx_new_regular_ads_wrap").owlCarousel({
         loop: true,
         autoplay : true,
         autoplayTimeout : 2000,
         margin:10,
         autoplayHoverPause : true,
         responsiveClass:true,
         responsive:{
             0:{
                 items:1,
                 nav:true,
                 loop:true
             },
             767:{
                 items:2,
                 nav:false,
                 loop:true
             },
             1000:{
                 items:3,
                 nav:true,
                 loop:true
             }
         },
         navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>']
     });
    });
        
</script>
@endsection