@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="row mlr0">
   <div class="page_wrapper">
      <div class="container">
         <div class="col-lg-12">
            <h2 class="single_page_heading">Inbox</h2>
            <div class="notification all_messages clearfix">
               <div class="table-responsive">
                  <table class="table">
                     <tr>
                        <td><i class="fa fa-envelope" aria-hidden="true"></i>
                           <div class="float-left">
                              <p>Gregory (gregorywea@gmail.com)</p>
                              <span>5 Hours Ago | Applicant Tracking</span>
                           </div>
                        </td>
                        <td>I would like to negotiate about......</td>
                        <td>26/06/2018</td>
                        <td>
                           <a href="#" class="btn btn-primary small-btn">Start Chat</a>
                           <a href="#" class="btn btn-primary small-btn">Delete</a>
                        </td>
                     </tr>
                      <tr>
                        <td><i class="fa fa-envelope" aria-hidden="true"></i>
                           <div class="float-left">
                              <p>Gregory (gregorywea@gmail.com)</p>
                              <span>5 Hours Ago | Applicant Tracking</span>
                           </div>
                        </td>
                        <td>I would like to negotiate about......</td>
                        <td>26/06/2018</td>
                        <td>
                           <a href="#" class="btn btn-primary small-btn">Start Chat</a>
                           <a href="#" class="btn btn-primary small-btn">Delete</a>
                        </td>
                     </tr>
                      <tr>
                        <td><i class="fa fa-envelope" aria-hidden="true"></i>
                           <div class="float-left">
                              <p>Gregory (gregorywea@gmail.com)</p>
                              <span>5 Hours Ago | Applicant Tracking</span>
                           </div>
                        </td>
                        <td>I would like to negotiate about......</td>
                        <td>26/06/2018</td>
                        <td>
                           <a href="#" class="btn btn-primary small-btn">Start Chat</a>
                           <a href="#" class="btn btn-primary small-btn">Delete</a>
                        </td>
                     </tr>
                      <tr>
                        <td><i class="fa fa-envelope" aria-hidden="true"></i>
                           <div class="float-left">
                              <p>Gregory (gregorywea@gmail.com)</p>
                              <span>5 Hours Ago | Applicant Tracking</span>
                           </div>
                        </td>
                        <td>I would like to negotiate about......</td>
                        <td>26/06/2018</td>
                        <td>
                           <a href="#" class="btn btn-primary small-btn">Start Chat</a>
                           <a href="#" class="btn btn-primary small-btn">Delete</a>
                        </td>
                     </tr>
                      <tr>
                        <td><i class="fa fa-envelope" aria-hidden="true"></i>
                           <div class="float-left">
                              <p>Gregory (gregorywea@gmail.com)</p>
                              <span>5 Hours Ago | Applicant Tracking</span>
                           </div>
                        </td>
                        <td>I would like to negotiate about......</td>
                        <td>26/06/2018</td>
                        <td>
                           <a href="#" class="btn btn-primary small-btn">Start Chat</a>
                           <a href="#" class="btn btn-primary small-btn">Delete</a>
                        </td>
                     </tr>
                      <tr>
                        <td><i class="fa fa-envelope" aria-hidden="true"></i>
                           <div class="float-left">
                              <p>Gregory (gregorywea@gmail.com)</p>
                              <span>5 Hours Ago | Applicant Tracking</span>
                           </div>
                        </td>
                        <td>I would like to negotiate about......</td>
                        <td>26/06/2018</td>
                        <td>
                           <a href="#" class="btn btn-primary small-btn">Start Chat</a>
                           <a href="#" class="btn btn-primary small-btn">Delete</a>
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
              
   </div>
</div>
@endsection
@section('page-js')
<script>
   @if(session('success'))
       toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
   @endif
</script>
@endsection