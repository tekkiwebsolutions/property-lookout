@if(!empty($notification))
        @foreach($notification as $notify)
            <li>
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <div class="fleft">
                    <a href="{{url('agent/'.$notify['user']['slug'])}}" target="_blank"><b>
                        @if(!empty(@$notify['user']))
                            @if(!empty(@$notify['user']['name']))
                                {{ $notify['user']['name'] }}
                            @elseif(!empty(@$notify['user']['first_name']) || !empty(@$notify['user']['last_name']))
                                {{ @$notify['user']['first_name'].' '.@$notify['user']['last_name'] }}
                            @endif
                        @endif
                    </b></a> {{ @$notify['type_text'] ? @$notify['type_text'] : '' }} 
                    <p>{{ @$notify['message'] ? substr(@$notify['message'],0,40).'...' : '' }}</p>
                    <span> {{ @$notify['created_at'] }} | Applicant Tracking</span>
                </div>
                <a href="" class="rIcon pull-right"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
            </li>
            <hr class="clearfix">
        @endforeach
@else
    <li>No notifications to show !!</li>
@endif