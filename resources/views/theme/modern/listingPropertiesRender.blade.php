<?php use App\ApartmentUnit; ?>
<div class="ad-box-wrap">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
            <div class="ad-box-grid-view property_list">
                <div itemscope="" itemtype="http://schema.org/Product" class="ads-item-thumbnail ad-box-regular">
                @if(!empty($ads))
                    @foreach($ads as $ad)
                    <?php $apart_values = showUnitsFloorsNumber($ad->id); ?>
                    <?php $agent_val = getStaffMemberOrDeveloper($ad->id,'apartment'); ?>
                    <div class="prprty-lst-vu">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="ads-thumbnail">
                                    <a href="{{url('propertyShow/'.$ad->type.'/'.$ad->slug)}}">
                                        @if($ad->type == 'house')
                                            <img itemprop="image" src="{{ asset('uploads/houseImages/'.$ad->cover_photo) }}" class="img-responsive property_image" alt="{{ $ad->title }}">
                                        @else
                                            <img itemprop="image" src="{{ asset('uploads/apartmentImages/'.$ad->cover_photo) }}" class="img-responsive property_image" alt="{{ $ad->title }}">
                                        @endif
                                        <span style="@if($ad->property_status == 0)background:#33aa58;@elseif($ad->property_status == 1)background:#f26c61 @endif" class="modern-sale-rent-indicator">
                                            @if($ad->property_status == 0) 
                                                {{'Available'}}
                                            @elseif($ad->property_status == 1)
                                                {{'Sold'}}
                                            @endif
                                        </span>
                                        <p class="date-posted text-muted" style="color:red;"> <i class="fa fa-clock-o"></i> {{ $ad->created_at->diffForHumans() }}</p>
                                        <div class="img-ftr">
                                            <span class="price">
                                                @if($ad->type == 'house')
                                                    <b itemprop="price" content="{{$ad->price}}"> $ {{ $ad->price }} </b>
                                                @else
                                                    <b itemprop="price" content="Unit wise">Unit wise</b>
                                                @endif
                                            </span>
                                            <span class="avatar">
                                                @if(!empty($agent_val->photo))
                                                    <img src="{{asset('uploads/avatar/'.$agent_val->photo)}}" alt="#">
                                                @else
                                                    <img src="{{asset('assets/img/default_user.png')}}" alt="#">
                                                @endif
                                                <span class="user_name">{{$agent_val->name}}</span>
                                                <span class="modern-img-indicator">
                                                    @if(! empty($ad->video_url))
                                                        <span>Video</span>
                                                    @else
                                                        <span>{{ $ad->media_img->count() }}&nbsp;Photos</span>
                                                    @endif
                                                </span>
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="caption">
                                    <h4><a href="{{url('propertyShow/'.$ad->type.'/'.$ad->slug)}}" title="{{ $ad->title }}"><span itemprop="name">{{ $ad->title }}</span></a></h4>
                                    @if($ad->city)
                                        <a class="location text-muted" href="{{ route('listing', ['city' => $ad->city->id]) }}"> <i class="fa fa-map-marker"></i> {{ $ad->city->city_name }} </a>
                                    @endif  
                                    <hr>
                                    <p>{{ substr($ad->description, 0, 80)}}...</p>
                                    <table class="table table-responsive property-box-info">
                                        <tbody>
                                            <tr>
                                                <td><i class="fa fa-building"></i>{{ucfirst($ad->type)}}</td>
                                                @if($ad->type == 'house')
                                                    <td>
                                                        <i class="fa fa-arrows-alt "></i>  {{ $ad->square_unit_space.' '.$ad->unit_type }}
                                                    </td>
                                                @else 
                                                    <td>
                                                        <div class="icon"><img src="<?php echo asset('/');?>/assets/img/floor.png" alt="#"></div>  {{$apart_values['floors'] ? $apart_values['floors'].' Floors': 'N/A' }}
                                                    </td>
                                                @endif
                                                <td>
                                                    <span class="view"><i class="fa fa-eye" aria-hidden="true"></i>@if(!empty($ad->view)){{$ad->view}}@else{{0}}@endif</span>
                                                </td>
                                            </tr>
                                            @if($ad->type == 'house')
                                                <tr>
                                                    @if($ad->beds)
                                                        <td><i class="fa fa-bed"></i> {{ $ad->beds.' '.trans('app.bedrooms') }}</td>
                                                    @endif
                                                    @if($ad->attached_bath)
                                                        <td><i class="fa fa-shower"></i> {{ $ad->attached_bath.' Bathrooms' }}</td>
                                                    @endif
                                                </tr>
                                            @else
                                                <tr>
                                                    <td><i class="fa fa-home"></i> {{ $apart_values['units'] ? $apart_values['units'] : 0 }} Units</td>
                                                    <?php $available_units = ApartmentUnit::whereAdId($ad->id)->Where('property_status',0)->count(); ?>
                                                    <td><i class="fa fa-check"></i> {{ $available_units }} Available</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    <div class="property-btn">
                                        <a href="{{url('propertyShow/'.$ad->type.'/'.$ad->slug)}}" class="btn btn-primary black-btn">Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="latest_listings">
                <h3>Latest Listings</h3>
                @if(!empty($latest_properties))
                    @foreach($latest_properties as $key => $ad)
                        <div class="latest-list">
                            <div class="latest_listing_img">
                                <a href="{{url('propertyShow/'.$ad->type.'/'.$ad->slug)}}">
                                    @if($ad->type == 'house')
                                        <img src="{{ asset('uploads/houseImages/'.$ad->cover_photo) }}" class="img-responsive" alt="{{ $ad->title }}">
                                    @else
                                        <img src="{{ asset('uploads/apartmentImages/'.$ad->cover_photo) }}" class="img-responsive" alt="{{ $ad->title }}">
                                    @endif
                                </a>
                            </div>
                            <div class="latest_listing_des">
                                <a href="{{url('propertyShow/'.$ad->type.'/'.$ad->slug)}}">{{ $ad->title }}</a>
                                <p>{{ substr($ad->description, 0, 30)}}...</p>
                                <!-- <span>215 Terry Lane </span> -->
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="feature_agents clearfix">
                <h3>Feature Agents</h3>
                @if(!empty($lastFourAgents))
                    @foreach($lastFourAgents as $key => $value)
                        <div class="author-area clearfix">
                            <div class="author-img">
                                <a href="{{url('agent/'.$value['slug'])}}">
                                    @if(!empty($value['photo']))
                                        <img src="{{asset('uploads/avatar/'.$value['photo'])}}" alt="#">
                                    @else
                                        <img src="{{asset('assets/img/default_user.png')}}" alt="#">
                                    @endif
                                </a>
                            </div>
                            <div class="author-info">
                                <ul>
                                    <li class="name"><a href="{{url('agent/'.$value['slug'])}}">{{$value['name']}}</a></li>
                                    <li class="address">{{substr($value['address'],0,60)}}</li>
                                    <li class="call">+{{$value['phone']}}</li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>