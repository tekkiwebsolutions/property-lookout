<div class="ad-box-wrap">
    @if($ads->total() > 0)
        <!-- <h3>Property Listing</h3> -->
        <!-- <h6>@lang('app.listing_properties')</h6> -->
        <div class="ad-box-grid-view" style="display: {{ session('grid_list_view') ? (session('grid_list_view') == 'grid'? 'block':'none') : 'block' }};">
           <div class="row">
              <?php 
                $numOfCols = 3;
                $rowCount = 0;
                $bootstrapColWidth = 12 / $numOfCols;
              ?>
              @foreach($ads as $ad)
                <div class="col-lg-{{$bootstrapColWidth}} col-md-{{$bootstrapColWidth}} col-sm-6 col-xs-12">
                   <div itemscope itemtype="http://schema.org/Product" class="ads-item-thumbnail ad-box-{{$ad->price_plan}}">
                      <div class="ads-thumbnail">
                         <a href="{{url('propertyShow/'.$ad->type.'/'.$ad->slug)}}">
                            @if($ad->type == 'house')
                                <img itemprop="image"  src="{{ asset('uploads/houseImages/'.$ad->cover_photo) }}" class="img-responsive property_image" alt="{{ $ad->title }}">
                            @else
                                <img itemprop="image"  src="{{ asset('uploads/apartmentImages/'.$ad->cover_photo) }}" class="img-responsive property_image" alt="{{ $ad->title }}">
                            @endif
                            <span style="@if($ad->property_status == 0)background:#33aa58;@elseif($ad->property_status == 1)background:#f26c61 @endif" class="modern-sale-rent-indicator">
                                @if($ad->property_status == 0) 
                                  {{'Available'}}
                                @elseif($ad->property_status == 1)
                                    {{'Sold'}}
                                @endif
                            </span>
                            <p class="date-posted text-muted" style="color:red;"> <i class="fa fa-clock-o"></i> {{ $ad->created_at->diffForHumans() }}</p>
                            <p class="price"> <span itemprop="price" content="{{$ad->price}}"> $ {{ $ad->price }} </span></p>
                            <div class="avtar">
                                @if(!empty($ad->user->photo))
                                  <img src="{{asset('uploads/avatar/'.$ad->user->photo)}}" alt="#">
                                @else
                                  <img src="{{asset('assets/img/default_user.png')}}" alt="#">
                                @endif
                                 <span class="user_name">{{$ad->user->name}}</span>
                                 <span class="modern-img-indicator">
                                 @if(! empty($ad->video_url))
                                  <span>Video</span>
                                 @else
                                  <span>{{ $ad->media_img->count() }}&nbsp;Photos</span>
                                 @endif
                               </span>
                            </div>
                         </a>
                      </div>
                      <div class="caption">
                         <h4><a href="{{url('propertyShow/'.$ad->type.'/'.$ad->slug)}}" title="{{ $ad->title }}"><span itemprop="name">{{ str_limit($ad->title, 40) }} </span></a></h4>
                         @if($ad->city)
                         <a class="location text-muted" href="{{ route('listing', ['city' => $ad->city->id]) }}"> <i class="fa fa-map-marker"></i> {{ $ad->city->city_name }} </a>
                         @endif        
                         <hr/>
                         <table class="table table-responsive property-box-info">
                            <tr>
                               <td><i class="fa fa-building"></i> {{ ucfirst($ad->type) }} </td>
                               <td><i class="fa fa-arrows-alt "></i>  {{ $ad->square_unit_space.' '.$ad->unit_type }}</td>
                               <td><span class="view"><i class="fa fa-eye" aria-hidden="true"></i>@if(!empty($ad->view)){{$ad->view}}@else{{0}}@endif</span></td>
                            </tr>
                            @if($ad->beds)
                            <tr>
                               <td><i class="fa fa-bed"></i> {{ $ad->beds.' '.trans('app.bedrooms') }}</td>
                               <!-- <td><img src="assets/img/floor.png" /> {{ $ad->floor.' '.trans('app.floor') }} </td> -->
                            </tr>
                            @endif
                         </table>
                      </div>
                   </div>
                </div>
                <?php
                    $rowCount++;
                    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                ?>
              @endforeach
           </div>
        </div>
    @else
      <div class="alert alert-warning">
         <h2><i class="fa fa-info-circle"></i> @lang('app.search_not_found') </h2>
      </div>
    @endif
</div>