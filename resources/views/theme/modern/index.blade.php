@extends('layout.main')
@section('page-css')
<?php use App\ApartmentUnit; ?>
<link rel="stylesheet" href="{{ asset('assets/plugins/owl.carousel/assets/owl.carousel.css') }}">
<style>
    .ads-thumbnail img{
    width: 100%;
    height: 400px;
    }
    .ads-item-thumbnail .ads-thumbnail img {
    width: 100%;
    max-height: 209px;
    height: 200px;
    }
    .ads-thumbnail .property_image{
    height: 200px;
    }
</style>
@endsection
@section('main')
<div class="modern-top-intoduce-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="modern-top-hom-cat-section">
                    <div class="modern-home-search-bar-wrap">
                        <form class="form-inline" action="{{ route('listing') }}" method="get">
                            <div class="search-wrapper">
                                <h3><i class="fa fa-home"></i> @lang('app.find_your_property')</h3>
                                <h6>@lang('app.million_properties')</h6>
                                <div class="form-group row-width8">
                                    <input type="text" class="form-control" name="q" value="{{ request('q') }}" placeholder="@lang('app.search___')" maxlength="255" />
                                </div>
                                <button type="submit" class="btn theme-btn"> <i class="fa fa-search"></i> Search</button>
                                <div class="or-search"> OR </div>
                            </div>
                            <div class="search-wrapper advance_search" style="display:none;">
                                <div class="row row-width">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <select class="form-control select2" name="country">
                                                <option value="">@lang('app.select_a_country')</option>
                                                @if(!empty($countries))
                                                @foreach($countries as $country)
                                                <option value="{{ $country->id }}" {{ request('country') == $country->id ? 'selected' :'' }}>{{ $country->country_name }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control select2" id="state_select" name="state" disabled="disabled">
                                                <option value=""> @lang('app.select_state') </option>
                                                @if(!empty(@$previous_states))
                                                @foreach(@$previous_states as $state)
                                                <option value="{{ $state->id }}" @if($state->id == request('state')) selected @endif>{{ $state->state_name }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control select2" id="city_select" name="city" disabled="disabled">
                                                <option value="">Select City</option>
                                                @if(!empty($previous_cities))
                                                @foreach($previous_cities as $city)
                                                <option value="{{ $city->id }}" @if($city->id == request('city')) selected @endif>{{ $city->city_name }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control price_val_input" name="zip" id="zip" value="{{ request('zip') }}" placeholder="Enter Zip Code" maxlength="6">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <select class="form-control select2" name="property_type">
                                                <option value="">@lang('app.property_type')</option>
                                                @if(!empty(@$house_property_types))
                                                @foreach($house_property_types as $key => $value)
                                                <option value="{{$value['id']}}" {{ request('property_type') == $value['id'] ? 'selected' :'' }}>{{$value['name']}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control price_val_input" name="min_price" value="{{ request('min_price') }}" placeholder="Price From" maxlength="12">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control price_val_input" name="max_price" placeholder="Price to" value="{{ request('max_price') }}" maxlength="12">
                                        </div>
                                        <div class="form-flx">
                                            <div class="form-group">
                                                <pre>Parkings</pre>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control select2" id="" name="parking_start">
                                                    <option value="">Select</option>
                                                    <option value="1" @if(request('parking_start') == 1) selected @endif>1</option>
                                                    <option value="2" @if(request('parking_start') == 2) selected @endif>2</option>
                                                    <option value="3" @if(request('parking_start') == 3) selected @endif>3</option>
                                                    <option value="4" @if(request('parking_start') == 4) selected @endif>4</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control select2" id="" name="parking_end">
                                                    <option value="">Select</option>
                                                    <option value="5" @if(request('parking_end') == 5) selected @endif>5</option>
                                                    <option value="7" @if(request('parking_end') == 7) selected @endif>7</option>
                                                    <option value="9" @if(request('parking_end') == 9) selected @endif>9</option>
                                                    <option value="10+" @if(request('parking_end') == '10+') selected @endif>10+</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-flx">
                                            <div class="form-group">
                                                <pre>Sqft</pre>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control select2" id="" name="square_unit_start">
                                                    <option value="">Sq.</option>
                                                    <option value="10" @if(request('square_unit_start') == 10) selected @endif>10</option>
                                                    <option value="50" @if(request('square_unit_start') == 50) selected @endif>50</option>
                                                    <option value="100" @if(request('square_unit_start') == 100) selected @endif>100</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control select2" id="" name="square_unit_end">
                                                    <option value="">Sq.</option>
                                                    <option value="150" @if(request('square_unit_end') == 150) selected @endif>150</option>
                                                    <option value="200" @if(request('square_unit_end') == 200) selected @endif>200</option>
                                                    <option value="300" @if(request('square_unit_end') == 300) selected @endif>300</option>
                                                    <option value="400+" @if(request('square_unit_end') == '400+') selected @endif>400+</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-flx">
                                            <div class="form-group w-70">
                                                <pre>Bedroom</pre>
                                            </div>
                                            <div class="form-group w-70">
                                                <select class="form-control select2" id="" name="bedroom_start">
                                                    <option value="">Select</option>
                                                    <option value="1" @if(request('bedroom_start') == 1) selected @endif>1</option>
                                                    <option value="2" @if(request('bedroom_start') == 2) selected @endif>2</option>
                                                    <option value="3" @if(request('bedroom_start') == 3) selected @endif>3</option>
                                                    <option value="4" @if(request('bedroom_start') == 4) selected @endif>4</option>
                                                </select>
                                            </div>
                                            <div class="form-group w-70">
                                                <select class="form-control select2" id="" name="bedroom_end">
                                                    <option value="">Select</option>
                                                    <option value="5" @if(request('bedroom_end') == 5) selected @endif>5</option>
                                                    <option value="7" @if(request('bedroom_end') == 7) selected @endif>7</option>
                                                    <option value="9" @if(request('bedroom_end') == 9) selected @endif>9</option>
                                                    <option value="10+" @if(request('bedroom_end') == '10+') selected @endif>10+</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-flx">
                                            <div class="form-group w-70">
                                                <pre>Bathroom</pre>
                                            </div>
                                            <div class="form-group w-70">
                                                <select class="form-control select2" id="" name="bathroom_start">
                                                    <option value="">Select</option>
                                                    <option value="1" @if(request('bathroom_start') == 1) selected @endif>1</option>
                                                    <option value="2" @if(request('bathroom_start') == 2) selected @endif>2</option>
                                                    <option value="3" @if(request('bathroom_start') == 3) selected @endif>3</option>
                                                    <option value="4" @if(request('bathroom_start') == 4) selected @endif>4</option>
                                                </select>
                                            </div>
                                            <div class="form-group w-70">
                                                <select class="form-control select2" id="" name="bathroom_end">
                                                    <option value="">Select</option>
                                                    <option value="5" @if(request('bathroom_end') == 5) selected @endif>5</option>
                                                    <option value="7" @if(request('bathroom_end') == 7) selected @endif>7</option>
                                                    <option value="9" @if(request('bathroom_end') == 9) selected @endif>9</option>
                                                    <option value="10+" @if(request('bathroom_end') == '10+') selected @endif>10+</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                    </div>
                                </div>
                            </div>
                        </form>
                        <a href="javascript:;" class="btn btn-info btn-lg try-nothr-srch"><i class="fa fa-search-plus"></i> @lang('app.try_advance_search')</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
@if($properties->count() > 0)
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="carousel-header">
                <h3>@lang('app.new_urgent_ads')</h3>
                <h6>Listing Properties in the Website</h6>
            </div>
            <div class="themeqx_new_regular_ads_wrap themeqx-carousel-ads">
                @foreach($properties as $ad)
                <?php $apart_values = showUnitsFloorsNumber($ad->id); ?>
                <?php $agent_val = getStaffMemberOrDeveloper($ad->id,'apartment'); ?>
                <div>
                    <div itemscope itemtype="http://schema.org/Product" class="ads-item-thumbnail ad-box-{{$ad->price_plan}}">
                        <div class="ads-thumbnail">
                            <a href="{{url('propertyShow/'.$ad->type.'/'.$ad->slug)}}">
                                @if($ad->type == 'house')
                                <img itemprop="image"  src="{{asset('uploads/houseImages/'.$ad->cover_photo) }}" class="img-responsive property_image" alt="{{ $ad->title }}">
                                @else
                                <img itemprop="image"  src="{{asset('uploads/apartmentImages/'.$ad->cover_photo) }}" class="img-responsive property_image" alt="{{ $ad->title }}">
                                @endif
                                <div class="img-hdr">
                                    <span style="@if($ad->property_status == 0)background:#33aa58;@elseif($ad->property_status == 1)background:#f26c61 @endif" class="modern-sale-rent-indicator">
                                    @if($ad->property_status == 0) 
                                    {{'Available'}}
                                    @elseif($ad->property_status == 1)
                                    {{'Sold'}}
                                    @endif
                                    </span>
                                    <p class="date-posted" style="color:red;"> <i class="fa fa-clock-o"></i> {{ $ad->created_at->diffForHumans() }}</p>
                                </div>
                                <div class="img-ftr">
                                    <span class="avatar">
                                    @if(!empty($agent_val->photo))
                                    <img src="{{asset('uploads/avatar/'.$agent_val->photo)}}" alt="#">
                                    @else
                                    <img src="{{asset('assets/img/default_user.png')}}" alt="#">
                                    @endif
                                    <span class="user_name">{{@$agent_val->name}}</span>
                                    <span class="modern-img-indicator">
                                    @if(! empty($ad->video_url))
                                    <span>Video</span>
                                    @else
                                    <span>{{ $ad->media_img->count() }} Photos</span>
                                    @endif
                                    </span>
                                    </span>
                                    <span class="price">
                                    @if($ad->type == 'house')
                                    <b itemprop="price" content="{{$ad->price}}"> $ {{$ad->price}} </b>
                                    @else
                                    <b itemprop="price" content="Unit wise">Unit wise</b>
                                    @endif
                                    </span>
                                </div>
                            </a>
                        </div>
                        <div class="caption">
                            <h4><a href="{{url('propertyShow/'.$ad->type.'/'.$ad->slug)}}" title="{{ $ad->title }}"><span itemprop=name>{{ str_limit($ad->title, 40) }} </span></a></h4>
                            @if($ad->city)
                            <a class="location text-muted" href="{{ route('listing', ['city' => $ad->city->id]) }}"> <i class="fa fa-map-marker"></i> {{ $ad->city->city_name }} </a>
                            @endif
                            <hr/>
                            <table class="table table-responsive property-box-info">
                                <tr>
                                    <td> 
                                        <i class="fa fa-building"></i> {{ ucfirst($ad->type) }} 
                                    </td>
                                    @if($ad->type == 'house')
                                    <td>
                                        <i class="fa fa-arrows-alt "></i>  {{ $ad->square_unit_space.' '.$ad->unit_type }}
                                    </td>
                                    @else 
                                    <td>
                                        <div class="icon"><img src="<?php echo asset('/');?>/assets/img/floor.png" alt="#"></div>
                                        {{$apart_values['floors'] ? $apart_values['floors'].' Floors': 'N/A' }}
                                    </td>
                                    @endif
                                    <td>
                                        <span class="view"><i class="fa fa-eye" aria-hidden="true"></i>{{$ad->view}}</span>
                                    </td>
                                </tr>
                                @if($ad->type == 'house')
                                <tr>
                                    @if($ad->beds)
                                    <td><i class="fa fa-bed"></i> {{ $ad->beds.' '.trans('app.bedrooms') }}</td>
                                    @endif
                                    @if($ad->attached_bath)
                                    <td><i class="fa fa-shower"></i> {{ $ad->attached_bath.' Bathrooms' }}</td>
                                    @endif
                                </tr>
                                @else
                                <tr>
                                    <td><i class="fa fa-home"></i> {{ $apart_values['units'] ? $apart_values['units'] : 0 }}</td>
                                    <?php $available_units = ApartmentUnit::whereAdId($ad->id)->Where('property_status',0)->count(); ?>
                                    <td><i class="fa fa-check"></i> {{ $available_units }} Available</td>
                                </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif
<div class="company_work">
    <div class="container">
        <div class="d-flx">
            <div class="flx-inr">
                <i class="fa fa-home" aria-hidden="true"></i>
                <div class="col-text">
                    <h4>{{@$complete_projects?@$complete_projects:0}}</h4>
                    <h5>Complete Project</h5>
                </div>
            </div>
            <div class="flx-inr">
                <i class="fa fa-key" aria-hidden="true"></i>
                <div class="col-text">
                    <h4>{{@$property_sold?@$property_sold:0}}</h4>
                    <h5>Property Sold</h5>
                </div>
            </div>
            <div class="flx-inr">
                <i class="fa fa-smile-o" aria-hidden="true"></i>
                <div class="col-text">
                    <h4>{{@$happy_clients?@$happy_clients:0}}</h4>
                    <h5>Happy Clients</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="feature">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <h3>@lang('app.awesome_feature')</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                <div class="featureflex">
                    <div class="featureflexinr feature-col">
                        <img src="assets/img/feature.jpg" class="fleft img-responsive" />
                        <div class="feature-text">
                            <h4>Full Furnished</h4>
                            <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                        </div>
                    </div>
                    <div class="featureflexinr feature-col">
                        <img src="assets/img/feature.jpg" class="fleft img-responsive" />
                        <div class="feature-text">
                            <h4>Royal Touch Paint</h4>
                            <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                        </div>
                    </div>
                    <div class="featureflexinr feature-col">
                        <img src="assets/img/feature.jpg" class="fleft img-responsive" />
                        <div class="feature-text">
                            <h4>Latest Interior Design</h4>
                            <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                        </div>
                    </div>
                    <div class="featureflexinr feature-col">
                        <img src="assets/img/feature.jpg" class="fleft img-responsive" />
                        <div class="feature-text">
                            <h4>Luxurious Fittings</h4>
                            <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                        </div>
                    </div>
                    <div class="featureflexinr feature-col">
                        <img src="assets/img/feature.jpg" class="fleft img-responsive" />
                        <div class="feature-text">
                            <h4>Living Inside a Nature</h4>
                            <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                        </div>
                    </div>
                    <div class="featureflexinr feature-col">
                        <img src="assets/img/feature.jpg" class="fleft img-responsive" />
                        <div class="feature-text">
                            <h4>Non Stop Security</h4>
                            <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="feature-img">
                    <img src="assets/img/feature-img.jpg" class="img-responsive" alt="feature image" />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script src="{{ asset('assets/plugins/owl.carousel/owl.carousel.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $(".themeqx_new_premium_ads_wrap").owlCarousel({
            loop: true,
            autoplay:true,
            autoplayTimeout:3000,
            margin:10,
            autoplayHoverPause : true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    loop:true
                },
                767:{
                    items:2,
                    nav:false,
                    loop:true
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:true
                }
            },
            navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>']
        });
    });
    
    $(document).ready(function(){
        $(".themeqx_new_regular_ads_wrap").owlCarousel({
            loop: true,
            autoplay : true,
            autoplayTimeout : 2000,
            margin:10,
            autoplayHoverPause : true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    loop:true
                },
                767:{
                    items:2,
                    nav:false,
                    loop:true
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:true
                }
            },
            navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>']
        });
    });
    $(document).ready(function(){
        $(".home-latest-blog").owlCarousel({
            loop: true,
            autoplay : true,
            autoplayTimeout : 3000,
            margin:10,
            autoplayHoverPause : true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    loop:true
                },
                767:{
                    items:2,
                    nav:false,
                    loop:true
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:true
                }
            },
            navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>']
        });
    });
    
    function generate_option_from_json(jsonData, fromLoad){
        //Load Category Json Data To Brand Select
        if(fromLoad === 'country_to_state'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="" selected> @lang('app.select_state') </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
                }
                $('#state_select').html(option);
                $('#state_select').select2();
                $('#state_select').removeAttr('disabled');
                // $('#state_select').prop('disabled', false);
                // $('#state_select').select2(function() { $('#state_select').prop('disabled', false); });
            }else {
                $('#state_select').html('<option value="" selected> @lang('app.select_state') </option>');
                $('#state_select').select2();
            }
            // $('#loaderListingIcon').hide('slow');
            // document.getElementById('load').style.visibility="hidden";
        }else if(fromLoad === 'state_to_city'){
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="" selected> @lang('app.select_city') </option>';
                for ( i in jsonData){
                    option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
                }
                $('#city_select').html(option);
                $('#city_select').select2();
                $('#city_select').removeAttr('disabled');
            }else {
                $('#city_select').html('<option value="" selected> @lang('app.select_city') </option>');
                $('#city_select').select2();
            }
            // $('#loaderListingIcon').hide('slow');
            // document.getElementById('load').style.visibility="hidden";
        }
    }
    
    $(document).ready(function(){
        $('[name="country"]').change(function(){
            var country_id = $(this).val();
            $('#loaderListingIcon').show();
            // $('#loadingOverlay').show();
            // document.getElementById('load').style.visibility="visible";
            // $('#load').removeAttr('visibility');
            $.ajax({
                type : 'POST',
                url : '{{ route('get_state_by_country') }}',
                data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
                success : function (data) {
                    generate_option_from_json(data, 'country_to_state');
                }
            });
        });
        $('[name="state"]').change(function(){
            var state_id = $(this).val();
            // $('#loaderListingIcon').show();
            // document.getElementById('load').style.visibility="visible";
            $.ajax({
                type : 'POST',
                url : '{{ route('get_city_by_state') }}',
                data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
                success : function (data) {
                    generate_option_from_json(data, 'state_to_city');
                }
            });
        });
    });
    
    $(document).ready(function(){
        $('.price_val_input').keyup(function(){
            $(this).val($(this).val().replace(/[^0-9]/g,""));
        });

        // $('.advance_search').slideUp();
        $('.try-nothr-srch').click(function(){
            $('.or-search').hide();
            $('.try-nothr-srch').hide();
            $('.advance_search').slideDown();
        });
    });
</script>
@endsection