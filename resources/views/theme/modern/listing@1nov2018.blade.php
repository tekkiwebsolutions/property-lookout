@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<style type="text/css">
  
  .sortByBtn{
      font-size: 15px;
      box-shadow: 2px 3px 3px 1px #cccccc;
      text-align: left;
      border-radius: 5px;
      font-weight: 600;
      /* padding: 10px 15px; */
      padding-left: 15px;
      padding-right: 15px;
  }

  .sortByBtn:hover{
    background-color: white; 
  }
  .ads-thumbnail .property_image{
        height: 200px;
  }
</style>
<div class="modern-top-intoduce-section">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="modern-top-hom-cat-section">
               <div class="modern-home-search-bar-wrap">
                  <div class="search-wrapper advance_search">
                     <h3> <i class="fa fa-home"></i> @lang('app.find_your_property')</h3>
                     <h6>@lang('app.million_properties')</h6>
                     <form class="form-inline" action="{{ route('listing') }}" method="get">
                        <div class="form-group row-width8">
                           <input type="text" class="form-control" name="q" value="{{ request('q') }}" placeholder="@lang('app.search___')" />
                        </div>
                        <button type="submit" class="btn theme-btn"> <i class="fa fa-search"></i> Search</button>
                        <div class="row row-width">
                            <div class="col-md-3 col-sm-6">
                               <div class="form-group">
                                  <select class="form-control select2" name="country">
                                     <option value="">@lang('app.select_a_country')</option>
                                     @foreach($countries as $country)
                                     <option value="{{ $country->id }}" {{ request('country') == $country->id ? 'selected' :'' }}>{{ $country->country_name }}</option>
                                     @endforeach
                                  </select>
                               </div>                                
                              <div class="form-group">
                                   <select class="form-control select2" id="state_select" name="state">
                                      <option value=""> @lang('app.select_state') </option>
                                   </select>
                                </div> 
                               <div class="form-group">
                                  <input type="number" class="form-control" name="min_price" value="{{ request('min_price') }}" placeholder="Price From">
                               </div>      
                                                                                                    
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="form-group">
                                   <select class="form-control select2" id="city_select" name="city">
                                      <option value="">City</option>
                                   </select>
                               </div>
                               <div class="form-group">
                                    <input id="area" type="text" size="50" placeholder="Enter Area" value="{{ request('area') }}" autocomplete="on" runat="server" class="form-control" name="area"/>  
                                    <input type="hidden" id="area2" name="area2" value="{{ request('area2') }}"/>
                                    <input type="hidden" id="lat" name="lat" value="{{ request('lat') }}"/>
                                    <input type="hidden" id="long" name="long" value="{{ request('long') }}" />
                               </div>                                   
                               <div class="form-group">
                                  <input type="number" class="form-control" name="max_price" placeholder="Price to" value="{{ request('max_price') }}">
                               </div>                                                                 
                            </div>
                            <div class="col-md-3 col-sm-6">
                               <div class="form-group w-70">
                                  <pre>Sqft</pre>
                               </div>
                               <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="square_unit_start">
                                     <option value="">Sq.</option>
                                     <option value="10" @if(request('square_unit_start') == 10) selected @endif>10</option>
                                     <option value="50" @if(request('square_unit_start') == 50) selected @endif>50</option>
                                     <option value="100" @if(request('square_unit_start') == 100) selected @endif>100</option>
                                  </select>
                               </div>
                               <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="square_unit_end">
                                     <option value="">Sq.</option>
                                     <option value="150" @if(request('square_unit_end') == 150) selected @endif>150</option>
                                     <option value="200" @if(request('square_unit_end') == 200) selected @endif>200</option>
                                     <option value="300" @if(request('square_unit_end') == 300) selected @endif>300</option>
                                  </select>
                               </div> 
                                <div class="form-group w-70">
                                  <pre>Bedroom</pre>
                                </div>
                                <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="bedroom_start">
                                     <option value="">Select</option>
                                     <option value="1" @if(request('bedroom_start') == 1) selected @endif>1</option>
                                     <option value="2" @if(request('bedroom_start') == 2) selected @endif>2</option>
                                     <option value="3" @if(request('bedroom_start') == 3) selected @endif>3</option>
                                     <option value="4" @if(request('bedroom_start') == 4) selected @endif>4</option>
                                  </select>
                                </div>
                                <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="bedroom_end">
                                     <option value="">Select</option>
                                     <option value="5" @if(request('bedroom_end') == 5) selected @endif>5</option>
                                     <option value="7" @if(request('bedroom_end') == 7) selected @endif>7</option>
                                     <option value="9" @if(request('bedroom_end') == 9) selected @endif>9</option>
                                  </select>
                                </div> 
                                <div class="form-group w-70">
                                  <pre>Bathroom</pre>
                                </div>
                                <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="bathroom_start">
                                     <option value="">Select</option>
                                     <option value="1" @if(request('bathroom_start') == 1) selected @endif>1</option>
                                     <option value="2" @if(request('bathroom_start') == 2) selected @endif>2</option>
                                     <option value="3" @if(request('bathroom_start') == 3) selected @endif>3</option>
                                     <option value="4" @if(request('bathroom_start') == 4) selected @endif>4</option>
                                  </select>
                               </div>
                               <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="bathroom_end">
                                     <option value="">Select</option>
                                     <option value="5" @if(request('bathroom_end') == 5) selected @endif>5</option>
                                     <option value="7" @if(request('bathroom_end') == 7) selected @endif>7</option>
                                     <option value="9" @if(request('bathroom_end') == 9) selected @endif>9</option>
                                  </select>
                               </div>                                        
                            </div>
                            <div class="col-md-3 col-sm-6">
                               <div class="form-group">
                                  <select class="form-control select2" name="property_type">
                                     <option value="">@lang('app.property_type')</option>
                                      <!-- <option value="apartment">@lang('app.apartment')</option>
                                      <option value="condos">@lang('app.condos')</option>
                                      <option value="house">@lang('app.house')</option>
                                      <option value="land">@lang('app.land')</option>
                                      <option value="commercial_space">@lang('app.commercial_space')</option>
                                      <option value="villa">@lang('app.villa')</option> -->
                                      @if(!empty(@$house_property_types))
                                        @foreach($house_property_types as $key => $value)
                                            <option value="{{$value['id']}}" {{ request('property_type') == $value['id'] ? 'selected' :'' }}>{{$value['name']}}</option>
                                        @endforeach
                                      @endif
                                  </select>
                               </div>
                            </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
</div>
<div class="container">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12"> 
              <h3>Property Listing</h3>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                <div class="form-group" style="margin-top:16px;">
                  <select class="btn btn-default sortByBtn" id="sortByBtn">
                    <option value="grid" selected>Grid View</option>
                    <option value="list">List View</option>
                  </select>
                </div>
            </div>
         </div>
         <span id="grid_properties">
            @include('theme.modern.gridListingRender')
         </span>
         <span id="listing_properties">
            @include('theme.modern.listingPropertiesRender')
         </span>
          <div class="row">
              <div class="col-xs-12">
                 {{ $ads->appends(request()->input())->links() }}
              </div>
          </div>
      </div>
   </div>
</div>
@endsection
@section('page-js')
<script>
   $(document).ready(function() {
       $('#shortBySelect').change(function () {
           var form_input = $('#listingFilterForm').serialize();
           location.href = '{{ route('listing') }}?' + form_input + '&shortBy=' + $(this).val();
       });
   });
   function generate_option_from_json(jsonData, fromLoad){
       //Load Category Json Data To Brand Select
       if (fromLoad === 'category_to_sub_category'){
           var option = '';
           if (jsonData.length > 0) {
               option += '<option value="" selected> <?php echo trans('app.select_a_sub_category') ?> </option>';
               for ( i in jsonData){
                   option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].category_name +' </option>';
               }
               $('#sub_category_select').html(option);
               $('#sub_category_select').select2();
           }else {
               $('#sub_category_select').html('<option value="">@lang('app.select_a_sub_category')</option>');
               $('#sub_category_select').select2();
           }
           $('#loaderListingIcon').hide('slow');
       }else if (fromLoad === 'category_to_brand'){
           var option = '';
           if (jsonData.length > 0) {
               option += '<option value="" selected> <?php echo trans('app.select_a_brand') ?> </option>';
               for ( i in jsonData){
                   option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
               }
               $('#brand_select').html(option);
               $('#brand_select').select2();
           }else {
               $('#brand_select').html('<option value="">@lang('app.select_a_brand')</option>');
               $('#brand_select').select2();
           }
           $('#loaderListingIcon').hide('slow');
       }else if(fromLoad === 'country_to_state'){
           var option = '';
           if (jsonData.length > 0) {
               option += '<option value="" selected> @lang('app.select_state') </option>';
               for ( i in jsonData){
                   option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
               }
               $('#state_select').html(option);
               $('#state_select').select2();
           }else {
               $('#state_select').html('<option value="" selected> @lang('app.select_state') </option>');
               $('#state_select').select2();
           }
           $('#loaderListingIcon').hide('slow');
   
       }else if(fromLoad === 'state_to_city'){
           var option = '';
           if (jsonData.length > 0) {
               option += '<option value="" selected> @lang('app.select_city') </option>';
               for ( i in jsonData){
                   option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
               }
               $('#city_select').html(option);
               $('#city_select').select2();
           }else {
               $('#city_select').html('<option value="" selected> @lang('app.select_city') </option>');
               $('#city_select').select2();
           }
           $('#loaderListingIcon').hide('slow');
       }
   }
   
   $(function(){
   
       $('[name="country"]').change(function(){
           var country_id = $(this).val();
           $('#loaderListingIcon').show();
           $.ajax({
               type : 'POST',
               url : '{{ route('get_state_by_country') }}',
               data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
               success : function (data) {
                   generate_option_from_json(data, 'country_to_state');
               }
           });
       });
       $('[name="state"]').change(function(){
           var state_id = $(this).val();
           $('#loaderListingIcon').show();
           $.ajax({
               type : 'POST',
               url : '{{ route('get_city_by_state') }}',
               data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
               success : function (data) {
                   generate_option_from_json(data, 'state_to_city');
               }
           });
       });
   });
   $(function(){
       $('#showGridView').click(function(){
           $('.ad-box-grid-view').show();
           $('.ad-box-list-view').hide();
           $.ajax({
               type : 'POST',
               url : '{{ route('switch_grid_list_view') }}',
               data : { grid_list_view : 'grid',  _token : '{{ csrf_token() }}' },
           });
       });
       $('#showListView').click(function(){
           $('.ad-box-grid-view').hide();
           $('.ad-box-list-view').show();
           $.ajax({
               type : 'POST',
               url : '{{ route('switch_grid_list_view') }}',
               data : { grid_list_view : 'list',  _token : '{{ csrf_token() }}' },
           });
       });
   });
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{get_option('google_map_api_key')}}&libraries=places"></script>
<script>
    function initialize() {
      var input = document.getElementById('area');
      var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('area2').value = place.name;
            document.getElementById('lat').value = place.geometry.location.lat();
            document.getElementById('long').value = place.geometry.location.lng();
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
   @if(session('success'))
       toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
   @endif
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#listing_properties").css('display','none');
  });
  $(document).on('change','.sortByBtn',function(){
      var value = $(this).val();
      if(value == 'list'){
          $("#listing_properties").css('display','block');
          $("#grid_properties").css('display','none');
      }else{
        $("#listing_properties").css('display','none');
        $("#grid_properties").css('display','block');
      }
  });
</script>
@endsection