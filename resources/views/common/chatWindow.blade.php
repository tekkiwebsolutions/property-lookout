@extends('layout.main')
@section('title') {{ucfirst($user_type)}} Chat @parent @endsection
@section('main')
<?php //prx($window_type); ?>
<style>
	.input_msg_write .write_msg {
	    width: 87%;
	    float: left;
	    border-radius: 5px;
	    padding: 20px 10px;
	    margin-left: 10px;
	}
	.input_msg_write .chatsendbtn {
	    margin-right: 15px;
	}
	.clearfix {
    	float: left;
    	width: 100%;
	}

	.chatcontainer .agent_window{
		padding: 6px;
	    margin: 8px 0;
	    width: 57%;
	    float: right;
	    border-radius: 5px;
	    border: 1px solid #e3e3e3;
	    background-color:#2c3e50;
	    color: white;
	}
	.chatcontainer .user_window{
		padding: 6px;
	    margin: 8px 0;
	    width: 57%;
	    float: left;
	    border-radius: 5px;
	    border: 1px solid #e3e3e3;
	    background-color:#2c3e50;
	    color: white;
	}
	.sender_style{
		padding: 6px;
	    margin: 8px 0;
	    width: 57%;
	    float: right;
	    border-radius: 5px;
	    border: 1px solid #e3e3e3;
	    background-color:#2c3e50;
	    color: white;
	}
	.reciever_style{
		padding: 6px;
	    margin: 8px 0;
	    width: 57%;
	    float: left;
	    border-radius: 5px;
	    background-color: #f5f4f4;
	    border: 1px solid #e3e3e3;	
	}
	.chatcontainer{
	 	height: 400px;
	 	overflow-y: scroll;
	}

	/*new css (13 dec 2018)*/
	.chatcontainer .agent_div{
		float:left;
		width:100%;
	}

	.chatcontainer .user_div{
		float:left;
		width:100%;
	}

	.chatcontainer .agent{
		padding:6px;
		margin:8px 0px;
		width:57%;
		float:right;
		border-radius:5px;
		background-color:#2c3e50; 
		border: 1px solid #e3e3e3;
		color: #fff;
	}

	.chatcontainer .user{
		padding:6px;
		margin:8px 0px;
		width:57%;
		float:left;
		border-radius:5px;
		background-color:#f5f4f4;
		border:1px solid #e3e3e3;
	}
</style>

<div class="row mlr0">
   	<div class="page_wrapper">
      	<div class="container">
         	<div class="col-lg-12">
				<h2 class="single_page_heading">Chat</h2>
				<div class="panel panel-default chat_panel clearfix">
					<div class="panel-heading clearfix">
	                  	<div class="float-left">
		                     <h4>{{$chatUser['name']}}</h4>
	                  	</div>
	              	</div>
	              	<div class="chatcontainer panel-body clearfix" id="chatcontainer">
						@if(!empty($chat_messages))
							@foreach($chat_messages as $key => $value)
								<div data-msgid="{{$value['id']}}" class="@if($value['sent_from'] == 'agent'){{'agent_div'}}@else{{'user_div'}}@endif">

									<!-- check which classes to use based on the window type and sent messages -->
									@if($window_type == 'agentWindow')
										@if($value['sent_from'] == 'user')
											<?php $show_class = 'user'; ?>
										@else
											<?php $show_class = 'agent'; ?>
										@endif
									@else
										@if($value['sent_from'] == 'user')
											<?php $show_class = 'agent'; ?>
										@else
											<?php $show_class = 'user'; ?>
										@endif
									@endif
									<!-- check which classes to use based on the window type and sent messages -->

									<p data-msgid="{{$value['id']}}" class="{{$show_class}}">{{$value['Message']}}<span>{{date('d/m/Y',strtotime($value['created_at']))}}</span></p>

								</div>
				        	@endforeach
			            @endif
      				</div>
					<div class="chatbuttoncontainer">
						<form id="insertChat" method="post">
							{{csrf_field()}}
							<input type="hidden" name="agent_id" value="{{$agent_id}}" id="agent_id">
							<input type="hidden" name="user_id" value="{{$user_id}}" id="user_id">
							<input type="hidden" name="window_type" value="{{$window_type}}" id="window_type">

							<!-- <textarea id="textmessageready" name="textMessage" placeholder="Message..."></textarea>
							<input type="button" value="sent" class="chatsendbtn" /> -->
							<div class="input_msg_write">
								<input type="text" class="write_msg form-control" placeholder="Write a message" id="textmessageready" name="textMessage" />
                  				<button type="button" class="btn btn-primary f-right black-btn chatsendbtn">Send</button>
                  			</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('page-js')
	<script type="text/javascript">
		$(function(){
			var wtf 	= $("#chatcontainer");
			var height 	= wtf[0].scrollHeight;
			wtf.scrollTop(height); 
		});

		var usertype = '{{$user_type}}';
		var agent_id 	= $("#agent_id").val(); 
		var user_id 	= $("#user_id").val();
		var window_type = $("#window_type").val();

		// call ajax function after every 2.5 seconds to check new message from any side 
		setInterval(ajaxCall, 2500);

		function ajaxCall() {
			var last_id = $("#chatcontainer div:last-child").attr('data-msgid');
			$.ajax({
				data: {agent_id:agent_id , user_id:user_id , window_type:window_type,last_id:last_id,_token : '{{ csrf_token() }}'},
		        type: "post",
		        url: "{{route('ajax_chat_after_interval')}}",
		        success: function(data){
			        if(data != '') {

			        	// function to show latest messages on agent side or user side
			        	showLatestMessage(data);
			        }

		        }
			});
		}

		$(document).on('click','.chatsendbtn',function() {

			// send latest message with button click
			sendMessageShow();
		});

		$('#textmessageready').keypress(function(e) {

			var key = e.which;
			if(key == 13) {
				e.preventDefault();
				// send latest message with enter pressed
				sendMessageShow();
			}
		});

		// common function to save and show latest message when button clicked or enter pressed
		function sendMessageShow() {
			var last_id_two = $("#chatcontainer div:last-child").attr('data-msgid');
			last_id_two = parseInt(last_id_two)+1;
			
			// insert chat
	        var data = $("#insertChat").serialize();
			$.ajax({
			    data: data,
			    type: "post",
			    url: "{{route('insert_chat')}}",
			    success: function(data){
					
					if(data != '') {
			        	// function to show data on agent side or user side
			        	showLatestMessage(data);
			        }

			    }
			});
		}

		// common function to show data on agent side or user side
		function showLatestMessage(data) {
			if(data.sent_from == 'agent') {
        		var class_name = 'agent_div';
        	} else {
        		var class_name = 'user_div';
        	}
        	if(window_type == 'agentWindow') {
        		if(data.sent_from == 'agent') {
        			var message_class_name = 'agent';
        		} else {
        			var message_class_name = 'user';
        		}
        	} else {
        		if(data.sent_from == 'agent') {
        			var message_class_name = 'user';
        		} else {
        			var message_class_name = 'agent';
        		}
        	}
        	
        	$(".chatcontainer").append('<div data-msgid="'+data.id+'" class="'+class_name+'"><p data-msgid="'+data.id+'" class="'+message_class_name+'">'+data.Message+'<span>'+data.sent_time+'</span></p></div>');
        	$("#textmessageready").val("");
        	var wtf    = $('#chatcontainer');
			var height = wtf[0].scrollHeight;
			wtf.scrollTop(height);
		}

	</script>
@endsection

