<!DOCTYPE html>
<html>
<head>
	<title>{{ucfirst($user_type)}} Chat</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<style>
	.chatcontainer{
	 	width: 400px;
	 	height: 400px;
	 	border: solid 1px #000;
	 	margin: 0 auto;
	 	padding: 10px;
	 	overflow-y: scroll;
	}
	.chatcontainer .user{
		margin-left: 14px;
		color:red;
	}

	.chatbuttoncontainer{
		text-align: center;
	}
	</style>
</head>
<body>
	<H1>CHAT</H1>
	<div class="chatcontainer" id="chatcontainer">
		@if(!empty($chat_messages))
			@foreach($chat_messages  as $key => $value)
				<p data-msgId="{{$value['id']}}" class="{{$value['sent_from']}}">{{ucfirst($value['sent_from'])}} : {{$value['Message']}}</p>
			@endforeach
		@endif
	</div>
	<div class="chatbuttoncontainer">
		<form id="insertChat" method="post">
			{{csrf_field()}}
			<textarea id="textmessageready" name="textMessage" placeholder="Message..."></textarea>
			<input type="hidden" name="agent_id" value="{{$agent_id}}" id="agent_id">
			<input type="hidden" name="user_id" value="{{$user_id}}" id="user_id">
			<input type="hidden" name="window_type" value="{{$window_type}}" id="window_type">

			<input type="button" value="sent" class="chatsendbtn" />
		</form>
	</div>
</body>
	<script>
		$(function() {
		  var wtf    = $('#chatcontainer');
		  var height = wtf[0].scrollHeight;
		  wtf.scrollTop(height);
		});

		var usertype = '{{$user_type}}';
		// setInterval(function(){ 
		// 	$(".chatcontainer").append('<p class="agent">I am good</p>');
		// }, 2500);
		setInterval(ajaxCall, 2500);
		var agent_id 	= $("#agent_id").val(); 
		var user_id 	= $("#user_id").val();
		var window_type = $("#window_type").val();	
		function ajaxCall() {
		var last_id = $("#chatcontainer p:last-child").attr('data-msgId');

			  	$.ajax({
			         data: {agent_id:agent_id , user_id:user_id , window_type:window_type,last_id:last_id,_token : '{{ csrf_token() }}'},
			         type: "post",
			         url: "{{route('ajax_chat_after_interval')}}",
			         success: function(data){
			             // alert("Data Save: " + data);
			             if(data != ''){
		             			$(".chatcontainer").append('<p data-msgId="'+data.id+'" class="'+data.sent_from+'">'+data.Message+'</p>');

			             }

			        }
				});
		}

		$(document).on('click', '.chatsendbtn', function(){
			var last_id_two = $("#chatcontainer p:last-child").attr('data-msgId');
			last_id_two = parseInt(last_id_two)+1;
			if(usertype == 'agent'){				
				$(".chatcontainer").append('<p data-msgId="'+last_id_two+'" class="agent"> Agent:'+$("#textmessageready").val()+'</p>');
			}else{			
				$(".chatcontainer").append('<p data-msgId="'+last_id_two+'" class="user"> User:'+$("#textmessageready").val()+'</p>');
			}
			// insert chat
	        var data = $("#insertChat").serialize();
			  $.ajax({
			         data: data,
			         type: "post",
			         url: "{{route('insert_chat')}}",
			         success: function(data){
			             // alert("Data Save: " + data);
						 $("#textmessageready").val("");

			        }
			});

		});
	</script>
</html>