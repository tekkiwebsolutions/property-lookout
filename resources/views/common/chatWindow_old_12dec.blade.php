@extends('layout.main')
@section('title') {{ucfirst($user_type)}} Chat @parent @endsection
@section('main')
<?php //prx($window_type); ?>
<style>
	.input_msg_write .write_msg {
	    width: 87%;
	    float: left;
	    border-radius: 5px;
	    padding: 20px 10px;
	    margin-left: 10px;
	}
	.input_msg_write .chatsendbtn {
	    margin-right: 15px;
	}
	.clearfix {
    	float: left;
    	width: 100%;
	}

	.chatcontainer .agent_window{
		padding: 6px;
	    margin: 8px 0;
	    width: 57%;
	    float: right;
	    border-radius: 5px;
	    border: 1px solid #e3e3e3;
	    background-color:#2c3e50;
	    color: white;
	}
	.chatcontainer .user_window{
		padding: 6px;
	    margin: 8px 0;
	    width: 57%;
	    float: left;
	    border-radius: 5px;
	    border: 1px solid #e3e3e3;
	    background-color:#2c3e50;
	    color: white;
	}
	.sender_style{
		padding: 6px;
	    margin: 8px 0;
	    width: 57%;
	    float: right;
	    border-radius: 5px;
	    border: 1px solid #e3e3e3;
	    background-color:#2c3e50;
	    color: white;
	}
	.reciever_style{
		padding: 6px;
	    margin: 8px 0;
	    width: 57%;
	    float: left;
	    border-radius: 5px;
	    background-color: #f5f4f4;
	    border: 1px solid #e3e3e3;	
	}
	.chatcontainer{
	 	height: 400px;
	 	overflow-y: scroll;
	}
</style>

<div class="row mlr0">
   	<div class="page_wrapper">
      	<div class="container">
         	<div class="col-lg-12">
				<h2 class="single_page_heading">Chat</h2>
				<div class="panel panel-default chat_panel clearfix">
					<div class="panel-heading clearfix">
	                  	<div class="float-left">
		                     <h4>{{$chatUser['name']}}</h4>
	                  	</div>
	              	</div>
	              	<div class="chatcontainer panel-body clearfix" id="chatcontainer">
						@if(!empty($chat_messages))
							@foreach($chat_messages as $key => $value)
								<div data-msgid="{{$value['id']}}" class="@if($value['sent_from'] == 'agent'){{'agent_div'}}@else{{'user_div'}}@endif">
									<p data-msgid="{{$value['id']}}" class="{{$value['sent_from']}}@if($window_type =='agent_window' && $value['sent_from'] == 'agent'){{'sender_style'}}@elseif($window_type == 'user_window' && $value['sent_from'] == 'user'){{'sender_style'}}@elseif($window_type == 'user_window' && $value['sent_from'] == 'agent'){{'reciever_style'}}@elseif($window_type == 'agent_window' && $value['sent_from'] == 'agent'){{'reciever_style'}}@endif">{{$value['Message']}}<span>{{date('d/m/Y',strtotime($value['created_at']))}}</span></p>

								</div>
				        	@endforeach
			            @endif
      				</div>
					<div class="chatbuttoncontainer">
						<form id="insertChat" method="post">
							{{csrf_field()}}
							<input type="hidden" name="agent_id" value="{{$agent_id}}" id="agent_id">
							<input type="hidden" name="user_id" value="{{$user_id}}" id="user_id">
							<input type="hidden" name="window_type" value="{{$window_type}}" id="window_type">

							<!-- <textarea id="textmessageready" name="textMessage" placeholder="Message..."></textarea>
							<input type="button" value="sent" class="chatsendbtn" /> -->
							<div class="input_msg_write">
								<input type="text" class="write_msg form-control" placeholder="Write a message"  id="textmessageready" name="textMessage" />
                  				<button type="button" class="btn btn-primary f-right black-btn chatsendbtn">Send</button>
                  			</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('page-js')
	<script>

		$(document).ready(function(){
			var window_type = $("#window_type").val();
			if(window_type == 'agentWindow'){
				$(".agent_div").css({	
									"float": "left",
    								"width": "100%",
								});
				$(".user_div").css({
									"float": "left",
    								"width": "100%",
								});
				$(".agent").css({
							"padding": "6px",
						    "margin": "8px 0",
						    "width": "57%",
						    "float": "right",
						    "border-radius": "5px",
						    "background-color": "#2c3e50",
						    "border": "1px solid #e3e3e3",
						    "color": "white",

				});
				$(".user").css({
							"padding": "6px",
						    "margin": "8px 0",
						    "width": "57%",
						    "float": "left",
						    "border-radius": "5px",
						    "background-color": "#f5f4f4",
						    "border": "1px solid #e3e3e3"

				});

			}else if(window_type == 'userWindow'){
				$(".user_div").css({	
									"float": "left",
    								"width": "100%",
								});
				$(".agent_div").css({
									"float": "left",
    								"width": "100%",
								});
				$(".user").css({
							"padding": "6px",
						    "margin": "8px 0",
						    "width": "57%",
						    "float": "right",
						    "border-radius": "5px",
						    "border": "1px solid #e3e3e3",
						    "color": "white",
						    "background-color": "#2c3e50",

				});
				$(".agent").css({
							"padding": "6px",
						    "margin": "8px 0",
						    "width": "57%",
						    "float": "left",
						    "border-radius": "5px",
						    "background-color": "#f5f4f4",
						    "border": "1px solid #e3e3e3"

				});
			}
		});
		
		$(function() {
		  	var wtf    = $('#chatcontainer');
		  	var height = wtf[0].scrollHeight;
		  	wtf.scrollTop(height);
		});

		var usertype = '{{$user_type}}';
		setInterval(ajaxCall, 2500);
		var agent_id 	= $("#agent_id").val(); 
		var user_id 	= $("#user_id").val();
		var window_type = $("#window_type").val();	
		function ajaxCall() {
			var last_id = $("#chatcontainer div:last-child").attr('data-msgid');
         	if(window_type == 'agentWindow'){
					$(".agent_div").css({	
										"float": "left",
	    								"width": "100%",
									});
					$(".user_div").css({
										"float": "left",
	    								"width": "100%",
									});
					$(".agent").css({
								"padding": "6px",
							    "margin": "8px 0",
							    "width": "57%",
							    "float": "right",
							    "border-radius": "5px",
							    "border": "1px solid #e3e3e3",
							     "background-color": "#2c3e50",
							    "color": "white" ,

					});
					$(".user").css({
								"padding": "6px",
							    "margin": "8px 0",
							    "width": "57%",
							    "float": "left",
							    "border-radius": "5px",
							    "background-color": "#f5f4f4",
							    "border": "1px solid #e3e3e3"

					});
			}else if(window_type == 'userWindow'){

				$(".user_div").css({	
									"float": "left",
    								"width": "100%",
								});
				$(".agent_div").css({
									"float": "left",
    								"width": "100%",
								});
				$(".user").css({
							"padding": "6px",
						    "margin": "8px 0",
						    "width": "57%",
						    "float": "right",
						    "border-radius": "5px",
						    "border": "1px solid #e3e3e3",
						    "background-color": "#2c3e50",
						    "color": "white" ,

				});
				$(".agent").css({
							"padding": "6px",
						    "margin": "8px 0",
						    "width": "57%",
						    "float": "left",
						    "border-radius": "5px",
						    "background-color": "#f5f4f4",
						    "border": "1px solid #e3e3e3"

				});
			}
		  	$.ajax({
		        data: {agent_id:agent_id , user_id:user_id , window_type:window_type,last_id:last_id,_token : '{{ csrf_token() }}'},
		        type: "post",
		        url: "{{route('ajax_chat_after_interval')}}",
		        success: function(data){

					if(data.sent_from == 'agent'){
						var class_name = 'agent_div';
					}else{
						var class_name = 'user_div';
					}
					if(data.sent_from == 'agent' && window_type == 'agentWindow'){
						var style_tags = 'padding: 6px; margin: 8px 0;width: 57%;float: right;border-radius: 5px;border: 1px solid #e3e3e3;background-color:#2c3e50;color: white;'
					}else if(data.sent_from == 'agent' && window_type == 'userWindow'){
						var style_tags = 'padding: 6px;margin: 8px 0; width: 57%;float: left;border-radius: 5px;background-color: #f5f4f4;border: 1px solid #e3e3e3;'
					}else if(data.sent_from == 'user' && window_type == 'agentWindow'){
						var style_tags = 'padding: 6px;margin: 8px 0; width: 57%;float: left;border-radius: 5px;background-color: #f5f4f4;border: 1px solid #e3e3e3;'
					}else if(data.sent_from == 'user' && window_type == 'agentWindow'){

					}

		            if(data != ''){
             			$(".chatcontainer").append('<div data-msgid="'+data.id+'" class="'+class_name+'"><p data-msgid="'+data.id+'" class="'+data.sent_from+'" style="'+style_tags+'">'+data.Message+'<span>'+data.sent_time+'</span></p></div>');

		            }
		            var wtf    = $('#chatcontainer');
		  			var height = wtf[0].scrollHeight;
		  			wtf.scrollTop(height);
		        }
			});
		}

		$(document).on('click', '.chatsendbtn', function(){
			//alert("button clicked");
			var last_id_two = $("#chatcontainer div:last-child").attr('data-msgid');
			last_id_two = parseInt(last_id_two)+1;

			if(usertype == 'agent'){	
			
				$(".chatcontainer").append('<div data-msgid="'+last_id_two+'" class="agent_div"><p data-msgid="'+last_id_two+'" class="agent agent_window sender_style">'+$("#textmessageready").val()+'</p></div');
			}else{	

				$(".chatcontainer").append('<div data-msgid="'+last_id_two+'" class="user_div"><p data-msgid="'+last_id_two+'" class="user user_window reciever_style">'+$("#textmessageready").val()+'</p></div>');
			}
			// insert chat
	        var data = $("#insertChat").serialize();
			$.ajax({
			    data: data,
			    type: "post",
			    url: "{{route('insert_chat')}}",
			    success: function(data){
					$("#textmessageready").val("");
					var wtf    = $('#chatcontainer');
		  			var height = wtf[0].scrollHeight;
		  			wtf.scrollTop(height);

			    }
			});
		});

		$('.write_msg').keypress(function(e) {
			
			var key = e.which;
			if(key == 13) {
				e.preventDefault();
				var last_id_two = $("#chatcontainer div:last-child").attr('data-msgid');
				last_id_two = parseInt(last_id_two)+1;

				if(usertype == 'agent'){	
				
					$(".chatcontainer").append('<div data-msgid="'+last_id_two+'" class="agent_div"><p data-msgid="'+last_id_two+'" class="agent agent_window sender_style">'+$("#textmessageready").val()+'</p></div');
				}else{	

					$(".chatcontainer").append('<div data-msgid="'+last_id_two+'" class="user_div"><p data-msgid="'+last_id_two+'" class="user user_window reciever_style">'+$("#textmessageready").val()+'</p></div>');
				}
				// insert chat
		        var data = $("#insertChat").serialize();
				$.ajax({
				    data: data,
				    type: "post",
				    url: "{{route('insert_chat')}}",
				    success: function(data){
						$("#textmessageready").val("");
						var wtf    = $('#chatcontainer');
		  				var height = wtf[0].scrollHeight;
		  				wtf.scrollTop(height);

				    }
				});
			}
		});
	</script>
@endsection

