@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
    <link rel="stylesheet" href="{{asset('assets/css/admin.css')}}">
    <div class="container">
        <div id="wrapper">
            <div id="page-wrapper">
                <form method="post" action="{{url('testGoogleStorage')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                     <input type="file" name="image">
                     <input type="submit" name="submit" value="submit">
                </form>
            </div>  
        </div>  
    </div> 
@endsection

@section('page-js')

<script>
    @if(session('success'))
        toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
    @endif
</script>

@endsection