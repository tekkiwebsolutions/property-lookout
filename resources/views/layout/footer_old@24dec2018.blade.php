<!-- remove this section to view penoramic system -->

<div class="footer">
    <div class="footer-top">
       <div class="container">
           <div class="row">

               <div class="col-md-5 col-sm-12 col-xs-12">
                   <div class="footer-widget">
                       <img src="<?php echo asset('/');?>/assets/img/footer-logo.png" class="fleft img-responsive" alt="Company logo" />
                       <p>{{ get_option('footer_about_us') }}</p>
                   </div>
               </div>

               <div class="col-sm-6 col-md-3 col-xs-12">
                   <div class="footer-widget footer-address clearfix">
                       <h4>@lang('app.contact_us')</h4>
                       <address>
                           @if(get_option('footer_address'))
                               <i class="fa fa-map-marker"></i>
                               <span>
                                   {!! get_option('footer_address') !!}
                               </span>
                           @endif
                       </address>
                       <div class="footer-call">
                           @if(get_option('site_phone_number'))
                               <i class="fa fa-phone"></i>
                               <abbr title="Phone">{!! get_option('site_phone_number') !!}</abbr>
                           @endif
                       </div>

                       @if(get_option('site_email_address'))
                           <address>
                               <i class="fa fa-envelope-o"></i>
                               <a href="mailto:{{ get_option('site_email_address') }}"> {{ get_option('site_email_address') }} </a>
                           </address>
                       @endif
                   </div>
               </div>

               <div class="col-sm-6 col-md-4 col-xs-12">
                   <div class="footer-widget">
                       <h4>Connect With Us</h4>
                       @if(!Auth::check())
                           <div class="newsletter">
                               <input type="text" name="user_email" id="user_email" placeholder="Your email address"/>
                               <button type="submit" class="signupbtn open_signup_page">Sign Up</button>
                           </div>
                           <div class="error" id="user_email_warning" style="color:#FF6347"></div>
                       @endif
                       <div class="social-icons">
                           <ul class="social-ul">
                               @if(get_option('facebook_url'))
                                   <li class="fb">
                                       <a href="{{ get_option('facebook_url') }}" target="_blank">
                                           <i class="fa fa-facebook"></i>
                                       </a>
                                   </li>
                               @endif

                               @if(get_option('twitter_url'))
                                   <li class="tw">
                                       <a href="{{ get_option('twitter_url') }}" target="_blank">
                                           <i class="fa fa-twitter"></i>
                                       </a>
                                   </li>
                               @endif
                               @if(get_option('linked_in_url'))
                                   <li class="inked">
                                       <a href="{{ get_option('linked_in_url') }}" target="_blank">
                                           <i class="fa fa-linkedin"></i>
                                       </a>
                                   </li>
                               @endif
                               @if(get_option('dribble_url'))
                                   <li class="dribl">
                                       <a href="{{ get_option('dribble_url') }}" target="_blank">
                                           <i class="fa fa-dribbble"></i>
                                       </a>
                                   </li>
                               @endif
                               @if(get_option('google_plus_url'))
                                   <li class="gplus">
                                       <a href="{{ get_option('google_plus_url') }}" target="_blank">
                                           <i class="fa fa-google-plus"></i>
                                       </a>
                                   </li>
                               @endif
                               @if(get_option('youtube_url'))
                                   <li class="utub">
                                       <a href="{{ get_option('youtube_url') }}" target="_blank">
                                           <i class="fa fa-youtube"></i>
                                       </a>
                                   </li>
                               @endif
                           </ul>
                           <div class="clearfix"></div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

   <div class="footer-bottom">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
                   <p>{!! get_text_tpl(get_option('footer_left_text')) !!}</p>
               </div>
           </div>
       </div>
   </div>
</div>

<!-- remove this section to view penoramic system -->

<div id="loadingOverlay" style="display: none;">
   <div class="circleLoader"></div>
   <p>@lang('app.loading')...</p>
</div>


<!-- loader script start -->

<script src="{{ asset('assets/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/vendor/bootstrap-confirmation.min.js') }}"></script>
<script src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('assets/select2-3.5.3/select2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/nprogress/nprogress.js') }}"></script>
<!-- <script src="{{ asset('assets/js/vendor/jquery-1.11.2.min.js') }}"></script> -->
<!-- <script src="https://www.gstatic.com/firebasejs/5.5.8/firebase.js"></script> -->
<!-- <script src="https://www.gstatic.com/firebasejs/4.10.1/firebase.js"></script> -->
<!-- <script src="https://www.gstatic.com/firebasejs/5.6.0/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-messaging.js"></script> -->

<script type="text/javascript">
   document.onreadystatecha nge = function () {
       var state = document.readyState
       if (state == 'complete') {
              document.getElementById('interactive');
              document.getElementById('load').style.visibility="hidden";
       }
   }
   // loader script ends

   // Initialize Firebase
   // var config = {
   //   apiKey: "AIzaSyDNoDNMMTD2JEU98QnAc2d5cse3645P2Xk",
   //   authDomain: "property-lookout.firebaseapp.com",
   //   databaseURL: "https://property-lookout.firebaseio.com",
   //   projectId: "property-lookout",
   //   storageBucket: "property-lookout.appspot.com",
   //   messagingSenderId: "702088922988"
   // };
   // firebase.initializeApp(config);

   // const messaging = firebase.messaging();

   // messaging.requestPermission()
   // .then(function() {
   //   console.log('Notification permission granted.');
   //   return messaging.getToken();
   // })
   // .then(function(token) {
   //   console.log(token); // Display user token
   // })
   // .catch(function(err) { // Happen if user deney permission
   //   console.log('Unable to get permission to notify.', err);
   // });
 
   NProgress.start();
   NProgress.done();

   function ValidateEmail(email) {
       var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
       return expr.test(email);
   };

   $(document).on('click','.open_signup_page',function() {
       // alert("button clicked");
       $('#user_email_warning').text("");
       var email_val = $('#user_email').val();
       if(email_val == '') 
       {
           $('#user_email_warning').text("Please enter your email");
       }
       else
       {
           if(!ValidateEmail(email_val))
           {
               $('#user_email_warning').text("Please enter valid email address");
           }
           else
           {
               var path = "{{ url('/packages') }}"+'/'+email_val+'/register';
               window.location.href = path;
           }
       }
   })
</script>
<!-- Conditional page load script -->
@if(request()->segment(1) === 'dashboard')
   <script src="{{ asset('assets/plugins/metisMenu/dist/metisMenu.min.js') }}"></sc ript>
   <script>
       $(function() {
           $('#side-menu').metisMenu();
       });
       // jQuery(function($) {
       //      var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
       //      $('ul a').each(function() {
       //       if (this.href === path) {
       //        $(this).addClass('active');
       //       }
       //      });
       //     });

   </script>
@endif
<script src="{{ asset('assets/js/main.js') }}"></sc ript>
<script>
   var toastr_options = {closeButton : true};
</script>
@yield('page-js')


@if(get_option('additional_js'))
   {!! get_option('additional_js') !!}}
@endif
<script>
   $(document).on('click', '.ghuranti', function(){
       $('.themeqx-demo-chooser-wrap').toggleClass('open');
   });
   // mark all notifications as read
   // mark all notifications as read

   // reload the latest notifications function after every 5 seconds

   function setCookie(cname,cvalue,exdays) {
       var d = new Date();
       d.setTime(d.getTime() + (100 * exdays * 24 * 60 * 60 * 1000));
       var expires = "expires=" + d.toGMTString();
       document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
   }

   function getCookie(cname) {
       var name = cname + "=";
       var decodedCookie = decodeURIComponent(document.cookie);
       var ca = decodedCookie.split(';');
       for(var i = 0; i < ca.length; i++) {
           var c = ca[i];
           while (c.charAt(0) == ' ') {
               c = c.substring(1);
           }
           if (c.indexOf(name) == 0) {
               return c.substring(name.length, c.length);
           }
       }
       return "";
   }
   $(document).ready(function() {

       function current(id = ""){
           
           var TOKEN = getCookie("TOKEN");
           
           
           var createNewToken = getCookie("XSRF-TOKEN");
           if(TOKEN == ''){
               TOKEN = createNewToken;
               /*life set this token*/
               setCookie("TOKEN", createNewToken, 365);
           }

           var url = "{{url('/')}}";  
          // alert(url);
           $.ajax({
               url: url + '/notifications-token-save/?id=' + id + '&token=' + TOKEN,
               type: "GET",
               success:function(result) {
                   // var url = "{{url('/')}}";  
                   // if(result != 0){
                   //     var notification = new Notification('Notification', {
                   //       icon: url + '/uploads/logo/1535524351mh7sn-logo.jpg',
                   //       body: result.message,
                   //     });
                   // }
               },
               error:function() {
                   console.log('error');
               }
           }); 
           
       } 
       @if(Auth::check())
           current("{{Auth::user()->id}}");
       @else
           current();
       @endif

       document.addEventListener('DOMContentLoaded', function () {
           if (!Notification) {
               alert('Desktop notifications not available in your browser. Try Chromium.'); 
               return;
           }
           if (Notification.permission !== "granted")
               Notification.requestPermission();
          
       });

       var myVar = setInterval(getLatestNotifications, 2000);
       function getLatestNotifications() {

           function notifyMe() {
               if (Notification.permission !== "granted")
                   Notification.requestPermission();
               else {

                   var url = "{{url('/')}}";
                   var token = getCookie("TOKEN");

                   $.ajax({ 
                       url: url + '/get-latest-notifications?token=' + token,
                       type: "get",
                       success:function(result) {
                           var url = "{{url('/')}}";
                           
                           if(result.data != 0){
                               if(result.data.notification_token == token){
                                   var notification = new Notification('Notification', {
                                       icon: url + '/uploads/logo/1535524351mh7sn-logo.jpg',
                                       body: result.data.message,
                                   });
                               }                        
                           }
                           var asset = "{{asset('/')}}";
                           if(result.html != ""){

                               //$('.notify').html('<img src="'+ asset +'/assets/img/notify.png" alt="#">Live Notifications');  
                               $('#header_notifications').html(result.html);
                               $('#header_notifications1').html(result.html);

                               // var url = "{{url('')}}";
                               //   var token = getCookie('TOKEN');
                               //   var page1 = 1;
                               //   $.ajax({
                               //             url: url + '/all-notifications?page=' + page1 + '&token=' + token,
                               //             type: "get",
                                           
                               //         })
                               //         .done(function(data)
                               //         {
                               //             if(data.html == ""){
                               //                 $('.read').remove();
                               //                 $('.show_more').html('<span class="btn btn-primary">No more records found</span>');
                               //                 return;
                               //             }
                               //             //$('.ajax-load').hide();
                               //             $("#notification-data").html(data.html);
                               //         })
                               //         .fail(function(jqXHR, ajaxOptions, thrownError)
                               //         {
                               //               alert('server not responding...');

                               //         }); 
                           } else {
                               //$('#header_notifications').html('');
                               //$('#header_notifications1').html('');

                           }
                           //$('#all_read').html('<a href="jav * ascript:void(0);" class="rIcon allRead mark_notifications_read" data-tooltip="tooltip" data-placement="bottom" title="tümü okundu.">Mark All as Read</a>');
                           
                           
                       },
                       error:function() {
                           console.log('error');
                       }

                   });

               //    });               

               // notification.oncl ick = function () {
               //   window.open("http://stackoverflow.com/a/13328397/1269037");      
               // };

               }

           }
           notifyMe();
       }
   });
   
   $(document).on('click','.mark_notifications_read',function(e) {
       e.preventDefault();
       var Token = getCookie('TOKEN');
      
       $.ajax({
           url: "{{ url('notifications/mark-read') }}" + '?token=' +  Token,
           type: "GET",
           success:function(data) {
               
               if(data.success == 1) {
                   toastr.success(data.msg, '@lang('app.success')', toastr_options);
               } else {
                   toastr.error(data.msg, '@lang('app.success')', toastr_options);
               }
           },
           error:function() {
               toastr.error(data.msg, '@lang('app.success')', toastr_options);
           }
       })
   });

   // reload the latest notifications function after every 5 seconds

   // remove credits from houses after every 29th of the month
   $(document).ready(function() {
       $.ajax({
           url: "{{ url('/agents/remove-credits') }}",
           type: "get",
           success:function() {
               console.log('success');
           },
           error:function() {
               console.log('error');
           }
       })
   });

   // remove credits from houses after every 29th of the month
</script>


</body>
</html>