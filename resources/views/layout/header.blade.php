
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>@section('title') {{ get_option('site_title') }} @show</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @section('social-meta')
        <meta property="og:title" content="{{ get_option('site_title') }}">
        <meta property="og:description" content="{{ get_option('meta_description') }}">
        <meta property="og:url" content="{{ route('home') }}">
        <meta name="twitter:card" content="summary_large_image">
        <!--  Non-Essential, But Recommended -->
        <meta name="og:site_name" content="{{ get_option('site_name') }}">
        @show
        <style type="text/css">
            /*#load{
            width:100%;
            height:100%;
            position:fixed;
            z-index:9999;
            background:url("assets/img/loading.gif") no-repeat center center rgba(0,0,0,0.25)
            }*/
            #load{
                width:100%;
                height:100%;
                position:fixed;
                z-index:9999;
                background:url("../../assets/img/loading.gif") no-repeat center center rgba(0,0,0,0.25)
            }
        </style>
        <!-- <div id="load"></div> -->
        <!-- bootstrap css -->
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-theme.min.css') }}">
        <!-- load page specific css -->
        <!-- main select2.css -->
        <link rel="stylesheet" href="{{ asset('assets/select2-3.5.3/select2.css') }}"/>
        <link rel="stylesheet" href="{{ asset('assets/select2-3.5.3/select2-bootstrap.css') }}"/>
        <link rel="stylesheet" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/plugins/nprogress/nprogress.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/fonts/stylesheet.css') }}">
        <!-- Conditional page load script -->
        @if(request()->segment(1) === 'dashboard')
        <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/plugins/metisMenu/dist/metisMenu.min.css') }}">
        @endif
        <!-- main style.css -->
        <?php $default_style = get_option('default_style'); ?>
        @if($default_style == 'default')
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        @else
        <link rel="stylesheet" href="{{ asset("assets/css/style-{$default_style}.css") }}">
        @endif
        <link rel="manifest" href="{{asset('assets/manifest.json')}}">
        @yield('page-css')
        @if(get_option('additional_css'))
            <style type="text/css">
                {{ get_option('additional_css') }}
            </style>
        @endif
        <script src="{{ asset('assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
        <!-- <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script> -->
        <!-- Font awesome 4.4.0 -->
        <link rel="stylesheet" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
        <script type="text/javascript" src="{{ asset('assets/js/jquery-1.9.1.js')}}"></script>
        <!-- <script src="{{ asset('assets/firebase-messaging.js') }}"></script> -->
        <!-- <script src="{{ asset('assets/js/jquery.panorama_viewer.js') }}"></script> -->
        <!-- <link rel="stylesheet" href="{{ asset('assets/css/panorama_viewer.css') }}"> -->
        <!-- <style>
            html {
                height: 100%;
            }

            body {
                background: #F1f1f2;
                padding: 0;
                text-align: center;
                font-family: 'open sans';
                position: relative;
                margin: 0;
                height: 100%;
            }
            
            #panorama-wrapper > .wrapper {
                height: auto !important;
                height: 100%;
                margin: 0 auto; 
                overflow: hidden;
            }
            
            #panorama-wrapper > a {
                text-decoration: none;
            }
            
            #panorama-wrapper > h1, h2 {
                width: 100%;
                float: left;
            }

            #panorama-wrapper > h1 {
                margin-top: 100px;
                color: #999;
                margin-bottom: 5px;
                font-size: 70px;
                font-weight: 100;
            }

            #panorama-wrapper > h2 {
                padding: 00px 20px 30px 20px;
                box-sizing: border-box;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                letter-spacing: 0px;
                color: #888;
                font-size: 20px;
                line-height: 160%;
                font-weight: 100;
                margin-top: 10px;
                margin-bottom: 0;
            }
            
            #panorama-wrapper > .pointer {
                color: #00B0FF;
                font-family: 'Pacifico';
                font-size: 24px;
                margin-top: 15px;
                position: absolute;
                top: 130px;
                right: -40px;
            }

            #panorama-wrapper > .pointer2 {
                color: #00B0FF;
                font-family: 'Pacifico';
                font-size: 24px;
                margin-top: 15px;
                position: absolute;
                top: 130px;
                left: -40px;
            }

            #panorama-wrapper > pre {
                margin: 80px auto;
            }

            #panorama-wrapper > pre code {
                padding: 35px;
                border-radius: 5px;
                font-size: 15px;
                background: rgba(0,0,0,0.1);
                border: rgba(0,0,0,0.05) 5px solid;
                max-width: 500px;
            }
            
            #panorama-wrapper > .main {
                float: left;
                width: 100%;
                margin: 0 auto;
            }
            
            #panorama-wrapper > .main h1 {
                padding:20px 50px 10px;
                float: left;
                width: 100%;
                font-size: 60px;
                box-sizing: border-box;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                font-weight: 100;
                margin: 0;
                padding-top: 55px;
                font-family: 'Open Sans';
                letter-spacing: -1px;
                text-transform: capitalize;
            }
            
            #panorama-wrapper > .main h1.demo1 {
                background: #1ABC9C;
            }
            
            #panorama-wrapper > .reload.bell {
                font-size: 12px;
                padding: 20px;
                width: 45px;
                text-align: center;
                height: 47px;
                border-radius: 50px;
                -webkit-border-radius: 50px;
                -moz-border-radius: 50px;
            }
            
            #panorama-wrapper > .reload.bell #notification {
                font-size: 25px;
                line-height: 140%;
            }
            
            #panorama-wrapper > .reload, .btn{
                display: inline-block;
                border-radius: 3px;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                display: inline-block;
                line-height: 100%;
                padding: 0.7em;
                text-decoration: none;
                width: 100px;
                line-height: 140%;
                font-size: 17px;
                font-family: Open Sans;
                font-weight: bold;
                -webkit-box-shadow: none;
                box-shadow: none;
                background-color: #4D90FE;
                background-image: -webkit-linear-gradient(top,#4D90FE,#4787ED);
                background-image: -webkit-moz-gradient(top,#4D90FE,#4787ED);
                background-image: linear-gradient(top,#4d90fe,#4787ed);
                border: 1px solid #3079ED;
                color: #FFF;
            }

            #panorama-wrapper > .reload:hover{
                background: #317af2;
            }

            #panorama-wrapper > .btn {
                width: 200px;
                -webkit-box-shadow: none;
                box-shadow: none;
                background-color: #4D90FE;
                background-image: -webkit-linear-gradient(top,#4D90FE,#4787ED);
                background-image: -moz-linear-gradient(top,#4D90FE,#4787ED);
                background-image: linear-gradient(top,#4d90fe,#4787ed);
                border: 1px solid #3079ED;
                color: #FFF;
            }

            #panorama-wrapper > .clear {
                width: auto;
            }

            #panorama-wrapper > .btn:hover, .btn:hover {
                background: #317af2;
            }

            #panorama-wrapper > .btns {
                float: left;
                width: 100%;
                margin: 50px auto;
            }

            #panorama-wrapper > .credit {
                text-align: center;
                color: #888;
                padding: 10px 10px;
                margin: 0 0 0 0;
                background: #f5f5f5;
                float: left;
                width: 100%;
            }

            #panorama-wrapper > .credit a {
                text-decoration: none;
                font-weight: bold;
                color: black;
            }
            
            #panorama-wrapper > .back {
                position: absolute;
                top: 0;
                left: 0;
                text-align: center;
                display: block;
                padding: 7px;
                width: 100%;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                background:#f5f5f5;
                font-weight: bold;
                font-size: 13px;
                color: #888;
                -webkit-transition: all 200ms ease-out;
                -moz-transition: all 200ms ease-out;
                -o-transition: all 200ms ease-out;
                transition: all 200ms ease-out;
            }

            #panorama-wrapper > .back:hover {
                background: #eee;
            }
            
            #panorama-wrapper > .page-container {
                float: left;
                width: 100%;
                margin: 0 auto 300px;
                position: relative;
            }

            #panorama-wrapper > .panorama {
                width: 100%;
                float: left;
                margin-top: -5px;
                height: 700px;
                position: relative;
            }
            
            #panorama-wrapper > .panorama .credit {
                background: rgba(0,0,0,0.2);
                color: white;
                font-size: 12px;
                text-align: center;
                position: absolute;
                bottom: 0;
                right: 0;
                box-sizing: border-box;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                float: right;
            }

            </style> -->
        <!-- <script>
            // Use $(window).load() on live site instead of document ready. This is for the purpose of running locally only
            $(document).ready(function(){
              $(".panorama").panorama_viewer({
                repeat: true
              });
            });
            </script> -->
        <!-- uncomment this section to view panoramic system -->
        <!-- google recptcha script -->
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <!-- google recptcha script -->
    </head>
    <body>
        <!-- remove this section to view penoramic system -->
        <div class="header-nav-top">
            <div class="container">
                <div class="d-flx">
                    <div class="flx-inr">
                        <div class="topContactInfo">
                            <ul class="nav nav-pills">
                                <li>
                                    @if(!Auth::check())
                                        <a href="{{url('frontend/agentsList')}}">
                                            <i class="fa fa-comments" aria-hidden="true"></i>
                                            Live Chat
                                        </a>
                                    @else
                                        @if(Auth::user()->user_type == 'user')
                                            <a href="{{ route('user_chat_messages')}}">
                                                <i class="fa fa-comments" aria-hidden="true"></i>
                                                Live Chat
                                            </a>
                                        @elseif(Auth::user()->user_type == 'developer')
                                            <!-- <a href="{{ route('developer_user_chat_messages')}}">
                                                <i class="fa fa-comments" aria-hidden="true"></i>
                                                Live Chat
                                            </a> -->
                                        @elseif(Auth::user()->user_type == 'staff')
                                            <a href="{{ route('staff_chat_messages')}}">
                                                <i class="fa fa-comments" aria-hidden="true"></i>
                                                Live Chat
                                            </a>
                                        @endif
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="flx-inr">
                        <div class="topContactInfo center-info">
                            <ul class="nav nav-pills">
                                @if(get_option('site_phone_number'))
                                <li>
                                    <a href="callto://+{{get_option('site_phone_number')}}">
                                        <i class="fa fa-phone"></i>
                                        +{{ get_option('site_phone_number') }}
                                    </a>
                                </li>
                                @endif
                                @if(get_option('site_email_address'))
                                <li>
                                    <a href="mailto:{{ get_option('site_email_address') }}">
                                        <i class="fa fa-envelope"></i>
                                        {{ get_option('site_email_address') }}
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="flx-inr">
                        @if(Auth::check())
                        @if(Auth::user()->user_type == 'user' || Auth::user()->user_type == 'developer' || Auth::user()->user_type == 'staff')
                            <div class="topContactInfo top-nvrght">
                                <ul class="nav nav-pills">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <i class="fa fa-user"></i>
                                            @lang('app.hi'), {{ $logged_user->name }} 
                                        </a>
                                    </li>
                                    <!--<li>
                                        <a href="{{ route('dashboard') }}">
                                        <i class="fa fa-dashboard"></i>
                                        Dashboard </a>
                                        </li> -->
                                </ul>
                            </div>
                        @else
                            <div class="topContactInfo">
                                <ul class="nav nav-pills top-nvrght">
                                    <li>
                                        <a href="{{ route('profile') }}">
                                            <i class="fa fa-user"></i>
                                            @lang('app.hi'), {{ $logged_user->name }} 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('dashboard') }}">
                                            <i class="fa fa-dashboard"></i>
                                            Dashboard 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}">
                                            <i class="fa fa-sign-out"></i>
                                            @lang('app.logout')
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        @endif
                        @else
                            <div class="topContactInfo login_right">
                                <ul class="nav nav-pills top-nvrght">
                                    @if($header_menu_pages->count() > 0)
                                        @foreach($header_menu_pages as $page)
                                            <li><a href="{{ route('single_page', $page->slug) }}">{{ $page->title }} </a></li>
                                        @endforeach
                                    @endif
                                    @if( ! Auth::check())
                                        <li><a href="{{ route('login') }}"> <i class="fa fa-lock"></i>  {{ trans('app.login') }}  </a>  </li>
                                    @endif
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ route('home') }}">
                    @if(get_option('logo_settings') == 'show_site_name')
                        {{ get_option('site_name') }}
                    @else
                        @if(logo_url())
                            <img src="{{ logo_url() }}" class="img-responsive" alt="Company Logo" />
                        @else
                            {{ get_option('site_name') }}
                        @endif
                    @endif
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @if($header_menu_pages->count() > 0)
                            @foreach($header_menu_pages as $page)
                                <li><a href="{{ route('single_page', $page->slug) }}">{{ $page->title }} </a></li>
                            @endforeach
                        @endif
                        <?php 
                            if(Auth::check()){
                               if(Auth::user()->user_type == 'user' || Auth::user()->user_type == 'developer' || Auth::user()->user_type == 'staff'){
                                     $header_menu = 1;
                               }else{
                                     $header_menu = 0;
                               }
                            }else{
                               $header_menu = 0;
                            }
                        ?>
                        @if($header_menu == 1)
                            <?php $url = Request::url(); ?>
                            @if(Auth::user()->user_type == 'user')    
                                <li><a href="{{ route('agent_properties_list') }}">Home</a></li>
                            @elseif(Auth::user()->user_type == 'developer')
                                <!-- <li><a @if(empty(Auth::user()->no_of_staff)) href="{{ url('/developers/add-staff-number') }}" @else href="{{ route('developer_properties_list') }}" @endif>Home</a></li> -->
                                <li><a href="{{ route('developer_properties_list') }}">Home</a></li>
                            @elseif(Auth::user()->user_type == 'staff')
                                <li><a href="{{ route('staff_properties_list') }}">Home</a></li>
                            @endif
                            @if(Auth::check()) 
                                @if(Auth::user()->user_type == 'user')
                                    <li><a href="{{url('agents/credits-list')}}">My Credits</a></li>
                                    <li><a href="{{url('agents/reports')}}">Reports</a></li>
                                @endif
                            @endif
                            <!-- <li><a href="{{url('/blog')}}"> Blogs</a></li> -->
                            <li class="dropdown hidden-xs">
                                <a href="#" class="dropdown-toggle notify" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php echo asset('/');?>/assets/img/notify.png" alt="#">Live Notifications</a>
                                <ul class="dropdown-menu notify-drop">
                                    <i class="fa fa-sort-asc" aria-hidden="true"></i>
                                    <div class="notify-drop-title">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">Notification</div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 text-right ">
                                            </div> <!-- function to mark all as read is kept in footer.blade.php in the same folder -->
                                        </div>
                                    </div>
                                    <!-- end notify title -->
                                    <!-- notify content -->
                                    <div class="drop-content" id="header_notifications">
                                        @include('theme.notification-header-render')
                                    </div>
                                    <div class="notify-drop-footer text-center">
                                        <a href="{{ route('all_notifications') }}" class="allRead ">See All Notifications</a>
                                    </div>
                                </ul>
                            </li>
                            <!-- <li class="welcome">Welcome! You are logged in as</li> -->
                            <li class="dropdown admin-profile">
                                <a href="javascript:void(0);" class="dropdown-toggle admin-img" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                @if(!empty(Auth::user()->photo))
                                    <span class="welcome-msg">Welcome !</span>
                                    <img src="{{asset('uploads/avatar/'.Auth::user()->photo)}}" alt="Profile Photo" class="img-responsive"/>
                                @else
                                    <span class="welcome-msg">Welcome !</span>
                                    <img src="{{asset('uploads/avatar/default_user.png')}}" alt="Profile Photo" class="img-responsive"/>
                                @endif
                                </a>
                                <ul class="dropdown-menu admin-dropdown">
                                    <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    <!-- plan upgrade button not in use now as there are only two packages left -->
                                    @if(Auth::user()->user_type == 'user')    
                                        <li><a href="{{url( '/agent'.'/'.Auth::user()->slug )}}"><i class="fa fa-user" aria-hidden="true"></i>@if(!empty(Auth::user()->name)){{Auth::user()->name}}@endif</a></li>
                                    @elseif(Auth::user()->user_type == 'developer')
                                        <!-- <li><a @if(empty(Auth::user()->no_of_staff)) href="{{ url('/developers/add-staff-number') }}" @else href="{{url( '/developers'.'/'.Auth::user()->slug )}}" @endif><i class="fa fa-user" aria-hidden="true"></i>@if(!empty(Auth::user()->name)){{Auth::user()->name}}@endif</a></li> -->
                                        <li><a href="{{url( '/developers'.'/'.Auth::user()->slug )}}"><i class="fa fa-user" aria-hidden="true"></i>@if(!empty(Auth::user()->name)){{Auth::user()->name}}@endif</a></li>
                                    @elseif(Auth::user()->user_type == 'staff')
                                        <li><a href="{{url( '/staff'.'/'.Auth::user()->slug )}}"><i class="fa fa-user" aria-hidden="true"></i>@if(!empty(Auth::user()->name)){{Auth::user()->name}}@endif</a></li>
                                    @endif
                                    @if(Auth::user()->user_type == 'user')    
                                        <li><a href="{{ route('settings_view') }}"><i class="fa fa-cog" aria-hidden="true"></i>Account Settings</a></li>
                                    @elseif(Auth::user()->user_type == 'developer')
                                        <!-- <li><a @if(empty(Auth::user()->no_of_staff)) href="{{ url('/developers/add-staff-number') }}" @else href="{{ route('developer_settings_view') }}" @endif><i class="fa fa-cog" aria-hidden="true"></i>Account Settings</a></li> -->
                                        <li><a href="{{ route('developer_settings_view') }}"><i class="fa fa-cog" aria-hidden="true"></i>Account Settings</a></li>
                                    @elseif(Auth::user()->user_type == 'staff')
                                        <li><a href="{{ route('staff_settings_view') }}"><i class="fa fa-cog" aria-hidden="true"></i>Account Settings</a></li>
                                    @endif
                                    @if(Auth::user()->user_type == 'user')
                                    @endif
                                    <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a></li>
                                </ul>
                            </li>
                        @else
                            <li><a href="{{URL::to('/page/maintainance')}}">Maintenance</a></li>
                            <li><a href="{{URL::to('/frontend/agentsList')}}">Agents</a></li>
                            <li><a href="{{URL::to('/page/about-us')}}">@lang('app.about_us')</a></li>
                            <li><a href="{{URL::to('/contact-us')}}"> @lang('app.contact_us')</a></li>
                            @if(Auth::check() && Auth::user()->user_type != 'admin')
                            @elseif(!Auth::check())
                            @endif
                            <li class="dropdown hidden-xs">
                                <a href="javascript:;" class="dropdown-toggle notify" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <img src="<?php echo asset('/');?>/assets/img/notify.png" alt="Notify-img">    
                                Live Notifications</a>
                                <ul class="dropdown-menu notify-drop">
                                    <i class="fa fa-sort-asc" aria-hidden="true"></i>
                                    <div class="notify-drop-title">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">Notification</div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 text-right" id="all_read">
                                                <a href="javascript:void(0);" class="rIcon allRead mark_notifications_read" data-tooltip="tooltip" data-placement="bottom" title="Mark as read">Mark All as Read</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end notify title -->
                                    <!-- notify content -->
                                    <div class="drop-content" id="header_notifications1">

                                        @include('theme.notification-header-render')
                                    </div>
                                    <div class="notify-drop-footer text-center">
                                        <a href="{{ route('all_notifications') }}" class="allRead ">See All Notifications</a>
                                    </div>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <!-- remove this section to view penoramic system -->