<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Tower extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  	protected $guarded = [];

    public function tower_unit_floor()
    {
        return $this->hasMany('App\TowerUnitFloor','tower_id','id');
    }
    public function tower_apartment_unit()
    {
        return $this->hasMany('App\ApartmentUnit','tower_id','id');
    }

    public function ad_name()
    {
        return $this->hasOne('App\Ad','id','ad_id');

    }
    public function units() {

        return $this->hasMany('App\ApartmentUnit','tower_id','id');
    }
}
