<?php

namespace App\Providers;

use App\Post;
use App\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use DB;
use App\NotificationSent;
use App\User;
use App\UserPackage;
use App\Package;
use App\Payment;
use Carbon\Carbon;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        /**
         * Set dynamic configuration for third party services
         */
        $amazonS3Config = [
            'filesystems.disks.s3' =>
                [
                    'driver' => 's3',
                    'key' => get_option('amazon_key'),
                    'secret' => get_option('amazon_secret'),
                    'region' => get_option('amazon_region'),
                    'bucket' => get_option('bucket'),
                ]
        ];
        $facebookConfig = [
            'services.facebook' =>
                [
                    'client_id'     => get_option('fb_app_id'),
                    'client_secret' => get_option('fb_app_secret'),
                    'redirect'      => url('login/facebook-callback'),
                ]
        ];
        $googleConfig = [
            'services.google' =>
                [
                    'client_id'     => get_option('google_client_id'),
                    'client_secret' => get_option('google_client_secret'),
                    'redirect'      => url('login/google-callback'),
                ]
        ];

        $generalConfig = [
            'app.name' => get_option('site_name')
        ];

        config($amazonS3Config);
        config($facebookConfig);
        config($googleConfig);
        config($generalConfig);

        //dd(config('app.name'));

        view()->composer('*', function($view)
        {
            
            $header_menu_pages = Post::whereStatus(1)->where('show_in_header_menu', 1)->get();

            $show_in_footer_menu = Post::whereStatus(1)->where('show_in_footer_menu', 1)->get();

            $enable_monetize = get_option('enable_monetize');
            $loggedUser = null;
            $header_notifications = '';
            $total_staff_added = 0;
            if(Auth::check()) {

                // $user = User::where('id',Auth::id())->whereUserType('user')->first();
                
                // if(!empty($user)){
                //    // prx($user->user_package);
                //     $today = Carbon::today();
                //     $date = $user->user_package->expiry_date;
                //     $package_interval = $user->user_package->package->period;
                //     Session::forget('plan_information');
                //     if (Carbon::parse($date)->gt(Carbon::now())){
                //         // if $date > now (gt is greater than, gte is greater than or equal, etc)
                //         // not expired 
                //         Session::forget('plan_information');
                //     } else {

                //         // if $date < now (gt is greater than, gte is greater than or equal, etc)
                //         // expired
                //         if($user->charge_payment_method != 'manual' && !empty($user->user_package->payment->stripe_customer_id)) {
                //             \Stripe\Stripe::setApiKey(get_stripe_key('secret'));
                //             $cu = \Stripe\Customer::Retrieve(
                //                 [ 
                //                     'id' => $user->user_package->payment->stripe_customer_id,
                //                 ]
                //             );
                //             if(!empty($cu->subscriptions->data)){
                //                 $current_period_start  = date('Y-m-d',$cu->subscriptions->data[0]->current_period_start);
                //                 $current_period_end  = date('Y-m-d',$cu->subscriptions->data[0]->current_period_end);

                //                 $subscription_id = $cu->subscriptions->data[0]->id;
                //                 $charge_payment_method = $cu->subscriptions->data[0]->billing;
                //                 $payment_created = $cu->subscriptions->data[0]->created;
                //                 $customer_id = $cu->subscriptions->data[0]->customer;
                //                 $plan_id = $cu->subscriptions->data[0]->plan->id;
                //                 $product_id = $cu->subscriptions->data[0]->plan->product;

                //                 $payment = Payment::create([
                //                                     'stripe_start_date' => $current_period_start,
                //                                     'stripe_end_date' => $current_period_end,
                //                                     'stripe_customer_id' => $customer_id,
                //                                     'charge_id_or_token' => $subscription_id,
                //                                     'plan_id'   => $plan_id,
                //                                     'charge_payment_method' => $charge_payment_method,
                //                                     'local_transaction_id' => $user->user_package->payment->local_transaction_id,
                //                                     'user_id' => $user->id,
                //                                     'currency' => 'USD',
                //                                     'status' => 'success',
                //                                     'payment_method' => 'stripe',
                //                                 ]);
                //                 $user_packages = UserPackage::create([
                //                                             'user_id' => $user->id,
                //                                             'packages_id' => $user->user_package->packages_id,
                //                                             'payment_id' => $payment->id,
                //                                             'expiry_date' => $current_period_end
                //                                     ]);
                //                 User::where('id',$user->id)->update([
                //                                 'current_active_plan_id' => $user_packages->id,
                //                                 'charge_payment_method'  => $charge_payment_method,
                //                                 'plan_active_status'  => 1,
                //                                 'stripe_customer_id'        => $customer_id,
                //                             ]);
                //             } else {
                //                     //Session::put('plan_information','Card expired / payment not available in account. for continue please add amount in your account.');
                //                 User::where('id',$user->id)->update([
                //                             'plan_active_status'  => 0,
                //                             'expiry_message'  => 'Please transfer valid amount in your Bank for continue this service.'
                //                         ]);
                //             }
                //         } else {
                //             // equal manual
                //             //Session::put('plan_information','For continue please activate the plan again.');
                //             User::where('id',$user->id)->update([
                //                             'plan_active_status'  => 0,
                //                             'expiry_message'  => 'Please repayment for continue this service.'
                //                         ]);
                //         }
                //     }
                //}
                
                $loggedUser = Auth::user();
                $user_id = $loggedUser->id;

                $token = DB::table('notification_user_tokens')->where('user_id',$user_id)->value('token');
                $notiToken = DB::table('notification_user_tokens')->where('token',$token)->whereIn('user_id',[$user_id,0])->get();
                $notiToken = json_decode(json_encode($notiToken),true);
                $noti_token_ids  = array();
                foreach ($notiToken as $key => $value) {
                    $noti_token_ids[] = $value['id']; 
                }
                
                $noti_sent = NotificationSent::whereIn('notification_user_token_id',$noti_token_ids)->where('read_status',0)->get();
                $noti_sent_ids  = array();
                foreach ($noti_sent as $key => $value) {
                    $noti_sent_ids[] = $value['notification_id']; 
                }

                $header_notifications = Notification::with('user')->whereIn('id',$noti_sent_ids)->orderBy('id','DESC')->paginate(3);

                $view->with(['lUser' => $loggedUser, 'enable_monetize'=>$enable_monetize , 'header_menu_pages' => $header_menu_pages, 'show_in_footer_menu' => $show_in_footer_menu,'notification'=>$header_notifications]);

                // staff members added by developer
                $total_staff_added = User::whereDeveloperId(Auth::user()->id)->whereUserType('staff')->count();
                // staff members added by developer

            } 
            $view->with(['lUser' => $loggedUser, 'enable_monetize'=>$enable_monetize , 'header_menu_pages' => $header_menu_pages, 'show_in_footer_menu' => $show_in_footer_menu, 'total_staff_added' => $total_staff_added]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
