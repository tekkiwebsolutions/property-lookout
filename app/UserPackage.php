<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class UserPackage extends Model
{
    //
    protected $guarded = [];

    public function package()
    {    	
    	return $this->hasOne('App\Package','id','packages_id');
    }
    public function payment()
    {    	
    	return $this->hasOne('App\Payment','id','payment_id');
    }

}
