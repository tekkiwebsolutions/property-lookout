<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class InvitedAgent extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  	protected $guarded = [];

  	public function developer()
  	{
  		return $this->hasOne('App\User','id','developer_id');
  	}

    public function agent()
    {
        return $this->hasOne('App\User','id','agent_id');
    }
    

}
