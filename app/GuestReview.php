<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestReview extends Model
{
    protected $guarded = [];
}
