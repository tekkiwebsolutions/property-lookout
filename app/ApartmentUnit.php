<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ApartmentUnit extends Model
{
    protected $guarded = [];
    
    public function media_img(){
        return $this->hasMany(Media::class)->whereType('image');
    }

    public function apartment(){
    	return $this->hasOne('App\Ad','id','ad_id')->whereType('apartments');
    }

}
