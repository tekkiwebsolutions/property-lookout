<?php
/**
 * @return mixed
 * Custom functions made by themeqx
 */

/**
 * @param string $img (object)
 * @param bool $full_size
 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
 */
use Illuminate\Support\Facades\Storage;

function media_url($img = '', $full_size = false){
    $url_path = '';

    if ($img){
        if ($img->type == 'image'){
            if ($img->storage == 'public'){
                if ($full_size){
                    $url_path = asset('uploads/images/'.$img->media_name);
                }else{
                    $url_path = asset('uploads/images/thumbs/'.$img->media_name);
                }
            }elseif ($img->storage == 's3'){
                if ($full_size){
                    $url_path = \Illuminate\Support\Facades\Storage::disk('s3')->url('uploads/images/'.$img->media_name);
                }else{
                    $url_path = \Illuminate\Support\Facades\Storage::disk('s3')->url('uploads/images/thumbs/'.$img->media_name);
                }

            }
        }
    }else{
        $url_path = asset('uploads/placeholder.png');
    }

    return $url_path;
}

function media_url_property($img = '', $full_size = false){
    $url_path = '';
    //prx($img);
    if ($img){
        if ($img->type == 'image'){
            if($img->ref == 'house') {

                $image_path = 'uploads/houseImages/';
                $image_thumb_path = 'uploads/houseImages/thumbs/';
                
            } else {

                $image_path = 'uploads/apartmentImages/';
                $image_thumb_path = 'uploads/apartmentImages/thumbs/';
            }
            if ($img->storage == 'public'){
                if ($full_size){
                    $url_path = asset($image_path.$img->media_name);
                }else{
                    $url_path = asset($image_thumb_path.$img->media_name);
                }
            }elseif ($img->storage == 's3'){
                if ($full_size){
                    $url_path = \Illuminate\Support\Facades\Storage::disk('s3')->url($image_path.$img->media_name);
                }else{
                    $url_path = \Illuminate\Support\Facades\Storage::disk('s3')->url($image_thumb_path.$img->media_name);
                }

            }
        }
    }else{
        $url_path = asset('uploads/placeholder.png');
    }

    return $url_path;
}

function media_url_property_array($img = '', $full_size = false){
    $url_path = '';
    //prx($img);
    if ($img){
        if ($img['type'] == 'image'){
            if($img['ref'] == 'house') {

                $image_path = 'uploads/houseImages/';
                $image_thumb_path = 'uploads/houseImages/thumbs/';
                
            } else {

                $image_path = 'uploads/apartmentImages/';
                $image_thumb_path = 'uploads/apartmentImages/thumbs/';
            }
            if ($img['storage'] == 'public'){
                if ($full_size){
                    $url_path = asset($image_path.$img->media_name);
                }else{
                    $url_path = asset($image_thumb_path.$img->media_name);
                }
            }elseif ($img['storage'] == 's3'){
                if ($full_size){
                    $url_path = \Illuminate\Support\Facades\Storage::disk('s3')->url($image_path.$img->media_name);
                }else{
                    $url_path = \Illuminate\Support\Facades\Storage::disk('s3')->url($image_thumb_path.$img->media_name);
                }

            }
        }
    }else{
        $url_path = asset('uploads/placeholder.png');
    }

    return $url_path;
}


function slider_url($img = ''){
    $url_path = '';
    if ($img){
        if ($img->storage == 'public'){
            $url_path = asset('uploads/sliders/'.$img->media_name);
        }elseif ($img->storage == 's3'){
            $url_path = \Illuminate\Support\Facades\Storage::disk('s3')->url('uploads/sliders/'.$img->media_name);
        }
    }
    return $url_path;
}

function avatar_img_url($img = '', $source){
    $url_path = '';
    if ($img){
        if ($source == 'public'){
            $url_path = asset('uploads/avatar/'.$img);
        }elseif ($source == 's3'){
            $url_path = \Illuminate\Support\Facades\Storage::disk('s3')->url('uploads/avatar/'.$img);
        }
    }
    return $url_path;
}

/**
 * @return string
 * 
 * @return logo url
 */
function logo_url(){
    $url_path = '';
    $img = get_option('logo');
    $source = get_option('logo_storage');

    $exists = \Illuminate\Support\Facades\Storage::disk($source)->exists('uploads/logo/'.$img);
    if ( ! $exists)
        return false;

    if ($source == 'public'){
        $url_path = asset('uploads/logo/'.$img);
    }elseif ($source == 's3'){
        $url_path = \Illuminate\Support\Facades\Storage::disk('s3')->url('uploads/logo/'.$img);
    }

    return $url_path;
}

/**
 * @return mixed
 */
function current_disk(){
    $current_disk = \Illuminate\Support\Facades\Storage::disk(get_option('default_storage'));
    return $current_disk;
}

/**
 * @param string $option_key
 * @return string
 */
function get_option($option_key = ''){
    $get = \App\Option::where('option_key', $option_key)->first();
    if($get) {
        return $get->option_value;
    }
    return $option_key;
}

/**
 * @param string $text
 * @return mixed
 */
function get_text_tpl($text = ''){
    $tpl = ['[year]', '[copyright_sign]', '[site_name]'];
    $variable = [date('Y'), '&copy;', get_option('site_name')];
    
    $tpl_option = str_replace($tpl,$variable,$text);
    return $tpl_option;
}

/**
 * @param string $title
 * @param $model
 * @return string
 */

function unique_slug($title = '', $model = 'Ad'){
    $slug = str_slug($title);
    // prx($model);
    //get unique slug...
    $nSlug = $slug;
    // prx($nSlug);
    $i = 0;

    $model = str_replace(' ','',"\App\ ".$model);
    // prx($model);
    while( ($model::whereSlug($nSlug)->count()) > 0){
        $i++;
        $nSlug = $slug.'-'.$i;
    }
    // prx($i);
    if($i > 0) {
        $newSlug = substr($nSlug, 0, strlen($slug)) . '-' . $i;
    } else
    {
        $newSlug = $slug;
    }
    // prx($newSlug);
    
    return $newSlug;
}

/**
 * @param string $plan
 * @return int|string
 */
function get_ads_price($plan = 'regular'){
    $price = 0;

    if ($plan == 'regular') {
        if (get_option('ads_price_plan') == 'all_ads_paid') {
            $price = get_option('regular_ads_price');
        }
    }elseif($plan == 'premium'){
        if (get_option('ads_price_plan') != 'all_ads_free') {
            $price = get_option('premium_ads_price');
        }
    }

    return $price;
}

function get_languages(){
    $languages = \App\Language::all();
    return $languages;
}

/**
 * @param string $type
 * @return string
 *
 * @return stripe secret key or test key
 */

function get_stripe_key($type = 'publishable'){
    $stripe_key = '';

    if ($type == 'publishable'){
        if (get_option('stripe_test_mode') == 1){
            $stripe_key = get_option('stripe_test_publishable_key');
        }else{
            $stripe_key = get_option('stripe_live_publishable_key');
        }
    }elseif ($type == 'secret'){
        if (get_option('stripe_test_mode') == 1){
            $stripe_key = get_option('stripe_test_secret_key');
        }else{
            $stripe_key = get_option('sk_live_ojldRoMZ3j14I5pwpfCxidvT');
        }
    }

    return $stripe_key;
}

/**
 * @param int $ad_id
 * @param string $status
 */
function ad_status_change($ad_id = 0, $status = 1){
    if ($ad_id > 0){
        $ad = \App\Ad::find($ad_id);
        
        if ($ad){
            $previous_status = $ad->status;
            //Publish ad
            $ad->status = $status;
            $ad->save();
        }
    }

    return false;
}

/**
 * @return bool
 *
 * return all fa icon list
 */
function fa_icons(){
    $pattern = '/\.(fa-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"\\\\(.+)";\s+}/';
    $subject =  file_get_contents(public_path('assets/font-awesome-4.7.0/css/font-awesome.css'));
    preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);

    foreach($matches as $match) {
        $icons[$match[1]] = $match[2];
    }
    ksort($icons);
    return $icons;
}

function category_classes(){
    $classes = [
        'green'     => 'green',
        'gold'      => 'gold',
        'purple'    => 'purple',
        'orange'    => 'orange',
        'brick'     => 'brick',
        'blue'      => 'blue',
        'honey'     => 'honey',
    ];
    
    return $classes;
}

/**
 * @param int $price
 * @return string
 */
function themeqx_price($price = 0){
    return get_option('currency_sign').' '.$price;
}

/**
 * @param int $price
 * @param int $negotiable
 * @return string
 */

function themeqx_price_ng($ad) {
    
    $price = 0.00;
    if($ad->price) {

        $price = $ad->price;
    } 
    $negotiable = $ad->is_negotiable;
    $purpose = ($ad->purpose == 'rent') ? ' /month':'';

    $ng = $negotiable ? ' ('.trans('app.negotiable').') ' : '';
    $show_price = '';
    if ($price > 0) {
        $show_price = get_option('currency_sign').' '.number_format($price,2);
    }



    return $show_price.$ng.$purpose;
}

function update_option($key, $value){
    $option = \App\Option::firstOrCreate(['option_key' => $key]);
    $option -> option_value = $value;
    return $option->save();
}

function themeqx_distance_measure($meters = 0){
    if ($meters >= 1000){
        return number_format($meters / 1000, 2).' KM';
    }
    return $meters. ' M';
}

function themeqx_classifieds_currencies(){
    return array(
        'AED' => 'United Arab Emirates dirham',
        'AFN' => 'Afghan afghani',
        'ALL' => 'Albanian lek',
        'AMD' => 'Armenian dram',
        'ANG' => 'Netherlands Antillean guilder',
        'AOA' => 'Angolan kwanza',
        'ARS' => 'Argentine peso',
        'AUD' => 'Australian dollar',
        'AWG' => 'Aruban florin',
        'AZN' => 'Azerbaijani manat',
        'BAM' => 'Bosnia and Herzegovina convertible mark',
        'BBD' => 'Barbadian dollar',
        'BDT' => 'Bangladeshi taka',
        'BGN' => 'Bulgarian lev',
        'BHD' => 'Bahraini dinar',
        'BIF' => 'Burundian franc',
        'BMD' => 'Bermudian dollar',
        'BND' => 'Brunei dollar',
        'BOB' => 'Bolivian boliviano',
        'BRL' => 'Brazilian real',
        'BSD' => 'Bahamian dollar',
        'BTC' => 'Bitcoin',
        'BTN' => 'Bhutanese ngultrum',
        'BWP' => 'Botswana pula',
        'BYR' => 'Belarusian ruble',
        'BZD' => 'Belize dollar',
        'CAD' => 'Canadian dollar',
        'CDF' => 'Congolese franc',
        'CHF' => 'Swiss franc',
        'CLP' => 'Chilean peso',
        'CNY' => 'Chinese yuan',
        'COP' => 'Colombian peso',
        'CRC' => 'Costa Rican col&oacute;n',
        'CUC' => 'Cuban convertible peso',
        'CUP' => 'Cuban peso',
        'CVE' => 'Cape Verdean escudo',
        'CZK' => 'Czech koruna',
        'DJF' => 'Djiboutian franc',
        'DKK' => 'Danish krone',
        'DOP' => 'Dominican peso',
        'DZD' => 'Algerian dinar',
        'EGP' => 'Egyptian pound',
        'ERN' => 'Eritrean nakfa',
        'ETB' => 'Ethiopian birr',
        'EUR' => 'Euro',
        'FJD' => 'Fijian dollar',
        'FKP' => 'Falkland Islands pound',
        'GBP' => 'Pound sterling',
        'GEL' => 'Georgian lari',
        'GGP' => 'Guernsey pound',
        'GHS' => 'Ghana cedi',
        'GIP' => 'Gibraltar pound',
        'GMD' => 'Gambian dalasi',
        'GNF' => 'Guinean franc',
        'GTQ' => 'Guatemalan quetzal',
        'GYD' => 'Guyanese dollar',
        'HKD' => 'Hong Kong dollar',
        'HNL' => 'Honduran lempira',
        'HRK' => 'Croatian kuna',
        'HTG' => 'Haitian gourde',
        'HUF' => 'Hungarian forint',
        'IDR' => 'Indonesian rupiah',
        'ILS' => 'Israeli new shekel',
        'IMP' => 'Manx pound',
        'INR' => 'Indian rupee',
        'IQD' => 'Iraqi dinar',
        'IRR' => 'Iranian rial',
        'ISK' => 'Icelandic kr&oacute;na',
        'JEP' => 'Jersey pound',
        'JMD' => 'Jamaican dollar',
        'JOD' => 'Jordanian dinar',
        'JPY' => 'Japanese yen',
        'KES' => 'Kenyan shilling',
        'KGS' => 'Kyrgyzstani som',
        'KHR' => 'Cambodian riel',
        'KMF' => 'Comorian franc',
        'KPW' => 'North Korean won',
        'KRW' => 'South Korean won',
        'KWD' => 'Kuwaiti dinar',
        'KYD' => 'Cayman Islands dollar',
        'KZT' => 'Kazakhstani tenge',
        'LAK' => 'Lao kip',
        'LBP' => 'Lebanese pound',
        'LKR' => 'Sri Lankan rupee',
        'LRD' => 'Liberian dollar',
        'LSL' => 'Lesotho loti',
        'LYD' => 'Libyan dinar',
        'MAD' => 'Moroccan dirham',
        'MDL' => 'Moldovan leu',
        'MGA' => 'Malagasy ariary',
        'MKD' => 'Macedonian denar',
        'MMK' => 'Burmese kyat',
        'MNT' => 'Mongolian t&ouml;gr&ouml;g',
        'MOP' => 'Macanese pataca',
        'MRO' => 'Mauritanian ouguiya',
        'MUR' => 'Mauritian rupee',
        'MVR' => 'Maldivian rufiyaa',
        'MWK' => 'Malawian kwacha',
        'MXN' => 'Mexican peso',
        'MYR' => 'Malaysian ringgit',
        'MZN' => 'Mozambican metical',
        'NAD' => 'Namibian dollar',
        'NGN' => 'Nigerian naira',
        'NIO' => 'Nicaraguan c&oacute;rdoba',
        'NOK' => 'Norwegian krone',
        'NPR' => 'Nepalese rupee',
        'NZD' => 'New Zealand dollar',
        'OMR' => 'Omani rial',
        'PAB' => 'Panamanian balboa',
        'PEN' => 'Peruvian nuevo sol',
        'PGK' => 'Papua New Guinean kina',
        'PHP' => 'Philippine peso',
        'PKR' => 'Pakistani rupee',
        'PLN' => 'Polish z&#x142;oty',
        'PRB' => 'Transnistrian ruble',
        'PYG' => 'Paraguayan guaran&iacute;',
        'QAR' => 'Qatari riyal',
        'RON' => 'Romanian leu',
        'RSD' => 'Serbian dinar',
        'RUB' => 'Russian ruble',
        'RWF' => 'Rwandan franc',
        'SAR' => 'Saudi riyal',
        'SBD' => 'Solomon Islands dollar',
        'SCR' => 'Seychellois rupee',
        'SDG' => 'Sudanese pound',
        'SEK' => 'Swedish krona',
        'SGD' => 'Singapore dollar',
        'SHP' => 'Saint Helena pound',
        'SLL' => 'Sierra Leonean leone',
        'SOS' => 'Somali shilling',
        'SRD' => 'Surinamese dollar',
        'SSP' => 'South Sudanese pound',
        'STD' => 'S&atilde;o Tom&eacute; and Pr&iacute;ncipe dobra',
        'SYP' => 'Syrian pound',
        'SZL' => 'Swazi lilangeni',
        'THB' => 'Thai baht',
        'TJS' => 'Tajikistani somoni',
        'TMT' => 'Turkmenistan manat',
        'TND' => 'Tunisian dinar',
        'TOP' => 'Tongan pa&#x2bb;anga',
        'TRY' => 'Turkish lira',
        'TTD' => 'Trinidad and Tobago dollar',
        'TWD' => 'New Taiwan dollar',
        'TZS' => 'Tanzanian shilling',
        'UAH' => 'Ukrainian hryvnia',
        'UGX' => 'Ugandan shilling',
        'USD' => 'United States dollar',
        'UYU' => 'Uruguayan peso',
        'UZS' => 'Uzbekistani som',
        'VEF' => 'Venezuelan bol&iacute;var',
        'VND' => 'Vietnamese &#x111;&#x1ed3;ng',
        'VUV' => 'Vanuatu vatu',
        'WST' => 'Samoan t&#x101;l&#x101;',
        'XAF' => 'Central African CFA franc',
        'XCD' => 'East Caribbean dollar',
        'XOF' => 'West African CFA franc',
        'XPF' => 'CFP franc',
        'YER' => 'Yemeni rial',
        'ZAR' => 'South African rand',
        'ZMW' => 'Zambian kwacha',
    );
    
}

// functions to print data in array form

if(!function_exists('prx')) {
    function prx($value) {
        $value = json_decode(json_encode($value),true);

        $colors = ['#FFC0CB','#FFF8DC','#DEB887','#FFF5EE','#00FFFF','#FFE4E1'];
        echo '<pre style="background-color:'.$colors[array_rand($colors,1)].';padding-left:5px;margin-left:5px;width:100%;">';
        print_r($value);
        echo "</pre>";
        die;
    }
}

if(!function_exists('prxx')) {
    function prxx($value) {
        $value = json_decode(json_encode($value),true);

        $colors = ['#FFC0CB','#FFF8DC','#DEB887','#FFF5EE','#00FFFF','#FFE4E1'];
        echo '<pre style="background-color:'.$colors[array_rand($colors,1)].';padding-left:5px;margin-left:5px;width:100%;">';
        print_r($value);
        echo "</pre>";
    }
}

// functions to print data in array form

// functions to encrypt or decrypt data

function getDecrypted($value) {

    if(!empty($value)) {
        return \Crypt::decrypt($value);
    }
}

function getEncrypted($value) {

    if(!empty($value)) {
        
        return \Crypt::encrypt($value);
    }
}

// functions to encrypt or decrypt data

// function to get avaerage ratings from all reviews of a property

function get_avrg_ratings($ad_id) {

    $review = \App\GuestReview::whereAdsId($ad_id)->whereApartmentUnitId(0);
    $ad_reviews = $review->count();
    $ad_total_rating_given = $review->sum('ratings');
    $ratings = $ad_reviews != 0 ? round(($ad_total_rating_given/$ad_reviews),2) : 0;
    
    return $ratings;
}

// function to get avaerage ratings from all reviews of a property

// common function to get apartment units for property list page

function get_apartment_common_units($ad_id = null,$tower_id = null,$floors = null,$units = null) {

    // prxx($ad_id); prxx($tower_id); prxx($floors); prxx($units);
    // get total units filled by user in that apartment grouped by floor no
    $apartment_units = \App\ApartmentUnit::whereAdId($ad_id)
                                           ->whereTowerId($tower_id)
                                           ->select('id','slug','apartment_floor_id','apartment_unit_id','property_status')
                                           ->get()
                                           ->groupBy('apartment_floor_id');
    $apartment_units = json_decode(json_encode($apartment_units),true);

    // prxx($apartment_units);

    $html = '';

    if(!empty($apartment_units)) {

        $filled_floors = count($apartment_units);
        foreach($apartment_units as $key => $floor) {
            $html .= '<ul>';
                $filled_units = count($floor);
                foreach($floor as $key1 => $unit) {

                    if($unit['property_status'] == 0) {   
                        $html .= '<li class="green unitSoldbtn" data-placement="right" data-toggle="confirmation" rel="'.getEncrypted($unit['id']).'" rel1="sold">&nbsp;</li>';
                    } elseif($unit['property_status'] == 1) {

                        // $html .= '<li class="red unitSoldbtn" data-placement="right" data-toggle="confirmation" rel="'.getEncrypted($unit['id']).'" rel1="sold">&nbsp;</li>';
                        $html .= '<li class="red">&nbsp;</li>';
                    } else {

                        $html .= '<li>&nbsp;</li>';
                    }
                }
                // show unavailable units if user has not saved full units
                $left_units = (int)$units - (int)$filled_units;
                for($k = 1; $k <= $left_units; $k++) {
                    $html .= '<li>&nbsp;</li>';
                }
            $html .= '</ul>';
        }
        // show unavailable floors if user has not filled full floors and units
        $left_floors = (int)$floors - (int)$filled_floors;
        for($i = 1; $i <= $left_floors; $i++) {
            $html .= '<ul>';
                for($j = 1; $j <= $units; $j++) {
                    $html .= '<li>&nbsp;</li>';
                }
            $html .= '</ul>';
        }
        return $html;
    } else {

        // "show non available units here for added tower (if tower details are added but no unit is added in that tower yet)"

        for($i = 1; $i <= $floors; $i++) {
            $html .= '<ul>';
            for($j = 1; $j <= $units; $j++) {
                $html .= '<li>&nbsp;</li>';
            }
            $html .= '</ul>';
        }
        return $html;
    }
}

function get_apartment_common_avail_units($ad_id = null,$tower_id = null,$floors = null,$units = null) {

    // get total units filled by user in that apartment grouped by floor no
    $apartment_units = \App\ApartmentUnit::whereAdId($ad_id)
                                           ->whereTowerId($tower_id)
                                           ->select('id','slug','apartment_floor_id','apartment_unit_id','property_status')
                                           ->get()
                                           ->groupBy('apartment_floor_id');
    $apartment_units = json_decode(json_encode($apartment_units),true);

    $html = '';

    if(!empty($apartment_units)) {

        $filled_floors = count($apartment_units);
        foreach($apartment_units as $key => $floor) {
            $html .= '<ul>';
                $filled_units = count($floor);
                foreach($floor as $key1 => $unit) {

                    if($unit['property_status'] == 0) {   
                        $html .= '<li class="green">&nbsp;</li>';
                    } elseif($unit['property_status'] == 1) {

                        $html .= '<li class="red unitAvailbtn" data-placement="right" data-toggle="confirmation" rel="'.getEncrypted($unit['id']).'" rel1="available">&nbsp;</li>';
                    } else {

                        $html .= '<li>&nbsp;</li>';
                    }
                }
                // show unavailable units if user has not saved full units
                $left_units = (int)$units - (int)$filled_units;
                for($k = 1; $k <= $left_units; $k++) {
                    $html .= '<li>&nbsp;</li>';
                }
            $html .= '</ul>';
        }
        // show unavailable floors if user has not filled full floors and units
        $left_floors = (int)$floors - (int)$filled_floors;
        for($i = 1; $i <= $left_floors; $i++) {
            $html .= '<ul>';
                for($j = 1; $j <= $units; $j++) {
                    $html .= '<li>&nbsp;</li>';
                }
            $html .= '</ul>';
        }
        return $html;
    }
}

// old functions for one tower only (changes made in same function above for multiple towers)

// function get_apartment_common_units($ad_id,$floors,$units) {

//     // get total units filled by user in that apartment grouped by floor no
//     $apartment_units = \App\ApartmentUnit::whereAdId($ad_id)->select('id','slug','apartment_floor_id','apartment_unit_id','property_status')->get()->groupBy('apartment_floor_id');
//     $apartment_units = json_decode(json_encode($apartment_units),true);

//     $html = '';

//     if(!empty($apartment_units)) {

//         $filled_floors = count($apartment_units);
//         foreach($apartment_units as $key => $floor) {
//             $html .= '<ul>';
//                 $filled_units = count($floor);
//                 foreach($floor as $key1 => $unit) {

//                     if($unit['property_status'] == 0) {   
//                         $html .= '<li class="green unitSoldbtn" data-placement="right" data-toggle="confirmation" rel="'.getEncrypted($unit['id']).'" rel1="sold">&nbsp;</li>';
//                     } elseif($unit['property_status'] == 1) {

//                         $html .= '<li class="red unitSoldbtn" data-placement="right" data-toggle="confirmation" rel="'.getEncrypted($unit['id']).'" rel1="sold">&nbsp;</li>';
//                     } else {

//                         $html .= '<li>&nbsp;</li>';
//                     }
//                 }
//                 // show unavailable units if user has not saved full units
//                 $left_units = (int)$units - (int)$filled_units;
//                 for($k = 1; $k <= $left_units; $k++) {
//                     $html .= '<li>&nbsp;</li>';
//                 }
//             $html .= '</ul>';
//         }
//         // show unavailable floors if user has not filled full floors and units
//         $left_floors = (int)$floors - (int)$filled_floors;
//         for($i = 1; $i <= $left_floors; $i++) {
//             $html .= '<ul>';
//                 for($j = 1; $j <= $units; $j++) {
//                     $html .= '<li>&nbsp;</li>';
//                 }
//             $html .= '</ul>';
//         }
//         return $html;
//     }
// }

// function get_apartment_common_avail_units($ad_id,$floors,$units) {

//     // get total units filled by user in that apartment grouped by floor no
//     $apartment_units = \App\ApartmentUnit::whereAdId($ad_id)->select('id','slug','apartment_floor_id','apartment_unit_id','property_status')->get()->groupBy('apartment_floor_id');
//     $apartment_units = json_decode(json_encode($apartment_units),true);

//     $html = '';

//     if(!empty($apartment_units)) {

//         $filled_floors = count($apartment_units);
//         foreach($apartment_units as $key => $floor) {
//             $html .= '<ul>';
//                 $filled_units = count($floor);
//                 foreach($floor as $key1 => $unit) {

//                     if($unit['property_status'] == 0) {   
//                         $html .= '<li class="green unitAvailbtn" data-placement="right" data-toggle="confirmation" rel="'.getEncrypted($unit['id']).'" rel1="available">&nbsp;</li>';
//                     } elseif($unit['property_status'] == 1) {

//                         $html .= '<li class="red unitAvailbtn" data-placement="right" data-toggle="confirmation" rel="'.getEncrypted($unit['id']).'" rel1="available">&nbsp;</li>';
//                     } else {

//                         $html .= '<li>&nbsp;</li>';
//                     }
//                 }
//                 // show unavailable units if user has not saved full units
//                 $left_units = (int)$units - (int)$filled_units;
//                 for($k = 1; $k <= $left_units; $k++) {
//                     $html .= '<li>&nbsp;</li>';
//                 }
//             $html .= '</ul>';
//         }
//         // show unavailable floors if user has not filled full floors and units
//         $left_floors = (int)$floors - (int)$filled_floors;
//         for($i = 1; $i <= $left_floors; $i++) {
//             $html .= '<ul>';
//                 for($j = 1; $j <= $units; $j++) {
//                     $html .= '<li>&nbsp;</li>';
//                 }
//             $html .= '</ul>';
//         }
//         return $html;
//     }
// }

// common function to get apartment units for property list page

// function to set package permissions

function packPermissions($user_id) {

    $plan_not_active = 0;
    $plan_expired = 0;
    $apartments_limit_reached = 0;
    $house_limit_reached = 0;
    $blog_limit_reached = 0;

    $current_package = \App\User::where('id',$user_id)->with('user_package.package')->first();
    $current_package = json_decode(json_encode($current_package),true);
    //prx($current_package);

    // no of apartments allowed as in package
    $apartment_permit = $current_package['user_package']['package']['apartments'];

    // no of houses allowed as in package
    $house_permit = $current_package['user_package']['package']['properties'];

    // no of blogs allowed as in package
    $blog_permit = $current_package['user_package']['package']['blog'];

    // expiry date of user package
    $expiry_date = $current_package['user_package']['expiry_date'];
    
    // package created date(to know the number of properties added after purchasing package)
    $package_created_date = $current_package['user_package']['created_at'];

    // no of apartments added between the package limit time
    $user_apartments = \App\Ad::whereUserId($user_id)->whereType('apartments')->whereBetween('created_at',[$package_created_date,$expiry_date])->count();
    // prx($user_apartments);

    // no of properties(houses) added between the package limit time
    $user_houses = \App\Ad::whereUserId($user_id)->whereType('house')->whereBetween('created_at',[$package_created_date,$expiry_date])->count();

    // no. of blogs added by user between the package time limit
    $user_blogs = \App\Post::whereUserId($user_id)->whereType('post')->count();
    // check permissions
    if($current_package['current_active_plan_id'] > 0) {
        if(date('Y-m-d') > date('Y-m-d',strtotime($expiry_date))) {

            $plan_expired = 1;

        } else {

            if($user_apartments >= $apartment_permit) {

                $apartments_limit_reached = 1;
            }

            if($user_houses >= $house_permit) {

                $house_limit_reached = 1;
            }

            if($user_blogs >= 1) {

                $blog_limit_reached = 1;
            }
        }
    } else {

        $plan_not_active = 1;
    }

    
    // prx($house_limit_reached);
    return ['plan_not_active' => $plan_not_active, 'plan_expired' => $plan_expired, 'apartments_limit_reached' => $apartments_limit_reached, 'house_limit_reached' => $house_limit_reached, 'blog_limit_reached' => $blog_limit_reached];
}

function videoLimit($user_id,$video_id) {

    $video_limit_extended = 0;

    $current_package = \App\User::where('id',$user_id)->with('user_package.package')->first();
    $current_package = json_decode(json_encode($current_package),true);

    // video limit allowed to the user
    $video_limit = $current_package['user_package']['package']['video_limit'];

    // video limit added by user
    $user_video_limit = $video_id; // add the code of finding the limit of video

    if($user_video_limit > $video_limit) {

        $video_limit_extended = 1;
    }

    return $video_limit_extended;
}

// function to set package permissions

// delete attached media

function deleteUnlinkedMedia($user_id,$ref,$uploaded_for) {

    if($ref == 'units') {
        $unlinked_media = \App\Media::whereUserId($user_id)->whereRef($ref)->whereUploadedFor($uploaded_for)->whereApartmentUnitId('0')->get();
    } else {
        $unlinked_media = \App\Media::whereUserId($user_id)->whereRef($ref)->whereUploadedFor($uploaded_for)->whereAdId('0')->get();
    }
    if(!empty($unlinked_media)) {
        foreach($unlinked_media as $key => $value) {
            $media = \App\Media::find($value->id);
            $storage = Storage::disk($media->storage);
    
            if($media->ref == 'house') {

                $image_path = 'uploads/houseImages/';
                $image_thumb_path = 'uploads/houseImages/thumbs/';

            } else {

                // for apartments images and units images

                $image_path = 'uploads/apartmentImages/';
                $image_thumb_path = 'uploads/apartmentImages/thumbs/';
            }

            if ($storage->has($image_path.$media->media_name)){
                $storage->delete($image_path.$media->media_name);
            }
            if ($media->type == 'image'){
                if ($storage->has($image_thumb_path.$media->media_name)){
                    $storage->delete($image_thumb_path.$media->media_name);
                }
            }
            $media->delete();
        }
    }
    
    return true;
}

// delete attached media

// common function to get number of units, no of floors and no of towers of apartment
function showUnitsFloorsNumber($ad_id)
{
    $apartment = \App\Ad::whereId($ad_id)->select('id','user_id','title','no_of_tower')->first();
   
    $units = \App\Tower::whereAdId($ad_id)->select(DB::raw('sum(no_of_floor*no_of_unit) as total_units'))->first();
    $numbers_array = [

        'towers' => $apartment['no_of_tower'],
        'floors' => \App\Tower::whereAdId($ad_id)->sum('no_of_floor'),
        'units'  => $units['total_units'],
    ];
    return $numbers_array;
}

function getStaffMemberOrDeveloper($ad_id,$type)
{
    $apartment = $type == 'apartment' ? \App\Ad::whereId($ad_id)->with('user')->select('id','user_id','title','no_of_tower')->first() : \App\AprtmentUnit::whereId($ad_id)->with('apartment.user')->select('id','user_id','title','ad_id','tower_id')->first();

    // check if staff member is assigned to the property, then show his details else show developer's details
    $staff = \App\StaffAd::whereAdId($ad_id)->with('staff')->first();
    if(!empty($staff)) {
        $agent = $staff['staff'];
    } else {
        $agent = $type == 'apartment' ? $apartment['user'] : $apartment['apartment']['user'];
    }
    // $agent = $staff ? $staff['staff'] : $type == 'apartment' ? $apartment['user'] : $apartment['apartment']['user'];
    return $agent;
}
// common function to get number of units, no of floors and no of towers of apartment
