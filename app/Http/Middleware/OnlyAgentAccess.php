<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class OnlyAgentAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(! Auth::check()){
            return redirect()->guest(route('login'))->with('error','You are not authorised to access this page.');
        }

        $user = Auth::user();
        if(! $user->is_agent()) {
            return redirect('/')->with('error','You are not authorised to access this page.');
        }

        return $next($request);
    }
}
