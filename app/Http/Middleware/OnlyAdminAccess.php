<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class OnlyAdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // redirect end user to login page if no user is login
        if ( ! Auth::check()){
            return redirect()->guest(route('login'))->with('error', trans('app.unauthorized_access'));
        }

        // check if login user is admin or not and only let admin access the pages
        $user = Auth::user();

        if ( ! $user->is_admin()){
            
            return redirect('/')->with('error', trans('app.access_restricted'));
        }

        return $next($request);
    }
}
