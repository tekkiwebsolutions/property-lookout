<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Intervention\Image\Facades\Image;

use App\Ad;
use App\City;
use App\Post;
use App\User;
use App\Media;
use App\State;
use App\Country;
use App\Package;
use App\UserMeta;
use App\UserPackage;

class BlogController extends Controller
{
    // user blogs add and listing and details page

    public function blogListing() 
    {
        $title = 'Blogs';
        $posts = Post::with('feature_img')->whereType('post')->orderBy('id','DESC')->get();
        return view('blogs.blogs', compact('title','posts'));
    }

    public function create()   // to open create page
    { 
        $title = 'Create New Blog';
        if(Auth::check()) {

            $ads_images = Media::whereUserId(Auth::user()->id)->wherePostId('0')->whereRef('blog')->get();
            return view('blogs.create',compact('title','ads_images'));
        } else {
            return redirect('/blog')->with('error','You are not authenticated to access this page.');
        }
    }

    public function store(Request $request)   // to store new blog in database
    { 
        $user_id = Auth::user()->id;
        $user = Auth::user();
        $rules = [
            'title'     => 'required',
            'post_content'   => 'required',
        ];
        $this->validate($request, $rules);

        $slug = unique_slug($request->title, 'Post');
        $data = [
            'user_id'               => $user->id,
            'title'                 => ucfirst($request->title),
            'slug'                  => $slug,
            'post_content'          => $request->post_content,
            'type'                  => 'post',
            'status'                => '1',
        ];

        $post_created = Post::create($data);

        if ($post_created){ // attach media images with the blog
            Media::whereUserId($user_id)->wherePostId('0')->whereRef('blog')->update(['post_id'=>$post_created->id]);

            // return to blogs listing page
            return redirect('/blog')->with('success', "Blog has been created successfully.");
        }
        return redirect()->back()->with('error', trans('app.error_msg'));
    }

    public function delete(Request $request)    // to delete a blog of user
    { 
        if(!empty($request->id)) {

            $delete_post = Post::whereId($request->id)->whereType('post')->delete();
            if($delete_post) {

                $image_name = Media::wherePostId($request->id)->whereUserId(Auth::user()->id)->whereRef('blog')->value('media_name');
                $image_path = "/uploads/images/".$image_name;  // Value is not URL but directory file path
                $image_path_thumb = "/uploads/images/thumbs/".$image_name;

                // unlink images from the folder in media table for that post
                if(\File::exists($image_path)) {
                    \File::delete($image_path);
                }

                // unlink images from the thumbs folder in media table for that post
                if(\File::exists($image_path_thumb)) {
                    \File::delete($image_path_thumb);
                }

                // delete related image
                Media::wherePostId($request->id)->whereUserId(Auth::user()->id)->delete();
            }
            return ['success' => 1];
        } else {
            return ['success' => 0];
        }
    }


    public function edit($blog_id)    
    {  
        $title = 'Edit Blog';

        $blog_id = getDecrypted($blog_id);

        if(Auth::check()) {

            $post = Post::with('feature_img')->whereId($blog_id)->whereUserId(Auth::user()->id)->whereType('post')->first();
            return view('blogs.edit',compact('title','post'));
        } else {
            return redirect('/blog')->with('error','You are not authenticated to access this page.');
        }
    }

    public function update(Request $request)    // to update existing blog
    { 
        $user_id = Auth::user()->id;
        $user = Auth::user();
        $rules = [
            'title'     => 'required',
            'post_content'   => 'required',
        ];
        $this->validate($request, $rules);

        $slug = unique_slug($request->title, 'Post');
        $data = [
            'user_id'               => $user->id,
            'title'                 => ucfirst($request->title),
            'slug'                  => $slug,
            'post_content'          => $request->post_content,
            'type'                  => 'post',
            'status'                => '1',
        ];

        $post_created = Post::whereId($request->post_id)->whereUserId($user_id)->whereType('post')->update($data);

        if ($post_created){
            Media::whereUserId($user_id)->wherePostId('0')->whereRef('blog')->update(['post_id'=>$request->post_id]);

            // return to the blogs listing page
            return redirect('/blog')->with('success', "Blog has been updated successfully.");
        }
        return redirect()->back()->with('error', trans('app.error_msg'));
    }

    // blog listing and single blog page is in postcontroller (blogIndex and blogSingle functions)
    public function view($slug) 
    {
        $title = "View Blog";
        $post = Post::with(['feature_img','author'=>function($query) {
                            $query->select('id','name','first_name','last_name','user_name');
                      }])->whereSlug($slug)
                      ->whereUserId(Auth::user()->id)
                      ->whereType('post')
                      ->first();

        $post = json_decode(json_encode($post),true);
        //prx($post);
        $agents = User::whereUserType('user')->whereActiveStatus('1')->orderBy('id','desc')->limit(4)->get()->toArray();
        return view('blogs.view',compact('title','post','agents'));
    }
}
