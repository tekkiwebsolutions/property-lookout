<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Intervention\Image\Facades\Image;
use Google\Cloud\Storage\StorageClient;

class TestStorageController extends Controller
{
    public function test(Request $request)
    {
    	return view('common.testStorageImage');
    }

    public function testPost(Request $request)
    {
    	$storage = new StorageClient([
		    'keyFile' => json_decode(file_get_contents(__DIR__ . '/file/Property Lookout-8082428a0192.json'), true)
		]);
		$storage = new StorageClient([
		    'keyFilePath' =>  __DIR__ . '/file/Property Lookout-8082428a0192.json'
		]);

		$storage = new StorageClient([
		    'projectId' => 'property-lookout-223710'
		]);

		$storage = new StorageClient();

		$bucket = $storage->bucket('property-lookout-223710.appspot.com');
		
		// Upload a file to the bucket.
		$bucket->upload(
		    fopen( __DIR__ . '/data/test.jpg', 'r')
		);

		// Using Predefined ACLs to manage object permissions, you may
		// upload a file and give read access to anyone with the URL.
		$bucket->upload(
		    fopen( __DIR__ . '/data/test.jpg', 'r'),
		    [
		        'predefinedAcl' => 'publicRead'
		    ]
		);

		// Download and store an object from the bucket locally.
		$object = $bucket->object( __DIR__ . '/data/test_back.jpg');
		$object->downloadToFile(__DIR__ . '/data/test_back.jpg');

		$storage = new StorageClient();
		$storage->registerStreamWrapper();

		$contents = file_get_contents('gs://property-lookout-223710.appspot.com/test_back.jpg');

    	echo '<pre>';
    	print_r($storage);
    	die;
    }


    public function testPost1(Request $request)
    {
    	// $bucketName = 'property-lookout-223710.appspot.com'; 
    	// $objectName = 'test.jpg';
    	// $source = __DIR__ . '/data/test.jpg';
	    // $storage = new StorageClient();
	    // $file = fopen($source, 'r');
	    // $bucket = $storage->bucket($bucketName);
	    // $object = $bucket->upload($file, [
	    //     'name' => $objectName
	    // ]);
	    // printf('Uploaded %s to gs://%s/%s' . PHP_EOL, basename($source), $bucketName, $objectName);
	    // die;
	    // $config = [
     //    'keyFilePath' => __DIR__ . '/file/Property Lookout-8082428a0192.json',
     //    'projectId' => 'property-lookout-223710',
	    // ];
	    // $storage = new StorageClient($config);

	    // # Make an authenticated API request (listing storage buckets)
	    // foreach ($storage->buckets() as $bucket) {
	    // 	echo '<pre>';
	    // 	print_r($bucket);
	    //     printf('Bucket: %s' . PHP_EOL, $bucket->name());
	    // }
    }
}
