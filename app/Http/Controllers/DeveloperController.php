<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Http\Requests;
use App\Mail\ResetPasswordLink;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use Yajra\Datatables\Datatables;
use Intervention\Image\Facades\Image;

use App\Ad;
use App\City;
use App\User;
use App\Post;
use App\State;
use App\Country;
use App\Package;
use App\Payment;
use App\StaffAd;
use App\Favorite;
use App\UserPackage;
use App\InvitedAgent;
use App\Notification;
use App\AgentActivity;
use App\UsersChatTable;
use App\DeveloperContact;

class DeveloperController extends Controller
{
    public function inviteAgents()    // developer invite agent page
    {                         
        $title = 'Invite Agents';
        
        $meta_data = $this->meta_data(Auth::user()->id);
        $current_package = $this->current_package(Auth::user()->id);
        $invited_agents = InvitedAgent::where('developer_id',Auth::user()->id)->pluck('agent_id')->toArray();
        $agents = User::whereActiveStatus('1')->whereUserType('user')->limit(10)->orderBy('id','DESC')->get();
        return view('developer.invite_agents',compact('title','meta_data','current_package','agents','invited_agents'));
    }

    public function sendAgentInvitation(Request $request)    // send invitation request to agent by clicking request button
    {               
        if(!empty($request->agent_id)) {
            if(Auth::check()) {

                $data = [];
                $data['agent_id'] = $request->agent_id;
                $data['developer_id'] = Auth::user()->id;
                InvitedAgent::create($data);

                // add notification regarding new request to agent here

                $notify = [

                        'user_id'       =>  Auth::user()->id,
                        'to_user_id'    =>  $request->agent_id,
                        'type'          =>  'developer_request',
                        'type_text'     =>  ' has sent you invitation to sell his apartments ',
                        'message'       =>  ucfirst(Auth::user()->name).' has sent you invitation to sell his apartments '
                ];

                Notification::create($notify);

                // add notification regarding new request to agent here
                return array('success' => '1', 'msg' => 'Request is sent to agent.');
            } else {
                return array('success' => 0);
            }
        } else {
            return array('success' => 0);
        }
    }

    public function searchInviteAgent(Request $request,$value)    // search agent name and change div
    {              
        $invited_agents = InvitedAgent::where('developer_id',Auth::user()->id)->pluck('agent_id')->toarray();
        if($value != 'no_value') {

            $agents = User::whereActiveStatus('1')->whereUserType('user')->where(function($query) use($value) {
                                $query->where('name','LIKE','%'.$value.'%')
                                      ->orWhere('first_name','LIKE','%'.$value.'%')
                                      ->orWhere('middle_name','LIKE','%'.$value.'%')
                                      ->orWhere('last_name','LIKE','%'.$value.'%')
                                      ->orWhere('user_name','LIKE','%'.$value.'%')
                                      ->orWhere('slug','LIKE','%'.$value.'%')
                                      ->orWhere('email','LIKE','%'.$value.'%')
                                      ->orWhere('company','LIKE','%'.$value.'%')
                                      ->orWhere('address','LIKE','%'.$value.'%')
                                      ->orWhere('agent_number','LIKE','%'.$value.'%');

                            })->orderBy('id','DESC')
                            ->get();

        } else {

            $agents = User::whereActiveStatus('1')->whereUserType('user')->limit(10)->orderBy('id','DESC')->get();
        }
        
        return view('developer.search_invite_agents',compact('invited_agents','agents'));
    }

    public function invitedAgents(Request $request)    // view invited agents who have accepted request
    {           
        $title = 'Invited Agents';
        $meta_data = $this->meta_data(Auth::user()->id);
        $current_package = $this->current_package(Auth::user()->id);
        $invited_agents = InvitedAgent::whereHas('agent',function($query) {
                                            $query->whereActiveStatus('1');
                                        })
                                        ->with(['agent' => function($query) {
                                            $query->select('id','name','slug','address','active_status','photo');
                                        }])
                                        ->whereDeveloperId(Auth::user()->id)
                                        ->whereAcceptStatus(1)
                                        ->orderBy('id','DESC')
                                        ->get();

        return view('developer.invited_agents',compact('title','meta_data','current_package','invited_agents'));
    }

    public function deleteInvitedAgent(Request $request)    // delete agent request function
    {                 
        if(!empty($request->request_id)) {

            $delete = InvitedAgent::whereId($request->request_id)->delete();
            if($delete) {
                return array('success' => 1,'msg' => 'Agent deleted successfully.');
            } else {
                return array('success' => 0);
            }
        } else {
            return array('success' => 0);
        }
    }

    public function assignProperty($ad_slug)    // assign property to agent (currently only one property is assigned to one agent)
    {    
        $title = 'Assign Property';
        $apartment = Ad::whereSlug($ad_slug)->whereStatus('1')->first();

        $invited_agents = InvitedAgent::whereHas('agent',function($query) {
                                            $query->whereActiveStatus('1');
                                        })
                                        ->with(['agent' => function($query) {
                                            $query->select('id','name','slug','address','active_status','photo');
                                        }])
                                        ->whereDeveloperId(Auth::user()->id)
                                        ->whereAcceptStatus(1)
                                        ->orderBy('id','DESC')
                                        ->get();

        return view('developer.assign_property',compact('title','apartment','invited_agents'));
        
    }

    public function assignAgentProperty(Request $request)    // ajax call to assign property to agent
    {    
        if(!empty($request->agent_id) && !empty($request->apartment_id)) {

            $assign_agent = Ad::whereId($request->apartment_id)->update(['assigned_agent_id' => $request->agent_id]);
            if($assign_agent) {
                // add notification regarding property assigned to agent here
                
                $apartment_name = Ad::whereId($request->apartment_id)->value('title');
                $notify = [

                        'user_id'       =>  Auth::user()->id,
                        'to_user_id'    =>  $request->agent_id,
                        'ad_id'         =>  $request->apartment_id,
                        'type'          =>  'assign_property',
                        'type_text'     =>  ' has assigned you a new apartment ',
                        'message'       =>  ucfirst(Auth::user()->name).' has assigned you a new apartment '.ucfirst($apartment_name)
                ];

                Notification::create($notify);

                // add notification regarding property assigned to agent here
                return array('success' => 1, 'msg' => 'Agent has been assigned to this property');
            } else {
                return array('success' => 0);
            }
        } else {
            return array('success' => 0);
        }
    }

    public function agentDetails($slug) 
    {
        $agent = User::whereSlug($slug)->withCount(['property','agent_apartment'])->with(['user_meta','country'])->first()->toArray();
        // add staff apartments number here
        if($agent['user_type'] == 'staff') {
            $agent['agent_apartment_count'] = StaffAd::whereStaffId($agent['id'])->count();
        }
        // add staff apartments number here
        
        $meta_data = array();
        if(!empty($agent['user_meta'])){
            foreach ($agent['user_meta'] as $key => $value) {   
                $meta_data[$value['meta_key']] = $value['meta_value'];
            }
        }

        if($agent['user_type'] == 'user') {
            $title = 'Agent Details';
        } else {
            $title = 'Staff Details';
        }

        if($agent['user_type'] == 'user') {
            $assigned_properties = Ad::whereUserId(Auth::user()->id)->whereAssignedAgentId($agent['id'])->get()->toArray();
        } else {
            $ad_ids = StaffAd::whereStaffId($agent['id'])->pluck('ad_id');
            $assigned_properties = Ad::whereIn('id',$ad_ids)->get()->toArray();
        }

        // last activities of staff or agent
        $last_activities = AgentActivity::whereUserId($agent['id'])->get()->toArray();
        // last activities of staff or agent

        // chat history of that staff or agent
        $agent_chats = UsersChatTable::whereAgentId($agent['id'])->with('user_detail')->groupBy('user_id')->get()->toArray();
        // chat history of that staff or agent

        return view('developer.agent_details',compact('title','agent','assigned_properties','meta_data','last_activities','agent_chats'));
    }

    public function chatHistory($agent_id,$user_id) 
    {
        $title = 'Chat History';
        $agent_id = decrypt($agent_id);
        $user_id = decrypt($user_id);
        $chat_messages = UsersChatTable::where(['user_id'=>$user_id,'agent_id'=>$agent_id])->orderBy('id','asc')->get()->toArray();
        $agent = User::whereId($agent_id)->first()->toArray();
        $user = User::whereId($user_id)->first()->toArray();
        $window_type = 'agentWindow';

        return view('developer.chat_history',compact('title','agent_id','user_id','chat_messages','agent','user','window_type'));
    }

    // function to update agent's online status
    public function checkAgentStatus(Request $request) 
    {
        if(!empty($request->agent_id)) {
            $online_status = '';
            $online_time = User::whereId($request->agent_id)->value('last_activity_expires');
            if(!empty($online_time)) {
                if(Carbon::parse($online_time) > Carbon::now()->setTimezone(get_option('default_timezone'))) {
                    $online_status = 'Online';
                } else {
                    $online_status = Carbon::parse($online_time)->diffForHumans();
                }
            } else {
                $online_status = 'N/A';
            }
            return array('success' => 1, 'status' => $online_status);
        } else {
            return array('success' => 0);
        }
    }

    public function addStaffNumber() {

        $title = 'Add Staff';
        return view('developer.add_staff',compact('title'));
    }

    public function staffNumberAdd(Request $request) {

        if(Auth::check()) {
            $rules = [
                'staff_description'     => 'required|max:255',
                'no_of_staff'           => 'required|numeric',
            ];

            $this->validate($request, $rules);

            $user_info = User::whereId(Auth::user()->id)->update(['staff_description' => $request->staff_description, 'no_of_staff' => $request->no_of_staff]);
            if($user_info) {
                return redirect('developers/properties-list')->with('success','Staff number added successfully.');
            } else {
                return redirect('developers/add-staff-number')->with('error','Some error occured. Please try again.');
            }
        } else {
            Auth::logout();
            return redirect('/login');
        }
    }

    public function addStaff() {

        $title = 'Add Staff Members';
        return view('developer.add_staff_member',compact('title'));
    }

    public function addStaffMember(Request $request) {

        $rules = [
            'user_name'             => 'required|max:255|unique:users,user_name',
            'email'                 => 'required|max:255|email|unique:users,email',
            'name'                  => 'required|max:255',
            'password'              => 'required|min:3|max:255|confirmed',
            'password_confirmation' => 'required|min:3|max:255'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        unset($data['_token']); unset($data['password_confirmation']);
        $data['user_type'] = 'staff';
        $data['developer_id'] = Auth::user()->id;
        $slug = unique_slug($request->name, 'User');
        $data['slug'] = $slug;
        $data['active_status'] = '1';
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        
        if($user) {
            // send mail to registered email here
            $msg = '';
            $email = $request->email;
            $login_link = url('/login');
            try{
                Mail::send('emails.add_staff_member', ['developer' => Auth::user()->name,'name' => $request->name,'email' => $request->email,'password'=> $request->password,'login_link' => $login_link], function ($m) use ($email) {
                    $m->from(get_option('email_address'), get_option('site_name'));
                    $m->to($email)->subject('Login details');
                });
            }catch (\Exception $exception){
                $msg = $exception->getMessage();
                return back()->with('error', $msg);
            }
            // send mail to registered email here

            $total_staff_added = User::whereDeveloperId(Auth::user()->id)->whereUserType('staff')->count();

            if($total_staff_added <= Auth::user()->no_of_staff) {
                return redirect('/developers/add-staff')->with('success','Staff member added successfully.');
            } else {
                return redirect('/developers/properties-list')->with('success','Staff member added successfully.');
            } 
        } else {
            return redirect::back()->with('error','Something went wrong. Please try again.');
        }
    }

    // public function checkExistUserName(Request $request) {

    //     if($request->type == 'user_name') {
    //         $user = User::whereUserName($request->user_name)->first();
    //     } else {
    //         $user = User::whereEmail($request->email)->first();
    //     }
    //     if(!empty($user)) {
    //         return array('success' => '0');
    //     } else {
    //         return array('success' => '1');
    //     }
    // }

    public function assignStaffProperty($ad_slug) {

        $title = 'Assign Staff Member';
        $apartment = Ad::whereSlug($ad_slug)->whereStatus('1')->first();

        $staff_members = User::whereUserType('staff')->whereDeveloperId(Auth::user()->id)->orderBy('id','DESC')->get()->toArray();

        $already_assigned_staff = [];
        $already_assigned_staff = StaffAd::whereAdId($apartment['id'])->pluck('staff_id')->toArray();
        return view('developer.assign_property_staff',compact('title','apartment','staff_members','already_assigned_staff'));

    }

    public function staffAssignProperty(Request $request) {

        $data = $request->all();
        if(!empty($data)) {
            
            $data['ad_id'] = $data['apartment_id'];
            $data['developer_id'] = Auth::user()->id;
            
            unset($data['_token']); unset($data['apartment_id']);
            
            $assign = StaffAd::create($data);
            if($assign) {
                return array('success' => 1);
            } else {
                return array('success' => 0);
            }           
        }
    }

    public function addedStaffMembers() {

        $title = 'Added Staff Members';

        $added_members = User::whereDeveloperId(Auth::user()->id)->get()->toArray();

        return view('developer.added_staff',compact('title','added_members'));
    }

    public function allStaffMembers($ad_id) {

        $title = 'All Staff Members';
        $ad_id = decrypt($ad_id);
        $all_staff = StaffAd::with('staff')->whereAdId($ad_id)->get()->toArray();

        return view('developer.all_staff_members',compact('ad_id','all_staff'));
    }

    public function deleteStaffMembers(Request $request) {       // developer can delete a staff member added by him

        if(!empty($request->staff_id)) {
            $staff = User::whereId($request->staff_id)->first();
            if($staff) {
                
                // delete user's profile picture first
                $staff_photo_path = 'uploads/avatar/'.$staff['photo'];
                $storage = Storage::disk($staff['photo_storage']);
                if ($storage->has($staff_photo_path)){
                    $storage->delete($staff_photo_path);
                }

                // remove all assigned propeties to the staff member
                StaffAd::whereStaffId($staff['id'])->delete();

                // delete staff chats
                UsersChatTable::whereAgentId($staff['id'])->delete();

                // delete all notifications of that staff
                Notification::whereUserId($staff['id'])->delete();

                // delete staff activities
                AgentActivity::whereUserId($staff['id'])->delete();

                // delete staff id from user table
                User::whereId($staff['id'])->delete();

                return array('success' => 1,'msg' => 'Staff Member deleted successfully.');
            } else {
                return array('success' => 0);
            }
        } else {
            return array('success' => 0);
        }
    }

}
