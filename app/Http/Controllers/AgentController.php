<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Intervention\Image\Facades\Image;

use App\Ad;
use App\User;
use App\Post;
use App\City;
use App\State;
use App\Media;
use App\Slider;
use App\StaffAd;
use App\Package;
use App\Country;
use App\UserMeta;
use App\Category;
use App\Report_ad;
use App\UserPackage;
use App\PropertyType;
use App\InvitedAgent;
use App\Notification;
use App\Contact_query;

class AgentController extends Controller
{
    
    public function agentProfile($slug)
    {
        $agent = User::where('slug',$slug)->with(['user_meta','country'])->first();
        $agent = json_decode(json_encode($agent),true);
        // set tile of the page
        if($agent['user_type'] == 'user') {
            $title = 'Agent Profile';
        } elseif($agent['user_type'] == 'developer') {
            $title = 'Developer Profile';
        } elseif($agent['user_type'] == 'staff') {
            $title = 'Staff Profile';
        }
        // set tile of the page
        $meta_data = array();
        if(!empty($agent['user_meta'])){
            foreach ($agent['user_meta'] as $key => $value) {   
                $meta_data[$value['meta_key']] = $value['meta_value'];
            }
        }

        if(empty($agent)){

            return view('agent.agentNotFound');

        }else{

            $limit_urgent_ads = get_option('number_of_urgent_ads_in_home');
            $sliders = Slider::all();
            $countries = Country::all();
            $properties_types = ['apartments','house'];
            // $properties = Ad::where('user_id',$agent['id'])->whereStatus(1)->whereIn('type',$properties_types)->with('city')->limit($limit_urgent_ads)->where('trash','0')->orderBy('credits','DESC')->orderBy('id', 'desc')->get();
            if($agent['user_type'] == 'staff') {
                $ad_ids = StaffAd::whereStaffId($agent['id'])->pluck('ad_id');
                $properties = Ad::whereIn('id',$ad_ids)
                              ->whereStatus(1)
                              ->where('type','apartments')
                              ->with('city')
                              ->limit($limit_urgent_ads)
                              ->where('trash','0')
                              ->orderBy('id', 'desc')
                              ->get();
            } else {
                $properties = Ad::where('user_id',$agent['id'])
                              ->whereStatus(1)
                              ->whereIn('type',$properties_types)
                              ->with('city')
                              ->limit($limit_urgent_ads)
                              ->where('trash','0')
                              ->orderBy('credits','DESC')
                              ->orderBy('id', 'desc')
                              ->get();
            }

            $posts = Post::whereType('post')->whereStatus('1')->limit(get_option('blog_post_amount_in_homepage'))->get();

            return view('agent.agentProfile', compact('properties', 'countries', 'sliders', 'posts','agent','meta_data','title')); 
        }
    }

    public function agentNotFound()
    {
        return view('agent.agentNotFound');
    }

    public function agentSetting()
    {
        $title = 'Edit Settings';
        $user_meta = UserMeta::whereUserId(Auth::user()->id)->get();
        $array = array();
        foreach ($user_meta as $key => $value) {
            $array[$value->meta_key] = $value->meta_value;
        }
        $user_meta = $array;
        $countries = Country::all();
        $previous_states = State::get();
        $previous_cities = City::get();
        // $previous_cities = '';
        return view('agent.settings',compact('user_meta','countries','previous_states','previous_cities','title'));
    }

    public function agentSettingView()
    {
        $title = 'Settings';
        $user_detail = User::where('id',Auth::user()->id)->with(['user_package.package','user_meta','country'])->first(); 
        // prx($user_detail);
        $user_detail = json_decode(json_encode($user_detail),true);
        $meta_data = array();
        if(!empty($user_detail['user_meta'])){
            foreach ($user_detail['user_meta'] as $key => $value) {   
                $meta_data[$value['meta_key']] = $value['meta_value'];
            }
        }
        $current_package = $this->current_package(Auth::user()->id);

        return view('agent.settingsView',compact('user_detail','meta_data','current_package','title'));
    }

    public function agentSettingsUpdate(Request $request) 
    {
        $inputs = array_except($request->input(), ['_token']);
        foreach($inputs as $key => $value) {
            $update = UserMeta::whereUserId(Auth::user()->id)
                        ->whereMetaKey($key)
                        ->update([
                                    'meta_value' => $value
                                ]);
            if(empty($update)){
                $option = new UserMeta;
                $option->user_id = Auth::user()->id;
                $option->meta_key = $key;
                $option->meta_value = $value;
                $option->save();
            }
        }
        //check is request comes via ajax?
        if ($request->ajax()){
            return ['success' => 1, 'msg'=>'Data Saved Successfully.'];
        }
        return redirect()->back()->with('success','Data Saved Successfully.');
    }

    public function updateAgentProfile(Request $request)
    {   
        // echo "<pre>"; print_r($request->all()); die;
        $rules = [
            'name'                  => 'required|min:3|max:255',
            'email'                 => 'required|email|max:255',
            'phone'                 => 'required|min:8|max:255',
            'gender'                => 'required',
            'country'               => 'required',
            'state'                 => 'required',
            'city'                  => 'required',
            'company'               => 'required|min:3|max:255',
            'postal_code'           => 'required',
            'address'               => 'required',
        ];
        $messages = [];
        if(Auth::user()->user_type == 'staff') {
            if(empty(Auth::user()->photo)) {
                $rules['photo'] = 'required';
            }
            if(empty(Auth::user()->agent_card_photo)) {
                $rules['agent_card'] = 'required';
            }

            $messages = [
                'agent_card.required' => 'The contract photo field is required.'
            ];
        }
        $this->validate($request, $rules, $messages);

        if($request->hasFile('photo')){
            $image_name = '';
        }else{
            $image_name = Auth::user()->photo;
        }

        if ($request->hasFile('photo')){
            $rules1 = ['photo'=>'mimes:jpeg,jpg,png'];
            $validator = Validator::make($request->all(), $rules1);
            if ($validator->fails()) {
                return redirect::back()->withErrors($validator)->withInput();
            }
            $image = $request->file('photo');
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $resized_thumb = Image::make($image)->resize(300, 300)->stream();
            $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
            $imageFileName = 'uploads/avatar/'.$image_name;
            //Upload original image
            $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');
            $user = Auth::user();

            if ($is_uploaded){
                $previous_photo = $user->photo;
                $previous_photo_storage = $user->photo_storage;

                $user->photo = $image_name;
                $user->photo_storage = get_option('default_storage');
                $user->save();

                if ($previous_photo){
                    $previous_photo_path = 'uploads/avatar/'.$previous_photo;
                    $storage = Storage::disk($previous_photo_storage);
                    if ($storage->has($previous_photo_path)){
                        $storage->delete($previous_photo_path);
                    }
                }
            }
        }

        if($request->hasFile('agent_card')){
            $agent_card_photo = '';
        }else{
            $agent_card_photo = Auth::user()->agent_card_photo;
        }

        if ($request->hasFile('agent_card')){
            $rules1 = ['agent_card'=>'mimes:jpeg,jpg,png,doc,pdf'];
            $validator = Validator::make($request->all(), $rules1);
            if ($validator->fails()) {
                return redirect::back()->withErrors($validator)->withInput();
            }
            $card_image = $request->file('agent_card');
            $card_file_base_name = str_replace('.'.$card_image->getClientOriginalExtension(), '', $card_image->getClientOriginalName());
            $card_resized_thumb = Image::make($card_image)->resize(300, 300)->stream();
            $agent_card_photo = strtolower(time().str_random(5).'-'.str_slug($card_file_base_name)).'.' . $card_image->getClientOriginalExtension();
            $card_imageFileName = 'uploads/agentCards/'.$agent_card_photo;
            //Upload original image
            $card_is_uploaded = current_disk()->put($card_imageFileName, $card_resized_thumb->__toString(), 'public');
        } else {
            $agent_card_photo = Auth::user()->agent_card_photo;
        }

        $data = [
         
            'name'              => ucfirst($request->name),
            'email'             => $request->email,
            'country_id'        => $request->country,
            'phone'             => $request->phone,
            'city_id'           => $request->city,
            'state_id'          => $request->state,
            'company'           => ucfirst(@$request->company),
            'postal_code'       => @$request->postal_code,
            'address'           => @$request->address,
            'photo'             => $image_name,
            'website'           => $request->website,
            'agent_card_photo'  => $agent_card_photo,
            'is_email_verified' => '1',
            'gender'            => $request->gender

        ];

        $user_create = User::where('id',Auth::user()->id)->update($data);

        if ($user_create){
            return redirect::back()->with('success','Profile Updated Successfully.');
        } else {
            return back()->withInput()->with('error', trans('app.error_msg'));
        }
    }

    public function reports()
    {
        $reports = Report_ad::orderBy('id', 'desc')->where('report_for','agent')->with('user.reports')->paginate(10);
        $title = 'Agent Reports';
        return view('admin.agent_reports', compact('title', 'reports'));
    }
    
    public function reportsByAgents($slug) 
    {
        $user = Auth::user();

        if ($user->is_admin()){
            $ad = User::whereSlug($slug)->first();
        }else{
            $ad = User::whereSlug($slug)->whereUserId($user->id)->first();
        }

        if (! $ad){
            return view('admin.error.error_404');
        }
        $reports = $ad->reports()->paginate(10);
        $title = 'Agent Reports';
        return view('admin.reports_by_agents', compact('title', 'ad', 'reports'));

    }

    public function developerRequests() 
    {
        $title = 'Developer Requests';
        $meta_data = $this->meta_data(Auth::user()->id);
        $current_package = $this->current_package(Auth::user()->id);

        $pending_requests = InvitedAgent::with('developer')

                                          ->where('agent_id',Auth::user()->id)
                                          ->orderBy('id','DESC')
                                          ->get();
        
        return view('agent.developer_requests',compact('pending_requests','title','meta_data','current_package'));
    }

    public function acceptCancelRequest(Request $request) 
    {
        if(!empty($request->request_id) && !empty($request->accept_val)) {

            InvitedAgent::whereId($request->request_id)->update(['accept_status' => $request->accept_val]);
            $developer_id = InvitedAgent::whereId($request->request_id)->value('developer_id');
            
            if($request->accept_val == 1) {
                $msg = 'Request accepted successfully.';

                // send notification to developer regarding accept or reject status here

                $notify = [

                        'user_id'       =>  Auth::user()->id,
                        'to_user_id'    =>  $developer_id,
                        'type'          =>  'accept_request',
                        'type_text'     =>  ' has accepted your invitation ',
                        'message'       =>  ucfirst(Auth::user()->name).' has accepted your invitation '
                ];

                Notification::create($notify);

                // send notification to developer regarding accept or reject status here
            } elseif($request->accept_val == 2) {
                $msg = 'Request cancelled successfully.';

                // send notification to developer regarding accept or reject status here

                $notify = [

                        'user_id'       =>  Auth::user()->id,
                        'to_user_id'    =>  $developer_id,
                        'type'          =>  'reject_request',
                        'type_text'     =>  ' has rejected your invitation ',
                        'message'       =>  ucfirst(Auth::user()->name).' has rejected your invitation '
                ];

                Notification::create($notify);

                // send notification to developer regarding accept or reject status here
            }

            return array('success' => 1, 'accept_val' => $request->accept_val, 'msg' => $msg);

        } else {

            return array('success' => 0);
        }
    }
    
    public function agentReports(Request $request)
    {
        $title = 'Agent Reports';
        $reports = Report_ad::orderBy('id', 'desc')->where('ad_id',Auth::user()->id)->where('report_for','agent')->with('user.reports')->paginate(10);
        $user_id = Auth::user()->id;
        $current_package = $this->current_package($user_id);
        return view('agent.reports',compact('reports','current_package','title'));
    }

}
