<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use DB;
use App\Http\Requests;

use App\Ad;
use App\User;
use App\Payment;
use App\Report_ad;
use App\Notification;
use App\Contact_query;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $title = 'Dashboard';
        $user = Auth::user();
        $user_id = $user->id;            
        $total_users = 0;
        $total_reports = 0;
        $total_payments = 0;
        $ten_contact_messages = 0;
        $reports = 0;
        $total_payments_amount = 0;

        if ($user->is_admin()){
            $approved_ads = Ad::whereStatus(1)->where('trash','0')->count();
            $pending_ads = Ad::whereStatus(0)->where('trash','0')->count();
            $blocked_ads = Ad::whereStatus(2)->where('trash','0')->count();

            $total_users = User::whereUserType('user')->whereIsEmailVerified('1')->count();
            $total_reports = Report_ad::count();
            $properties_sold = Ad::wherePropertyStatus(1)->count();
            $total_payments = Payment::whereStatus('success')->count();
            $total_payments_amount = Payment::whereStatus('success')->sum('amount');
            $ten_contact_messages = Contact_query::take(10)->orderBy('id', 'desc')->get();
            $reports = Report_ad::orderBy('id', 'desc')->whereReportFor('property')->with('ad')->take(10)->get();
        }else{
            $approved_ads = Ad::whereStatus(1)->whereUserId($user_id)->where('trash','0')->count();
            $pending_ads = Ad::whereStatus(0)->whereUserId($user_id)->where('trash','0')->count();
            $blocked_ads = Ad::whereStatus(2)->whereUserId($user_id)->where('trash','0')->count();
        }

        return view('admin.dashboard', compact('approved_ads', 'pending_ads', 'blocked_ads', 'total_users', 'total_reports', 'total_payments', 'total_payments_amount', 'ten_contact_messages', 'reports', 'properties_sold','title'));
    }

    public function logout()
    {
        if (Auth::check()){
            Auth::logout();
        }

        return redirect(route('login'));
    }
}
