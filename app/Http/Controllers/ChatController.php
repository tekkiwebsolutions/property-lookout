<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;

use Intervention\Image\ImageManager;

use App\Http\Requests;
use App\Mail\SendChatLink;

use App\Ad;
use App\User;
use App\Slider;
use App\UsersChatTable;


class ChatController extends Controller
{
    
    // public function index($agent_id,$user_id,$window_type)
    // {
        
    //     $agent_id   = decrypt($agent_id);
    //     $user_id    = decrypt($user_id);
    //     $window_type = decrypt($window_type);
    //     if($window_type == 'agentWindow'){
    //         $user_type = 'agent';
    //     }else{
    //         $user_type = 'user';
    //     }
    //     // if(Auth::user()->user_type == 'user' && Auth::user()->user_type != 'admin'){
    //     //     $user_type = 'agent';
    //     // }else{
    //     //     $user_type = 'user';
    //     // }
    //     $chat_messages      = UsersChatTable::where(['user_id'=>$user_id,'agent_id'=>$agent_id])->orderBy('id','asc')->get()->toArray();

    //     return view('common.chat',compact('chat_messages','user_id','agent_id','user_type','window_type'));
    // }

    public function index($agent_id,$user_id,$window_type)
    {
        
        $agent_id   = decrypt($agent_id);
        $user_id    = decrypt($user_id);
        $window_type = decrypt($window_type);

        // check if chat is open for agent or user
        if($window_type == 'agentWindow'){
            $user_type  = 'agent';
            $chatUser   =  User::where('id',$user_id)->first();
            $chatUser   =  json_decode(json_encode($chatUser),true);
        }else{
            $user_type = 'user';
            $chatUser   =  User::where('id',$agent_id)->first();
            $chatUser   =  json_decode(json_encode($chatUser),true);
        }
        // if(Auth::user()->user_type == 'user' && Auth::user()->user_type != 'admin'){
        //     $user_type = 'agent';
        // }else{
        //     $user_type = 'user';
        // }
        // get chat message for these users
        $chat_messages = UsersChatTable::where(['user_id'=>$user_id,'agent_id'=>$agent_id])->orderBy('id','asc')->get()->toArray();
        return view('common.chatWindow',compact('chat_messages','user_id','agent_id','user_type','window_type','chatUser'));
    }

    public function ajax_chat_after_interval(Request $request)
    {
     
        $data = $request->all();

        $agent_id       = $data['agent_id'];
        $user_id        = $data['user_id'];
        $window_type    = $data['window_type'];

        // check if page is opened at agent side or user side
        if($window_type == 'agentWindow'){
            $user_type = 'agent';
            $sent_from = 'user';
        }else{
            $user_type = 'user';
            $sent_from = 'agent';
        }

        // get all messages for these users
        $chat_messages = UsersChatTable::where('id','>',$data['last_id'])->where(['user_id'=>$user_id,'agent_id'=>$agent_id])->first();
        $chat_messages = json_decode(json_encode($chat_messages),true);

        if(!empty($chat_messages)) {
            
            // show the time of latest messages
            $message_time = date('d/m/Y',strtotime($chat_messages['created_at']));
            $chat_messages['sent_time'] = $message_time;
        }

        return $chat_messages;
    }


    public function agent_chat_list()     // this function is not in use
    {       
        $title = 'Chat Messages';
        // get all chats of auth agent id
        $users = UsersChatTable::where('agent_id',Auth::user()->id)->with('user_detail')->groupBy('user_id')->get()->toArray();
        return view('agent.chatMessagesList',compact('title','users'));
    }

    public function user_chat_list()
    {   
        $title = 'Chat Messages';
        $users = UsersChatTable::where('agent_id',Auth::user()->id)->with('user_detail')->groupBy('user_id')->get()->toArray();
        return view('agent.chatMessagesList',compact('title','users'));
    }

    public function insert_chat(Request $request)
    {
        $data = $request->all();
        $rules = [
            'textMessage'  => 'required',
        ];
        $this->validate($request, $rules);

        // for mail
        $chat_status_update = UsersChatTable::where(['Agent_id' => $data['agent_id'], 'user_id'=> $data['user_id'] ])->where('sent_from','user')->where('first_sent_from_agent','1')->first();
        if(!empty($chat_status_update)){

            if($chat_status_update->first_sent_from_agent == '1'){
                // mail is sent only first time the agent replies to message
                $sent_agent_id       =   encrypt($data['agent_id']);
                $sent_user_id        =   encrypt($data['user_id']);
                $sent_window_type    =   encrypt('userWindow');
                $link = url('chat-room').'/'.$sent_agent_id.'/'.$sent_user_id.'/'.$sent_window_type;
                $msg = '';
                $agent_detail = User::where('id',$data['agent_id'])->first();
                $user_detail = User::where('id',$data['user_id'])->first();
                $email = $user_detail->email;
                
                try{
                    $arr = ['user_name'=>$user_detail->name,'user_email' => $email,'agent_reply'=> $data['textMessage'] , 'agent_name' => $agent_detail->name ,'unique_link'=>$link];
                    Mail::to($email)->send(new SendChatLink($arr));
                    $update = UsersChatTable::where(['Agent_id' => $data['agent_id'], 'user_id'=> $data['user_id'] ])->where('sent_from','user')->where('first_sent_from_agent','1')->update(['first_sent_from_agent' => '2']);
                    // $update = UsersChatTable::where('id',$chat_status_update->id)->update(['first_sent_from_agent' => '2']);
                    // $msg = trans('app.reset_password_sent');
                }catch (\Exception $exception){
                    echo "string1";
                    $msg = $exception->getMessage();
                }
            }
        }

        if($data['window_type'] == 'agentWindow'){
            $sent_from = 'agent';
        }else{
            $sent_from = 'user';
        }

        $insert_chat =   UsersChatTable::create([
                            'Message'        => $data['textMessage'],
                            'sent_from'      => $sent_from,
                            'agent_id'       => $data['agent_id'],
                            'user_id'        => $data['user_id'],   
                        ]);

        // return last message with id and time
        $chat_messages = UsersChatTable::whereId($insert_chat->id)->first();
        $chat_messages = json_decode(json_encode($chat_messages),true);
        
        if(!empty($chat_messages)) {
            
            $message_time = date('d/m/Y',strtotime($chat_messages['created_at']));
            $chat_messages['sent_time'] = $message_time;
        }

        return $chat_messages;
    }

    public function insert_guest_user(Request $request)  // insert the chat for the first time from user side
    {
        
        $data = $request->all();
        $rules = [
            'guestUserEmail'    => 'required|email',
            'guestUserName'     => 'required',
            'guestUserMessage'  => 'required',

        ];
        $this->validate($request, $rules);
        // aget_id , guestUserEmail , guestUserName , guestUserMessage
        $user = User::where(['email'=>$data['guestUserEmail'], 'user_type' => 'guest_user'])->first();
        if(!empty($user)){
            // only insert chat or new message in the chat table but do not insert user
            $insert_message = UsersChatTable::create([
                                    'Agent_id'              => $data['agent_id'],
                                    'user_id'               => $user->id,
                                    'Message'               => $data['guestUserMessage'],
                                    'sent_from'             => 'user',
                                    'first_sent_from_agent' => '1'
                                ]);

            return Redirect::back()->with('success','Agent will contact you soon through Email.');

        }else{
            // insert in table
            $insert_guest = User::create([
                                'email'         => $data['guestUserEmail'],
                                'name'          => $data['guestUserName'],
                                'user_type'     => 'guest_user',

                            ]);

            $insert_message = UsersChatTable::create([
                                    'Agent_id'              => $data['agent_id'],
                                    'user_id'               => $insert_guest->id,
                                    'Message'               => $data['guestUserMessage'],
                                    'sent_from'             => 'user',
                                    'first_sent_from_agent' => '1'
                                ]);

            return Redirect::back()->with('success','Agent will contact you soon through Email.');
        }
    }

    // public function insert_guest_user_old(Request $request)  // insert the chat for the first time from user side (had errors)
    // {
        
    //     $data = $request->all();
    //     // prx($data);
    //     $rules = [
    //         'guestUserEmail'    => 'required|email',
    //         'guestUserName'     => 'required',
    //         'guestUserMessage'  => 'required',

    //     ];
    //     $this->validate($request, $rules);
    //     // aget_id , guestUserEmail , guestUserName , guestUserMessage
    //     $user = User::where(['email'=>$data['guestUserEmail'], 'user_type' => 'guest_user'])->get()->toArray();
    //     if(!empty($user)){
    //         // not inserted in table
    //         return Redirect::back()->with('error','You already a guest user please use old link for chat.');

    //     }else{
    //         // insert in table

    //         $insert_guest = User::create([
    //                             'email'         => $data['guestUserEmail'],
    //                             'name'          => $data['guestUserName'],
    //                             'user_type'     => 'guest_user',

    //                         ]);

    //         $insert_message = UsersChatTable::create([
    //                                 'Agent_id'              => $data['agent_id'],
    //                                 'user_id'               => $insert_guest->id,
    //                                 'Message'               => $data['guestUserMessage'],
    //                                 'sent_from'             => 'user',
    //                                 'first_sent_from_agent' => '1'
    //                             ]);

    //         return Redirect::back()->with('success','Agent will contact you soon through Email.');

    //     }
    // }
}
