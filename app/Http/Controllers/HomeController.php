<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Intervention\Image\Facades\Image;

use App\Ad;
use App\User;
use App\Post;
use App\Media;
use App\Slider;
use App\Country;
use App\Category;
use App\PropertyType;
use App\Contact_query;
use App\DeveloperContact;

use Carbon\Carbon;

class HomeController extends Controller
{   
    //BACKUP ON 12-OCT-2018
    // public function index(){


    //     $limit_premium_ads = get_option('number_of_premium_ads_in_home');
    //     $limit_regular_ads = get_option('number_of_free_ads_in_home');
    //     $limit_urgent_ads = get_option('number_of_urgent_ads_in_home');

    //     $sliders = Slider::all();
    //     $countries = Country::all();
    //     $premium_ads = Ad::activePremium()->with('city')->limit($limit_premium_ads)->orderBy('id', 'desc')->get();
    //     $regular_ads = Ad::activeRegular()->with('city')->limit($limit_regular_ads)->orderBy('id', 'desc')->get();
    //     $urgent_ads = Ad::activeUrgent()->with('city')->limit($limit_urgent_ads)->orderBy('id', 'desc')->get();
        
    //     $posts = Post::whereType('post')->whereStatus('1')->limit(get_option('blog_post_amount_in_homepage'))->get();
        
    //     return view($this->theme.'index', compact( 'premium_ads', 'regular_ads','urgent_ads', 'countries', 'sliders', 'posts'));
    // }

    public function index()
    {
        $limit_urgent_ads = get_option('number_of_urgent_ads_in_home');
        $sliders    = Slider::all();
        $countries  = Country::all();
        $property_type = ['apartments','house'];
        $properties    = Ad::with('country','state','city','user')->whereStatus(1)->whereIn('type',$property_type)->limit($limit_urgent_ads)->where('trash','0')->orderBY('credits','DESC')->orderBy('id', 'DESC')->get();
        
        $posts = Post::whereType('post')->whereStatus('1')->limit(get_option('blog_post_amount_in_homepage'))->get();

        $house_property_types = PropertyType::where('status',0)->get()->toArray(); // for property types select box

        // dynamic numbers shown in index page
        $complete_projects = Ad::whereStatus('1')->where('trash','0')->count(); // no. of all running projects
        $property_sold = Ad::wherePropertyStatus('1')->count(); // no. of properties sold
        $happy_clients = User::whereIn('user_type',['user','developer'])->count(); // no. of users in the website
        $awards_win = 120; // not known what field should be there

        return view($this->theme.'index', compact( 'properties', 'countries', 'sliders', 'posts', 'house_property_types','complete_projects','property_sold','happy_clients','awards_win'));
    }

    public function contactUs() 
    {
        $title = trans('app.contact_us');
        $posts = Post::where('type','contact-us')->first();
        $posts = json_decode(json_encode($posts),true);
        return view('theme.contact_us', compact('title','posts'));
    }

    public function contactUsPost(Request $request) 
    {
        $rules = [
            'name'      => 'required',
            'email'     => 'required|email',
            'message'   => 'required',
        ];
        $this->validate($request, $rules);
        Contact_query::create(array_only($request->input(), ['name', 'email', 'message']));
        return redirect()->back()->with('success', trans('app.your_message_has_been_sent'));
    }

    public function contactMessages() 
    {
        $title = trans('app.contact_messages');
        return view('admin.contact_messages', compact('title'));
    }

    public function contactMessagesData()
    {
        $contact_messages = Contact_query::select('name', 'email', 'message','created_at','replied_message','reply_from_admin','id')->orderBy('id', 'desc')->get();
        $button = '';
        return  Datatables::of($contact_messages)
            ->editColumn('created_at',function($contact_message){
                return $contact_message->created_at_datetime();
            })->addColumn('reply_from_admin', function($contact_message) use($button){
                        if($contact_message->reply_from_admin == 0){
                            $button .='<a href="'.route('contact_messages_reply',encrypt($contact_message->id)).'" class="btn btn-primary reply_button" ><i class="fa fa-reply"></i> </a>'; 
                        }else{
                            $button .='<a href="javascript:void(0);">Reply Sent</a>';
                        }
                        return $button;
                })->removeColumn('id')->make();
    }

    public function contactUsMessageReply($id) 
    {
        $title = trans('app.contact_us_reply');
        $id_de = decrypt($id);
        $contact_us_details = Contact_query::where('id',$id_de)->first();
        return view('admin.contact_messages_reply', compact('title','contact_us_details','id'));
    }

    public function contactUsMessageReplySend(Request $request) 
    {
        $rules = [
            'message'  => 'required',
        ];
        $this->validate($request, $rules);
        $id = decrypt($request->id);

        Contact_query::where('id',$id)->update([
            'reply_from_admin'  => '1',
            'replied_message'   =>  ucfirst($request->message)
        ]);

        // ***********mail send hide due to ENV settings.

        $contact_query = Contact_query::where('id',$id)->first();
        //prx($Contact_query);
        $msg = '';
        $email = $contact_query->email;
        try{
            Mail::send('emails.contact_us_reply', ['user' => $email,'message_reply'=> $request->message], function ($m) use ($email) {
                $m->from(get_option('email_address'), get_option('site_name'));
                $m->to($email)->subject('Contact Us Reply');
            });
            $msg = trans('app.contact_us_reply');
        }catch (\Exception $exception){
            $msg = $exception->getMessage();
            return back()->with('error', $msg);
        }

        return \Redirect::to('/dashboard/contact-messages')->with('success', trans('app.your_message_has_been_sent'));
    }
    

    /**
     * Switch Language
     */
    public function switchLang($lang)
    {
        session(['lang'=>$lang]);
        //return redirect(route('home'));
        return back();
    }

    /**
     * Reset Database
     */
    public function resetDatabase()
    {
        $database_location = base_path("database-backup/classified.sql");
        // Temporary variable, used to store current query
        $templine = '';
        // Read in entire file
        $lines = file($database_location);
        // Loop through each line
        foreach ($lines as $line)
        {
            // Skip it if it's a comment
            if (substr($line, 0, 2) == '--' || $line == '')
                continue;
            // Add this line to the current segment
            $templine .= $line;
            // If it has a semicolon at the end, it's the end of the query
            if (substr(trim($line), -1, 1) == ';')
            {
                // Perform the query
                DB::statement($templine);
                // Reset temp variable to empty
                $templine = '';
            }
        }
        $now_time = date("Y-m-d H:m:s");
        DB::table('ads')->update(['created_at' => $now_time, 'updated_at' => $now_time]);
    }

    public function agent_list()
    {
        $title = 'Agents List';
        $agents_list = User::where('active_status','1')->where('user_type','user')->orderBy('id','desc')->get()->toArray();

        return view('agent.agentList',compact('agents_list','title'));
    }

    public function aboutUs()
    {
        $title = trans('app.about_us');
        return view('theme.about_us', compact('title'));
    }

    public function properties()
    {
        $title = trans('app.properties');
        return view('theme.properties', compact('title'));
    }

    public function property_detail()
    {
        $title = trans('app.property_detail');
        $title = trans('app.property_sold');
        // $limit_premium_ads = get_option('number_of_premium_ads_in_home');
        // $limit_regular_ads = get_option('number_of_free_ads_in_home');
        // $limit_urgent_ads = get_option('number_of_urgent_ads_in_home');

        // $sliders = Slider::all();
        // $countries = Country::all();
        // $premium_ads = Ad::activePremium()->with('city')->limit($limit_premium_ads)->orderBy('id', 'desc')->get();
        // $regular_ads = Ad::activeRegular()->with('city')->limit($limit_regular_ads)->orderBy('id', 'desc')->get();
        // $urgent_ads = Ad::activeUrgent()->with('city')->limit($limit_urgent_ads)->orderBy('id', 'desc')->get();
        // return view('theme.property_detail',compact('title','premium_ads', 'regular_ads','urgent_ads', 'countries', 'sliders', 'posts'));
        return view('theme.property_detail',compact('title'));

    }

    public function property_unit_detail() 
    {
        $title = trans('app.property_detail');
        $title = trans('app.property_sold');
        // $limit_premium_ads = get_option('number_of_premium_ads_in_home');
        // $limit_regular_ads = get_option('number_of_free_ads_in_home');
        // $limit_urgent_ads = get_option('number_of_urgent_ads_in_home');

        // $sliders = Slider::all();
        // $countries = Country::all();
        // $premium_ads = Ad::activePremium()->with('city')->limit($limit_premium_ads)->orderBy('id', 'desc')->get();
        // $regular_ads = Ad::activeRegular()->with('city')->limit($limit_regular_ads)->orderBy('id', 'desc')->get();
        // $urgent_ads = Ad::activeUrgent()->with('city')->limit($limit_urgent_ads)->orderBy('id', 'desc')->get();
        // return view('theme.property_detail',compact('title','premium_ads', 'regular_ads','urgent_ads', 'countries', 'sliders', 'posts'));
        return view('theme.property_detail',compact('title'));
    }

    public function property_sold()
    {
        $posts = Post::whereType('post')->whereStatus('1')->limit(get_option('blog_post_amount_in_homepage'))->get();
        return view('theme.property_sold',compact('title'));
    }
        
    public function property_available()
    {
        $posts = Post::whereType('post')->whereStatus('1')->limit(get_option('blog_post_amount_in_homepage'))->get();
        return view('theme.property_available',compact('title'));
    } 

    // public function agentBio()
    // {
    //     return view('theme.agent_bio');
    // }

    public function multipleImages()
    {
        $user_id = Auth::user()->id;
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();

        return view('admin.multipleImages', compact('ads_images'));
    }

    public function accountSetting()
    {
        return view('theme.account_setting');
    }
    
    public function agentCms()
    {
        return view('theme.agent_cms');
    }
    
    public function agentEditcms()
    {
        return view('theme.agent_edit_cms');
    }
    
    public function Message()
    {
        return view('theme.message');
    }
    
    public function Chat()
    {
        return view('theme.chat');
    }
    
    public function CreateApartment()
    {
         $user_id = Auth::user()->id; 
         $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();
        return view('theme.agent_create_apartment',compact('user_id','ads_images'));
    }

    public function CreateUnits()
    {
        $user_id = Auth::user()->id; 
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();
        return view('theme.agent_create_units',compact('user_id','ads_images'));
    }
    
    public function CreateHouse()
    {
        $user_id = Auth::user()->id;
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();
        return view('theme.agent_create_house',compact('user_id','ads_images'));
    }

    // developer requests section starts

    public function developerContactMessages() 
    {
        $title = 'Developer Contact Messages';
        $contacts = DeveloperContact::with('user')->whereType('registration')->get()->toArray();
        return view('admin.developer_contact_messages',compact('title','contacts'));
    }

    public function developerContactReply($id) 
    {
        $id_de = getDecrypted($id);
        // get the details of contact request
        $contact_us_details = DeveloperContact::where('id',$id_de)->with('user')->first();
        
        if($contact_us_details['type'] == 'registration') {
            $title = 'Developer Contact Reply';
        } else {
            $title = 'Developer Apartment Request Reply';
        }
        return view('admin.developer_contact_reply', compact('title','contact_us_details','id'));
    }

    public function developerContactReplySave(Request $request) 
    {
        $rules = [
            'message'  => 'required',
        ];
        $this->validate($request, $rules);
        $id = decrypt($request->id);

        DeveloperContact::where('id',$id)->update([
            
            'status'  => '1',
            'replied_message'   =>  ucfirst($request->message)
        ]);

        $new_time = Carbon::parse(date('Y-m-d H:i:s'))->setTimezone(get_option('default_timezone'))->addHour(24);
        
        if($request->type == 'registration') {

            // save in user table if registration process
            User::where('id',$request->user_id)->update([
                'admin_access_key'      => $request->access_key,
                'access_token_validity' => $new_time
            ]);
        } else {

            // save in developer contacts table if new apartment request
            DeveloperContact::where('id',$id)->update([
                'admin_access_key'      => $request->access_key,
                'access_token_validity' => $new_time
            ]);
        }

        // ***********mail send hide due to ENV settings. change env settings for mail

        $contact_query = DeveloperContact::where('id',$id)->with('user')->first();
        $msg = '';
        $email = $contact_query['user']['email'];
        // $email = 'newdeveloper@mailinator.com';
        $regirter_link = url('/packages/').'/'.$contact_query['user']['email'];
        // $regirter_link = url('/packages/').'/newdeveloper@mailinator.com';
        try{
            Mail::send('emails.developer_contact_us_reply', ['user' => $email,'message_reply'=> $request->message,'access_key' => $request->access_key,'type' => $request->type,'register_link' => $regirter_link], function ($m) use ($email) {
                $m->from(get_option('email_address'), get_option('site_name'));
                $m->to($email)->subject('Contact Us Reply');
            });
            $msg = trans('app.reset_password_sent');
        }catch (\Exception $exception){
            $msg = $exception->getMessage();
            return back()->with('error', $msg);
        }

        return \Redirect::to('/dashboard/developer-contact-messages')->with('success', trans('app.your_message_has_been_sent'));
    }

    public function developerApartmentRequests() 
    {
        $title = 'Developer Apartment Requests';
        $contacts = DeveloperContact::with('user')->whereType('apartment_request')->get()->toArray();
        // prx($contacts);
        return view('admin.developer_contact_messages',compact('title','contacts'));
    }

    // developer request section ends
}




