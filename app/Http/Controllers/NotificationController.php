<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Intervention\Image\Facades\Image;

use App\Ad;
use App\Post;
use App\User;
use App\Media;
use App\Slider;
use App\Country;
use App\Category;
use App\PropertyType;
use App\Notification;
use App\Contact_query;
use App\NotificationSent;
use App\NotificationUserToken;

class NotificationController extends Controller
{   
    public function markRead(Request $request) 
    {
        if(Auth::check()) {
            $user_id = Auth::id();
            $notiToken = DB::table('notification_user_tokens')->where('token',$request->token)->whereIn('user_id',[$user_id,0])->get();
            $notiToken = json_decode(json_encode($notiToken),true);
            $noti_token_ids  = array();
            foreach ($notiToken as $key => $value) {
                $noti_token_ids[] = $value['id']; 
            }
            
            $noti_sent = NotificationSent::whereIn('notification_user_token_id',$noti_token_ids)->where('read_status','!=',2)->get();

            $noti_sent_ids  = array();
            foreach ($noti_sent as $key => $value) {
                $noti_sent_ids[] = $value['notification_id'];
                NotificationSent::where('id',$value['id'])->update([
                        'read_status' => 3
                ]);
            }
            
        } else {
            $notiToken = DB::table('notification_user_tokens')->where('token',$request->token)->where('user_id',0)->get();
            $notiToken = json_decode(json_encode($notiToken),true);
            $noti_token_ids  = array();
            foreach ($notiToken as $key => $value) {
                $noti_token_ids[] = $value['id']; 
            }
            
            $noti_sent = NotificationSent::whereIn('notification_user_token_id',$noti_token_ids)->where('read_status','!=',2)->get();

            $noti_sent_ids  = array();
            foreach ($noti_sent as $key => $value) {
                $noti_sent_ids[] = $value['notification_id'];
                NotificationSent::where('id',$value['id'])->update([
                        'read_status' => 3
                ]);
            }
        }
        return $response = ['success' => 1, 'msg' => 'All notifications are marked as read'];
    }

    public function allNotifications(Request $request) 
    { 
        $title = 'Notifications';
        return view('theme.notification',compact('notifications','title'));
    }
    public function allNotificationsjson(Request $request) 
    { 
        $title = 'Notifications';
        $notifications = '';
        $previous_notifications = '';
        if(Auth::check()) {
            if ($request->ajax()) {
                $user_id = Auth::id();
                $notiToken = DB::table('notification_user_tokens')->where('token',$request->token)->whereIn('user_id',[$user_id,0])->get();
                $notiToken = json_decode(json_encode($notiToken),true);
                $noti_token_ids  = array();
                foreach ($notiToken as $key => $value) {
                    $noti_token_ids[] = $value['id']; 
                }
                
                $noti_sent = NotificationSent::whereIn('notification_user_token_id',$noti_token_ids)->whereNotIn('read_status',[2,3])->get();
                $noti_sent_ids  = array();
                foreach ($noti_sent as $key => $value) {
                    $noti_sent_ids[] = $value['notification_id'];
                    NotificationSent::where('id',$value['id'])->update([
                            'read_status' => 1,
                    ]); 
                }
                $notifications = Notification::with('user')->whereIn('id',$noti_sent_ids)->orderBy('id','DESC')->paginate(5);
                $view = view('theme.notification-render',compact('notifications'))->render();
                return response()->json(['html'=>$view]);
            }  
        } else {
            if ($request->ajax()) {
                $notiToken = DB::table('notification_user_tokens')->where('token',$request->token)->where('user_id',0)->get();
                $notiToken = json_decode(json_encode($notiToken),true);
                $noti_token_ids  = array();
                foreach ($notiToken as $key => $value) {
                    $noti_token_ids[] = $value['id']; 
                }
                
                $noti_sent = NotificationSent::whereIn('notification_user_token_id',$noti_token_ids)->whereNotIn('read_status',[2,3])->get();
                $noti_sent_ids  = array();
                foreach ($noti_sent as $key => $value) {
                    $noti_sent_ids[] = $value['notification_id'];
                    NotificationSent::where('id',$value['id'])->update([
                            'read_status' => 1
                    ]) ; 
                }

                $notifications = Notification::with('user')->whereIn('id',$noti_sent_ids)->orderBy('id','DESC')->paginate(5);
                $view = view('theme.notification-render',compact('notifications'))->render();
                return response()->json(['html'=>$view]);
            }            
        }
        return view('theme.notification',compact('notifications','title'));
        
    }

    public function delete(Request $request) 
    {
        if(Auth::check()) {
            $user_id = Auth::user()->id;
            $notiToken = DB::table('notification_user_tokens')->where('token',$request->token)->whereIn('user_id',[$user_id,0])->get();
            $notiToken = json_decode(json_encode($notiToken),true);
            $noti_token_ids  = array();
            foreach ($notiToken as $key => $value) {
                $noti_token_ids[] = $value['id']; 
            }
                
            $del_notify  = NotificationSent::whereIn('notification_user_token_id',$noti_token_ids)->where('notification_id',$request->notify_id)->update([
                            'read_status' => 2
                        ]);
            // $del_notify = NotificationSent::where('notification_id',$request->notify_id)->update([
            //                 'read_status'   => 2
            //             ]);
            if($del_notify) {
                return ['success' => 1, 'msg' => 'Notification Deleted Successfully.'];
            } else {
                return ['success' => 0, 'msg' => 'Some error occured. Please try again.'];
            }

        } else {

            return ['success' => 0, 'msg' => 'You are not allowed to do this action.'];
        }
    }

    public function tokenWiseNotification($user_id = 0, $token = '')
    {

        $notiToken = DB::table('notification_user_tokens')->where('token',$token)->get();
        $notiToken = json_decode(json_encode($notiToken),true);
        $noti_token_ids  = array();
        foreach ($notiToken as $key => $value) {
            $noti_token_ids[] = $value['id']; 
        }
        $noti_sent = NotificationSent::whereIn('notification_user_token_id',$noti_token_ids)->get();
        $noti_sent_ids  = array();
        foreach ($noti_sent as $key => $value) {
            $noti_sent_ids[] = $value['notification_id']; 
        }

        $notification = Notification::with('user')->whereNotIn('id',$noti_sent_ids)->where('to_user_id',$user_id)->first();
        
        $users = json_decode(json_encode($notification),true);
        $notiToken = DB::table('notification_user_tokens')->where('token',$token)->where('user_id',$user_id)->first();
        $notiToken = json_decode(json_encode($notiToken),true);

        //if($notiToken['created_at'] <= $users['created_at']){
            $users['notification_token'] = $token;
            $users['notiToken'] = $notiToken;
            return $users;
        // }
        // $users = 0;
        // return $users;        
    }

    public function getLatestNotifications(Request $request) 
    {
        $request = $request->all();
        $data = 0;
        $html = '';
        if(Auth::check()) {
            $user_id = Auth::id();
            $user = $this->tokenWiseNotification($user_id,$request['token']);
            //prx($user);
            if(!empty($user['id']) && !empty($user['notiToken']['token'])){
                //if($user['notiToken']['token'] == $request['token']){
                    if(!empty($user['notification_sent_id'])){
                        if($user['read_status'] == 0){
                            NotificationSent::where('id',$user['notification_sent_id'])->update([
                                'read_status' => 1,
                            ]);
                        }
                    } else {
                        NotificationSent::create([
                            'read_status' => 0,
                            'notification_id' => $user['id'],
                            'notification_user_token_id' => $user['notiToken']['id']
                        ]);
                    }
                    $data = $user;    
                //}
            }
            
            $notiToken = DB::table('notification_user_tokens')->where('token',trim($request['token']))->whereIn('user_id',[$user_id,0])->get();

            $notiToken = json_decode(json_encode($notiToken),true);
            $noti_token_ids  = array();
            foreach ($notiToken as $key => $value) {
                $noti_token_ids[] = $value['id']; 
            }
            
            $noti_sent = NotificationSent::whereIn('notification_user_token_id',$noti_token_ids)->whereIn('read_status',[0,1])->get();
            
            $noti_sent_ids  = array();
            foreach ($noti_sent as $key => $value) {
                $noti_sent_ids[] = $value['notification_id']; 
            }
            
            $notification = Notification::with('user')->whereIn('id',$noti_sent_ids)->orderBy('id','DESC')->paginate(3);

            $view = view('theme.notification-header-render',compact('notification'))->render();

            return response()->json(['data' => $data,'html' => $view]);

        } else {
            $user = $this->tokenWiseNotification(0,$request['token']);

            if(!empty($user['id']) && !empty($user['notiToken']['token'])){
                //if($request['token'] == $user['notiToken']['token']){
                    if(!empty($user['notification_sent_id'])){
                        if($user['read_status'] == 0){
                            NotificationSent::where('id',$user['notification_sent_id'])->update([
                                'read_status' => 1,
                            ]);
                        }
                    } else {
                        NotificationSent::create([
                            'read_status' => 0,
                            'notification_id' => $user['id'],
                            'notification_user_token_id' => $user['notiToken']['id']
                        ]);
                    }
                    $data = $user;           
                //}
            }
            
            $notiToken = DB::table('notification_user_tokens')->where('token',$request['token'])->where('user_id',0)->get();
            $notiToken = json_decode(json_encode($notiToken),true);
            $noti_token_ids  = array();
            foreach ($notiToken as $key => $value) {
                $noti_token_ids[] = $value['id']; 
            }
            
            $noti_sent = NotificationSent::whereIn('notification_user_token_id',$noti_token_ids)->whereIn('read_status',[0,1])->get();
            $noti_sent_ids  = array();
            foreach ($noti_sent as $key => $value) {
                $noti_sent_ids[] = $value['notification_id']; 
            }

            $notification = Notification::with('user')->whereIn('id',$noti_sent_ids)->orderBy('id','DESC')->paginate(3);

            $view = view('theme.notification-header-render',compact('notification'))->render();

            return response()->json(['data' => $data,'html' => $view]);
        }   
    }

    public function notificationsTokenSave(Request $request)
    {
        $data = $request->all();
        if(empty($data['id'])){
            $notiTokenExist = NotificationUserToken::whereToken($data['token'])->first();
            $notiTokenExist = json_decode(json_encode($notiTokenExist),true);

            if(empty($notiTokenExist)){
                NotificationUserToken::create([
                                            'token' =>  $data['token'],
                                        ]);
            } 
        } else {
            $id = $data['id'];
            $type = 'user';
            $notiTokenExist = NotificationUserToken::whereToken($data['token'])->whereUserId($id)->first();
            $notiTokenExist = json_decode(json_encode($notiTokenExist),true);
            
            if(empty($notiTokenExist)){
                NotificationUserToken::create([
                                            'token' =>  $data['token'],
                                            'user_id' => $id,
                                            'type'  => $type
                                        ]);
            } 
        }
        return $response = 1;
    }         
}




