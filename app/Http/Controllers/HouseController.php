<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Intervention\Image\Facades\Image;

use App\Ad;
use App\User;
use App\Post;
use App\City;
use App\Media;
use App\State;
use App\Slider;
use App\Package;
use App\Country;
use App\UserMeta;
use App\Category;
use App\UserPackage;
use App\PropertyType;
use App\Notification;
use App\Contact_query;

class HouseController extends Controller
{

    public function createHouse()
    {
        $title = "Create House";
        $user_id = Auth::user()->id;
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();
        $countries = Country::all();
        $previous_states = State::where('country_id', old('country'))->get();
        $previous_cities = City::where('state_id', old('state'))->get();

        $property_types = PropertyType::where('status',0)->get()->toArray();

        // delete if there is any media not attached with any house yet
        deleteUnlinkedMedia($user_id,'house','house_images');
        // delete if there is any media not attached with any house yet

        return view('property.house.create',compact('title','user_id','ads_images','countries','previous_states','previous_cities','property_types'));
    }

    public function uploadPropertyImage(Request $request)
    {
        $user_id = Auth::user()->id;

        // restrict user to add more than 30 images
        if($request->image_type == 'house_images') {

            $attached_media_count = Media::whereUserId($user_id)->whereRef('house')->whereUploadedFor('house_images')->whereAdId('0')->count();
        } elseif($request->image_type == 'apartment_images') {

            $attached_media_count = Media::whereUserId($user_id)->whereRef('apartments')->whereUploadedFor('apartment_images')->whereAdId('0')->count();
        } else {

            $attached_media_count = Media::whereUserId($user_id)->whereRef('units')->whereUploadedFor('unit_images')->whereApartmentUnitId('0')->count();
        }

        if ($attached_media_count >= 30){
            return ['success' => 0, 'msg' => 'You cannot upload more than 30 images'];
        }
        // restrict user to add more than 30 images

        if ($request->hasFile('house_photos')){
            $image = $request->file('house_photos');
            $valid_extensions = ['jpg','jpeg','png'];

            if ( ! in_array(strtolower($image->getClientOriginalExtension()), $valid_extensions) ){
                return ['success' => 0, 'msg' => implode(',', $valid_extensions).' '.trans('app.valid_extension_msg')];
            }

            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());

            $resized = Image::make($image)->resize(640, null, function ($constraint) {
                $constraint->aspectRatio();
            })->stream();
            $resized_thumb = Image::make($image)->resize(320, 213)->stream();

            $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();

            if($request->image_type == 'house_images') {

                $imageFileName = 'uploads/houseImages/'.$image_name;
                $imageThumbName = 'uploads/houseImages/thumbs/'.$image_name;

            } else {

                // for apartment_images and unit_images
                $imageFileName = 'uploads/apartmentImages/'.$image_name;
                $imageThumbName = 'uploads/apartmentImages/thumbs/'.$image_name;
            }

            try{
                //Upload original image
                $is_uploaded = current_disk()->put($imageFileName, $resized->__toString(), 'public');

                if ($is_uploaded) {
                    //Save image name into db
                    $created_img_db = Media::create(['user_id' => $user_id, 'media_name'=>$image_name, 'type'=>'image', 'storage' => get_option('default_storage'), 'ref'=> $request->property_type ,'uploaded_for'=> $request->image_type]);

                    //upload thumb image
                    current_disk()->put($imageThumbName, $resized_thumb->__toString(), 'public');
                    $img_url = media_url_property($created_img_db, false);

                    // echo "success";
                    return ['success' => 1, 'img_url' => $img_url,'image_type'=> $request->image_type , 'property_type'=>$request->property_type];
                } else {
                    return ['success' => 0];
                }
            } catch (\Exception $e){
                return $e->getMessage();
            }
        }
    }

    public function deletePropertyMedia(Request $request)
    {
        $media_id = $request->media_id;
        $media = Media::find($media_id);
        $storage = Storage::disk($media->storage);

        if($media->ref == 'house') {

            $image_path = 'uploads/houseImages/';
            $image_thumb_path = 'uploads/houseImages/thumbs/';

        } else {

            // for apartments images and units images

            $image_path = 'uploads/apartmentImages/';
            $image_thumb_path = 'uploads/apartmentImages/thumbs/';
        }

        if ($storage->has($image_path.$media->media_name)) {
            $storage->delete($image_path.$media->media_name);
        }
        if ($media->type == 'image') {
            if ($storage->has($image_thumb_path.$media->media_name)) {
                $storage->delete($image_thumb_path.$media->media_name);
            }
        }
        $media->delete();
        return ['success'=>1, 'msg'=>trans('app.media_deleted_msg')];
    }

    public function appendPropertyMedia($property_type,$image_type)
    {

        $user_id = Auth::user()->id;
        if($image_type == 'unit_images'){
            $media_images = Media::whereUserId($user_id)->whereApartmentUnitId(0)->whereRef($property_type)->whereUploadedFor($image_type)->get();
        } else {
            $media_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef($property_type)->whereUploadedFor($image_type)->get();
        }
        return view('property.append_media', compact('media_images'));
    }

    public function addHouse(Request $request)
    {
        $rules = [
            'name'                  => 'required|max:255',
            'description'           => 'required|min:80',
            'address'               => 'required|max:255',
            'property_type_input'   => 'required',
            'country'               => 'required',
            'state'                 => 'required',
            'city'                  => 'required',
            'no_of_bathrooms'       => 'required|max:10',
            'no_of_parking'         => 'required|max:10',
            'no_of_bedrooms'        => 'required|max:10',
            'house_size'            => 'required|numeric|between:0,99999.99',
            'price'                 => 'required|numeric|between:0,999999999.99',
            //'no_of_bedrooms'        => 'required',
            'cover_photo'           => 'required',
            'house_photos_check'    => 'required',
            'zip'                   => 'required|numeric|digits_between:3,6'
        ];

        $messages = [
            'property_type_input.required' => 'The property type field is required.'
        ];

        $this->validate($request, $rules, $messages);

        // if(empty($request->latitude) || empty($request->longitude)) {
        //     return redirect::back()->with('error','Please select a correct location');
        // }

        $title   = $request->name;
        $slug    = unique_slug($title);
        $user_id = Auth::user()->id;

        $cover_photo_name = '';
        if ($request->hasFile('cover_photo')){
            $rules1 = ['cover_photo'=>'mimes:jpeg,jpg,png'];
            $validator = Validator::make($request->all(), $rules1);
            if ($validator->fails()) {
                return redirect::back()->withErrors($validator)->withInput();
            }
            $image = $request->file('cover_photo');
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $resized_thumb = Image::make($image)->resize(700, 400)->stream();
            $resized_cover_thumb = Image::make($image)->resize(320, 213)->stream();
            $cover_photo_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
            $imageFileName = 'uploads/houseImages/'.$cover_photo_name;
            $resized_thumb_name = 'uploads/houseImages/thumbs/'.$cover_photo_name;
            //Upload original image
            $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');
            if($is_uploaded) {

                current_disk()->put($resized_thumb_name, $resized_cover_thumb->__toString(), 'public');
            }
        }

        if(empty($request->company_name)) {
            $request->company_name = '';
        }

        $property_data = [
                    'title'             => ucfirst($request->name),
                    'slug'              => $slug,
                    'description'       => ucfirst($request->description),
                    'type'              => $request->property_type,
                    'price'             => $request->price,
                    'unit_type'         => 'sqft',
                    'square_unit_space' => $request->house_size,
                    'beds'              => $request->no_of_bedrooms,
                    'attached_bath'     => $request->no_of_bathrooms,
                    'no_of_parking'     => $request->no_of_parking,
                    'country_id'        => $request->country,
                    'state_id'          => $request->state,
                    'city_id'           => $request->city,
                    'address'           => $request->address,
                    'latitude'          => $request->latitude,
                    'longitude'         => $request->longitude,
                    // 'video_url'         => $request->video_link,
                    'property_type_id'  => $request->property_type_input,
                    'cover_photo'       => $cover_photo_name,
                    'status'            => 0,
                    'user_id'           => $user_id,
                    'zip'               => $request->zip,
                    'company_name'      => ucfirst($request->company_name)

                ];

        $property_created = Ad::create($property_data);
        if ($property_created){
            //Attach all unused media with this ad
            Media::whereUserId($user_id)->whereAdId(0)->whereRef('house')->whereUploadedFor('house_images')->update(['ad_id'=>$property_created->id]);

            // add notification about property created here

            $notify = [

                    'user_id'       =>  $user_id,
                    'ad_id'         =>  $property_created->id,
                    'type'          =>  'add_property',
                    'type_text'     =>  ' has added a new Property ',
                    'message'       =>  ucfirst(Auth::user()->name).' has added a new Property '.ucfirst($request->name)
            ];

            Notification::create($notify);

            // add notification about property created here
            return redirect::to('agents/properties-list')->with('success','Data Saved Sucessfully');
        }

    }

    public function editHouse($id)
    {
        $title      = "Edit House";
        $user_id    = Auth::user()->id;
        $countries       = Country::all();
        $previous_states = State::get();
        $previous_cities = City::get();
        $property_types  = PropertyType::where('status',0)->get()->toArray();
        $id   = \Crypt::decrypt($id);
        $edit = Ad::whereId($id)->first();
        $edit = json_decode(json_encode($edit),true);

        // delete if there is any media not attached with any house yet
        deleteUnlinkedMedia($user_id,'house','house_images');
        // delete if there is any media not attached with any house yet

        $images = Media::whereUserId($user_id)->whereAdId($id)->whereRef('house')->whereUploadedFor('house_images')->get();
        return view('property.house.edit',compact('title','user_id','images','countries','previous_states','previous_cities','property_types','edit'));
    }

    public function updateHouse($id , Request $request)
    {
        $id = \Crypt::decrypt($id);
        $property = Ad::whereId($id)->first();
        $user_id = Auth::user()->id;
        $rules = [
            'name'                  => 'required|max:255',
            'description'           => 'required|min:80',
            'address'               => 'required|max:255',
            'property_type_input'   => 'required',
            'country'               => 'required',
            'state'                 => 'required',
            'city'                  => 'required',
            'no_of_bathrooms'       => 'required|max:10',
            'no_of_parking'         => 'required|max:10',
            'no_of_bedrooms'        => 'required|max:10',
            'house_size'            => 'required|numeric|between:0,99999.99',
            'price'                 => 'required|numeric|between:0,999999999.99',
            'zip'                   => 'required|numeric|digits_between:3,6',
            'house_photos_check'    => 'required'
            //'no_of_bedrooms'        => 'required',
        ];
        $this->validate($request, $rules);
        $title   = $request->name;
        // $slug    = unique_slug($title);

        if($request->hasFile('cover_photo')){

            $cover_photo_name = '';
        }else{
            $cover_photo_name = $property->cover_photo;
        }
        if ($request->hasFile('cover_photo')){
            $rules1 = ['cover_photo'=>'mimes:jpeg,jpg,png'];
            $validator = Validator::make($request->all(), $rules1);
            if ($validator->fails()) {
                return redirect::back()->withErrors($validator)->withInput();
            }
            $image = $request->file('cover_photo');
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $resized_thumb = Image::make($image)->resize(700, 400)->stream();
            $resized_cover_thumb = Image::make($image)->resize(320, 213)->stream();
            $cover_photo_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
            $imageFileName = 'uploads/houseImages/'.$cover_photo_name;
            $resized_thumb_name = 'uploads/houseImages/thumbs/'.$cover_photo_name;
            //Upload original image
            $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');
            if($is_uploaded) {

                current_disk()->put($resized_thumb_name, $resized_cover_thumb->__toString(), 'public');
            }
        }

        if(empty($request->company_name)) {
            $request->company_name = '';
        }

        $property_data = [
                    'title'             => ucfirst($request->name),
                    // 'slug'              => $slug,
                    'description'       => ucfirst($request->description),
                    'type'              => $request->property_type,
                    'price'             => $request->price,
                    'square_unit_space' => $request->house_size,
                    'beds'              => $request->no_of_bedrooms,
                    'attached_bath'     => $request->no_of_bathrooms,
                    'no_of_parking'     => $request->no_of_parking,
                    'country_id'        => $request->country,
                    'state_id'          => $request->state,
                    'city_id'           => $request->city,
                    'address'           => $request->address,
                    'latitude'          => $request->latitude,
                    'longitude'         => $request->longitude,
                    // 'video_url'         => $request->video_link,
                    'property_type_id'  => $request->property_type_input,
                    'cover_photo'       => $cover_photo_name,
                    'zip'               => $request->zip,
                    'company_name'      => ucfirst($request->company_name)

                ];

        $property_created = Ad::where('id',$id)->update($property_data);
        if ($property_created){
            //Attach all unused media with this ad
            Media::whereUserId($user_id)->whereAdId(0)->whereRef('house')->whereUploadedFor('house_images')->update(['ad_id'=>$id]);
            return redirect::to('agents/properties-list')->with('success','Data Updated Sucessfully');
        }
    }

    public function delete_house(Request $request)
    {
        $data   = $request->all();
        $id     = \Crypt::decrypt($data['property_id']);
        $delete = Ad::where('id',$id)->update([ 'trash' => 1]);
        if($delete){
            return $response = array('success' => 1,'msg'=>'Property deleted successfully !');
        }else{
            return $response = array('success'=>0);
        }
    }

    public function sold_house(Request $request)
    {
        $data   = $request->all();
        $id     = \Crypt::decrypt($data['property_id']);
        $sold   = Ad::where('id',$id)->update([ 'property_status' => 1]);
        if($sold){
            // add notification regarding property sold here

            $property_name = Ad::whereId($id)->value('title');
            $notify = [

                    'user_id'       =>  Auth::user()->id,
                    'ad_id'         =>  $id,
                    'type'          =>  'property_sold',
                    'type_text'     =>  ' Property has been sold by ',
                    'message'       =>  ucfirst($property_name).' Property has been sold by '.ucfirst(Auth::user()->name)
            ];

            Notification::create($notify);

            // add notification regarding property sold here
            return $response = array('success' => 1,'count'=>$id,'msg'=>'Property updated successfully !');
        }else{
            return $response = array('success'=>0);
        }
    }
}
