<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Intervention\Image\Facades\Image;

use App\Ad;
use App\User;
use App\Post;
use App\City;
use App\Media;
use App\State;
use App\Slider;
use App\Package;
use App\Country;
use App\StaffAd;
use App\UserMeta;
use App\Category;
use App\GuestReview;
use App\UserPackage;
use App\PropertyType;
use App\Contact_query;
use App\ApartmentUnit;
use App\DeveloperContact;
use App\Tower;
use Carbon\Carbon;

class PropertyController extends Controller
{

    public function propertiesList()
    {
        $title = "";
        $meta_data = $this->meta_data(Auth::user()->id);
        $current_package = $this->current_package(Auth::user()->id);

        $propety_arr = ['apartments','house']; // types of property available
        if(Auth::user()->user_type == 'admin') {

            $properties = Ad::whereIn('type',$propety_arr)->where('trash',0)->orderBy('id','DESC')->get()->toArray();
        }else{

            if(Auth::user()->user_type == 'user') {
                $title = "Agent Properties";
            } elseif(Auth::user()->user_type == 'staff') {
                $title = 'Staff Properties';
            } else {
                $title = "Developer Properties";
            }
            // $properties = Ad::whereIn('type',$propety_arr)->withCount('available_units','GuestReview')->where('trash',0)->where('user_id',Auth::user()->id)->orderBy('id','DESC')->get()->toArray();
            if(Auth::user()->user_type == 'staff') {

                $pro_ids = StaffAd::whereStaffId(Auth::user()->id)->pluck('ad_id');
                $properties = Ad::where('type','apartments')
                                  ->with('towers')
                                  ->withCount('available_units','GuestReview')
                                  ->whereIn('id',$pro_ids)
                                  ->where('trash','0')
                                  ->orderBy('id','DESC')
                                  ->get()
                                  ->toArray();
            } else {
                $properties = Ad::whereIn('type',$propety_arr)
                                  ->withCount('available_units','GuestReview')
                                  ->where(function($query) {
                                        $query->where('user_id',Auth::user()->id)
                                              ->orWhere('assigned_agent_id',Auth::user()->id);
                                  })
                                  ->where('trash','0')
                                  ->orderBy('id','DESC')
                                  ->get()
                                  ->toArray();
            }
        }

        //prx($properties);
        // check if developer has requested for an apartment
        $developer_request = '';
        $available_request = '';
        // to get current time for default timezone i.e. Asia/Kolkata
        $current_time = Carbon::parse(date('Y-m-d H:i:s'))->setTimeZone(get_option('default_timezone'));

        if(Auth::user()->user_type == 'developer') {

            // if a request is sent within 24 hours and admin has not replied to it yet, then he cannot add another request
            $developer_request = DeveloperContact::whereUserId(Auth::user()->id)->whereStatus('0')->orderBy('id','DESC')->first();
            // if request sent within 24 hour has been replied by admin, with a access token, then he can inset the token in the form
            $available_request = DeveloperContact::whereUserId(Auth::user()->id)->whereStatus('1')->whereDate('access_token_validity','>',$current_time)->whereTokenExpired('0')->first();
        }

        return view('property.propertiesList',compact('title','meta_data','current_package','properties','developer_request','available_request'));
    }

    public function captchaString()
    {
        $image = imagecreatetruecolor(200, 50);
        $background_color = imagecolorallocate($image, 255, 255, 255);
        imagefilledrectangle($image,0,0,200,50,$background_color);

        $line_color = imagecolorallocate($image, 64,64,64);
        $number_of_lines=rand(3,7);

        for($i=0;$i<$number_of_lines;$i++){
            imageline($image,0,rand()%50,250,rand()%50,$line_color);
        }

        $pixel = imagecolorallocate($image, 0,0,255);
        for($i=0;$i<500;$i++){
            imagesetpixel($image,rand()%200,rand()%50,$pixel);
        }

        $allowed_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $length = strlen($allowed_letters);
        $letter = $allowed_letters[rand(0, $length-1)];
        $word='';
        $text_color = imagecolorallocate($image, 0,0,0);
        $cap_length=6;// No. of character in image
        for ($i = 0; $i< $cap_length;$i++){
            $letter = $allowed_letters[rand(0, $length-1)];
            imagestring($image, 5,  5+($i*30), 20, $letter, $text_color);
            $word.=$letter;
        }

        imagepng($image, "captcha_image.png");
        return $captcha =  $_SESSION['captcha_string'] = $word;
    }

    public function property_show($type,$slug)
    {
        $captcha = $this->captchaString();

        // property details
        if($type == 'units') {

            $detail = ApartmentUnit::whereSlug($slug)
                                     ->with(['apartment','apartment.city','apartment.state','apartment.country','apartment.user'=>function($query){
                                            $query->select('id','name','slug','photo');
                                     }])
                                     ->orderBy('id','DESC')
                                     ->first();
        } else {

            $detail = Ad::whereType($type)
                          ->whereSlug($slug)
                          ->with(['city','country','state','propertyType','user'=>function($query) {
                                $query->select('id','name','slug','photo');
                          }])->first();
        }
        // property details

        // increase view by one
        if(!empty($detail)){

            $detail->view = $detail->view+1;
            $detail->save();
        }
        $detail = json_decode(json_encode($detail),true);
        // increase view by one
        $tower = '';
        // get apartment floor and units if type is apartment
        if($type == 'apartments') {

            $apartment_units = ApartmentUnit::whereAdId($detail['id'])->select('id','slug','apartment_floor_id','apartment_unit_id','property_status')->get()->groupBy('apartment_floor_id');
            $apartment_units = json_decode(json_encode($apartment_units),true);
            $units_total = (int)$detail['floor']*(int)$detail['units'];
            $tower = Tower::where('ad_id',$detail['id'])->where('trash',0)->get();
            
        }

        // get images attached with property
        if($type == 'units') {

            $images = Media::where('ad_id',$detail['apartment']['id'])->where('apartment_unit_id',$detail['id'])->get()->toArray();
        } else {

            $images = Media::where('ad_id',$detail['id'])->get()->toArray();
        }

        $title = ucfirst($type).' Show';

        // get other agents except the one who created this property
        $agents = User::whereUserType('user')->where('id','<>',$detail['user_id'])->whereActiveStatus('1')->orderBy('id','desc')->limit(4)->get()->toArray();

        // get reviews rega rding that property
        if($type == 'units') {
            $reviews = GuestReview::where('ads_id',$detail['apartment']['id'])->where('apartment_unit_id',$detail['id'])->orderBy('id','desc')->get()->toArray();
        } elseif($type == 'apartments') {

            $reviews = GuestReview::where('ads_id',$detail['id'])->where('apartment_unit_id',0)->orderBy('id','desc')->get()->toArray();
        } else {

            $reviews = GuestReview::where('ads_id',$detail['id'])->orderBy('id','desc')->get()->toArray();
        }

        // get reason for the reports
        $report_reasons = DB::table('report_reasons')->orderBy('reason','ASC')->get();
        $all_ads = Ad::whereStatus('1')->select('address','latitude','longitude')->get();
        $all_ads = json_encode($all_ads);

        // get staff details if property type is apartment or units
        if($type == 'apartments') {
            $staff = StaffAd::with(['staff' => function($query){
                                    $query->select('id','user_type','name','first_name','last_name','user_name','slug','photo');
                              }])->where('ad_id',$detail['id'])->orderBy('id','ASC')->first();
            $staff = json_decode(json_encode($staff),true);
        } elseif($type == 'units') {
            $staff = StaffAd::with(['staff' => function($query) {
                                    $query->select('id','user_type','name','first_name','last_name','user_name','slug','photo');
                              }])->where('ad_id',$detail['apartment']['id'])
                              ->orderBy('id','ASC')
                              ->first();

            $staff = json_decode(json_encode($staff),true);
        } else {
            $staff = '';
        }

        if($type == 'house'){

            return view('property.house.show',compact('detail','title','type','images','agents','captcha','reviews','report_reasons','all_ads','staff','tower'));

        } elseif($type == 'apartments') {

            return view('property.apartment.show',compact('detail','title','type','images','agents','captcha','reviews','units_total','apartment_units','report_reasons','staff','tower'));

        }elseif($type == 'units') {

            return view('property.apartment.show_unit',compact('detail','title','type','images','agents','captcha','reviews','report_reasons','staff','tower'));
        }
    }

    public function leaveReviewOld(Request $request)
    {
        // prx($request->all());
        $rules = [
            'name'            => 'required',
            'email'           => 'required|email',
            'message'         => 'required|max:80',
            'rating'          => 'required',
            'current_captcha' => 'required',
            'entered_captcha' => 'required|same:current_captcha',
        ];

        $messages = [
              'entered_captcha.same' => 'Please Enter Correct Captcha.',
        ];
        $this->validate($request, $rules,$messages);

        $save_review =  GuestReview::create([

                            'name'              => ucfirst($request->name),
                            'email'             => $request->email,
                            'message'           => ucfirst($request->message),
                            'ratings'           => $request->rating,
                            'ads_id'            => $request->ads_id,
                            'apartment_unit_id' => $request->apartment_unit_id,
                        ]);
        return redirect::back()->with('success','Review Saved Successfully !');
        // return redirect($request->redirect_url)->with('success','Review Saved Successfully !');
    }

    public function leaveReview(Request $request)
    {
        $rules = [
            'name'            => 'required',
            'email'           => 'required|email',
            'message'         => 'required|max:80',
            'rating'          => 'required',
            'current_captcha' => 'required',
            'entered_captcha' => 'required|same:current_captcha',
        ];

        $messages = [
              'entered_captcha.same' => 'Please Enter Correct Captcha.',
        ];
        // $this->validate($request, $rules,$messages);
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            // Validation failed
            return array('error'=> $validator->messages());
        }

        $save_review =  GuestReview::create([

                            'name'              => ucfirst($request->name),
                            'email'             => $request->email,
                            'message'           => ucfirst($request->message),
                            'ratings'           => $request->rating,
                            'ads_id'            => $request->ads_id,
                            'apartment_unit_id' => $request->apartment_unit_id,
                        ]);
        return array('status' => 1);
        // return redirect::back()->with('success','Review Saved Successfully !');
        // return redirect($request->redirect_url)->with('success','Review Saved Successfully !');
    }

}
