<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use Carbon\Carbon;
use App\Http\Requests;
use App\Mail\ResetPasswordLink;
use Yajra\Datatables\Datatables;
use Intervention\Image\Facades\Image;

use App\Ad;
use App\User;
use App\City;
use App\Post;
use App\State;
use App\Country;
use App\Payment;
use App\Package;
use App\Favorite;
use App\UserMeta;
use App\UserPackage;
use App\DeveloperContact;

class UserController extends Controller
{
    /**
        * Display a listing of the resource.
        *
        * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $title = trans('app.users');
        return view('admin.users', compact('title'));
    }

    public function usersData() 
    {
        $users = User::select('id','name','user_name','email','feature','active_status','plan_active_status','created_at')->whereUserType('user')->whereIsEmailVerified('1')->get();
        return  Datatables::of($users)
            ->editColumn('name', function($user){
                $html = '<a href="'.route('user_info', $user->id).'">'.$user->name.'</a>';
                return $html;
            })
            ->editColumn('active_status',function($user){
                
                if($user->plan_active_status == '0') {
                    return 'Payment Pending';
                } else {
                    if($user->active_status == '0') {
                        return 'Pending';
                    } elseif($user->active_status == '1') {
                        return 'Active';
                    } else {
                        return 'Blocked';
                    }
                }
                
                // return 'Pending';
            })
            ->editColumn('created_at',function($user){
                return $user->signed_up_datetime();
            })
            ->addColumn('action', function($user){
                $html = '<a href="'.route('user_info', $user->id).'" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> '.trans('app.view').'</a>';

                if($user->plan_active_status == '0') {
                    $html .= '<a href="javascript:void(0);" title="Blocked" class="btn btn-danger btn-xs" rel="'.$user->id.'" rel1="1" style="margin-left:10px;"><i class="fa fa-ban"></i> Restricted</a>';
                } else {
                    if($user->active_status == '0') {
                        $html .= '<a href="javascript:void(0);" title="Pending" class="btn btn-warning btn-xs change_user_status" rel="'.$user->id.'" rel1="1" style="margin-left:10px;"><i class="fa fa-retweet"></i> Activate</a>';
                    } elseif($user->active_status == '1'){
                        $html .= '<a href="javascript:void(0);" title="Active" class="btn btn-success btn-xs change_user_status" rel="'.$user->id.'" rel1="2" style="margin-left:10px;"><i class="fa fa-retweet"></i> Active</a>';
                    } else {
                        $html .= '<a href="javascript:void(0);" title="Blocked" class="btn btn-danger btn-xs change_user_status" rel="'.$user->id.'" rel1="1" style="margin-left:10px;"><i class="fa fa-retweet"></i> Blocked</a>';
                    }    
                }

                // if($user->feature == 0){
                //     $html .= '<a href="javascript:;" class="btn btn-default btn-xs agent-feature-btn" data-user-id="'.$user->id.'"><i class="fa fa-star-o"></i></a>';
                // }else{
                //     $html .= '<a href="javascript:;" class="btn btn-default btn-xs agent-feature-btn" data-user-id="'.$user->id.'"><i class="fa fa-star"></i></a>';
                // }

                return $html;
            })
            ->removeColumn('id', 'feature','plan_active_status')
            ->make();
    }

    public function userInfo($id) 
    {
        $title = trans('app.user_info');
        $user = User::find($id);
        $ads = $user->ads()->where('trash','0')->paginate(20);

        if (!$user){
            return view('admin.error.error_404');
        }

        return view('admin.user_info', compact('title', 'user', 'ads'));
    }

    public function changeStatus(Request $request) 
    {
        $user_id = $request->user_id;
        $status = $request->status;
        User::whereId($user_id)->update(['active_status' => $status]);
        if ($status == 1){
            return '<p class="alert alert-success">'.trans('app.user_has_been_activated').' </p>';
        }elseif ($status == 2){
            return '<p class="alert alert-warning">'.trans('app.user_has_been_blocked').' </p>';
        }
    }

    public function changeFeature(Request $request) 
    {
        $user_id = $request->user_id;
        $user = User::find($user_id);
        $current_feature = $user->feature;
        $user->feature = ($current_feature == 0) ? 1 : 0;
        $user->save();

        if ($current_feature == 1){
            return '<i class="fa fa-star-o"></i>';
        }elseif ($current_feature == 0){
            return '<i class="fa fa-star"></i>';
        }
    }

    /**
        * Show the form for creating a new resource.
        *
        * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $countries = Country::all();
        $previous_states = State::where('country_id', old('country'))->get();
        $previous_cities = City::where('state_id', old('state'))->get();
        if(!empty($_GET['email'])){
            $userEmail = $_GET['email'];
        } else {
            $userEmail = '';
        }

        if(!empty($_GET['id'])){
            $userId = decrypt($_GET['id']);
            $package_purchase = UserPackage::where('user_id',$userId)->with('package')->first();
            $user = User::where('id',$userId)->with('contact_query')->first();
        } else {
            $userId = '';
            $package_purchase = '';
            $user = '';
        }
        // prx($package_purchase);
        return view('theme.user_create', compact('countries','previous_states', 'previous_cities','userEmail','userId','package_purchase','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {   


    //     $rules = [
    //         'first_name'            => 'required|min:3',
    //         'last_name'             =>  'required|min:3',
    //         'email'                 => 'required|email',
    //         'phone'                 => 'required|min:8',
    //         'gender'                => 'required',
    //         'country'               => 'required',
    //         'state'                 => 'required',
    //         'city'                  => 'required',
    //         'description'           => 'required',
    //         'company'               => 'required|min:3',
    //         'password'              => 'required|min:3|confirmed',
    //         'password_confirmation' => 'required',
    //         'user_name'             => 'required',
    //         'postal_code'           => 'required',
    //         'agree'                 => 'required',
    //         // 'photo'                 => 'required',
    //         'agent_card'            => 'required|mimes:jpeg,jpg,png,doc,pdf', 
    //         'agent_number'          => 'required',
    //     ];
    //     // print_r($rules);
    //     // die;
    //     $this->validate($request, $rules);

    //     $image_name = '';
    //     if ($request->hasFile('photo')){
    //         $rules1 = ['photo'=>'mimes:jpeg,jpg,png'];
    //         $validator = Validator::make($request->all(), $rules1);
    //         if ($validator->fails()) {
    //             return redirect::back()->withErrors($validator)->withInput();
    //         }
    //         $image = $request->file('photo');
    //         $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
    //         $resized_thumb = Image::make($image)->resize(300, 300)->stream();
    //         $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
    //         $imageFileName = 'uploads/avatar/'.$image_name;
    //         //Upload original image
    //         $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');

    //     }

    //     $agent_card_photo = '';
    //     if ($request->hasFile('agent_card')){
    //         $rules1 = ['agent_card'=>'mimes:jpeg,jpg,png,doc,pdf'];
    //         $validator = Validator::make($request->all(), $rules1);
    //         if ($validator->fails()) {
    //             return redirect::back()->withErrors($validator)->withInput();
    //         }
    //         $card_image = $request->file('agent_card');
    //         $card_file_base_name = str_replace('.'.$card_image->getClientOriginalExtension(), '', $card_image->getClientOriginalName());
    //         $card_resized_thumb = Image::make($card_image)->resize(300, 300)->stream();
    //         $agent_card_photo = strtolower(time().str_random(5).'-'.str_slug($card_file_base_name)).'.' . $card_image->getClientOriginalExtension();
    //         $card_imageFileName = 'uploads/agentCards/'.$agent_card_photo;
    //         //Upload original image
    //         $card_is_uploaded = current_disk()->put($card_imageFileName, $card_resized_thumb->__toString(), 'public');
    //     }

    //     $slug = unique_slug($request->first_name, 'User');

    //     $data = [
    //         'first_name'        => $request->first_name,
    //         'last_name'         => $request->last_name,
    //         'name'              => $request->first_name.' '.$request->last_name,
    //         'email'             => $request->email,
    //         'password'          => bcrypt($request->password),
    //         'country_id'        => $request->country,
    //         'phone'             => $request->phone,
    //         'city_id'           => $request->city,
    //         'state_id'          => $request->state,
    //         'company'           => @$request->company,
    //         'postal_code'       => @$request->postal_code,
    //         'address'           => @$request->address,
    //         'description'       => @$request->descrpition,
    //         'middle_name'       => @$request->middle_name,
    //         'user_name'         => $request->user_name,
    //         'user_type'         => 'user',
    //         // 'active_status'     => '1',
    //         'activation_code'   => str_random(30),
    //         'photo'             => $image_name,
    //         'photo_storage'     => 'public',
    //         'agent_card_photo'  => $agent_card_photo,
    //         'agent_number'      => $request->agent_number,
    //         'slug'              => $slug,

    //     ];


    //     //$user_create = User::where('id',$request->user_id)->update($data);
    //     $user_create = User::create($data);

    //     if ($user_create){
    //         return redirect(route('login'))->with('success','Registeration process is complete now wait for administrator approval.');
    //     } else {
    //         return back()->withInput()->with('error', trans('app.error_msg'));
    //     }
    // }

    public function store(Request $request)
    {   
        // prx($request->all());
        $rules = [
            'first_name'            => 'required|min:3|max:255',
            'last_name'             => 'required|min:3|max:255',
            'email'                 => 'required|email|max:255',
            'phone'                 => 'required|numeric|digits_between:8,16',
            'gender'                => 'required',
            'country'               => 'required',
            'state'                 => 'required',
            'city'                  => 'required',
            'description'           => 'required',
            'company'               => 'required|min:3|max:255',
            'password'              => 'required|min:3|max:255|confirmed',
            'password_confirmation' => 'required|min:3|max:255',
            'user_name'             => 'required|max:255',
            'postal_code'           => 'required|numeric|digits_between:3,6',
            'agree'                 => 'required',
            // 'photo'                 => 'required',
            
        ];

        // if(isset($request->agent_card) && isset($request->agent_number)) {
        if($request->package == 'Agent') {

            $rules['agent_card']            = 'required|mimes:jpeg,jpg,png,doc,pdf'; 
            $rules['agent_number']          = 'required';
        }
        
        $this->validate($request, $rules);

        $image_name = '';
        if ($request->hasFile('photo')){
            $rules1 = ['photo'=>'mimes:jpeg,jpg,png'];
            $validator = Validator::make($request->all(), $rules1);
            if ($validator->fails()) {
                return redirect::back()->withErrors($validator)->withInput();
            }
            $image = $request->file('photo');
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $resized_thumb = Image::make($image)->resize(300, 300)->stream();
            $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
            $imageFileName = 'uploads/avatar/'.$image_name;
            //Upload original image
            $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');
        }

        $agent_card_photo = '';
        if ($request->hasFile('agent_card')){
            $rules1 = ['agent_card'=>'mimes:jpeg,jpg,png,doc,pdf'];
            $validator = Validator::make($request->all(), $rules1);
            if ($validator->fails()) {
                return redirect::back()->withErrors($validator)->withInput();
            }
            $card_image = $request->file('agent_card');
            $card_file_base_name = str_replace('.'.$card_image->getClientOriginalExtension(), '', $card_image->getClientOriginalName());
            $card_resized_thumb = Image::make($card_image)->resize(300, 300)->stream();
            $agent_card_photo = strtolower(time().str_random(5).'-'.str_slug($card_file_base_name)).'.' . $card_image->getClientOriginalExtension();
            $card_imageFileName = 'uploads/agentCards/'.$agent_card_photo;
            //Upload original image
            $card_is_uploaded = current_disk()->put($card_imageFileName, $card_resized_thumb->__toString(), 'public');
        } 

        $slug = unique_slug($request->first_name, 'User');
        $status = '0';
        if(!isset($request->agent_card) && !isset($request->agent_number)) {

            $agent_card_photo = '';
            $request->agent_number = '';
            
        }
        if($request->package == 'Agent') {

            $user_type = 'user';
        } elseif($request->package == 'Developer') {

            $user_type = 'developer';
            $status = '1';
        }

        $data = [
            'first_name'        => ucfirst($request->first_name),
            'last_name'         => ucfirst($request->last_name),
            'name'              => ucfirst($request->first_name).' '.ucfirst($request->last_name),
            'email'             => $request->email,
            'password'          => bcrypt($request->password),
            'country_id'        => $request->country,
            'phone'             => $request->phone,
            'city_id'           => $request->city,
            'state_id'          => $request->state,
            'company'           => ucfirst(@$request->company),
            'postal_code'       => @$request->postal_code,
            'address'           => @$request->address,
            'description'       => ucfirst(@$request->description),
            'middle_name'       => ucfirst(@$request->middle_name),
            'user_name'         => $request->user_name,
            'user_type'         => $user_type,
            'active_status'     => $status,
            'activation_code'   => str_random(30),
            'photo'             => $image_name,
            'photo_storage'     => 'public',
            'agent_card_photo'  => $agent_card_photo,
            'agent_number'      => $request->agent_number,
            'slug'              => $slug,
            'is_email_verified' => '1'
        ];

        $user_create = User::where('id',$request->user_id)->update($data);

        if ($user_create){
            
            // if user is created, the access token is expired
            User::whereId($request->user_id)->update(['token_expired'=>'1']);

            // put description in user's about as it is shown nowhere else
            UserMeta::create(['user_id' => $request->user_id,'meta_key' => 'about','meta_value' => ucfirst(@$request->description)]); 

            if($request->package == 'Agent') {
                $msg = 'Registeration process is complete now. Please wait for administrator approval.';
            } else {
                $msg = 'Registeration process is complete now. You can login to your account.';
            }
            return redirect(route('login'))->with('success',$msg);
        } else {
            return back()->withInput()->with('error', trans('app.error_msg'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function profile()
    {
        // sub_E8kAz6mzxZT6Nl
        // cus_E8kAPUEDak4Ftu
        // si_E8kA25F8fOGizA
        // plan_E8MiwzDDGv0jC0
        // prod_E8M9NyOB6F4KaW
            
        $title = trans('app.profile');
        $user = Auth::user();
        return view('admin.profile', compact('title', 'user'));
    }

    public function profileEdit()
    {
        $title = trans('app.profile_edit');
        $user = Auth::user();
        $countries = Country::all();
        $previous_states = State::where('country_id', $user->country_id)->get();
        $previous_cities = City::where('state_id', $user->state_id)->get();

        return view('admin.profile_edit', compact('title', 'user', 'countries', 'previous_states', 'previous_cities'));
    }

    public function profileEditPost(Request $request)
    {
        $user = Auth::user();

        //Validating
        $rules = [
            'name'          => 'required',
            'email'         => 'required|email|unique:users,email,'.$user->id,
            'gender'        => 'required',
            'mobile'        => 'required|numeric|digits_between:6,10',
            'country_id'    => 'required',
            'state_id'      => 'required',
            'city_id'       => 'required'
        ];
        $this->validate($request, $rules);

        $inputs = array_except($request->input(), ['_token', 'photo']);
        $user->update($inputs);

        if ($request->hasFile('photo')){
            $rules = ['photo'=>'mimes:jpeg,jpg,png|max:500'];
            $this->validate($request, $rules);
            
            $image = $request->file('photo');
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $resized_thumb = Image::make($image)->resize(300, 300)->stream();

            $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
            $imageFileName = 'uploads/avatar/'.$image_name;

            //Upload original image
            $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');

            if ($is_uploaded){
                $previous_photo= $user->photo;
                $previous_photo_storage= $user->photo_storage;

                $user->photo = $image_name;
                $user->photo_storage = get_option('default_storage');
                $user->save();

                if ($previous_photo){
                    $previous_photo_path = 'uploads/avatar/'.$previous_photo;
                    $storage = Storage::disk($previous_photo_storage);
                    if ($storage->has($previous_photo_path)){
                        $storage->delete($previous_photo_path);
                    }
                }
            }
        }
        return redirect(route('profile'))->with('success', trans('app.profile_edit_success_msg'));
    }

    public function administrators()
    {
        $title = trans('app.administrators');
        $users = User::whereUserType('admin')->get();

        return view('admin.administrators', compact('title', 'users'));
    }

    public function addAdministrator()
    {
        $title = trans('app.add_administrator');
        $countries = Country::all();

        return view('admin.add_administrator', compact('title', 'countries'));
    }


    public function storeAdministrator(Request $request)
    {
        $rules = [
            'first_name'            => 'required',
            'email'                 => 'required|email',
            'phone'                 => 'required',
            'gender'                => 'required',
            'country'               => 'required',
            'password'              => 'required|confirmed',
            'password_confirmation' => 'required',
        ];
        $this->validate($request, $rules);

        $data = [
            'first_name'        => ucfirst($request->first_name),
            'last_name'         => ucfirst($request->last_name),
            'name'              => ucfirst($request->first_name).' '.ucfirst($request->last_name),
            'email'             => $request->email,
            'password'          => bcrypt($request->password),
            'phone'             => $request->phone,
            'user_type'         => 'admin',
            'active_status'     => 1,
            'activation_code'   => str_random(30)
        ];

        $user_create = User::create($data);
        return redirect(route('administrators'))->with('success', trans('app.registration_success'));
    }

    public function administratorBlockUnblock(Request $request)
    {
        $status = $request->status == 'unblock'? 1 : 2;
        $user_id = $request->user_id;
        User::whereId($user_id)->update(['active_status' => $status]);
        
        if ($status ==1){
            return ['success' => 1, 'msg' => trans('app.administrator_unblocked')];
        }
        return ['success' => 1, 'msg' => trans('app.administrator_blocked')];
    }
    
    //Login view
    public function login()
    {
        return view('theme.login');
    }

    //Login action
    public function loginPost(Request $request)
    {
        $rules = [
            'email'    => 'required|email',
            'password'    => 'required',
        ];
        //$this->validate($request, $rules);

        //Manually creating validation
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('login'))->withErrors($validator)->withInput();
        }

        //Get input value
        $email      = $request->email;
        $password   = $request->password;

        //Authenticating
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = Auth::user();

            if($user->user_type != 'guest_user'){
                // check payment here
                if(($user->user_type == 'user' && $user->plan_active_status == '1') || $user->user_type == 'developer' || $user->user_type == 'admin' || $user->user_type == 'staff') {

                    if($user->active_status == '1'){
                        $user->last_login = Carbon::now();
                        $user->save();

                        // Authentication passed...
                        if($user->user_type == 'user'){
                            // return redirect()->intended(route('packages'));
                            return redirect('/agents/properties-list');

                        } elseif($user->user_type == 'developer') {

                            // check if developer is login for first time or not
                            // if(empty($user->no_of_staff)) {
                            //     return redirect('/developers/add-staff-number');
                            // } else {
                                return redirect('/developers/properties-list');
                            // }

                        } elseif($user->user_type == 'admin') {
                            //prx($user);
                            // return redirect()->intended(route('dashboard'));
                            return redirect('/dashboard');
                        } elseif($user->user_type == 'staff') {

                            // check if staff is login for the first time or not
                            if($user->is_email_verified == '0') {
                                return redirect('/staff/settings');
                            } else {
                                return redirect('/staff/properties-list');
                            }
                        }
                    } else {

                        Auth::logout();
                        return redirect(route('login'))->with('warning','Please wait for administrator approval.');
                    }
                } else {
                    Auth::logout();
                    $msg = $user->expiry_message;
                    if(empty($msg)) {

                        return redirect(route('login'))->with('warning','Please complete your registration process.');
                    } else {

                        return redirect('/user/register-payment?id='.encrypt($user->id).'&email='.$user->email)->with('warning',$msg);
                    }
                }
            } else {

                Auth::logout();
                return redirect(route('login'))->with('error','You are not authorised to login.');
            }
        } else {
            return redirect(route('login'))->with('error', trans('app.email_password_error'));
        }
    }

    public function changePassword()
    {
        $title = trans('app.change_password');
        return view('admin.change_password', compact('title'));
    }

    public function changePasswordPost(Request $request)
    {
        $rules = [
            'old_password'              => 'required',
            'new_password'              => 'required|confirmed',
            'new_password_confirmation' => 'required',
        ];
       
        if ($request->ajax()){
            $messages = [
              'old_password.required' => 'The Current Password field is required.',
            ];
            $validator = Validator::make($request->all(), $rules,$messages);
            if ($validator->fails()) {
                // Validation failed
                    return array('error'=> $validator->messages());
            }
        }else{
            $this->validate($request, $rules);     
        }

        $old_password = $request->old_password;
        $new_password = $request->new_password;
        //$new_password_confirmation = $request->new_password_confirmation;

        if(Auth::check())
        {
            $logged_user = Auth::user();
            if(Hash::check($old_password, $logged_user->password))
            {
                $logged_user->password = Hash::make($new_password);
                $logged_user->save();
                if ($request->ajax()){
                    return ['success' => 1, 'msg'=>'Password changed successfully.'];
                }
                return redirect()->back()->with('success','Password Changed Successfully !');
            }
            if ($request->ajax()){
                    return ['success' => 0, 'msg'=>'Your Current Password is Wrong.'];
            }
            return redirect()->back()->with('error','Your Current Password is Wrong.');
        }
    }

    /**
        * @param Request $request
        * @return array
    */

    public function saveAdAsFavorite(Request $request)
    {
        if ( ! Auth::check())
            return ['status'=>0, 'msg'=> trans('app.error_msg'), 'redirect_url' => route('login')];

        $user = Auth::user();

        $slug = $request->slug;
        $ad = Ad::whereSlug($slug)->first();

        if ($ad){
            $get_previous_favorite = Favorite::whereUserId($user->id)->whereAdId($ad->id)->first();
            if ( ! $get_previous_favorite){
                Favorite::create(['user_id'=>$user->id, 'ad_id'=>$ad->id]);
                return ['status'=>1, 'action'=>'added', 'msg'=>'<i class="fa fa-star"></i> '.trans('app.remove_from_favorite')];
            }else{
                $get_previous_favorite->delete();
                return ['status'=>1, 'action'=>'removed', 'msg'=>'<i class="fa fa-star-o"></i> '.trans('app.save_ad_as_favorite')];
            }
        }
        return ['status'=>0, 'msg'=> trans('app.error_msg')];
    }

    public function replyByEmailPost(Request $request)
    {
        $data = $request->all();
        $data['email'];
        $ad_id = $request->ad_id;
        $ad = Ad::find($ad_id);
        if ($ad){
            $to_email = $ad->user->email;
            if ($to_email){
                try{
                    Mail::send('emails.reply_by_email', ['data' => $data], function ($m) use ($data, $ad) {
                        $m->from(get_option('email_address'), get_option('site_name'));
                        $m->to($ad->user->email, $ad->user->name)->subject('query from '.$ad->title);
                        $m->replyTo($data['email'], $data['name']);
                    });
                }catch (\Exception $e){
                    //
                }
                return ['status'=>1, 'msg'=> trans('app.email_has_been_sent')];
            }
        }
        return ['status'=>0, 'msg'=> trans('app.error_msg')];
    }


    public function postEmailResetPasswordold(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $user = User::whereEmail($request->email)->first();
        if (!$user){
            return back()->with('error','Your email is not registered to our platform.');
        }
        $passwordResetToken = str_random(32);
        $passwordResetURL = route('password_reset_url', $passwordResetToken);

        $user->activation_code = $passwordResetToken;
        $user->save();

        $msg = '';
        $messageData = array();
        // send email to user with a reset password link
        try{
            //     Mail::send('emails.send_password_reset', ['user' => $user, 'resetURL' => $passwordResetURL], function ($m) use ($user) {
            //         $m->from(get_option('email_address'), get_option('site_name'));
            //         $m->to($user->email, $user->name)->subject('Password reset request');
            //     });
            //     $msg = trans('app.reset_password_sent');
            $messageData = ['user' => $user->name, 'resetURL' => $passwordResetURL ];
            Mail::to($user->email)->send(new ResetPasswordLink($messageData));
            $msg = trans('app.reset_password_sent');
            return back()->with('success', $msg);

        }catch (\Exception $exception){
            // if any error occurs, it will prevent from stopping the code
            $msg = $exception->getMessage();
            return back()->with('error', $msg);
        }
    }

    public function postEmailResetPassword(Request $request)
    {
        if(empty($request->email)) {
            return array('success' => 0,'error' => 'Please enter your email id.');
        }

        if(empty(filter_var( $request->email, FILTER_VALIDATE_EMAIL ))) {
            return array('success' => 0, 'error' => 'Please enter a valid email address.');
        }

        $user = User::whereEmail($request->email)->first();
        if (!$user){
            return array('success' => 0, 'error' => 'Your email is not registered to our platform.');
        }

        $passwordResetToken = str_random(32);
        $passwordResetURL = route('password_reset_url', $passwordResetToken);

        $user->activation_code = $passwordResetToken;
        $user->save();

        $msg = '';
        $messageData = array();
        // send email to user with a reset password link
        try{
            //     Mail::send('emails.send_password_reset', ['user' => $user, 'resetURL' => $passwordResetURL], function ($m) use ($user) {
            //         $m->from(get_option('email_address'), get_option('site_name'));
            //         $m->to($user->email, $user->name)->subject('Password reset request');
            //     });
            //     $msg = trans('app.reset_password_sent');
            $messageData = ['user' => $user->name, 'resetURL' => $passwordResetURL ];
            Mail::to($user->email)->send(new ResetPasswordLink($messageData));
            $msg = trans('app.reset_password_sent');
            return array('success' => 1);

        }catch (\Exception $exception){
            // if any error occurs, it will prevent from stopping the code
            $msg = $exception->getMessage();
            return array('success' => 0, 'error' => 'Some error occured. Please try again.');
        }
    }

    /**
        * @param $token
        * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function passwordResetForm($token)
    {
        $user = User::whereActivationCode($token)->first();
        if ( ! $user){
            return redirect(route('login'))->with('error', trans('app.invalid_password_token'));
        }

        $title = trans('app.reset');
        return view('auth.reset_password', compact('token', 'title'));
    }

    /**
        * @param Request $request
        * @param $token
        * @return mixed
        *
    */
    public function passwordResetPost(Request $request, $token)
    {
        $user = User::whereActivationCode($token)->first();
        if ( ! $user){
            return redirect(route('login'))->with('error', trans('app.invalid_password_token'));
        }
        $rules = [
            'password'  => 'required|confirmed',
        ];
        $this->validate($request, $rules);

        $user->password = Hash::make($request->password);
        $user->activation_code = '';
        $user->save();

        return redirect(route('login'))->with('success', trans('app.password_has_been_reset'));
    }

    public function packages($email = '',$register_type = '',Request $request)
    {
        $title = 'Register';
        $allowed_types = ['Agent','Developer'];
        $packages = Package::whereIn('name',$allowed_types)->get()->toArray();

        $active_package_id = '';
        $current_package_detail = array();
        if(Auth::check()){
           if(Auth::user()->user_type == 'user'){
                $current_package_detail = User::where('id',Auth::user()->id)->with('user_package.package')->first();
                $current_package_detail = json_decode(json_encode($current_package_detail),true);
                $active_package_id = $current_package_detail['user_package']['packages_id'];
           } 
        }

        $email = $email ? $email : '';
        $register_type = $register_type ? $register_type : '';
        return view('agent.packages',compact('packages','active_package_id','current_package_detail','email','register_type','title'));
    }

    public function insertPackage(Request $request)
    {   
        // registration process for both agent and developer
        $data = $request->all();
      
        $users =   User::where([
                            'email'                 => $data['email'],
                            'user_type'             => $data['user_type'],
                        ])->first();
        $users = json_decode(json_encode($users),true);
        if(!empty($users)){
            if($users['user_type'] == 'user' && $users['is_email_verified'] == '0') {
                return array('status' => 1,'email'=>$users['email'],'token'=>encrypt($users['id']),'plan_id'=>$users['current_active_plan_id']);
            }
        }
        $rules = [
            'email'    => 'required|email|unique:users,email',
            'charge_payment_method' => 'required'
        ];
        
        $contact_info = DeveloperContact::whereUserId($users['id'])->first(); // will be present only for developer account

        if($users['user_type'] == 'developer') {

            if(empty($users) || (!empty($users) && empty($contact_info)))
            {
                $rules = [
                    'email'    => 'required|email',
                ];
            } else {
                $rules = [
                    'email'    => 'required|email|unique:users,email',
                ];
            }
        } else {
            $rules = [
                'email'        => 'required|email|unique:users,email',
            ];
        }
        
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Validation failed
            return array('error'=> $validator->messages());
        }
        
        // add one month to current date for package expiry
        if( date('d') == 31 || (date('m') == 1 && date('d') > 28)){
            $date = strtotime('last day of next month');
        } else {
            $date = strtotime('+1 months');
        }

        $expiry_date = date('Y-m-d', $date);
        $charge_method = 'manual';
        // save agent for the first time

        if(!empty($data['charge_payment_method'])){
            $charge_method = $data['charge_payment_method'];
        }
        $new_time = Carbon::parse(date('Y-m-d H:i:s'))->setTimezone(get_option('default_timezone'))->addHour(24);
        if(!$users) {

            $save_agent =   User::create([
                                'email'                 => $data['email'],
                                'user_type'             => $data['user_type'],
                                'charge_payment_method' => $charge_method
                                // 'access_token_validity' => $new_time,
                            ]);

            // // save the newly added package for that user
            // if(empty($email)) {


                // save agent for the first time
                
                // $new_time = Carbon::parse(date('Y-m-d H:i:s'))->setTimezone(get_option('default_timezone'))->addHour(24);
                // $save_agent =   User::create([
                //                     'email'                 => $data['email'],
                //                     'user_type'             => $data['user_type'],
                //                     // 'access_token_validity' => $new_time,
                //                 ]);

                // save the newly added package for that user

            $save_package = UserPackage::create([
                                'user_id'       => $save_agent->id,
                                'packages_id'   => $data['plan_id'],
                                'expiry_date'   => $expiry_date
                            ]);
                
            User::where('id',$save_agent->id)->update([
                            'current_active_plan_id'    => $save_package->id,
                        ]);
            
            // when payment gateway implemented in sucess of payment we will redirect user to register page.
            return array('status' => 1,'email'=>$data['email'],'token'=>encrypt($save_agent->id),'plan_id'=>$data['plan_id']);
        } else {
            if($users['user_type'] == 'developer') {
                return array('status' => 1,'email'=>$users['email'],'token'=>encrypt($users['id']),'plan_id'=>5);
            }
        }
        // } else {
            
        //     return array('status' => 1,'email'=>$email['email'],'token'=>encrypt($email['id']),'plan_id'=>$data['plan_id']);
        // }
    }

    public function insertAccessKey(Request $request) 
    { 
        // to check if access key entered by developer at the time of registration is correct
        $rules = [
            'email'                 => 'required|email',
            'admin_access_token'    => 'required',
        ];
        // validate the input fields
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Validation failed
            return array('error'=> $validator->messages(),'plan_id'=> $request->plan_id);
        }
        
        // get user info with this id
        $user = User::with('user_package')->whereEmail($request->email)->first();
        if(!$user) {

            return array('status'=>2,'error'=>"This email id is not registered.");
        }
        
        // if agent is accessing developer account or vice versa
        if($user['user_package']['packages_id'] != $request->plan_id) {

            return array('status'=>3,'error'=>"You have not purchased this plan.",'plan_id'=> $request->plan_id);
        }

        // if access key entered by developer is not equal to access key sent by admin
        if($user['admin_access_key'] != $request->admin_access_token) {
            
            return array('status'=>3,'error'=>"Wrong Access Key.",'plan_id'=>$request->plan_id);
        }

        // if developer responds after 24 hours
        if($user['user_type'] == 'developer' && ($user['access_token_validity'] < Carbon::now()->setTimeZone(get_option('default_timezone')) || empty($user['admin_access_key']) || $user['admin_access_key'] === null || $user['token_expired'] == '1')) {

            return array('status'=>3,'error'=>"Your access token is expired. Please register again.",'plan_id'=> $request->plan_id);
        }
        
        // if everything is ok, developer is sent to registration page with email and user id 
        return array('status'=>1,'email'=>$user['email'],'token'=>encrypt($user['id']));
    }

    public function developerContactUs(Request $request) 
    {
        $title = 'Developer Contact Us';
        $id = decrypt($request->id);
        
        $user = User::whereId($id)->where('admin_access_key','=',null)->first();

        // get the contact us details
        $posts = Post::where('type','contact-us')->first();
        $posts = json_decode(json_encode($posts),true);

        // to check if request is for developer registartion or for a new apartment request
        if(isset($request->value) && !empty($request->value)) {
            $value = $request->value;
        } else {
            $value = 'registration';
        }
        return view('agent.developer_contact_us',compact('title','user','posts','value'));
    }

    public function saveContactUs(Request $request) 
    {
        $rules = [
            'person_to_address'     => 'required|max:255',
            'company_name'          => 'required|max:255',
            'contact_number'        => 'required|numeric',
            'other_contact'         => 'required|max:255',
            'cost'                  => 'required|numeric',
            'short_description'     => 'required|min:80',
        ];
        
        $this->validate($request, $rules);
        
        $data = $request->all();
        $contact = DeveloperContact::create($data);
        if($request->type == 'registration') {
            // for registration process
            return redirect(route('packages'))->with('success','Thanks for choosing us. We will contact you within 12 hours.');
        } else {
            // for a new apartment requests
            return redirect(route('developer_properties_list'))->with('success','Your request is successfully submitted. We will contact you within 12 hours.');
        }
    }

    public function updatePackage(Request $request) // not in use now as agent cannot upgrade his package
    {   
        $data = $request->all();

        // get the expiry date for the purchased package
        if( date('d') == 31 || (date('m') == 1 && date('d') > 28)){
            $date = strtotime('last day of next month');
        } else {
            $date = strtotime('+1 months');
        }
        $expiry_date = date('Y-m-d', $date);

        // save user's package
        $save_plan = UserPackage::create([
                        'user_id'       => $data['login_user'],
                        'packages_id'   => $data['plan_id_m'],
                        'expiry_date'   => $expiry_date
                    ]);

        // update agent's package id
        $up_agent =   User::where('id',$data['login_user'])->update([
                            'current_active_plan_id'    => $save_plan->id
                        ]);

        // when payment gateway implemented in success of payment we will redirect user to register page.
        if($data['plan_expire_or_not'] != 0)
        {
            return  redirect::back()->with('success','Package update successfully.');

        }else{
            return array('status'=> 1);
        }
    }

    // payment processes for registration for agent
    public function registerPayment(Request $request) 
    {
        $id = decrypt($request->id);
        $user = User::whereId($id)->first();
        
        if(@$user->user_package->payment_id != 0){
            return redirect(url('user/payment_checkout'.'/'.$user->user_package->payment->local_transaction_id));
        } 

        $transaction_id = 'tran_'.time().str_random(6);
        while( ( Payment::whereLocalTransactionId($transaction_id)->count() ) > 0) {
            $transaction_id = 'reid'.time().str_random(5);
        }
        $transaction_id = strtoupper($transaction_id);
        $currency = get_option('currency_sign');

        // save initial data before success
        $payments_data = [
                'ad_id'                 => 0,
                'user_id'               => $id,
                'amount'                => $user->user_package->package->price,
                'payment_method'        => 'stripe',
                'status'                => 'initial',
                'currency'              => $currency,
                'local_transaction_id'  => $transaction_id,
                'charge_payment_method' => $user->charge_payment_method
        ];
        
        $created_payment = Payment::create($payments_data);

        return redirect(url('user/payment_checkout'.'/'.$created_payment->local_transaction_id));
    }

    public function checkout($transaction_id)
    {
        $payment = Payment::whereLocalTransactionId($transaction_id)->whereStatus('initial')->first();
        
        $title = trans('app.checkout');
        
        if ($payment){
            return view('agent.register_checkout', compact('title','payment'));
        }

        // if initial payment is already done
        if(empty($payment)) {
            return view('admin.error.invalid_transaction', compact('title','payment'));
        }
        return view('admin.error.invalid_transaction', compact('title','payment'));
    }

    public function chargePayment(Request $request,$transaction_id) 
    {
        // send user to this route if he proceeds with payment charge (payment will be charged for agent only)
        $payment = Payment::whereLocalTransactionId($transaction_id)->whereStatus('initial')->first();
        $ad = $payment->ad;

        //Determine which payment method is this
        if ($payment->payment_method == 'paypal') {

            // PayPal settings
            $paypal_action_url = "https://www.paypal.com/cgi-bin/webscr";
            if (get_option('enable_paypal_sandbox') == 1)
                $paypal_action_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";

            $paypal_email = get_option('paypal_receiver_email');
            $return_url = url('user/payment_success_url'.'/'.$transaction_id);
            $cancel_url = url('user/payment_checkout'.'/'.$transaction_id);
            $notify_url = url('user/paypal_notify_url'.'/'.$transaction_id);

            $item_name = "Registration";

            // Check if paypal request or response
            $querystring = '';

            // Firstly Append paypal account to querystring
            $querystring .= "?business=".urlencode($paypal_email)."&";

            // Append amount& currency (£) to quersytring so it cannot be edited in html
            //The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
            $querystring .= "item_name=".urlencode($item_name)."&";
            $querystring .= "amount=".urlencode($payment->amount)."&";
            $querystring .= "currency_code=".urlencode($payment->currency)."&";

            $querystring .= "first_name=".urlencode(Auth::user()->first_name)."&";
            $querystring .= "last_name=".urlencode(Auth::user()->last_name)."&";
            $querystring .= "payer_email=".urlencode(Auth::user()->email)."&";
            $querystring .= "item_number=".urlencode($payment->local_transaction_id)."&";

            //loop for posted values and append to querystring
            foreach(array_except($request->input(), '_token') as $key => $value){
                $value = urlencode(stripslashes($value));
                $querystring .= "$key=$value&";
            }

            // Append paypal return addresses
            $querystring .= "return=".urlencode(stripslashes($return_url))."&";
            $querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
            $querystring .= "notify_url=".urlencode($notify_url);

            // Append querystring with custom field
            //$querystring .= "&custom=".USERID;

            // Redirect to paypal IPN
            header('location:'.$paypal_action_url.$querystring);
            exit();

        }elseif ($payment->payment_method == 'stripe'){

            // if payment process is stripe, it will use script and we will send response in ajax format
            $stripeToken = $request->stripeToken;
            \Stripe\Stripe::setApiKey(get_stripe_key('secret'));
            // Create the charge on Stripe's servers - this will charge the user's card
            try {
                $user = User::where('id',$payment->user_id)->first();
                $user_packages = UserPackage::where('id',$user->current_active_plan_id)->first();
                
                if($payment->charge_payment_method == 'charge_automatically'){

                    //recurring payment
                    $stripe_user = User::whereId($payment->user_id)->first();
                    $stripe_email = $stripe_user->email;
                    $customer = \Stripe\Customer::create(array(
                        "email" => $stripe_email, // amount in cents, again
                        "source" => $stripeToken
                    ));
                    
                    $subscribed = \Stripe\Subscription::create(array(
                        "customer" => $customer->id, // amount in cents, again
                        "items" => [
                            [
                                "plan" => $user_packages->package->stripe_product_id
                            ]
                        ]
                    ));

                    if($subscribed->status == 'active'){

                        //$amount  =  $subscription->plan->amount;
                        // $payment->stripe_start_date = date("Y-m-d H:i:s", $subscription->current_period_start);
                        // $payment->stripe_end_date  = date("Y-m-d H:i:s", $subscription->current_period_end);
                        $payment->stripe_start_date = date("Y-m-d", $subscribed->current_period_start);
                        $payment->stripe_end_date  = date("Y-m-d", $subscribed->current_period_end);
                        $payment->payment_created  = date("Y-m-d", $subscribed->created);
                        $payment->stripe_customer_id  = $subscribed->customer;
                        $payment->stripe_product_nickname = $subscribed->plan->nickname;
                        $payment->stripe_product_subscribe_name = $subscribed->plan->product;

                        $subscribed_id = $subscribed->id;
                        $plan_id = $subscribed->plan->id;

                        $payment->status = 'success';
                        $payment->charge_id_or_token = $subscribed_id;
                        $payment->plan_id = $plan_id;
                        $payment->charge_payment_method = 'charge_automatically';
                        //$payment->description = $subscribed->description;
                        //$payment->payment_created = $subscribed->created;
                        $payment->save();

                        // save added credit to the user_credits table

                        // make a unique access token for user (for developer there is another process and access token is sent by Admin after approving)

                        $access_token = md5(uniqid($payment->user_id, true));
                        // send email to user regarding this token
                        // ***********mail send hide due to ENV settings. change env settings for mail
                        // get user details
                        
                        UserPackage::where('packages_id',$user->user_package->packages_id)->where('payment_id',0)->where('user_id',$user->id)->update(['payment_id'=>$payment->id]);

                        $msg = '';
                        $email = $user->email;
                        $regirter_link = url('/packages/').'/'.$user->email;
                        try{
                            // send registration access key to user through email
                            Mail::send('emails.developer_contact_us_reply', ['user' => $email,'message_reply'=> 'no_message','access_key' => $access_token,'type' =>'agent_registration','register_link' => $regirter_link], function ($m) use ($email) {
                                $m->from(get_option('email_address'), get_option('site_name'));
                                $m->to($email)->subject('Agent Registration');
                            });
                            $msg = trans('app.reset_password_sent');
                        }catch (\Exception $exception){
                            $msg = $exception->getMessage();
                            return back()->with('error', $msg);
                        }
                        // send email to user regarding this token

                        $new_time = Carbon::parse(date('Y-m-d H:i:s'))->setTimezone(get_option('default_timezone'))->addHour(24);
                        // update user package (i.e. active plan)
                        User::where('id',$payment->user_id)->update([
                                    'admin_access_key'          => $access_token,
                                    'access_token_validity'     => $new_time,
                                    'stripe_customer_id'        => $subscribed->customer,
                                    'plan_active_status'        => '1',
                                    
                        ]);

                        return redirect(url('/packages'))->with('success', 'Payment has been made successfully. Access key has been sent to you in your email.');
                        // return ['success'=>1, 'msg'=> trans('app.payment_received_msg')];
                    } else {

                        return redirect(url('/packages'))->with('error', 'Oops... Something went wrong.');
                        // return ['success'=>0, 'msg'=> trans('app.payment_received_msg')];
                    }
                } else {
                    $charge = \Stripe\Charge::create(array(
                        "amount" => ($payment->amount * 100), // amount in cents, again
                        "currency" => $payment->currency,
                        "source" => $stripeToken,
                        "description" => "Registration"
                    ));
                    
                    if ($charge->status == 'succeeded'){

                        $payment->status = 'success';
                        $payment->charge_id_or_token = $charge->id;
                        $payment->description = $charge->description;
                        $payment->charge_payment_method = 'manual';
                        $payment->payment_created = date("Y-m-d", $charge->created);
                        // add one month to current date for package expiry
                        if( date('d') == 31 || (date('m') == 1 && date('d') > 28)){
                            $date = strtotime('last day of next month');
                        } else {
                            $date = strtotime('+1 months');
                        }

                        $expiry_date = date('Y-m-d', $date);
                        $payment->stripe_start_date = date("Y-m-d", $charge->created);
                        $payment->stripe_end_date = $expiry_date;
                        $payment->save();

                        // save added credit to the user_credits table

                        // make a unique access token for user (for developer there is another process and access token is sent by Admin after approving)

                        $access_token = md5(uniqid($payment->user_id, true));
                        // send email to user regarding this token
                        // ***********mail send hide due to ENV settings. change env settings for mail
                        // get user details

                        UserPackage::where('packages_id',$user->user_package->packages_id)->where('payment_id',0)->where('user_id',$user->id)->update(['payment_id'=>$payment->id,'expiry_date'=>$expiry_date]);
                        
                        $msg = '';
                        $email = $user->email;
                        $regirter_link = url('/packages/').'/'.$user->email;
                        try{
                            // send registration access key to user through email
                            Mail::send('emails.developer_contact_us_reply', ['user' => $email,'message_reply'=> 'no_message','access_key' => $access_token,'type' =>'agent_registration','register_link' => $regirter_link], function ($m) use ($email) {
                                $m->from(get_option('email_address'), get_option('site_name'));
                                $m->to($email)->subject('Agent Registration');
                            });
                            $msg = trans('app.reset_password_sent');
                        }catch (\Exception $exception){
                            $msg = $exception->getMessage();
                            return back()->with('error', $msg);
                        }
                        // send email to user regarding this token

                        $new_time = Carbon::parse(date('Y-m-d H:i:s'))->setTimezone(get_option('default_timezone'))->addHour(24);
                        // update user package (i.e. active plan)
                        User::where('id',$payment->user_id)->update([
                                    'admin_access_key'          => $access_token,
                                    'access_token_validity'     => $new_time,
                                    'charge_payment_method'     => 'manual',
                                    'plan_active_status'        => '1',

                        ]);

                        //Set publish ad by helper function
                        return redirect(url('/packages'))->with('success', 'Payment has been made successfully. Access key has been sent to you in your email.');
                        // return ['success'=>1, 'msg'=> trans('app.payment_received_msg')];
                        // return redirect::to('agents/credits-list/')->with('success',trans('app.payment_received_msg'));
                    }    
                }              
                
            } catch(\Stripe\Error\Card $e) {
                // The card has been declined
                $payment->status = 'declined';
                $payment->description = trans('app.payment_declined_msg');
                $payment->save();
                return ['success'=>0, 'msg'=> 'Some error occured. Please try again later.'];
            }
        }

        return ['success'=>0, 'msg'=> trans('app.error_msg')];
    }

    public function paymentSuccess(Request $request, $transaction_id)
    {
        // if success url is redirected from paypal payment page
        /**
         * Check is this transaction paid via paypal IPN
         */
        $transaction_check = Payment::whereLocalTransactionId($transaction_id)->first();
        if ($transaction_check){
            if($transaction_check->status == 'success'){
                return redirect(url('/packages'))->with('success', trans('app.payment_received_msg'));
            }elseif ($transaction_check->status == 'declined'){
                return redirect(url('/packages'))->with('error', trans('app.payment_declined_msg'));
            }
        }

        //Start original logic

        $payment = Payment::whereLocalTransactionId($transaction_id)->whereStatus('initial')->first();

        $paypal_action_url = "https://www.paypal.com/cgi-bin/webscr";
        if (get_option('enable_paypal_sandbox') == 1)
            $paypal_action_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";

        // STEP 1: read POST data
        // Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
        // Instead, read raw POST data from the input stream.
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode ('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
        // read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

        // STEP 2: POST IPN data back to PayPal to validate
        $ch = curl_init($paypal_action_url);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        // In wamp-like environments that do not come bundled with root authority certificates,
        // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set
        // the directory path of the certificate as shown below:
        // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
        if( !($res = curl_exec($ch)) ) {
            // error_log("Got " . curl_error($ch) . " when processing IPN data");
            curl_close($ch);
            exit;
        }
        curl_close($ch);

        // STEP 3: Inspect IPN validation result and act accordingly
        if (strcmp ($res, "VERIFIED") == 0) {

            //Payment success
            $payment->status = 'success';
            $payment->charge_id_or_token = $request->txn_id;
            $payment->description = $request->item_name;
            $payment->payer_email = $request->payer_email;
            $payment->payment_created = strtotime($request->payment_date);
            $payment->save();

            // send access token mail to the agent after successful registration
            $access_token = md5(uniqid($payment->user_id, true));
            
            // get user details
            $user = User::whereId($payment->user_id)->first();

            $msg = '';
            $email = $user->email;
            $regirter_link = url('/packages/').'/'.$user->email;
            try{
                Mail::send('emails.developer_contact_us_reply', ['user' => $email,'message_reply'=> 'no_message','access_key' => $access_token,'type' =>'agent_registration','register_link' => $regirter_link], function ($m) use ($email) {
                    $m->from(get_option('email_address'), get_option('site_name'));
                    $m->to($email)->subject('Agent Registration');
                });
                $msg = trans('app.reset_password_sent');
            }catch (\Exception $exception){
                $msg = $exception->getMessage();
                return back()->with('error', $msg);
            }
            // send email to user regarding this token

            $new_time = Carbon::parse(date('Y-m-d H:i:s'))->setTimezone(get_option('default_timezone'))->addHour(24);
            // update user package (i.e. active plan)
            User::where('id',$payment->user_id)->update([
                        'admin_access_key'          => $access_token,
                        'access_token_validity'     => $new_time,
            ]);

            //Set publish ad by helper function

            return redirect(url('/packages'))->with('success', 'Payment has been made successfully. Access key has been sent to you in your email.');

        } else if (strcmp ($res, "INVALID") == 0) {

            $payment->status = 'declined';
            $payment->description = trans('app.payment_declined_msg');
            $payment->save();
            return redirect(url('/packages'))->with('error', trans('app.payment_declined_msg'));
        }
    }

    /**
        * @param Request $request
        * @param $transaction_id
        * @return mixed
        *
        * Ipn notify, receive from paypal
    */
    public function paypalNotify(Request $request, $transaction_id)
    {
        // if payment is sent to paypal notify url while registration
        $payment = Payment::whereLocalTransactionId($transaction_id)->whereStatus('initial')->first();
        $ad = $payment->ad;

        $paypal_action_url = "https://www.paypal.com/cgi-bin/webscr";
        if (get_option('enable_paypal_sandbox') == 1)
            $paypal_action_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";

        // STEP 1: read POST data
        // Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
        // Instead, read raw POST data from the input stream.
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode ('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
        // read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

        // STEP 2: POST IPN data back to PayPal to validate
        $ch = curl_init($paypal_action_url);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        // In wamp-like environments that do not come bundled with root authority certificates,
        // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set
        // the directory path of the certificate as shown below:
        // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
        if( !($res = curl_exec($ch)) ) {
            // error_log("Got " . curl_error($ch) . " when processing IPN data");
            curl_close($ch);
            exit;
        }
        curl_close($ch);

        // STEP 3: Inspect IPN validation result and act accordingly
        if (strcmp ($res, "VERIFIED") == 0) {

            //Payment success, we are ready to live our ads
            $payment->status = 'success';
            $payment->charge_id_or_token = $request->txn_id;
            $payment->description = $request->item_name;
            $payment->payer_email = $request->payer_email;
            $payment->payment_created = strtotime($request->payment_date);
            $payment->save();

            //Set publish ad by helper function
            //ad_status_change($ad->id, 1);

            return redirect(url('/packages'))->with('success', 'Payment has been made successfully. Access key has been sent to you in your email.');

        } else if (strcmp ($res, "INVALID") == 0) {

            $payment->status = 'declined';
            $payment->description = trans('app.payment_declined_msg');
            $payment->save();
            return redirect(url('/packages'))->with('error', trans('app.payment_declined_msg'));
        }

    }
    // payment process for registartion of agent
    
    // public function panorama() {
    //     return view('property.panorama');
    // }

    public function reccuring_payment_cron(){
        $allUser = User::whereUserType('user')->where('current_active_plan_id','!=',0)->get(); 
        
        if(!empty($allUser)){
            foreach ($allUser as $key => $value) {
                // prx($user->user_package);
                $user = User::whereId($value->id)->whereUserType('user')->first();
                if(!empty($user->user_package)){
                    $today = Carbon::today();
                    
                    $expiryDate = $user->user_package->expiry_date;
                    $package_interval = $user->user_package->package->period;
                    
                    if (Carbon::parse($expiryDate)->gt(Carbon::yesterday())){
                        
                        // if $expiryDate > now (gt is greater than, gte is greater than or equal, etc)
                        // not expired
                        User::where('id',$user->id)->update([
                                                'plan_active_status'  => '1',
                                                'expiry_message'  => '',
                                            ]);

                        
                    } else {

                        
                        // if $expiryDate < now (gt is greater than, gte is greater than or equal, etc)
                        // expired
                        if($user->charge_payment_method != 'manual' && !empty($user->user_package->payment->stripe_customer_id)) {
                            \Stripe\Stripe::setApiKey(get_stripe_key('secret'));
                            $cu = \Stripe\Customer::Retrieve(
                                [ 
                                    'id' => $user->user_package->payment->stripe_customer_id,
                                ]
                            );

                            if(!empty($cu->subscriptions->data)){
                                $current_period_start  = date('Y-m-d',$cu->subscriptions->data[0]->current_period_start);
                                $current_period_end  = date('Y-m-d',$cu->subscriptions->data[0]->current_period_end);

                                $current_period_end_created = new Carbon($current_period_end);
                                $now = Carbon::now();
                                
                                // $difference = ($created->diff($now)->days == 2)
                                //     ? 'today'
                                //     : $created->diffForHumans($now);
                                   
                                if($current_period_end_created->diff($now)->days > 0){
                                    
                                    User::where('id',$user->id)->update([
                                                    'plan_active_status'  => '0',
                                                    'expiry_message'  => 'Please repayment for continue this service.',
                                                    
                                                ]);
                                } else {
                                     

                                    $subscription_id = $cu->subscriptions->data[0]->id;
                                    $charge_payment_method = $cu->subscriptions->data[0]->billing;
                                    $payment_created = $cu->subscriptions->data[0]->created;
                                    $customer_id = $cu->subscriptions->data[0]->customer;
                                    $plan_id = $cu->subscriptions->data[0]->plan->id;
                                    $product_id = $cu->subscriptions->data[0]->plan->product;

                                    $payment = Payment::create([
                                                        'stripe_start_date' => $current_period_start,
                                                        'stripe_end_date' => $current_period_end,
                                                        'stripe_customer_id' => $customer_id,
                                                        'charge_id_or_token' => $subscription_id,
                                                        'plan_id'   => $plan_id,
                                                        'charge_payment_method' => $charge_payment_method,
                                                        'local_transaction_id' => $user->user_package->payment->local_transaction_id,
                                                        'user_id' => $user->id,
                                                        'currency' => 'USD',
                                                        'status' => 'success',
                                                        'payment_method' => 'stripe',
                                                    ]);
                                    $user_packages = UserPackage::create([
                                                                'user_id' => $user->id,
                                                                'packages_id' => $user->user_package->packages_id,
                                                                'payment_id' => $payment->id,
                                                                'expiry_date' => $current_period_end
                                                        ]);
                                    User::where('id',$user->id)->update([
                                                    'current_active_plan_id' => $user_packages->id,
                                                    'charge_payment_method'  => $charge_payment_method,
                                                    'plan_active_status'  => '1',
                                                    'stripe_customer_id'        => $customer_id,
                                                ]);
                                }
                                
                            } else {
                                User::where('id',$user->id)->update([
                                                'plan_active_status'  => '0',
                                                'expiry_message'  => 'Please transfer valid amount in your Bank to continue this service.'
                                            ]);
                            }
                        } else {
                            // equal manual
                            $created = new Carbon($expiryDate);
                            $now = Carbon::now();
                            // $difference = ($created->diff($now)->days == 2)
                            //     ? 'today'
                            //     : $created->diffForHumans($now);
                           
                            if($created->diff($now)->days > 1){
                                
                                User::where('id',$user->id)->update([
                                                'plan_active_status'  => '0',
                                                'expiry_message'  => 'Please repay to continue this service.',
                                                
                                            ]);
                            }        
                        }
                    }
                }   
            }
        }
    }

}
