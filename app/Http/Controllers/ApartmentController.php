<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Intervention\Image\Facades\Image;

use App\Ad;
use App\User;
use App\Post;
use App\City;
use App\State;
use App\Media;
use App\Slider;
use App\Package;
use App\Country;
use App\UserMeta;
use App\Category;
use App\UserPackage;
use App\PropertyType;
use App\Notification;
use App\Contact_query;
use App\ApartmentUnit;
use App\AgentActivity;
use App\DeveloperContact;
use App\Tower;
use App\TowerUnitFloor;
use Carbon\Carbon;
use Crypt;

class ApartmentController extends Controller
{
    // apartment modules

    //tower module
    public function apartmentTower($ad_id)
    {

        $title = "";
        $meta_data = $this->meta_data(Auth::user()->id);
        $current_package = $this->current_package(Auth::user()->id);
        $propety_arr = ['apartments','house']; // types of property available
        $title = "Developer Towers";
        $apartment_tower = Tower::where('ad_id',Crypt::decrypt($ad_id))
                              ->where('trash',0)
                              ->orderBy('id','DESC')
                              ->get()
                              ->toArray();
        $towerCount = count($apartment_tower);
        $no_of_tower = Ad::where('id',Crypt::decrypt($ad_id))->value('no_of_tower');

        // to get current time for default timezone i.e. Asia/Kolkata
        $current_time = Carbon::parse(date('Y-m-d H:i:s'))->setTimeZone(get_option('default_timezone'));

        return view('property.apartment.tower.towerList',compact('title','meta_data','current_package','apartment_tower','towerCount','no_of_tower','ad_id'));
    }

    //tower module
    public function createTower($ad_id = '', $tower_id = '')       // open create apartment page
    {

        $title = "Create Tower";
        $user_id = Auth::user()->id;
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();
        $countries = Country::all();
        $previous_states = State::where('country_id', old('country'))->get();
        $previous_cities = City::where('state_id', old('state'))->get();
        // delete if there is any media not attached with any house yet
        deleteUnlinkedMedia($user_id,'apartments','apartment_images');
        // delete if there is any media not attached with any house yet
        if(!empty($tower_id))
        {
            $tower = Tower::whereId(Crypt::decrypt($tower_id))->first();
        } else {
            $tower = array();
        }

        return view('property.apartment.tower.create',compact('title','user_id','ads_images','countries','previous_states','previous_cities','ad_id','tower_id','tower'));

    }
    //tower module
    public function addApartmentTower(Request $request)       // add a new apartment
    {

        $rules = [
            'name'                  => 'required|max:255',
            //'no_of_floor'           => 'required|numeric',
            'area'                  => 'max:100',
            //'description'           => 'min:80',
            // 'address'               => 'required',
            // 'country'               => 'required',
            // 'state'                 => 'required',
            // 'city'                  => 'required',
            // 'zip'                   => 'required|numeric|digits:6'
        ];

        // if(empty($request->ad_id)) {

        //     $rules['cover_photo']           = 'required';
        //     $rules['house_photos_check']    = 'required';
        // }

        $this->validate($request, $rules);
        $title   = $request->name;

        $slug    = unique_slug($title);
        $user_id = Auth::user()->id;

        $cover_photo_name = '';
        // if ($request->hasFile('cover_photo')){
        //     $rules1 = ['cover_photo'=>'mimes:jpeg,jpg,png'];
        //     $validator = Validator::make($request->all(), $rules1);
        //     if ($validator->fails()) {
        //         return redirect::back()->withErrors($validator)->withInput();
        //     }
        //     $image = $request->file('cover_photo');
        //     $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
        //     $resized_thumb = Image::make($image)->resize(700, 400)->stream();
        //     $resized_cover_thumb = Image::make($image)->resize(320, 213)->stream();
        //     $cover_photo_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
        //     $imageFileName = 'uploads/apartmentImages/'.$cover_photo_name;
        //     $resized_thumb_name = 'uploads/apartmentImages/thumbs/'.$cover_photo_name;
        //     //Upload original image
        //     $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');
        //     if($is_uploaded) {

        //         current_disk()->put($resized_thumb_name, $resized_cover_thumb->__toString(), 'public');
        //     }

        // } else {

        //     if(isset($request->ad_id) && !empty($request->ad_id)) {

        //        $cover_photo_name = Tower::whereId($request->ad_id)->value('cover_photo');
        //     }
        // }

        $tower_data = [
                    'title'             => ucfirst($request->name),
                    'slug'              => $slug,
                    'ad_id'             => Crypt::decrypt($request->ad_id),
                    'area'              => $request->area,
                    //'no_of_tower'       => $request->no_of_tower,
                   // 'description'       => ucfirst($request->description),
                    // 'type'              => $request->property_type,
                    // 'country_id'        => $request->country,
                    // 'state_id'          => $request->state,
                    // 'city_id'           => $request->city,
                    // 'address'           => $request->address,
                    // 'latitude'          => $request->latitude,
                    // 'longitude'         => $request->longitude,
                    //'cover_photo'       => $cover_photo_name,
                   // 'status'            => 1,
                    //'property_type_id'  => 1,
                    //'user_id'           => $user_id,
                    //'zip'               => $request->zip,

                ];
        if(isset($request->id) && !empty($request->id)) {
            // tower_id
            $id = $request->id;
            $tower_created = Tower::where('id',Crypt::decrypt($request->id))->update($tower_data);
            //apartment_id
            $recent_apartment_id = $request->ad_id;
            $message = 'Data updated Successfully';
        } else {

            $tower_created = Tower::create($tower_data);
            $recent_apartment_id = Crypt::encrypt($tower_created->ad_id);
            $id = Crypt::encrypt($tower_created->id);
            $message = 'Data Saved Successfully';

        }
        if ($tower_created){

            //Attach all unused media with this ad

            Media::whereUserId($user_id)->whereAdId(0)->whereRef('apartments')->whereUploadedFor('apartment_images')->update(['ad_id'=>Crypt::decrypt($request->ad_id)]);

            // add notification regarding new apartment created here

            $notify = [
                    'user_id'       =>  $user_id,
                    'ad_id'         =>  Crypt::decrypt($request->ad_id),
                    'type'          =>  'add_property',
                    'type_text'     =>  ' has added a new Apartment ',
                    'message'       =>  ucfirst(Auth::user()->name).' has added a new Apartment '.ucfirst($request->name)
            ];

            Notification::create($notify);
            //return redirect::to('developers/properties-list')->with('success','Data Saved Sucessfully.');
            // add notification regarding new apartment created here
            $route = 'developers/create-floor/' . $recent_apartment_id . '/' . $id;
            return redirect::to($route)->with('success',$message);
        }
    }
    // edit and previous tower modules (setting for developer)

    public function editApartmentTower($ad_id,$type = '')    // open edit apartment page or previous from create floor page
    {
        $ad_id = getDecrypted($ad_id);
        $apartment = Ad::whereId($ad_id)->with(['media_img'=>function($query) {
                                                $query->whereUploadedFor('apartment_images');
                                            }])->orderBy('id','DESC')->first();

        $title = "Edit Apartment";
        $user_id = Auth::user()->id;
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();
        $countries = Country::all();
        $previous_states = State::where('country_id', $apartment->country_id)->get();
        $previous_cities = City::where('state_id', $apartment->state_id)->get();

        // delete if there is any media not attached with any house yet
        deleteUnlinkedMedia($user_id,'apartments','apartment_images');
        // delete if there is any media not attached with any house yet

        if(isset($type) && !empty($type) && $type == 'edit') {

            return view('property.apartment.tower.edit',compact('title','user_id','ads_images','countries','previous_states','previous_cities','apartment'));
        } else {

            return view('property.apartment.tower.create',compact('title','user_id','ads_images','countries','previous_states','previous_cities','apartment'));
        }
    }

    public function apartmentTowerEdit(Request $request)     // edit the apartment data
    {

        $rules = [
            'name'                  => 'required|max:255',
            'no_of_tower'           => 'required|numeric',
            'description'           => 'required|min:80',
            'address'               => 'required',
            'country'               => 'required',
            'state'                 => 'required',
            'city'                  => 'required',
            'zip'                   => 'required|numeric|digits_between:3,6',
            'house_photos_check'    => 'required'

        ];

        $this->validate($request, $rules);
        $title   = $request->name;

        // $slug    = unique_slug($title);
        $user_id = Auth::user()->id;

        $cover_photo_name = '';
        if ($request->hasFile('cover_photo')){
            $rules1 = ['cover_photo'=>'mimes:jpeg,jpg,png'];
            $validator = Validator::make($request->all(), $rules1);
            if ($validator->fails()) {
                return redirect::back()->withErrors($validator)->withInput();
            }
            $image = $request->file('cover_photo');
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $resized_thumb = Image::make($image)->resize(700, 400)->stream();
            $resized_cover_thumb = Image::make($image)->resize(320, 213)->stream();
            $cover_photo_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
            $imageFileName = 'uploads/apartmentImages/'.$cover_photo_name;
            $resized_thumb_name = 'uploads/apartmentImages/thumbs/'.$cover_photo_name;
            //Upload original image
            $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');
            if($is_uploaded) {

                current_disk()->put($resized_thumb_name, $resized_cover_thumb->__toString(), 'public');
            }

        } else {

            $cover_photo_name = Ad::whereId($request->ad_id)->value('cover_photo');
        }

        $apartment_data = [
                    'title'             => ucfirst($request->name),
                    // 'slug'              => $slug,/
                    'no_of_tower'       => $request->no_of_tower,
                    'description'       => ucfirst($request->description),
                    'type'              => $request->property_type,
                    'country_id'        => $request->country,
                    'state_id'          => $request->state,
                    'city_id'           => $request->city,
                    'address'           => $request->address,
                    'latitude'          => $request->latitude,
                    'longitude'         => $request->longitude,
                    'cover_photo'       => $cover_photo_name,
                    'user_id'           => $user_id,
                    'zip'               => $request->zip,

                ];

        $apartment_created = Ad::whereId($request->ad_id)->update($apartment_data);
        $recent_apartment_id = $request->ad_id;

        if ($apartment_created){
            //Attach all unused media with this ad

            Media::whereUserId($user_id)->whereAdId(0)->whereRef('apartments')->whereUploadedFor('apartment_images')->update(['ad_id'=>$recent_apartment_id]);
            //return redirect::to('developers/edit-floors/'.\Crypt::encrypt($recent_apartment_id).'/edit')->with('success','Data Updated Sucessfully');
            return redirect::to('developers/properties-list')->with('success','Data Updated Sucessfully');
        } else {
            //return redirect::to('developers/edit-floors/'.\Crypt::encrypt($recent_apartment_id).'/edit')->with('success','Data Updated Sucessfully');
            return redirect::to('developers/properties-list')->with('success','Data Updated Sucessfully');
        }
    }
    // end tower
    public function createApartment($request_id = '')       // open create apartment page
    {

        $title = "Create Apartment";
        $user_id = Auth::user()->id;
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();
        $countries = Country::all();
        $previous_states = State::where('country_id', old('country'))->get();
        $previous_cities = City::where('state_id', old('state'))->get();

        if(isset($request_id) && !empty($request_id)) {

            $request_id = decrypt($request_id);
        } else {
            $request_id = '';
        }

        // delete if there is any media not attached with any house yet
        deleteUnlinkedMedia($user_id,'apartments','apartment_images');
        // delete if there is any media not attached with any house yet

        return view('property.apartment.create',compact('title','user_id','ads_images','countries','previous_states','previous_cities','request_id'));
    }

    public function addApartment(Request $request)       // add a new apartment
    {
        $rules = [
            'name'          => 'required|max:255',
            'no_of_tower'   => 'required|numeric',
            'description'   => 'required|min:80',
            'address'       => 'required',
            'country'       => 'required',
            'state'         => 'required',
            'city'          => 'required',
            'zip'           => 'required|numeric|digits_between:3,6'
        ];

        if(empty($request->ad_id)) {

            $rules['cover_photo']           = 'required';
            $rules['house_photos_check']    = 'required';
        }

        $this->validate($request, $rules);
        $title   = $request->name;

        $slug    = unique_slug($title);
        $user_id = Auth::user()->id;

        $cover_photo_name = '';
        if ($request->hasFile('cover_photo')){
            $rules1 = ['cover_photo'=>'mimes:jpeg,jpg,png'];
            $validator = Validator::make($request->all(), $rules1);
            if ($validator->fails()) {
                return redirect::back()->withErrors($validator)->withInput();
            }
            $image = $request->file('cover_photo');
            // prx($image);
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $resized_thumb = Image::make($image)->resize(700, 400)->stream();
            $resized_cover_thumb = Image::make($image)->resize(320, 213)->stream();
            $cover_photo_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
            $imageFileName = 'uploads/apartmentImages/'.$cover_photo_name;
            $resized_thumb_name = 'uploads/apartmentImages/thumbs/'.$cover_photo_name;
            //Upload original image
            $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');
            if($is_uploaded) {

                current_disk()->put($resized_thumb_name, $resized_cover_thumb->__toString(), 'public');
            }

        } else {

            if(isset($request->ad_id) && !empty($request->ad_id)) {

                $cover_photo_name = Ad::whereId($request->ad_id)->value('cover_photo');
            }
        }

        $apartment_data = [
                    'title'             => ucfirst($request->name),
                    'slug'              => $slug,
                    'no_of_tower'       => $request->no_of_tower,
                    'description'       => ucfirst($request->description),
                    'type'              => $request->property_type,
                    'country_id'        => $request->country,
                    'state_id'          => $request->state,
                    'city_id'           => $request->city,
                    'address'           => $request->address,
                    'latitude'          => $request->latitude,
                    'longitude'         => $request->longitude,
                    'cover_photo'       => $cover_photo_name,
                    'status'            => 1,
                    'property_type_id'  => 1,
                    'user_id'           => $user_id,
                    'zip'               => $request->zip,

                ];

        if(isset($request->ad_id) && !empty($request->ad_id)) {

            $apartment_created = Ad::whereId($request->ad_id)->update($apartment_data);
            $recent_apartment_id = $request->ad_id;
        } else {

            $apartment_created = Ad::create($apartment_data);
            $recent_apartment_id = $apartment_created->id;
        }

        if ($apartment_created){

            if(!empty($request->request_id)) {

                // the access token has been used
                DeveloperContact::whereId($request->request_id)->update(['token_expired'=>'1']);
                $request = DeveloperContact::whereId($request->request_id)->first();
            }

            //Attach all unused media with this ad

            Media::whereUserId($user_id)->whereAdId(0)->whereRef('apartments')->whereUploadedFor('apartment_images')->update(['ad_id'=>$recent_apartment_id]);

            // add notification regarding new apartment created here

            $notify = [

                    'user_id'       =>  $user_id,
                    'ad_id'         =>  $recent_apartment_id,
                    'type'          =>  'add_property',
                    'type_text'     =>  ' has added a new Apartment ',
                    'message'       =>  ucfirst(Auth::user()->name).' has added a new Apartment '.ucfirst($request->name)
            ];

            Notification::create($notify);
            return redirect::to('developers/properties-list')->with('success','Data Saved Sucessfully.');
            // add notification regarding new apartment created here
            //return redirect::to('developers/create-floor/'.\Crypt::encrypt($recent_apartment_id))->with('success','Data Saved Sucessfully');
        }
    }

    // edit and previous apartment modules (setting for developer)

    public function editApartment($ad_id,$type = '')    // open edit apartment page or previous from create floor page
    {
        $ad_id = getDecrypted($ad_id);

        $apartment = Ad::whereId($ad_id)->with(['media_img'=>function($query) {
                                                $query->whereUploadedFor('apartment_images');
                                            }])->orderBy('id','DESC')->first();

        $title = "Edit Apartment";
        $user_id = Auth::user()->id;
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();
        $countries = Country::all();
        $previous_states = State::where('country_id', $apartment->country_id)->get();
        $previous_cities = City::where('state_id', $apartment->state_id)->get();

        // delete if there is any media not attached with any house yet
        deleteUnlinkedMedia($user_id,'apartments','apartment_images');
        // delete if there is any media not attached with any house yet

        if(isset($type) && !empty($type) && $type == 'edit') {

            return view('property.apartment.edit',compact('title','user_id','ads_images','countries','previous_states','previous_cities','apartment'));
        } else {

            return view('property.apartment.create',compact('title','user_id','ads_images','countries','previous_states','previous_cities','apartment'));
        }
    }

    public function apartmentEdit(Request $request)     // edit the apartment data
    {

        $rules = [
            'name'                  => 'required|max:255',
            'no_of_tower'           => 'required|numeric',
            'description'           => 'required|min:80',
            'address'               => 'required',
            'country'               => 'required',
            'state'                 => 'required',
            'city'                  => 'required',
            'zip'                   => 'required|numeric|digits_between:3,6',
            'house_photos_check'    => 'required'

        ];

        $this->validate($request, $rules);
        $title   = $request->name;

        // $slug    = unique_slug($title);
        $user_id = Auth::user()->id;

        $cover_photo_name = '';
        if ($request->hasFile('cover_photo')){
            $rules1 = ['cover_photo'=>'mimes:jpeg,jpg,png'];
            $validator = Validator::make($request->all(), $rules1);
            if ($validator->fails()) {
                return redirect::back()->withErrors($validator)->withInput();
            }
            $image = $request->file('cover_photo');
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $resized_thumb = Image::make($image)->resize(700, 400)->stream();
            $resized_cover_thumb = Image::make($image)->resize(320, 213)->stream();
            $cover_photo_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
            $imageFileName = 'uploads/apartmentImages/'.$cover_photo_name;
            $resized_thumb_name = 'uploads/apartmentImages/thumbs/'.$cover_photo_name;
            //Upload original image
            $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');
            if($is_uploaded) {

                current_disk()->put($resized_thumb_name, $resized_cover_thumb->__toString(), 'public');
            }

        } else {

            $cover_photo_name = Ad::whereId($request->ad_id)->value('cover_photo');
        }

        $apartment_data = [
                    'title'         => ucfirst($request->name),
                    // 'slug'         => $slug,/
                    'no_of_tower'   => $request->no_of_tower,
                    'description'   => ucfirst($request->description),
                    'type'          => $request->property_type,
                    'country_id'    => $request->country,
                    'state_id'      => $request->state,
                    'city_id'       => $request->city,
                    'address'       => $request->address,
                    'latitude'      => $request->latitude,
                    'longitude'     => $request->longitude,
                    'cover_photo'   => $cover_photo_name,
                    'user_id'       => $user_id,
                    'zip'           => $request->zip,

                ];

        $apartment_created = Ad::whereId($request->ad_id)->update($apartment_data);
        $recent_apartment_id = $request->ad_id;

        if ($apartment_created){
            //Attach all unused media with this ad

            Media::whereUserId($user_id)->whereAdId(0)->whereRef('apartments')->whereUploadedFor('apartment_images')->update(['ad_id'=>$recent_apartment_id]);
            //return redirect::to('developers/edit-floors/'.\Crypt::encrypt($recent_apartment_id).'/edit')->with('success','Data Updated Sucessfully');
            return redirect::to('developers/properties-list')->with('success','Data Updated Sucessfully');
        } else {
            //return redirect::to('developers/edit-floors/'.\Crypt::encrypt($recent_apartment_id).'/edit')->with('success','Data Updated Sucessfully');
            return redirect::to('developers/properties-list')->with('success','Data Updated Sucessfully');
        }
    }

    // public function apartmentEdit($ad_id) {

    //     $ad_id = getDecrypted($ad_id);
    //     $apartment = Ad::whereId($ad_id)->with(['media_img'=>function($query) {
    //                                             $query->whereUploadedFor('apartment_images');
    //                                         }])->orderBy('id','DESC')->first();

    //     // prx($apartment);

    //     $title = "Edit Apartment";
    //     $user_id = Auth::user()->id;
    //     $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();
    //     $countries = Country::all();
    //     $previous_states = State::where('country_id', $apartment->country_id)->get();
    //     $previous_cities = City::where('state_id', $apartment->state_id)->get();

    //     return view('property.apartment.edit',compact('title','user_id','ads_images','countries','previous_states','previous_cities','apartment'));
    // }

    // apartment modules

    // floors modules

    public function createFloor($apartment_id,$tower_id)    // open create floor page
    {
        $title = "Create Floors";
        $user_id = Auth::user()->id;
        $ads_images = array();
        $tower = Tower::whereId(Crypt::decrypt($tower_id))->first();
        return view('property.apartment.tower.createFloors',compact('user_id','ads_images','apartment_id','tower_id','tower'));
    }

    // public function appendFloorUnits($no_of_floors){

    //     $user_id = Auth::user()->id;
    //     return view('property.append_floors', compact('no_of_floors'));
    // }

    public function addFloor(Request $request)   // add a new floor
    {
        $data = $request->all();

        $getResponse = $this->floorAddEditCommon($request->apartment_id,$request->tower_id,$request->no_of_floor,$request->no_of_unit);

        // ApartmentFloor::whereAdId($request->apartment_id)->delete();
        // foreach($request->units as $key => $value) {

        //     $apartment_floors = [

        //         'ad_id'  => $request->apartment_id,
        //         'floor_no'      => $key,
        //         'no_of_units'   => $value
        //     ];
        //     ApartmentFloor::insert($apartment_floors);
        // }

        return redirect::to('developers/create-units/'.$request->tower_id .'/'.$request->apartment_id.'/'.\Crypt::encrypt($getResponse['unitId']));
    }

    // previous and edit page modules for floors

    public function editFloors($ad_id,$type = '')   // open the edit floor page and previous page for create unit previous link
    {
        $ad_id = getDecrypted($ad_id);

        $apartment = Ad::whereId($ad_id)->select('id','slug','floor','units')->first();
        $apartment_id = $ad_id;

        $user_id = Auth::user()->id;

        $ads_images = array();

        if(isset($type) && !empty($type) && $type == 'edit') {

            $title = "Edit Floors";
            return view('property.apartment.edit_floors',compact('user_id','ads_images','apartment_id','apartment','title'));
        } else {

            $title = "Create Floors";
            return view('property.apartment.createFloors',compact('user_id','ads_images','apartment_id','apartment','title'));
        }
    }

    public function floorEdit(Request $request)   // edit floor exixsting data
    {
        $this->floorAddEditCommon($request->apartment_id,$request->no_of_floors,$request->no_of_units);

        return redirect::to('developers/edit-units/'.\Crypt::encrypt($request->apartment_id).'/'.\Crypt::encrypt(1).'/'.\Crypt::encrypt(0))->with('success','Data Updated Sucessfully');
    }

    public function floorAddEditCommon($ad_id,$tower_id,$floors,$units)     // common function to add or edit floor
    {

        $ad_id = getDecrypted($ad_id);
        $tower_id = getDecrypted($tower_id);

        Tower::whereId($tower_id)->update(['no_of_floor' => $floors,'no_of_unit' => $units]);
        $tower = Tower::whereId($tower_id)->with('tower_apartment_unit')->select('no_of_floor','no_of_unit','id')->first();

        $no_of_total_unit = (int)$floors * (int)$units;
        if(count($tower->tower_apartment_unit) < $no_of_total_unit){
            $total_unit = 1;
            for ($i=1; $i <= (int)$floors ; $i++) {

                for ($j=1; $j <= (int)$units; $j++) {
                    $slug = unique_slug('title - ' . $total_unit,'ApartmentUnit');
                      $unit = ApartmentUnit::create([
                                            'ad_id' => $ad_id,
                                            'tower_id' => $tower_id,
                                            'apartment_floor_id' => $i,
                                            'apartment_unit_id'  => $total_unit,
                                            'slug'  => $slug,
                                            'title' => 'title - ' . $total_unit,
                                       ]);
                      $total_unit++;
                }
            }

            $unit = ApartmentUnit::where('ad_id', $ad_id)->where('tower_id', $tower_id)->orderBy('id','ASC')->value('id');
            // $no_of_total_unit = $no_of_total_unit - count($tower->tower_apartment_unit);
            // for($i = 1; $i <= $no_of_total_unit; $i++) {
            //     $unit = ApartmentUnit::create([
            //                                 'ad_id' => $ad_id,
            //                                 'tower_id' => $tower_id,
            //                                 'title' => 'title - ' . $i,
            //                            ]);
            // }
            // $response = array(
            //                         'no_of_total_unit' => $no_of_total_unit,
            //                         'unitId'  => $unit->id
            //                     );
            // reurn $response;
            $response = array(
                                        'no_of_total_unit' => $no_of_total_unit,
                                        'unitId'  => $unit
                                    );
            return $response;
        }  else {
            $unitId = ApartmentUnit::where('ad_id', $ad_id)->where('tower_id', $tower_id)->orderBy('id','ASC')->value('id');
            $response = array(
                                    'no_of_total_unit' => $no_of_total_unit,
                                    'unitId'  => $unitId
                            );
            return $response;
        }
    }

    // floor modules

    // unit modules

    public function createUnits($tower_id,$apartment_id,$unit_id)   // open create units page
    {

        $title = "Create Units";
        $user_id = Auth::user()->id;
        $ads_images = array();


        // floor id and unit id 1 mean there is no previous data user is redirected first tyme on page.

        // if maximum limit of 1 floor units reached or if all units filled

        //$floor_units = $this->checkApartmentFloorUnits($tower_id,$apartment_id,$floor_id,$unit_id);

        $apartment = Ad::whereId(getDecrypted($apartment_id))->select('id','slug','floor','units')->first();


        $unit_details = ApartmentUnit::withCount('media_img')->with('media_img')->whereAdId(getDecrypted($apartment_id))->whereId(getDecrypted($unit_id))->whereTowerId(getDecrypted($tower_id))->first();

        $unit_details_all = ApartmentUnit::withCount('media_img')->with('media_img')->whereAdId(getDecrypted($apartment_id))->whereTowerId(getDecrypted($tower_id))->orderBy('id','ASC')->get();
        // delete if there is any media not attached with any house yet
        deleteUnlinkedMedia($user_id,'units','unit_images');
        // delete if there is any media not attached with any house yet

        // if($floor_units['floor_id'] == 0) {

        //     return redirect::to('developers/properties-list')->with('success','Data Saved Sucessfully');
        // } else {

            return view('property.apartment.tower.createUnits',compact('user_id','ads_images','apartment','unit_details','title','tower_id','unit_details_all','unit_id'));
        //}
    }

    public function addUnits(Request $request)    // add a new unit
    {
        $data = $request->all();



        $rules = [
            // 'name'                  => 'required|max:255',
            // 'description'           => 'required|min:80',
            // 'no_of_bathrooms'       => 'required|max:10',
            // 'no_of_parking'         => 'required|max:10',
            // 'no_of_beds'            => 'required|max:10',
            // 'size'                  => 'required',
            // 'price'                 => 'required|max:10',
            // 'video_link'            => 'url',
            'unit_status'   => 'required',
        ];

        if($request->unit_status != 2 && $request->unit_status != "") {

            $rules['name']              = 'required|max:255';
            $rules['description']       = 'required|min:80';
            $rules['no_of_bathrooms']   = 'required|max:10';
            $rules['no_of_parking']     = 'required|max:10';
            $rules['no_of_bedrooms']    = 'required|max:10';
            $rules['size']              = 'required';
            $rules['price']             = 'required|max:10';
            // $rules['video_link']        = 'url';

            if(isset($request->apartment_unit_id) && empty($request->apartment_unit_id)) {

                $rules['house_photos_check'] = 'required';
            }
        }

        $this->validate($request, $rules);

        $title = $request->name;

        $slug    = unique_slug($title,'ApartmentUnit');
        $user_id = Auth::user()->id;


        // $video_url = '';
        // if(!empty($request->video_link)) {

        //     $video_url = $request->video_link;
        // }

        $units_data = [
                    'title'                 => ucfirst($request->name),
                    'ad_id'                 => Crypt::decrypt($request->apartment_id),
                    // 'apartment_floor_id'    => $request->floor_id,
                    // 'apartment_unit_id'     => $request->unit_id,
                    'slug'                  => $slug,
                    'description'           => ucfirst($request->description),
                    'price'                 => $request->price,
                    'beds'                  => $request->no_of_bedrooms,
                    'attached_bath'         => $request->no_of_bathrooms,
                    'no_of_parking'         => $request->no_of_parking,
                    'unit_type'             => 'sqft',
                    'square_unit_space'     => $request->size,
                    'status'                => 1,
                    'user_id'               => $user_id,
                    // 'video_url'             => $video_url,
                    'property_status'       => $request->unit_status,

                ];


        if(isset($request->apartment_unit_id) && !empty($request->apartment_unit_id)) {
            $units_created = ApartmentUnit::whereId($request->unit_id)->update($units_data);


            $apartments_all = ApartmentUnit::whereTowerId(Crypt::decrypt($data['tower_id']))->where('ad_id',Crypt::decrypt($data['apartment_id']))->orderBy('id','ASC')->get()->toArray();
            $unit_id = $data['unit_id'];
            $unit_temp_id = $data['unit_id'];

            if(!empty($apartments_all)){
                foreach ($apartments_all as $key => $value) {
                    if($value['id'] > $unit_id){
                        $unit_temp_id = $unit_id;
                        $unit_id = $value['id'];
                        break;
                    }
                }
            }
            $recent_unit_id = Crypt::decrypt($request->apartment_unit_id);
        } else {
            $units_created = ApartmentUnit::create($units_data);
            $recent_unit_id = $units_created->id;
        }

        //if ($units_created){
            //Attach all unused media with this ad
            Media::whereUserId($user_id)
                   ->whereAdId(0)
                   ->whereApartmentUnitId(0)
                   ->whereRef('units')
                   ->whereUploadedFor('unit_images')
                   ->update([
                        'ad_id'             =>  $request->apartment_id,
                        'apartment_unit_id' =>  $request->unit_id
                    ]);
            if($unit_temp_id == $unit_id){
                return redirect::to('agents/apartment-tower/' . $request->apartment_id)->with('success','Unit has been updated');
            } else {
                return redirect::to('developers/create-units/' . $request->tower_id . '/' . $request->apartment_id . '/' . Crypt::encrypt($unit_id))->with('success','Unit has been updated');
            }


        //}
    }

    // previous and edit page modules for units

    public function editPreviousUnits($ad_id,$floor_id,$unit_id,$type = '')    // open previous page for units
    {
        $title = 'Edit Units';

        $ad_id = getDecrypted($ad_id);
        $floor_id = getDecrypted($floor_id);
        $unit_id = getDecrypted($unit_id);

        $apartment_floors = Ad::whereId($ad_id)->orderBy('id','DESC')->value('floor');

        $selected_floor_unit = Ad::whereId($ad_id)->orderBy('id','DESC')->value('units');

        // selected unit is not first in that floor
        if($unit_id <= $selected_floor_unit && $unit_id > 1) {

            $unit_id = $unit_id-1;
        } else {

            // if first unit of any floor is selected, then show last unit of previous floor
            if($floor_id <= $apartment_floors && $floor_id > 1) {

                $floor_id = $floor_id-1;
                $unit_id  = Ad::whereId($ad_id)->orderBy('id','DESC')->value('units');
            }
        }

        $unit_details = ApartmentUnit::withCount('media_img')->with('media_img')->whereAdId($ad_id)->whereApartmentFloorId($floor_id)->whereApartmentUnitId($unit_id)->orderBy('id','DESC')->first();

        // data required for create units page

        $user_id = Auth::user()->id;
        $ads_images = array();
        $apartment = Ad::whereId($ad_id)->select('id','slug','floor','units')->first();
        $floor_units = [
                            'floor_id'  => $floor_id,
                            'unit_id'   => $unit_id
                       ];

        // data required for create units page

        // delete if there is any media not attached with any house yet
        deleteUnlinkedMedia($user_id,'units','unit_images');
        // delete if there is any media not attached with any house yet

        if(isset($type) && !empty($type) && $type == 'edit') {

            return view('property.apartment.edit_units',compact('user_id','ads_images','apartment','floor_units','unit_details','title'));
        } else {

            return view('property.apartment.createUnits',compact('user_id','ads_images','apartment','floor_units','unit_details','title'));
        }

    }

    public function appendApartmnetUnits($ad_id,$type)
    {

        $detail = Ad::whereType($type)->whereId($ad_id)->with('city','country','state','propertyType')->first();
        $detail = json_decode(json_encode($detail),true);
        $apartment_units = ApartmentUnit::whereAdId($ad_id)->select('id','slug','apartment_floor_id','apartment_unit_id','property_status')->get()->groupBy('apartment_floor_id');
        $apartment_units = json_decode(json_encode($apartment_units),true);
        $units_total = $detail['floor']*$detail['units'];

        return view('property.append_apartment_units',compact('detail','apartment_units','units_total'));
    }

    // public function soldUnitStatusChange_old(Request $request)
    // {
    //     $data   = $request->all();
    //     $id     = getDecrypted($data['unit_id']);

    //     $property_id = ApartmentUnit::whereId($id)->first()->toArray();
    //     $property_name = Ad::whereId($property_id['ad_id'])->value('title');

    //     if($data['type'] == 'sold') {

    //         $sold   = ApartmentUnit::where('id',$id)->update([ 'property_status' => 1]);
    //         // add notification regarding unit sold here

    //         $notify = [

    //                 'user_id'       =>  Auth::user()->id,
    //                 'ad_id'         =>  $property_id['ad_id'],
    //                 'type'          =>  'property_sold',
    //                 'type_text'     =>  ' Apartment has been sold by ',
    //                 'message'       =>  ucfirst($property_name).' Apartment has been sold by '.ucfirst(Auth::user()->name)
    //         ];

    //         Notification::create($notify);

    //         // add notification regarding unit sold here

    //         // save last activities data here
    //         $activity = [

    //                 'user_id'       =>  Auth::user()->id,
    //                 'type'          =>  'property_sold',
    //                 'ad_id'         =>  $property_id['ad_id'],
    //                 'floor_id'      =>  $property_id['apartment_floor_id'],
    //                 'unit_id'       =>  $id,
    //                 'description'   =>  ucfirst(Auth::user()->name)." sold unit ".ucfirst($property_id['title'])." of Floor No. ".$property_id['apartment_floor_id']." of Apartment ".ucfirst($property_name)
    //         ];

    //         AgentActivity::create($activity);
    //         // save last activities data here
    //     } else {

    //         $sold   = ApartmentUnit::where('id',$id)->update([ 'property_status' => 0]);
    //         // add notification regarding unit available here

    //         $notify = [

    //                 'user_id'       =>  Auth::user()->id,
    //                 'ad_id'         =>  $property_id['ad_id'],
    //                 'type'          =>  'property_available',
    //                 'type_text'     =>  ' Apartment has been made available by ',
    //                 'message'       =>  ucfirst($property_name).' Apartment has been made available by '.ucfirst(Auth::user()->name)
    //         ];

    //         Notification::create($notify);

    //         // add notification regarding unit available here

    //         // save last activities data here
    //         $activity = [

    //                 'user_id'       =>  Auth::user()->id,
    //                 'type'          =>  'property_available',
    //                 'ad_id'         =>  $property_id['ad_id'],
    //                 'floor_id'      =>  $property_id['apartment_floor_id'],
    //                 'unit_id'       =>  $id,
    //                 'description'   =>  ucfirst(Auth::user()->name)." made unit ".ucfirst($property_id['title'])." of Floor No. ".$property_id['apartment_floor_id']." of Apartment ".ucfirst($property_name)." available"
    //         ];

    //         AgentActivity::create($activity);
    //         // save last activities data here
    //     }
    //     // set property sold or available status for whole apartment (if any unit is available, then status is available else sold)
    //     $available_units = ApartmentUnit::whereAdId($property_id['ad_id'])->wherePropertyStatus('0')->count();
    //     if($available_units > 0) {
    //         Ad::whereId($property_id['ad_id'])->update(['property_status' => 0]);
    //     } else {
    //         Ad::whereId($property_id['ad_id'])->update(['property_status' => 1]);
    //     }

    //     if($sold){
    //         return $response = array('success' => 1,'count'=>$id,'msg'=>'Apartment Unit updated successfully !');
    //     }else{
    //         return $response = array('success'=>0);
    //     }
    // }

    public function soldUnitStatusChange(Request $request)
    {
        $data   = $request->all();
        $id     = getDecrypted($data['unit_id']);

        $property_id = ApartmentUnit::whereId($id)->first()->toArray();
        $property_name = Ad::whereId($property_id['ad_id'])->value('title');

        if($data['type'] == 'sold') {

            $sold   = ApartmentUnit::where('id',$id)->update([ 'property_status' => 1]);

            if($sold) {

                // add notification regarding unit sold here
                $notify = [

                        'user_id'       =>  Auth::user()->id,
                        'ad_id'         =>  $property_id['ad_id'],
                        'type'          =>  'property_sold',
                        'type_text'     =>  ' Unit of Apartment has been sold by ',
                        'message'       =>  ucfirst($property_name)." Apartment's unit ".ucfirst($property_id['title'])." has been sold by ".ucfirst(Auth::user()->name)
                ];

                Notification::create($notify);
                // add notification regarding unit sold here

                $tower_name = DB::table('towers')->where('id',$property_id['tower_id'])->value('title');
                // save last activities data here
                $activity = [

                        'user_id'       =>  Auth::user()->id,
                        'type'          =>  'property_sold',
                        'ad_id'         =>  $property_id['ad_id'],
                        'floor_id'      =>  $property_id['apartment_floor_id'],
                        'unit_id'       =>  $id,
                        'description'   =>  ucfirst(Auth::user()->name)." sold unit ".ucfirst($property_id['title'])." of Floor No. ".$property_id['apartment_floor_id']." of Tower ".ucfirst($tower_name)." of Apartment ".ucfirst($property_name)
                ];

                AgentActivity::create($activity);
                // save last activities data here
            }
        }
        // set property sold or available status for whole apartment (if any unit is available, then status is available else sold)
        $available_units = ApartmentUnit::whereAdId($property_id['ad_id'])->wherePropertyStatus('0')->count();
        if($available_units > 0) {
            Ad::whereId($property_id['ad_id'])->update(['property_status' => 0]);
        } else {
            Ad::whereId($property_id['ad_id'])->update(['property_status' => 1]);
        }

        if($sold){
            return $response = array('success' => 1,'count'=>$id,'msg'=>'Apartment Unit updated successfully !');
        }else{
            return $response = array('success'=>0);
        }
    }

    public function availUnitStatusChange(Request $request)
    {
        $data   = $request->all();
        $id     = getDecrypted($data['unit_id']);

        $property_id = ApartmentUnit::whereId($id)->first()->toArray();
        $property_name = Ad::whereId($property_id['ad_id'])->value('title');

        if($data['type'] == 'available') {

            $avail   = ApartmentUnit::where('id',$id)->update([ 'property_status' => 0]);

            if($avail) {

                // add notification regarding unit available here
                $notify = [

                        'user_id'       =>  Auth::user()->id,
                        'ad_id'         =>  $property_id['ad_id'],
                        'type'          =>  'property_available',
                        'type_text'     =>  ' Unit of Apartment has been made available by ',
                        'message'       =>  ucfirst($property_name)." Apartment's unit ".ucfirst($property_id['title'])." has been made available by ".ucfirst(Auth::user()->name),
                ];

                Notification::create($notify);
                // add notification regarding unit available here

                $tower_name = DB::table('towers')->where('id',$property_id['tower_id'])->value('title');
                // save last activities data here
                $activity = [

                        'user_id'       =>  Auth::user()->id,
                        'type'          =>  'property_available',
                        'ad_id'         =>  $property_id['ad_id'],
                        'floor_id'      =>  $property_id['apartment_floor_id'],
                        'unit_id'       =>  $id,
                        'description'   =>  ucfirst(Auth::user()->name)." made unit ".ucfirst($property_id['title'])." of Floor No. ".$property_id['apartment_floor_id']." of Tower ".ucfirst($tower_name)." of Apartment ".ucfirst($property_name)." available"
                ];

                AgentActivity::create($activity);
                // save last activities data here
            }
        }
        // set property sold or available status for whole apartment (if any unit is available, then status is available else sold)
        $available_units = ApartmentUnit::whereAdId($property_id['ad_id'])->wherePropertyStatus('0')->count();
        if($available_units > 0) {
            Ad::whereId($property_id['ad_id'])->update(['property_status' => 0]);
        } else {
            Ad::whereId($property_id['ad_id'])->update(['property_status' => 1]);
        }

        if($avail){
            return $response = array('success' => 1,'count'=>$id,'msg'=>'Apartment Unit updated successfully !');
        }else{
            return $response = array('success'=>0);
        }
    }

    public function editUnits($apartment_id,$floor_id,$unit_id)   // open edit unit page
    {
        $title = 'Edit Units';

        $user_id = Auth::user()->id;
        $ads_images = array();

        $apartment_id = getDecrypted($apartment_id);
        $floor_id = getDecrypted($floor_id);
        $unit_id = getDecrypted($unit_id);

        // floor id and unit id 1 mean the previous page was create floors/edit floors page.

        // get next floor and next unit no to redirect to that page

        //$floor_units = $this->checkApartmentFloorUnits($apartment_id,$floor_id,$unit_id);

        $apartment = Ad::whereId($apartment_id)->select('id','slug','floor','units')->first();

        $unit_details = ApartmentUnit::withCount('media_img')->with('media_img')->whereAdId($apartment_id)->whereApartmentFloorId($floor_units['floor_id'])->whereApartmentUnitId($floor_units['unit_id'])->first();

        // delete if there is any media not attached with any house yet
        deleteUnlinkedMedia($user_id,'units','unit_images');
        // delete if there is any media not attached with any house yet

        if($floor_units['floor_id'] == 0) {

            return redirect::to('developers/properties-list')->with('success','Data Saved Sucessfully');
        } else {

            return view('property.apartment.edit_units',compact('user_id','ads_images','apartment','floor_units','unit_details','title'));
        }
    }

    public function unitsEdit(Request $request)    // edit an existing unit
    {
        $rules = [
            'unit_status' => 'required'
        ];
        if($request->unit_status != 2 && $request->unit_status != '') {

            $rules['name']              = 'required|max:255';
            $rules['description']       = 'required|min:80';
            $rules['no_of_bathrooms']   = 'required|max:10';
            $rules['no_of_parking']     = 'required|max:10';
            $rules['no_of_bedrooms']    = 'required|max:10';
            $rules['size']              = 'required';
            $rules['price']             = 'required|max:10';
            // $rules['video_link']        = 'url';

            if(empty($request->image_count)) {

                $rules['house_photos_check'] = 'required';
            }
        }

        $this->validate($request,$rules);

        $title = $request->name;
        // $slug    = unique_slug($title);
        $slug    = unique_slug($title,'ApartmentUnit');
        $user_id = Auth::user()->id;

        // $video_url = '';
        // if(!empty($request->video_link)) {

        //     $video_url = $request->video_link;
        // } else {

        //     $video_url = ApartmentUnit::whereId('apartment_unit_id')->value('video_url');
        // }

        $units_data = [
                    'title'                 => ucfirst($request->name),
                    'ad_id'                 => $request->apartment_id,
                    'apartment_floor_id'    => $request->floor_id,
                    'apartment_unit_id'     => $request->unit_id,
                    'slug'                  => $slug,
                    'description'           => ucfirst($request->description),
                    'price'                 => $request->price,
                    'beds'                  => $request->no_of_bedrooms,
                    'attached_bath'         => $request->no_of_bathrooms,
                    'no_of_parking'         => $request->no_of_parking,
                    'unit_type'             => 'sqft',
                    'square_unit_space'     => $request->size,
                    'status'                => 1,
                    'user_id'               => $user_id,
                    // 'video_url'             => $video_url,
                    'property_status'       => $request->unit_status,
                ];

        // update if page is edited on previous page and create if unit is created for first time and get id from database

        if(isset($request->apartment_unit_id) && !empty($request->apartment_unit_id)) {

            $units_created = ApartmentUnit::whereId($request->apartment_unit_id)->update($units_data);
            $recent_unit_id = $request->apartment_unit_id;

        } else {

            $units_created = ApartmentUnit::create($units_data);
            $recent_unit_id = $units_created->id;
        }

        if ($units_created){
            //Attach all unused media with this ad
            Media::whereUserId($user_id)
                   ->whereAdId(0)
                   ->whereApartmentUnitId(0)
                   ->whereRef('units')
                   ->whereUploadedFor('unit_images')
                   ->update([
                        'ad_id'     =>  $request->apartment_id,
                        'apartment_unit_id'   =>  $recent_unit_id
                    ]);

            return redirect::to('developers/edit-units/'.\Crypt::encrypt($request->apartment_id).'/'.\Crypt::encrypt($request->floor_id).'/'.\Crypt::encrypt($request->unit_id))->with('success','Data Updated Successfully');
        }
    }

    // unit modules

    // public function apartment_floor_units($ad_id) {

    //     //prx($ad_id);
    //     $apartment = [];
    //     if(!empty($ad_id)) {

    //         $apartment['apartment_details'] = $this->get_apartment($ad_id);
    //         $apartment['floors'] = $this->get_floors($ad_id);
    //         $apartment['units'] = $this->get_units($ad_id);
    //     }

    //     return $apartment;
    // }

    // public function get_apartment($ad_id) {

    //     //prx($ad_id);
    //     $ads = Ad::whereId($ad_id)->with('media_img')->whereStatus(1)->ordrBy('id','DESC')->first();
    //     return $ads;
    // }

    // public function get_floors($ad_id) {

    //     //prx($ad_id);
    //     $floors = ApartmentUnit::whereAdId($ad_id)->groupBy('apartment_floor_id')->get();
    //     return $floors;
    // }

    // public function get_units($ad_id) {

    //     //prx($ad_id);
    //     $units = ApartmentUnit::whereAdId($ad_id)->where('apartment_unit_id','!=',0)->get();
    //     return $units;
    // }

    // developers functions to get one more apartment from admin

    public function apartmentRequest()    // not in use right now
    {
        $title = 'Apartment Request';
        $user = User::whereId(Auth::user()->id)->with('user_package.package')->first();

        $current_time = Carbon::parse(date('Y-m-d H:i:s'))->setTimeZone(get_option('default_timezone'));

        // get request that has access token for that developer
        $available_request = DeveloperContact::whereUserId(Auth::user()->id)->whereStatus('1')->whereDate('access_token_validity','>',$current_time)->whereTokenExpired('0')->first();

        return view('agent.get_new_apartment',compact('title','user','available_request'));
    }

    public function insertRequestAccessKey(Request $request)
    {
        $rules = [
            'access_key'    => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // if Validation failed
            return array('error'=> $validator->messages());
        }

        // if apartment is not requested by developer yet or 24 hour period has been expired and token not used
        if($request->request_id == 0 || empty($request->request_id)) {

            return array('status' => 2, 'error' => 'No request sent. Or your access key is expired. Please contact us.');
        }

        $user = DeveloperContact::whereId($request->request_id)->first();
        if(!$user) {

            return array('status'=>2,'error'=>"No apartment requested.");
        }

        // if key provided by user is not equal to key provided by user
        if($user['admin_access_key'] != $request->access_key) {

            // get error if wrong key inserted by user
            return array('status'=>2,'error'=>"Wrong Access Key.");
        }

        // if developer responds after 24 hours
        if($user['access_token_validity'] < Carbon::now()->setTimeZone(get_option('default_timezone')) || empty($user['admin_access_key']) || $user['admin_access_key'] === null || $user['token_expired'] == '1') {

            return array('status'=>2,'error'=>"Your access token is expired.Please contact us again.");
        }

        return array('status'=>1,'contact_id'=>encrypt($request->request_id));
    }
    public function delete_tower(Request $request)
    {
        $data   = $request->all();
        $ad_id     = \Crypt::decrypt($data['ad_id']);
        $tower_id     = \Crypt::decrypt($data['tower_id']);
        $delete = Tower::where('ad_id',$ad_id)->where('id',$tower_id)->update([ 'trash' => 1]);
        if($delete){
            return $response = array('success' => 1,'msg'=>'Property deleted successfully !');
        }else{
            return $response = array('success'=>0);
        }
    }
    // developers functions to get one more apartment from admin
}
