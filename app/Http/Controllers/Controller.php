<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\User;
use App\Package;
use App\UserMeta;
use App\UserPackage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $theme = '';

    public function __construct(){
        $selected_theme = get_option('default_theme');

        if ( ($selected_theme == 'default_theme') || $selected_theme == 'classic' ){
            $this->theme = 'theme.';
        }else{
            $this->theme = 'theme.'.$selected_theme.'.';
        }
    }

    public function meta_data($user_id)
    {
        $user_data = UserMeta::where('user_id',$user_id)->get()->toArray();
        $meta_data = array();
        if(!empty($user_data)){
            foreach ($user_data as $key => $value) {   
                $meta_data[$value['meta_key']] = $value['meta_value'];
            }
        }
        return $meta_data;
    }

    public function current_package($user_id)
    {
        $user = User::where('id',$user_id)->with('user_package.package')->first();
        $user = json_decode(json_encode($user),true);
        $current_package = array();
        if(!empty($user)){
                $current_package['id'] = $user['user_package']['packages_id'];
                $current_package['name'] = $user['user_package']['package']['name'];
                $current_package['description'] = $user['user_package']['package']['description'];
                $current_package['blog'] = $user['user_package']['package']['blog'];
                $current_package['apartments'] = $user['user_package']['package']['apartments'];
                $current_package['properties'] = $user['user_package']['package']['properties'];
                $current_package['expiry_date'] = $user['user_package']['expiry_date'];


        }
        return $current_package;
    }
    
}
