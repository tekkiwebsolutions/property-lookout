<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Intervention\Image\Facades\Image;

use App\Ad;
use App\Post;
use App\User;
use App\Media;
use App\Slider;
use App\Country;
use App\Payment;
use App\Category;
use App\UserCredit;
use App\Contact_query;
use App\PropertyType;

class CreditController extends Controller
{   
    
    public function creditList() 
    {
        if(Auth::check() && (Auth::user()->user_type == 'user')) {
        
            $title = 'My Credits';
            $user_id = Auth::user()->id;
            $current_package = $this->current_package($user_id);

            // get the list of credits for that user
            $user_credits = UserCredit::whereUserId($user_id)->orderBy('id','DESC')->get()->toArray();

            // get the total available credits for user and total of used credits till date

            $user_credit_data = $this->getTotalCreditsData($user_id);

            // get the total available credits for user and total of used credits till date

            return view('agent.credits',compact('title','current_package','user_credits','user_credit_data'));
        
        }  else {

            return redirect('/');
        }  
            
    }

    public function addCredit(Request $request) 
    {
        $user_id = Auth::user()->id;

        if(!empty($request->user_credits)) {

            // create a random transaction id for the payment
            $transaction_id = 'tran_'.time().str_random(6);
            while( ( Payment::whereLocalTransactionId($transaction_id)->count() ) > 0) {
                $transaction_id = 'reid'.time().str_random(5);
            }
            $transaction_id = strtoupper($transaction_id);

            $currency = get_option('currency_sign');

            // save initial data before success
            $payments_data = [
                    'ad_id'                 => 0,
                    'user_id'               => $user_id,
                    'amount'                => $request->user_credits,
                    'payment_method'        => $request->payment_method,
                    'status'                => 'initial',
                    'currency'              => $currency,
                    'local_transaction_id'  => $transaction_id
            ];
            
            $created_payment = Payment::create($payments_data);
            // prx($created_payment);
            return redirect(url('agents/payment_checkout'.'/'.$created_payment->local_transaction_id));
        }
    }

    public function getTotalCreditsData($user_id) 
    {
        $total_user_credit = UserCredit::whereUserId($user_id)->whereType('added')->sum('amount');
        $total_used_credit = UserCredit::whereUserId($user_id)->whereType('used')->sum('amount');
        $total_available_credit = $total_user_credit - $total_used_credit;

        return ['total_user_credit' => $total_user_credit, 'total_used_credit' => $total_used_credit, 'total_available_credit' => $total_available_credit];
    }

    // credit checkout and payments start

    public function checkout($transaction_id)
    {
        $payment = Payment::whereLocalTransactionId($transaction_id)->whereStatus('initial')->first();
        $title = trans('app.checkout');
        if ($payment){
            return view('agent.checkout', compact('title','payment'));
        }
        return view('admin.error.invalid_transaction', compact('title','payment'));
    }

    public function chargePayment(Request $request,$transaction_id) 
    {
        $payment = Payment::whereLocalTransactionId($transaction_id)->whereStatus('initial')->first();
        $ad = $payment->ad;

        //Determine which payment method is this
        if ($payment->payment_method == 'paypal') {

            // PayPal settings
            $paypal_action_url = "https://www.paypal.com/cgi-bin/webscr";
            if (get_option('enable_paypal_sandbox') == 1)
                $paypal_action_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";

            $paypal_email = get_option('paypal_receiver_email');
            $return_url = url('agents/payment_success_url'.'/'.$transaction_id);
            $cancel_url = url('agents/payment_checkout'.'/'.$transaction_id);
            $notify_url = url('agents/paypal_notify_url'.'/'.$transaction_id);

            $item_name = "Add Credits";


            // Check if paypal request or response
            $querystring = '';

            // Firstly Append paypal account to querystring
            $querystring .= "?business=".urlencode($paypal_email)."&";

            // Append amount& currency (£) to querystring so it cannot be edited in html
            //The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
            $querystring .= "item_name=".urlencode($item_name)."&";
            $querystring .= "amount=".urlencode($payment->amount)."&";
            $querystring .= "currency_code=".urlencode($payment->currency)."&";

            $querystring .= "first_name=".urlencode(Auth::user()->first_name)."&";
            $querystring .= "last_name=".urlencode(Auth::user()->last_name)."&";
            $querystring .= "payer_email=".urlencode(Auth::user()->email)."&";
            $querystring .= "item_number=".urlencode($payment->local_transaction_id)."&";

            //loop for posted values and append to querystring
            foreach(array_except($request->input(), '_token') as $key => $value){
                $value = urlencode(stripslashes($value));
                $querystring .= "$key=$value&";
            }

            // Append paypal return addresses
            $querystring .= "return=".urlencode(stripslashes($return_url))."&";
            $querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
            $querystring .= "notify_url=".urlencode($notify_url);

            // Append querystring with custom field
            //$querystring .= "&custom=".USERID;

            // Redirect to paypal IPN
            header('location:'.$paypal_action_url.$querystring);
            exit();

        }elseif ($payment->payment_method == 'stripe'){

            $stripeToken = $request->stripeToken;
            \Stripe\Stripe::setApiKey(get_stripe_key('secret'));
            // Create the charge on Stripe's servers - this will charge the user's card
            try {
                $charge = \Stripe\Charge::create(array(
                    "amount" => ($payment->amount * 100), // amount in cents, again
                    "currency" => $payment->currency,
                    "source" => $stripeToken,
                    "description" => "Add Credits"
                ));
                if ($charge->status == 'succeeded'){
                    $payment->status = 'success';
                    $payment->charge_id_or_token = $charge->id;
                    $payment->description = $charge->description;
                    $payment->payment_created = $charge->created;
                    $payment->save();

                    // save added credit to the user_credits table

                    $add_credit = [];
                    $add_credit['user_id'] = Auth::user()->id;
                    $add_credit['type'] = 'added';
                    $add_credit['amount'] = $payment->amount;
                    UserCredit::insert($add_credit);

                    //Set publish ad by helper function
                    //ad_status_change($ad->id, 1);

                    return ['success'=>1, 'msg'=> trans('app.payment_received_msg')];
                    // return redirect::to('agents/credits-list/')->with('success',trans('app.payment_received_msg'));
                }
            } catch(\Stripe\Error\Card $e) {
                // The card has been declined
                $payment->status = 'declined';
                $payment->description = trans('app.payment_declined_msg');
                $payment->save();
                return ['success'=>0, 'msg'=> trans('app.payment_declined_msg')];
            }
        }

        return ['success'=>0, 'msg'=> trans('app.error_msg')];
    }

    public function paymentSuccess(Request $request, $transaction_id)
    {
        /**
         * Check is this transaction paid via paypal IPN
         */
        $transaction_check = Payment::whereLocalTransactionId($transaction_id)->first();
        if ($transaction_check){
            if($transaction_check->status == 'success'){
                return redirect(url('agents/credits-list'))->with('success', trans('app.payment_received_msg'));
            }elseif ($transaction_check->status == 'declined'){
                return redirect(url('agents/credits-list'))->with('error', trans('app.payment_declined_msg'));
            }
        }

        //Start original logic

        $payment = Payment::whereLocalTransactionId($transaction_id)->whereStatus('initial')->first();
        //$ad = $payment->ad;

        $paypal_action_url = "https://www.paypal.com/cgi-bin/webscr";
        if (get_option('enable_paypal_sandbox') == 1)
            $paypal_action_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";

        // STEP 1: read POST data
        // Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
        // Instead, read raw POST data from the input stream.
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode ('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
        // read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

        // STEP 2: POST IPN data back to PayPal to validate
        $ch = curl_init($paypal_action_url);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        // In wamp-like environments that do not come bundled with root authority certificates,
        // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set
        // the directory path of the certificate as shown below:
        // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
        if( !($res = curl_exec($ch)) ) {
            // error_log("Got " . curl_error($ch) . " when processing IPN data");
            curl_close($ch);
            exit;
        }
        curl_close($ch);

        // STEP 3: Inspect IPN validation result and act accordingly
        if (strcmp ($res, "VERIFIED") == 0) {

            //Payment success, we are ready to live our ads
            $payment->status = 'success';
            $payment->charge_id_or_token = $request->txn_id;
            $payment->description = $request->item_name;
            $payment->payer_email = $request->payer_email;
            $payment->payment_created = strtotime($request->payment_date);
            $payment->save();

            $add_credit = [];
            $add_credit['user_id'] = Auth::user()->id;
            $add_credit['type'] = 'added';
            $add_credit['amount'] = $payment->amount;
            UserCredit::insert($add_credit);

            //Set publish ad by helper function
            //ad_status_change($ad->id, 1);

            return redirect(url('agents/credits-list'))->with('success', trans('app.payment_received_msg'));

        } else if (strcmp ($res, "INVALID") == 0) {

            $payment->status = 'declined';
            $payment->description = trans('app.payment_declined_msg');
            $payment->save();
            return redirect(url('agents/credits-list'))->with('error', trans('app.payment_declined_msg'));
        }
    }

    /**
     * @param Request $request
     * @param $transaction_id
     * @return mixed
     *
     * Ipn notify, receive from paypal
     */
    public function paypalNotify(Request $request, $transaction_id)
    {
        $payment = Payment::whereLocalTransactionId($transaction_id)->whereStatus('initial')->first();
        $ad = $payment->ad;

        $paypal_action_url = "https://www.paypal.com/cgi-bin/webscr";
        if (get_option('enable_paypal_sandbox') == 1)
            $paypal_action_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";

        // STEP 1: read POST data
        // Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
        // Instead, read raw POST data from the input stream.
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode ('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
        // read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

        // STEP 2: POST IPN data back to PayPal to validate
        $ch = curl_init($paypal_action_url);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        // In wamp-like environments that do not come bundled with root authority certificates,
        // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set
        // the directory path of the certificate as shown below:
        // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
        if( !($res = curl_exec($ch)) ) {
            // error_log("Got " . curl_error($ch) . " when processing IPN data");
            curl_close($ch);
            exit;
        }
        curl_close($ch);

        // STEP 3: Inspect IPN validation result and act accordingly
        if (strcmp ($res, "VERIFIED") == 0) {

            //Payment success, we are ready to live our ads
            $payment->status = 'success';
            $payment->charge_id_or_token = $request->txn_id;
            $payment->description = $request->item_name;
            $payment->payer_email = $request->payer_email;
            $payment->payment_created = strtotime($request->payment_date);
            $payment->save();

            $add_credit = [];
            $add_credit['user_id'] = Auth::user()->id;
            $add_credit['type'] = 'added';
            $add_credit['amount'] = $payment->amount;
            UserCredit::insert($add_credit);

            //Set publish ad by helper function
            //ad_status_change($ad->id, 1);

            return redirect(url('agents/credits-list'))->with('success', trans('app.payment_received_msg'));

        } else if (strcmp ($res, "INVALID") == 0) {

            $payment->status = 'declined';
            $payment->description = trans('app.payment_declined_msg');
            $payment->save();
            return redirect(url('agents/credits-list'))->with('error', trans('app.payment_declined_msg'));
        }

    }

    // credit checkout and payments end

    // use credit for properties functions start

    public function addCreditList() 
    {
        $title = "Add Credits";
        $meta_data = $this->meta_data(Auth::user()->id);
        $current_package = $this->current_package(Auth::user()->id);
        $propety_arr = ['apartments','house'];
        if(Auth::user()->user_type == 'admin'){
            $properties = Ad::whereIn('type',$propety_arr)->where('trash',0)->orderBy('id','DESC')->get()->toArray();
        }else{
            // $properties = Ad::whereIn('type',$propety_arr)->withCount('available_units','GuestReview')->whereStatus('1')->where('trash',0)->where('user_id',Auth::user()->id)->orderBy('id','DESC')->get()->toArray();
            $properties = Ad::whereIn('type',$propety_arr)
                              ->withCount('available_units','GuestReview')
                              ->whereStatus('1')
                              ->where('trash',0)
                              ->where(function($query) {
                                    $query->where('user_id',Auth::user()->id)
                                          ->orWhere('assigned_agent_id',Auth::user()->id);
                              })
                              ->orderBy('id','DESC')
                              ->get()
                              ->toArray();
        }

        $featured_apartment = Ad::whereType('apartments')->whereUserId(Auth::user()->id)->where('credits','!=',0)->orderBy('id','DESC')->value('id');

        // get the total available credits for user and total of used credits till date

        $user_credit_data = $this->getTotalCreditsData(Auth::user()->id);

        // get the total available credits for user and total of used credits till date

        return view('agent.add_credit_list',compact('title','meta_data','current_package','properties','featured_apartment','user_credit_data'));
    }

    public function addPropertyCredit($ad_id) 
    {
        $ad_id = getDecrypted($ad_id);
        $title = "Add Credits";
        $meta_data = $this->meta_data(Auth::user()->id);
        $current_package = $this->current_package(Auth::user()->id);

        $property = Ad::whereId($ad_id)->first();

        $feature_date = date('d-M-Y',strtotime(date('y-m-d H:i:s')."+ 28 days"));

        return view('agent.add_credits',compact('title','meta_data','current_package','ad_id','property','feature_date'));
    }

    public function addCreditProperty(Request $request) 
    {
        $user_id = Auth::user()->id;
        // get the total available credits for user and total of used credits till date

        $user_credit_data = $this->getTotalCreditsData($user_id);

        // get the total available credits for user and total of used credits till date

        // if the amount is greater than credit available in user balance, then show error
        if($request->amount > $user_credit_data['total_available_credit']) {

            return redirect()->back()->with('error','Insufficient Credits');
        }

        if($request->amount <= $user_credit_data['total_available_credit']) {
            $credit_data = [

                'user_id'       => $user_id,
                'ad_id'         => $request->ad_id,
                'type'          => $request->type,
                'amount'        => $request->amount
            ];

            UserCredit::insert($credit_data);

            // update property with credits (to use in sorting by credits in property listings)
            Ad::whereId($request->ad_id)->update(['credits'=>$request->amount,'credit_added'=>1]);

            return redirect('/agents/add-credit-list');
        }
    }

    // use credit for properties functions end

    // function to remove credits from houses on 29th of every month

    public function removeCredits() 
    {
        if(date('d') >= 29) {
            $property_type = ['apartments','house'];
            Ad::whereIn('type',$property_type)->update(['credits'=>0]);
        } 
    }
}




