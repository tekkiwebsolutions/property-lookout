<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffAd extends Model
{
    //
    protected $guarded = [];

    public function staff() {

    	return $this->hasOne('App\User','id','staff_id');
    }

    public function apartment() {

    	return $this->hasOne('App\Ad','id','ad_id');
    }

    public function developer() {

    	return $this->hasOne('App\User','id','developer_id');
    }
}
