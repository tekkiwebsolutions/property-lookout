<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordLink extends Mailable
{
    use Queueable, SerializesModels;
    public $arr;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($messageData)
    {
        //
        $this->messageData = $messageData;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        // returns view for reset password link
        return $this->view('emails.send_password_reset')->with(['messageData'=>$this->messageData]);
       
    }

    
}
