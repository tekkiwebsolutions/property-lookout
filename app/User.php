<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  	protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user_meta(){
        return $this->hasMany(UserMeta::class);
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function state(){
        return $this->belongsTo(State::class);
    }
    public function city(){
        return $this->belongsTo(City::class);
    }

    /**
     * @param int $s
     * @param string $d
     * @param string $r
     * @param bool $img
     * @param array $atts
     * @return string
     */
    public function get_gravatar( $s = 40, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {

        $email = $this->email;
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";

        if( ! empty($this->photo)) {
            $url = avatar_img_url($this->photo, $this->photo_storage);
        }

        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }

        return $url;
    }

    public function get_address(){
        $address = '';

        if ( ! empty($this->address))
            $address .= $this->address;

        if ($this->city){
            $address .= ', '.$this->city->city_name;
        }

        if ($this->state){
            $address .= ', '.$this->state->state_name;
        }
        
        if ($this->country){
            $address .= ', '.$this->country->country_name;
        }
        
        return $address;
    }

    public function ads(){
        return $this->hasMany(Ad::class);
    }
    public function favourite_ads(){
        return $this->belongsToMany(Ad::class, 'favorites');
    }

    public function signed_up_datetime(){
        $created_date_time = $this->created_at->timezone(get_option('default_timezone'))->format(get_option('date_format_custom').' '.get_option('time_format_custom'));
        return $created_date_time;
    }

    public function status_context(){
        $status = $this->active_status;

        $context = '';
        switch ($status){
            case '0':
                $context = 'Pending';
                break;
            case '1':
                $context = 'Active';
                break;
            case '2':
                $context = 'Block';
                break;
        }
        return $context;
    }

    public function is_admin(){
        if ($this->user_type == 'admin'){
            return true;
        }
        return false;
    }

    public function is_agent(){
        if ($this->user_type == 'user'){
            return true;
        }
        return false;
    }

    public function is_developer(){
        if ($this->user_type == 'developer'){
            return true;
        }
        return false;
    }

    public function is_staff(){
        if($this->user_type == 'staff'){
            return true;
        }
        return false;
    }

    public function user_package()
    {
        return $this->hasOne('App\UserPackage','id','current_active_plan_id');
    }

    public function contact_query()
    {
        return $this->hasOne('App\DeveloperContact','user_id','id')->where('type','registration');
    }

    public function reports()
    {
        return $this->hasMany('App\Report_ad','ad_id','id');
    }

    public function property()
    {
        return $this->hasMany('App\Ad','user_id','id')->whereType('house');
    }

    public function agent_apartment()
    {
        return $this->hasMany('App\Ad','assigned_agent_id','id')->whereType('apartments');
    }
    

}
