<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Auth\User as Authenticatable;

class TowerUnitFloor extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  	protected $guarded = [];

    public function tower_name()
    {
        return $this->hasMany('App\Tower','id','tower_id');
    }
    
}
