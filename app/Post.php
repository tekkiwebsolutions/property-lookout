<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    protected $guarded = [];

    public function created_at_datetime(){
        $created_date_time = $this->created_at->timezone(get_option('default_timezone'))->format(get_option('date_format_custom').' '.get_option('time_format_custom'));
        return $created_date_time;
    }

    public function feature_img(){
        return $this->hasOne(Media::class);
    }

    public function author(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function get_author() {
        
        // if post is written by admin, then write Admin else show the name of user
        $value = $this->belongsTo(User::class, 'user_id')->value('name');
        
        if($value == Auth::user()->name) {
            $value = 'Admin';
        }
        return $value;

        // return $this->belongsTo(User::class, 'user_id')->value('name');
    }

}
